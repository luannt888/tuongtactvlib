package vn.mediatech.istudiocafe.voucher.chatsupport

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.app.Constant

class ChatSupportActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voucher_tt)
        supportFragmentManager.addOnBackStackChangedListener { getListener() }
        initUI()
        initData()
    }

    fun initUI() {
//        setFullStatusTransparent()
//        MyApplication.getInstance().loadDrawableImageNow(this, R.drawable.bg_main, imageBgMain)
    }

    fun initData() {
        val bundle = intent.extras
        var fragment: Fragment? = null
        if (bundle != null) {
            fragment = ChatConversationFragment()
            fragment.arguments = bundle
        } else {
            fragment = ChatSupportFragment()
        }
        currentFragment = fragment
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()

    }


    fun getListener(): FragmentManager.OnBackStackChangedListener {
        val listener = object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val fragments = supportFragmentManager.fragments
                for (i in 0 until fragments.size) {
                    fragments.get(i).onResume()
                }
            }
        }
        return listener
    }

    fun goBack(isFinish: Boolean) {
        if (isFinish || supportFragmentManager.backStackEntryCount == 0) {
            finish()
            return
        }
        supportFragmentManager.popBackStack()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragments = supportFragmentManager.fragments
        for (i in 0 until fragments.size) {
            fragments.get(i).onActivityResult(requestCode, resultCode, data)
        }
    }

    var currentFragment: Fragment? = null
    fun showFragment(fragment: Fragment) {
        currentFragment = fragment
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_right_to_left, R.anim.fragment_right_to_left_exit, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun showChatConversationFragment(item: ItemConversation?) {
        val fragment = ChatConversationFragment()
        val bundle = Bundle()
        bundle.putParcelable(Constant.DATA, item)
        fragment.arguments = bundle
        showFragment(fragment)
    }
    fun updateConversion(item:ItemConversation?){
        if(currentFragment is ChatConversationFragment){
            (currentFragment as? ChatConversationFragment)?.updateUI(item)
        }
    }
}
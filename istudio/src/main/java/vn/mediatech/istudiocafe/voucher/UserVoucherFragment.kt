package vn.mediatech.istudiocafe.voucher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_user_voucher.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication

class UserVoucherFragment : Fragment() {
    var isViewCreated: Boolean = false
    var voucherFragment: VoucherFragment? = null
    var savedInstanceState: Bundle? = null
    var userId: Int = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_voucher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putInt(Constant.USER_ID, userId)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        textActionbarTitle.text = bundle.getString(Constant.USERNAME, "")
        
        val itemObj = ItemBaseCategory(1, "", "", VoucherAdapter.STYLE_LIST_STRING, false)
        bundle.putInt(Constant.VOUCHER_TYPE, Constant.USER_VOUCHER)
        bundle.putParcelable(Constant.DATA, itemObj)
        voucherFragment = VoucherFragment()
        voucherFragment!!.arguments = bundle
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, voucherFragment!!)
        fragmentTransaction.commit()
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).goBack(false)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}
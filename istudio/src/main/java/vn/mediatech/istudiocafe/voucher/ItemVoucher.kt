package vn.mediatech.istudiocafe.voucher

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemVoucher(
        @SerializedName("id") var id: Int?,
        @SerializedName("unique_id") var uniqueId: Int?,
        @SerializedName("name") var name: String?,
        @SerializedName("description") var description: String?,
        @SerializedName("image") var image: String?,
        @SerializedName("price") var price: Long?,
        @SerializedName("discount_price") var discountPrice: Long?,
        @SerializedName("expired_date") var expiredDate: String?,
        @SerializedName("percent_decrease") var percent: String?,
        @SerializedName("status_type") var statusType: Int,
        @SerializedName("status_name") var statusName: String?,
        @SerializedName("user_id") var userId: Int?,
        @SerializedName("user_fullname") var userName: String?,
        @SerializedName("user_avatar") var userAvatar: String?,
        @SerializedName("voucher_receive") var voucherReceive: String?,
        @SerializedName("voucher_tutorial") var voucherTutorial: String?,
        @SerializedName("voucher_location") var voucherLocation: String?,
        @SerializedName("donate") var voucherDonate: String?,
        @SerializedName("brand_id") var brandId: String?,
        @SerializedName("brand_name") var brandName: String?,
        @SerializedName("brand_image") var brandImage: String?,
        @SerializedName("brand_logo") var brandLogo: String?,
        @SerializedName("brand_description") var brandDescription: String?,
        @SerializedName("message") var message: String?,
        @SerializedName("status") var status: String?,
        @SerializedName("link_tutorial") var linkTutorial: String?,
        @SerializedName("ztype") var zType: String?,
        @SerializedName("galerry_tutorial") var galerryTutorial: Array<String>?,
        @SerializedName("channel") var channel: String?,
        @SerializedName("created_at") var createdAt: String?,
        @SerializedName("updated_at") var updatedAt: String?,
        @SerializedName("prize_type") var prizeType: String?
) : Parcelable
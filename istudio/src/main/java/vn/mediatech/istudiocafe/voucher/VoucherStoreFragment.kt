package vn.mediatech.istudiocafe.voucher

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_voucher_store.*
import org.json.JSONArray
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.*

class VoucherStoreFragment : Fragment() {
    var isViewCreated: Boolean = false
    var voucherFragment: VoucherFragment? = null
    lateinit var currentTab: ItemBaseCategory
    var savedInstanceState: Bundle? = null
    var tabList: ArrayList<ItemBaseCategory>? = null
    var categoryList: ArrayList<ItemBaseCategory>? = null
    var brandList: ArrayList<ItemBaseCategory>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_voucher_store, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
//            putInt(Constant.VOUCHER_TYPE, voucherType)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)
        textActionbarTitle.text = getString(R.string.voucher_store)
        textActionbarTitle.visibility = View.VISIBLE
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        getDataCategory()
        getDataBrand()
        getDataBoxHorizontal()
    }

    fun getDataCategory() {
        textNotifyCategory.visibility = View.GONE
        loadingCategory.visibility = View.VISIBLE
        ServiceManager.getBaseCategoryVoucher(activity!!, ServiceUtilTT.validAPIDGTH(context,Constant.API_VOUCHER_CATEGORY), object : ServiceManager.OnBaseCategoryRequestListener {
            override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
            }

            override fun onResponse(isSuccess: Boolean, message: String?, itemList: ArrayList<ItemBaseCategory>?) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    loadingCategory.visibility = View.GONE
                }
                if (!isSuccess) {
                    activity?.runOnUiThread {
                        textNotifyCategory.visibility = View.VISIBLE
                    }
                    return
                }
                categoryList?.clear()
                categoryList = itemList
                if (categoryList == null || categoryList!!.size == 0) {
                    return
                }
                activity?.runOnUiThread {
                    textBoxCategory.visibility = View.VISIBLE
                    recyclerCategory.visibility = View.VISIBLE
                }

                val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
                val numberColumn = 4
                val itemDecoration = SpacesItemDecoration(spacePx, numberColumn)
                val layoutManager = StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.VERTICAL)
                val adapter = BaseCategoryAdapter(activity!!, categoryList!!, BaseCategoryAdapter.STYLE_VERTICAL, numberColumn)
                activity?.runOnUiThread {
                    recyclerCategory.addItemDecoration(itemDecoration)
                    recyclerCategory.layoutManager = layoutManager
                    recyclerCategory.adapter = adapter
                    recyclerCategory.scheduleLayoutAnimation()
                    adapter.onItemClickListener = object : BaseCategoryAdapter.OnItemClickListener {
                        override fun onClick(itemObj: ItemBaseCategory, position: Int) {
                        }
                    }
                }
            }
        })
    }

    fun getDataBrand() {
        textNotifyBrand.visibility = View.GONE
        loadingBrand.visibility = View.VISIBLE
        ServiceManager.getBaseCategoryVoucher(activity!!, ServiceUtilTT.validAPIDGTH(context,Constant.API_VOUCHER_BRAND), object : ServiceManager.OnBaseCategoryRequestListener {
            override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
            }

            override fun onResponse(isSuccess: Boolean, message: String?, itemList: ArrayList<ItemBaseCategory>?) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    loadingBrand.visibility = View.GONE
                }
                if (!isSuccess) {
                    activity?.runOnUiThread {
                        textNotifyBrand.visibility = View.VISIBLE
                    }
                    return
                }
                brandList?.clear()
                brandList = itemList
                if (brandList == null || brandList!!.size == 0) {
                    return
                }
                activity?.runOnUiThread {
                    textBoxBrand.visibility = View.VISIBLE
                    recyclerBrand.visibility = View.VISIBLE
                }

                val spacePx: Int = resources.getDimensionPixelSize(R.dimen.dimen_7)
                val numberColumn = 2
                val itemDecoration = SpacesItemDecoration(spacePx, numberColumn, true)
                val layoutManager = StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.HORIZONTAL)
                val adapter = BaseCategoryAdapter(activity!!, brandList!!, BaseCategoryAdapter.STYLE_HORIZONTAL, numberColumn)
                activity?.runOnUiThread {
                    recyclerBrand.addItemDecoration(itemDecoration)
                    recyclerBrand.layoutManager = layoutManager
                    recyclerBrand.adapter = adapter
                    recyclerBrand.scheduleLayoutAnimation()
                    adapter.onItemClickListener = object : BaseCategoryAdapter.OnItemClickListener {
                        override fun onClick(itemObj: ItemBaseCategory, position: Int) {
                            val bundle = Bundle()
                            bundle.putInt(Constant.ID, itemObj.id)
                            bundle.putString(Constant.NAME, itemObj.name)
                            (activity as VoucherActivity).showDetailBrandFragment(bundle)
                        }
                    }
                }
            }
        })
    }

    fun getDataBoxHorizontal() {
        textNotifyBoxHorizontal.visibility = View.GONE
        loadingBoxHorizontal.visibility = View.VISIBLE
        ServiceManager.requestBase(activity!!, ServiceUtilTT.validAPIDGTH(context,Constant.API_VOUCHER_STORE_HOME), null, object : ServiceManager.OnBaseRequestListener {
            override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
            }

            override fun onResponse(isSuccess: Boolean, message: String?, responseString: String?) {
                if (isDetached) {
                    return
                }
                if (!isSuccess) {
                    activity?.runOnUiThread {
                        loadingBoxHorizontal.visibility = View.GONE
                        textNotifyBoxHorizontal.visibility = View.VISIBLE
                    }
                    return
                }
                val jsonObject = JsonParser.getJsonObject(responseString)
                val resultObject = JsonParser.getJsonObject(jsonObject, "result")
                val tabs = JsonParser.getString(resultObject, "tabs")
                val contentsArray = JsonParser.getJsonArray(resultObject, "contents")
                try {
                    initBoxHorizontal(contentsArray)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (!tabs.isNullOrEmpty()) {
                    val gsonType = object : TypeToken<ArrayList<ItemBaseCategory>>() {}.getType()
                    try {
                        val gson = Gson()
                        tabList = gson.fromJson(tabs, gsonType)
                        activity?.runOnUiThread {
                            try {
                                initTab()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                activity?.runOnUiThread {
                    loadingBoxHorizontal.visibility = View.GONE
                }
                if (layoutBoxHorizontal.childCount == 0 && (tabList == null || tabList!!.size == 0)) {
                    activity?.runOnUiThread {
                        textNotifyBoxHorizontal.visibility = View.VISIBLE
                    }
                }
            }
        }, false)
    }

    fun initBoxHorizontal(jsonArray: JSONArray?) {
        if (jsonArray == null || jsonArray.length() == 0) {
            return
        }
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val name = JsonParser.getString(jsonObject, "name")
            val type = JsonParser.getString(jsonObject, "type")
            val data = JsonParser.getString(jsonObject, "data")
            if (data.isNullOrEmpty() || type.isNullOrEmpty()) {
                continue
            }

            val gson = Gson()
            val gsonType = object : TypeToken<ArrayList<ItemVoucher>>() {}.getType()
            var itemList: ArrayList<ItemVoucher> = ArrayList()
            try {
                itemList = gson.fromJson(data, gsonType)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (itemList.size == 0) {
                continue
            }
            val layoutItem: View = layoutInflater.inflate(R.layout.layout_voucher_box, layoutBoxHorizontal, false)
            val textHeader: TextView = layoutItem.findViewById(R.id.textHeader)
            val recyclerView: RecyclerView = layoutItem.findViewById(R.id.recyclerView)
            activity?.runOnUiThread {
                if (name.isNullOrEmpty()) {
                    textHeader.visibility = View.GONE
                } else {
                    textHeader.text = name
                    textHeader.visibility = View.VISIBLE
                }
                val orientation: Int
                if (type.equals(VoucherAdapter.STYLE_HORIZONTAL_STRING)) {
                    orientation = StaggeredGridLayoutManager.HORIZONTAL
                } else {
                    orientation = StaggeredGridLayoutManager.VERTICAL
                }
                val spacePx: Int = resources.getDimensionPixelSize(R.dimen.dimen_7)
                val numberColumn = 1
                val itemDecoration = SpacesItemDecoration(spacePx, numberColumn, true)
                val layoutManager = StaggeredGridLayoutManager(numberColumn, orientation)
                val adapter = VoucherAdapter(activity!!, itemList, type, numberColumn)
                adapter.showLayoutUser = true
                recyclerView.addItemDecoration(itemDecoration)
                recyclerView.layoutManager = layoutManager
                recyclerView.adapter = adapter
                recyclerView.scheduleLayoutAnimation()
                layoutBoxHorizontal.addView(layoutItem)
                adapter.onItemClickListener = object : VoucherAdapter.OnItemClickListener {
                    override fun onClick(itemObj: ItemVoucher, position: Int) {
                        if (activity is VoucherActivity) {
                            val bundle = Bundle()
                            bundle.putString(Constant.TYPE_VOUCHER_AREA, Constant.TYPE_VOUCHER_AREA_MARKET)
                            bundle.putParcelable(Constant.DATA, itemObj)
                            (activity as VoucherActivity).showDetailVoucherFragment(bundle, null)
                        }
                    }
                }
            }
        }
    }

    var lastTabPosition = 1

    fun initTab() {
        if (tabList == null || tabList!!.size == 0) {
            return
        }
        tabList!!.get(0).isChecked = true
        textBoxAll.visibility = View.VISIBLE
        layoutTab.visibility = View.VISIBLE
        tabLayout.visibility = View.VISIBLE
        val tabFilter = tabLayout.newTab()
        tabFilter.text = getString(R.string.filter)
        tabFilter.icon = ContextCompat.getDrawable(activity!!, R.drawable.ic_filter_white)
        tabFilter.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_LABELED
        tabLayout.addTab(tabFilter)

        for (itemObj in tabList!!) {
            val tab = tabLayout.newTab()
            tab.text = itemObj.name
            tabLayout.addTab(tab)
        }
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab == null) {
                    return
                }
                val position = tab.position
                if (position == 0) {
                    showFilter()
                    return
                }
                handleOnPageSelected(position - 1)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab == null) {
                    return
                }
                lastTabPosition = tab.position
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })

        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            val rightMargin = resources.getDimensionPixelSize(R.dimen.dimen_3)
            val leftMargin = if (i == 0) resources.getDimensionPixelSize(R.dimen.dimen_10) else rightMargin
            p.setMargins(leftMargin, 0, rightMargin, 0)
            tab.requestLayout()
        }
        tabLayout.getTabAt(lastTabPosition)?.select()
//        tabLayout.setScrollPosition(lastTabPosition, 0f, true)
//        handleOnPageSelected(lastTabPosition)// 0 is filter
    }

    fun showFilter() {
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            val bundle = Bundle()
            bundle.putParcelableArrayList(Constant.STATUS_LIST, tabList)
            bundle.putParcelableArrayList(Constant.CATEGORY_LIST, categoryList)
            bundle.putParcelableArrayList(Constant.BRAND_LIST, brandList)
            (activity as VoucherActivity).showVoucherFilterFragment(bundle, object : VoucherFilterFragment.OnResultListener {
                override fun onResult(success: Boolean, categoryList: ArrayList<ItemBaseCategory>?, brandList: ArrayList<ItemBaseCategory>?, statusList: ArrayList<ItemBaseCategory>?) {
                    handleFilter(success, categoryList, brandList, statusList)
                }
            })
        }, 200)
    }

    var needRefreshTab = true
    fun handleFilter(success: Boolean, categoryList: ArrayList<ItemBaseCategory>?, brandList: ArrayList<ItemBaseCategory>?, statusList: ArrayList<ItemBaseCategory>?) {
//        tabLayout.setScrollPosition(lastTabPosition, 0f, true, true)
        needRefreshTab = false
        tabLayout.getTabAt(lastTabPosition)?.select()
        if (!success) {
            return
        }
        var categoryIdListStr = ""
        var brandIdListStr = ""
        var statusIdStr = ""
        if (categoryList != null) {
            for (item in categoryList) {
                if (item.isChecked) {
                    if (!categoryIdListStr.isEmpty()) {
                        categoryIdListStr += ","
                    }
                    categoryIdListStr += item.id
                }
            }
        }
        if (brandList != null) {
            for (item in brandList) {
                if (item.isChecked) {
                    if (!brandIdListStr.isEmpty()) {
                        brandIdListStr += ","
                    }
                    brandIdListStr += item.id
                }
            }
        }
        if (statusList != null) {
            for (item in statusList) {
                if (item.isChecked) {
                    statusIdStr = "${item.id}"
                    break
                }
            }
        }
        val requestParams = RequestParams()
        if (!categoryIdListStr.isEmpty()) {
            requestParams.put("categoryid", categoryIdListStr)
        }
        if (!brandIdListStr.isEmpty()) {
            requestParams.put("brandid", brandIdListStr)
        }
        if (!statusIdStr.isEmpty()) {
            requestParams.put("type", statusIdStr)
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            voucherFragment?.requestParamsStore = requestParams
            voucherFragment?.refreshData()
        }, 300)
    }

    fun handleOnPageSelected(position: Int) {
        Loggers.e("handleOnPageSelected", "position = $position")
        if (!needRefreshTab) {
            needRefreshTab = true
            return
        }
        currentTab = tabList!!.get(position)
        initVoucherFragment()
    }

    private fun initVoucherFragment() {
        val bundle = Bundle()
        bundle.putParcelable(Constant.DATA, currentTab)
        if (voucherFragment != null) {
            voucherFragment!!.refreshData(currentTab)
            return
        }
        val screenSize = ScreenSize(activity!!)
        val lp = frameLayoutVoucherList.layoutParams as LinearLayout.LayoutParams
        lp.height = screenSize.height - screenSize.height / 7
        bundle.putInt(Constant.VOUCHER_TYPE, Constant.VOUCHER_STORE)
        bundle.putString(Constant.TYPE_VOUCHER_AREA, Constant.TYPE_VOUCHER_AREA_MARKET)
        voucherFragment = VoucherFragment()
        voucherFragment!!.arguments = bundle
        voucherFragment!!.scrollView = stickyScrollView
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayoutVoucherList, voucherFragment!!)
        fragmentTransaction.commitAllowingStateLoss()
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).goBack(false)
            }
        }
        editSearch.setOnClickListener {
            if ((categoryList == null || categoryList!!.size == 0) && (brandList == null || brandList!!.size == 0) && (tabList == null || tabList!!.size == 0)) {
                (activity as BaseActivity).hideKeyboard(editSearch)
                return@setOnClickListener
            }
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).showSearchVoucherFragment()
            }
        }
        textNotifyCategory.setOnClickListener {
            getDataCategory()
        }
        textNotifyBrand.setOnClickListener {
            getDataBrand()
        }
        textNotifyBoxHorizontal.setOnClickListener {
            getDataBoxHorizontal()
        }
        /*Handler(Looper.getMainLooper()).postDelayed(Runnable {
//           voucherFragment?.recyclerView?.isNestedScrollingEnabled = false
            stickyScrollView?.setOnStickyViewListener(object : StickyNestedScrollView.OnStickyViewListener {
                override fun onStickyView(isSticky: Boolean) {
                    Loggers.e("stickyScrollView", "isSticky = $isSticky")
//                voucherFragment?.setNestedScrollingEnabled(isSticky)
                }
            })
        }, 1000)*/
    }
}
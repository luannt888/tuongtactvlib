package vn.mediatech.istudiocafe.voucher.chatsupport

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemChat(@SerializedName("id") val id: String?,
                    @SerializedName("voucher_id") val voucherId: String?,
                    @SerializedName("unique_id") var uniqueId: String?,
                    @SerializedName("user_id") val userId: String?,
                    @SerializedName("user_name") val userName: String?,
                    @SerializedName("user_avatar") val userAvatar: String?,
                    @SerializedName("status") val status: String?,
                    @SerializedName("type") val type: Int?,
                    @SerializedName("content") val content: String?,
                    @SerializedName("image") val image: String?,
                    @SerializedName("created_at") val createdAt: String?,
                    @SerializedName("ztype") var zType: String?="",
                    @SerializedName("prize_type") var prizeType: String?="") : Parcelable



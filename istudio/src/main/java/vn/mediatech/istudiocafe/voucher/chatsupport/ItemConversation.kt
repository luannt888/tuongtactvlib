package vn.mediatech.istudiocafe.voucher.chatsupport

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import vn.mediatech.istudiocafe.voucher.ItemVoucher

@Parcelize
//<<<<<<< HEAD
class ItemConversation(@SerializedName("ID") val id: String?,
                       @SerializedName("voucher_id") var voucherId: String?,
                       @SerializedName("user_id") val userId: String?,
                       @SerializedName("unique_id") var uniqueId: String?,
                       @SerializedName("full_name") val userName: String?,
                       @SerializedName("avatar") val userAvatar: String?,
//                        @SerializedName("status") val status: String?,
//                        @SerializedName("type") val type: String?,
                       @SerializedName("mess") var message: String?,
                       @SerializedName("image") val image: String?,
                       @SerializedName("date") val date: String?,
                       @SerializedName("voucher_name") var voucherName: String?,
                       @SerializedName("count") var count: Int?,
                       @SerializedName("list_voucher") var listVoucher: ArrayList<ItemVoucher>?,
                       @SerializedName("ztype") var zType: String? = "",
                       @SerializedName("prize_type") var prizeType: String? = "",
                       @SerializedName("status") var status: String? = "") : Parcelable
//=======
//class ItemConversation(
//        @SerializedName("ID") val id: String?,
//        @SerializedName("unique_id") var uniqueId: Int?,
//        @SerializedName("voucherid") val voucherId: String?,
//        @SerializedName("userid") val userId: String?,
//        @SerializedName("fullname") val userName: String?,
//        @SerializedName("avatar") val userAvatar: String?,
//        @SerializedName("status") val status: String?,
//        @SerializedName("type") val type: String?,
//        @SerializedName("mess") var message: String?,
//        @SerializedName("image") val image: String?,
//        @SerializedName("date") val date: String?,
//        @SerializedName("count") var count: Int?
//) : Parcelable
//>>>>>>> master_interactive

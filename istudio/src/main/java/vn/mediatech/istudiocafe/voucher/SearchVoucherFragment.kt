package vn.mediatech.istudiocafe.voucher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_search_voucher.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import java.util.*

class SearchVoucherFragment : Fragment() {
    var isViewCreated: Boolean = false
    var voucherFragment: VoucherFragment? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_voucher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
//            putInt(Constant.VOUCHER_TYPE, voucherType)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)
        (activity as BaseActivity).showKeyboardFocus(editSearch)
    }

    fun handleSearch() {
        val keyword = editSearch.text.toString().trim()
        if (keyword.isEmpty()) {
            return
        }
        textKeywordSearched.text = String.format(Locale.ENGLISH, requireActivity().getString(R.string.keyword_search), keyword)
        textKeywordSearched.visibility = View.VISIBLE
        if (voucherFragment != null) {
            voucherFragment!!.search(keyword)
            return
        }
        val itemObj = ItemBaseCategory(1, "", "", VoucherAdapter.STYLE_GRID_STRING, false)
        val bundle = Bundle()
        bundle.putString(Constant.KEY_WORD, keyword)
        bundle.putInt(Constant.VOUCHER_TYPE, Constant.VOUCHER_STORE)
        bundle.putParcelable(Constant.DATA, itemObj)
        voucherFragment = VoucherFragment()
        voucherFragment!!.arguments = bundle
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, voucherFragment!!)
        fragmentTransaction.commit()
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).goBack(false)
            }
        }
        editSearch.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                (activity as BaseActivity).hideKeyboard(editSearch)
                handleSearch()
                return@setOnEditorActionListener true
            }
            false
        }
    }

    override fun onDestroyView() {
        (activity as BaseActivity).hideKeyboard(editSearch)
        super.onDestroyView()
    }
}
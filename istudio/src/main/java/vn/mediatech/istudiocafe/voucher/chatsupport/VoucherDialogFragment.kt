package vn.mediatech.istudiocafe.voucher.chatsupport

import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_firework.*
import kotlinx.android.synthetic.main.fragment_voucher_dialog.*
import kotlinx.android.synthetic.main.fragment_voucher_dialog.layoutRoot
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.voucher.ItemVoucher
import vn.mediatech.istudiocafe.voucher.VoucherAdapter
import java.util.*

class VoucherDialogFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var list: ArrayList<ItemVoucher>? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var onItemClickListener: VoucherAdapter.OnItemClickListener? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_voucher_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

//    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return super.onCreateDialog(savedInstanceState).apply {
//            window?.setDimAmount(0f)
//        }
//    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelableArrayList(ConstantTT.DATA, list)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()
    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        val height = bundle?.getInt(Constant.HEIGHT)
        if (height != null && height > 0) {
            Loggers.e("Height", "HeightBidMulti" + height)
//            param?.height = height
            try {
                val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
                param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                param?.height = height
            } catch (e: Exception) {
            }
        }
        list = bundle?.getParcelableArrayList(Constant.DATA)!!
        updateUI(list!!)
    }


    fun initControl() {
    }

    var adapter: VoucherAdapter? = null
    fun updateUI(itemList: ArrayList<ItemVoucher>?) {
        if (itemList == null || itemList.size == 0) {
            dismiss()
            return
        }
        adapter = VoucherAdapter(requireActivity(), list!!, VoucherAdapter.STYLE_LIST_STRING, 1)
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        rcvBid?.layoutManager = layoutManager
        rcvBid?.adapter = adapter
        adapter?.onItemClickListener = onItemClickListener
    }

    override fun onDismiss(dialog: DialogInterface) {
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

}
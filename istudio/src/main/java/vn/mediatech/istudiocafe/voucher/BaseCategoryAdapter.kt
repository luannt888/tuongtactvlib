package vn.mediatech.istudiocafe.voucher

import android.app.Activity
import android.graphics.Outline
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.item_base_category_vertical.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.util.ScreenSize
import java.util.*

class BaseCategoryAdapter(val activity: Activity, val itemList: ArrayList<ItemBaseCategory>, val style: Int, val
numberColumn: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        val STYLE_VERTICAL = 1
        val STYLE_HORIZONTAL = 2
    }

    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var onItemClickListener: OnItemClickListener? = null

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int {
        return style
    }

    fun initViewSize(viewType: Int) {
        val screenSize = ScreenSize(activity)
        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
            screenSize.width
        val spacePx: Int = activity.resources.getDimensionPixelSize(R.dimen.space_item)
        when (viewType) {
            STYLE_VERTICAL -> {
                val whiteDistance = spacePx * (numberColumn + 1)
                imageWidth = (screenWidth - whiteDistance) / numberColumn
                imageHeight = imageWidth * 9 / 16
            }
            STYLE_HORIZONTAL -> {
                val whiteDistance = spacePx * 2
                imageWidth = (screenWidth - whiteDistance) / 2
//                imageHeight = imageWidth
            }
        }
    }

    fun setFullSpan(view: View, isFullSpan: Boolean) {
        if (view.layoutParams is StaggeredGridLayoutManager.LayoutParams) {
            (view.layoutParams as StaggeredGridLayoutManager.LayoutParams).isFullSpan = isFullSpan
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        initViewSize(viewType)
        when (viewType) {
            STYLE_VERTICAL -> {
                val view = LayoutInflater.from(activity).inflate(R.layout.item_base_category_vertical,
                        parent, false)
                setFullSpan(view, false)
                return VerticalViewHolder(view)
            }
            STYLE_HORIZONTAL -> {
                val view = LayoutInflater.from(activity).inflate(R.layout.item_base_category_horizontal,
                        parent, false)
                setFullSpan(view, false)
                return HorizontalViewHolder(view)
            }
        }
        //default
        val view = LayoutInflater.from(activity).inflate(R.layout.item_base_category_vertical,
                parent, false)
        setFullSpan(view, false)
        return VerticalViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemObj: ItemBaseCategory = itemList.get(position)
        if (holder is VerticalViewHolder) {
            bindDataVerticalView(holder, itemObj, position)
        } else if (holder is HorizontalViewHolder) {
            bindDataHorizontalView(holder, itemObj, position)
        }
    }

    fun bindDataVerticalView(holder: VerticalViewHolder, itemObj: ItemBaseCategory, position: Int) {
        holder.textName.text = itemObj.name
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)
    }

    fun bindDataHorizontalView(holder: HorizontalViewHolder, itemObj: ItemBaseCategory, position: Int) {
        holder.textName.text = itemObj.name
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)
    }

    inner class VerticalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val textName = itemView.textName

        init {
            layoutRoot.setOnClickListener {
                onItemClickListener?.onClick(itemList.get(adapterPosition), adapterPosition)
            }
            val lp: LinearLayout.LayoutParams = imageThumbnail.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
        }
    }

    inner class HorizontalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val textName = itemView.textName

        init {
            setRound(itemView)
            layoutRoot.setOnClickListener {
                onItemClickListener?.onClick(itemList.get(adapterPosition), adapterPosition)
            }
            val lpRoot: ViewGroup.LayoutParams = layoutRoot.layoutParams as ViewGroup.LayoutParams
            lpRoot.width = imageWidth
//            lpRoot.height = imageHeight

            val lp: LinearLayout.LayoutParams = imageThumbnail.layoutParams as LinearLayout.LayoutParams
            val imgWidth = imageWidth / 3
            lp.width = imgWidth
            lp.height = imgWidth
        }
    }


    fun setRound(view: View) {
        view.setOutlineProvider(object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 1f * activity.resources.getDimensionPixelSize(R.dimen.dimen_5)
//                val cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
//                        cornerRadiusDP, activity.resources.getDisplayMetrics())
//                if (isRoundBottomStyleListBox) {
//                    outline.setRoundRect(0, 0, view.width, (view.height + cornerRadius).toInt()
//                    , cornerRadius)
//                } else {
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
//                }
            }
        })
        view.setClipToOutline(true)
    }

    interface OnItemClickListener {
        fun onClick(itemObj: ItemBaseCategory, position: Int)
    }
}
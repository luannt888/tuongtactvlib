package vn.mediatech.istudiocafe.voucher.chatsupport

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.view.*
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_chat_conversation.*
import okhttp3.Response
import org.json.JSONObject
import vn.mediatech.interactive.app.*
import vn.mediatech.interactive.app.EncryptUtil
import vn.mediatech.interactive.service.SocketMangager
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Constant.*
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.service.SocketIOManager
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.voucher.ItemVoucher
import vn.mediatech.istudiocafe.voucher.VoucherAdapter
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ChatConversationFragment : Fragment() {
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var itemObj: ItemConversation? = null
    var myHttpRequest: MyHttpRequest? = null
    var itemUser: ItemUser? = null
    var itemChatList: ArrayList<ItemChat> = ArrayList()
    var itemVoucherList: ArrayList<ItemVoucher> = ArrayList()
    var itemVoucher: ItemVoucher? = null
    var isSummitRead: Boolean = false

    //<<<<<<< HEAD
//    var itemVoucher: ItemVoucher? = null
//    val API_CHAT_HISTORY = "https://daugiatruyenhinh.com/api/v2.1/istudio/myuse/chat/admin?userid=%1\$s&voucherid=%2\$s"
//    val TYPE_ADMIN = 1
//    val TYPE_USER = 0
//=======

    //>>>>>>> master_interactive
    var voucherId: String? = "0"
    var uniqueIdVoucher: String? = "0"
    var fragmentCallBackListenner: FragmentCallBackListenner? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chat_conversation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.DATA, itemObj)
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
//        initHeightProvider()
//        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
//        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main, imageBgMain)
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        itemObj = bundle.getParcelable(Constant.DATA) ?: return
        if (savedInstanceState != null) {
            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
        if (itemUser == null) {
            activity?.onBackPressed()
            return
        }
        voucherId = itemObj!!.voucherId
        uniqueIdVoucher = itemObj!!.uniqueId
        if (itemObj!!.listVoucher != null) {
            itemVoucherList = itemObj!!.listVoucher!!
        }
//        initSocket()
        initSocketIO()
//        initWebviewGuide()
        getData()
    }

    private fun updateVoucher(item: ItemVoucher) {

    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemChatList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        MyApplication.getInstance().loadImageCircle(
            context,
            imgAvatar,
            itemObj!!.userAvatar,
            R.drawable.ic_avatar
        )
        textActionbarTitle.text = itemObj!!.userName
        layoutVoucher.visibility = View.GONE
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        progressBar.visibility = View.VISIBLE
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
//        val api = String.format(Locale.ENGLISH, Constant.API_USE_VOUCHER_HISTORY, itemUser!!.id, itemObj!!.id)
        val requestParams = RequestParams()
        requestParams.put("user_id", "${itemObj!!.userId}")
        requestParams.put("voucher_id", voucherId)
        requestParams.put("unique_id", uniqueIdVoucher)
        myHttpRequest!!.request(
            false,
//            ServiceUtilTT.validAPIDGTH(context, API_CHAT_HISTORY),
            ServiceUtilTT.validAPITT(context, API_VOUCHER_CHAT_LIST),
            requestParams,
            object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                    isLoading = false
                    showErrorNetwork(activity?.getString(R.string.msg_network_error))
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    if (isDetached) {
                        return
                    }
                    handleData(responseString)
                    activity?.runOnUiThread {
                        try {
                            progressBar.visibility = View.GONE
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    isLoading = false
                }
            })
    }

    fun handleData(responseStr: String?) {
        if (isSummitRead) {
            isSummitRead = false
            return
        }
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { message?.let { showErrorNetwork(it) } }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
//        val dataStr = JsonParser.getString(resultObj, Constant.DATA)
        val dataStr = JsonParser.getString(jsonObject, Constant.RESULT)
        val dataVoucher = JsonParser.getString(resultObj, "list_voucher")
        val gson = Gson()
        val gsonType = object : TypeToken<ArrayList<ItemChat>>() {}.getType()
        var itemList: ArrayList<ItemChat>? = null
        try {
            itemList = gson.fromJson(dataStr, gsonType)
//            itemList = gson.fromJson(resultObjStr, gsonType)
        } catch (e: Exception) {
            e.printStackTrace()
            itemList = ArrayList()
        }
        if (itemList != null) {
            itemChatList.clear()
//            itemList.reverse()
            itemChatList.addAll(itemList)
        }
        val gsonTypeVoucher = object : TypeToken<ArrayList<ItemVoucher>>() {}.getType()
        try {
//            itemVoucherList = gson.fromJson(dataVoucher, gsonTypeVoucher)
        } catch (e: Exception) {
            e.printStackTrace()
            itemVoucherList = ArrayList()
        }
//        itemObj = JsonParser.parseItemVoucher(dataObj)
        activity?.runOnUiThread {
            try {
                setData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    var isFirsAddLayoutChat = true
    fun setData() {
        if (isFirsAddLayoutChat) {
            isFirsAddLayoutChat = false
        }
        if (isDetached) {
            return
        }
        MyApplication.getInstance().loadImage(context, imageThumbnail, itemObj!!.image)
        textName.text = itemObj!!.voucherName
//        textFirstPrice.text = TextUtil.formatMoney(item.price!!.toInt())
//        textPrice.text = TextUtil.formatMoney(item.discountPrice!!.toInt())
//        textDiscountPercent.text = item.percent
//        textExpired.text = item.expiredDate
        if (!itemObj!!.status.isNullOrEmpty()) {
            when (itemObj!!.status) {
                STATUS_NOT_SOLVE -> {
                    textFilter.text = NOT_SOLVE
                }
                STATUS_SOLVING -> {
                    textFilter.text = SOLVING
                }
                STATUS_SOLVED -> {
                    textFilter.text = SOLVED
                }
            }
        }
        layoutVoucher.visibility = View.VISIBLE
        for (i in 0 until itemVoucherList.size) {
            val item = itemVoucherList.get(i)
            if (voucherId.equals(item.id.toString()) && uniqueIdVoucher.equals(item.uniqueId.toString())) {
                itemVoucher = item
                itemObj?.voucherName = item.name
                MyApplication.getInstance().loadImage(context, imageThumbnail, item.image)
                textName.text = item.name
                textFirstPrice.text = TextUtil.formatMoney(item.price!!.toInt())
                textPrice.text = TextUtil.formatMoney(item.discountPrice!!.toInt())
                textDiscountPercent.text = item.percent
                textExpired.text = item.expiredDate
                layoutVoucher.visibility = View.VISIBLE
                when (item.status) {
                    STATUS_NOT_SOLVE -> {
                        textFilter.text = NOT_SOLVE
                    }
                    STATUS_SOLVING -> {
                        textFilter.text = SOLVING
                    }
                    STATUS_SOLVED -> {
                        textFilter.text = SOLVED
                    }
                }
            }
        }
        if (adapter != null) {
            adapter?.notifyDataSetChanged()
        } else {
            initAdapter()
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            scrollToBottom()
        }, 500)
    }

    fun addItemChat(item: ItemChat) {
        isScroll = true
        itemChatList.add(item)
        if (isDetached) {
            return
        }
        activity?.runOnUiThread {
            if (adapter != null) {
                adapter?.notifyDataSetChanged()
            } else {
                initAdapter()
            }
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                scrollToBottom()
            }, 500)
        }
    }

    var isScroll: Boolean = true
    var adapter: ItemChatAdapter? = null
    fun initAdapter() {
        isScroll = true
        adapter = ItemChatAdapter(requireContext(), itemChatList, true)
        adapter?.onItemClickListener = object : ItemChatAdapter.OnItemClickListener {
            override fun onClick(item: ItemConversation, position: Int) {
            }

            override fun onLoadImgFinish() {
                if (isScroll) {
                    isScroll = false
                    activity?.runOnUiThread {
                        Handler(Looper.getMainLooper()).postDelayed({ scrollToBottom() }, 500)
                    }
                }
            }
        }
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    fun sendMsg() {
        val msg = editMessage.text.toString().trim()
        if (msg.isEmpty()) {
            (activity as BaseActivity).showToast(R.string.msg_comment_empty)
            return
        }
//        (activity as BaseActivity).hideKeyboard(editMessage)
        editMessage.setText("")
//        (activity as BaseActivity).showLoadingDialog(true, null)
        if (itemUser == null) {
            return
        }
        val userName = itemUser!!.fullname ?: itemUser!!.phone
        val itemVoucherChat = ItemChat(
            "",
            itemObj!!.id,
            itemObj!!.uniqueId,
            itemUser!!.id.toString(),
            userName,
            itemUser!!.avatar,
            "0",
            TYPE_ADMIN,
            msg,
            null,
            TextUtil.getCurrentDateString()
        )
        addItemChat(itemVoucherChat)
        sendMessageChat(msg, null, TYPE_ADMIN)
        addChatServer(itemObj!!.userId, Constant.TYPE_ADMIN.toString(), null, msg)
    }

    fun scrollToBottom() {
        if (isDetached) {
            return
        }
//        Handler(Looper.getMainLooper()).postDelayed({
        if (adapter != null) {
            recyclerView?.getLayoutManager()?.scrollToPosition(adapter!!.getItemCount() - 1);
        }
//        }, 200)
    }

    //    START: CHOOSE IMAGE
    var popupMenuChooseImage: PowerMenu? = null
    fun showChooseImagePopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.select_from_gallery)))
        itemList.add(PowerMenuItem(getString(R.string.select_from_camera)))
        val screenSize = ScreenSize(requireActivity())
        val popupWidth = screenSize.width * 2 / 3
        val textView = TextView(activity)
        textView.textSize = 17f
        textView.gravity = Gravity.CENTER
        textView.setTextColor(
            ContextCompat.getColor(
                requireActivity(),
                R.color.color_primary_dark_tt
            )
        )
        textView.text = activity?.getString(R.string.send_image_title) ?: ""
        textView.setPadding(25, 15, 15, 15)
        textView.setBackgroundColor(Color.parseColor("#f8f8f8"))
        textView.setTypeface(Typeface.create("sans-serif-medium", Typeface.BOLD))

        popupMenuChooseImage = PowerMenu.Builder(requireActivity())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOW_UP_CENTER)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    onMenuItemChooseImageClickListener(position, item?.title.toString())
                    popupMenuChooseImage!!.dismiss()
                }
            })
            .setHeaderView(textView)
            .build()
        popupMenuChooseImage!!.showAtCenter(buttonGallery)
    }

    fun onMenuItemChooseImageClickListener(position: Int, title: String) {
        if (title.equals(getString(R.string.select_from_gallery))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    Constant.REQUEST_CODE_GALLERY
                )
                return
            }
            openGallery()
            return
        }
        if (title.equals(getString(R.string.select_from_camera))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    Constant.REQUEST_CODE_CAMERA
                )
                return
            }
            openCamera()
        }
    }

    fun openGallery() {
//        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(intent, Constant.REQUEST_CODE_GALLERY)
    }

    private fun createImageFile(): File? {
        val fileName = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? =
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(fileName, ".jpg", storageDir)
    }

    var photoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(
                    requireActivity(), requireActivity().packageName + ".provider",
                    photoFile
                )
                this.photoFile = photoFile
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(intent, Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY, Constant.REQUEST_CODE_CAMERA -> {
                for (permission in permissions) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(),
                            permission
                        )
                    ) {
                        (activity as BaseActivity).showDialog(getString(R.string.msg_accept_permission))
                        break
                    } else {
                        if (ActivityCompat.checkSelfPermission(
                                requireActivity(),
                                permission
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                                openGallery()
                            } else if (requestCode == Constant.REQUEST_CODE_CAMERA) {
                                openCamera()
                            }
                        } else {
                            callPermissionSettings(requestCode)
                        }
                    }
                }
            }
        }
    }

    fun callPermissionSettings(requestCode: Int) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", requireActivity().packageName, null)
        intent.data = uri
        startActivityForResult(intent, requestCode)
    }

    fun isPopupMenuChooseImage(): Boolean {
        if (popupMenuChooseImage == null || !popupMenuChooseImage!!.isShowing()) {
            return false
        }
        popupMenuChooseImage!!.dismiss()
        return true
    }
    //    END: CHOOSE IMAGE

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        buttonBack.setOnClickListener {
            (activity as? BaseActivity)?.hideKeyboard(editMessage)
            activity?.onBackPressed()
        }
        buttonGallery.setOnClickListener {
            showChooseImagePopup()
        }
        buttonSend.setOnClickListener {
            sendMsg()
        }
        buttonMore.visibility =View.INVISIBLE
        buttonMore.setOnClickListener {
            val fragment = VoucherDialogFragment()
            val bundle = Bundle()
            bundle.putParcelableArrayList(Constant.DATA, itemVoucherList)
//            val height = ScreenSize(requireActivity()).height - layoutActionbar.height
            val height = recyclerView.height + layoutInput.height + layoutNotify.height
            bundle.putInt(Constant.HEIGHT, height)
            fragment.arguments = bundle
            fragment.onItemClickListener = object : VoucherAdapter.OnItemClickListener {
                override fun onClick(itemObj: ItemVoucher, position: Int) {
                    voucherId = itemObj.id.toString()
                    uniqueIdVoucher = itemObj.uniqueId.toString()
                    fragment?.dismiss()
                    getData()
                }

            }
            fragment.show(requireFragmentManager(), "Voucher")
        }
        layoutVoucher.setOnClickListener {
//            val fragment = DetailVoucherFragment()
//            val bundle = Bundle()
//            itemVoucher?.statusType = Constant.VOUCHER_STATUS_USED
//            bundle.putParcelable(Constant.DATA, itemVoucher)
//            fragment.arguments = bundle
//            fragment.onResultListener = object : OnResultListener {
//                override fun onResult(isSuccess: Boolean, msg: String?) {
//                    (activity as? BaseActivity)?.setStatusBarNormal()
//                }
//            }
//            (activity as? ChatSupportActivity)?.showFragment(fragment)
        }
        recyclerView.setOnTouchListener { v, event ->
            (activity as BaseActivity).hideKeyboard(editMessage)
            false
        }
        buttonFilter.visibility =View.GONE
        buttonFilter.setOnClickListener {
            initMenuStatus()
        }
        editMessage.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus && adapter != null) {
                Handler(Looper.getMainLooper()).postDelayed({ scrollToBottom() }, 500)
            }
        }
        editMessage.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed({ scrollToBottom() }, 500)
        }
    }

    val SOLVED = "Đã xử lý"
    val SOLVING = "Đang xử lý"
    val NOT_SOLVE = "Chưa xử lý"
    val STATUS_SOLVED = "3"
    val STATUS_SOLVING = "2"
    val STATUS_NOT_SOLVE = "1"
    private fun initMenuStatus() {
        val menu = PopupMenu(context, buttonFilter)
        menu.getMenu().add(NOT_SOLVE)
        menu.getMenu().add(SOLVING)
        menu.getMenu().add(SOLVED)
        menu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem?): Boolean {
//                textFilter.text = item!!.title
                updateDataStatus(item!!.title.toString())
                return true
            }
        })
        menu.show()
    }

    fun updateDataStatus(type: String? = NOT_SOLVE) {
        if (isLoading) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        progressBar.visibility = View.VISIBLE
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        var status = "1"
        if (type.equals(NOT_SOLVE)) {
            status = "1"
        } else if (type.equals(SOLVING)) {
            status = "2"
        } else {
            status = "3"
        }
        requestParams.put("status", status)
        requestParams.put("voucher_id", voucherId)
        requestParams.put("unique_id", uniqueIdVoucher)
        requestParams.put("user_id", itemObj!!.userId.toString())
//        val api = String.format(Locale.ENGLISH, Constant.API_USE_VOUCHER_HISTORY, itemUser!!.id, itemObj!!.id)
//        val api = "https://daugiatruyenhinh.com/api/v2.1/istudio/myuse/chat/status"
        val api = ServiceUtilTT.validAPITT(context, Constant.API_CHAT_CONVERSATION_HISTORY)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                if (responseString.isNullOrEmpty()) {
                    showErrorNetwork(activity?.getString(R.string.msg_empty_data))
                    return
                }
                val response = responseString.trim()
                val jsonObject = JsonParser.getJsonObject(response);
                if (jsonObject == null) {
                    showErrorNetwork(activity?.getString(R.string.msg_empty_data))
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                if (errorCode != Constant.SUCCESS) {
                    activity?.runOnUiThread {
                        message?.let {
//                            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                            activity?.runOnUiThread {
                                showErrorNetwork(it!!)
                            }
                        }
                    }
                    return
                }
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                        textFilter.text = type
                        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    var fileCompressor: FileCompressor? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY -> {
                if (data == null) return
                data.let {
                    val selectedImage = data.data
                    if (selectedImage == null) {
                        return
                    }
                    if (fileCompressor == null) {
                        fileCompressor = FileCompressor(requireActivity())
                    }
                    val realPath =
                        fileCompressor!!.getRealPathFromUri(requireActivity(), selectedImage)
                    if (realPath.isNullOrEmpty()) {
                        return
                    }
                    photoFile = fileCompressor!!.compressToFile(File(realPath))
//                    MyApplication.getInstance()
//                            ?.loadImage(activity!!, imageThumbnail, photoFile, true)
//                    uploadImageToServer(photoFile.toString())
                    addChatServer(
                        itemObj!!.userId,
                        Constant.TYPE_ADMIN.toString(),
                        photoFile.toString(),
                        null
                    )
                    photoFile = null
                }
            }
            Constant.REQUEST_CODE_CAMERA -> {
                if (photoFile == null) {
                    return
                }
                if (fileCompressor == null) {
                    fileCompressor = FileCompressor(requireActivity())
                }
                photoFile = fileCompressor!!.compressToFile(photoFile)
//                MyApplication.getInstance()?.loadImage(activity!!, imageThumbnail, photoFile, true)
//                uploadImageToServer(photoFile.toString())
                addChatServer(
                    itemObj!!.userId,
                    Constant.TYPE_ADMIN.toString(),
                    photoFile.toString(),
                    null
                )
                photoFile = null
            }
        }
    }

    fun addChatServer(userid: String?, type: String?, imageFilePath: String?, content: String?) {
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(requireActivity())
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("user_id", "${itemUser!!.id}")
        requestParams.put("access_token", "${itemUser!!.accessToken}")
        requestParams.put("voucher_id", "${voucherId}")
        requestParams.put("unique_id", "${uniqueIdVoucher}")
        if (itemVoucher != null) {
            requestParams.put("ztype", "${itemVoucher!!.zType}")
            requestParams.put("prize_type", "${itemVoucher!!.prizeType}")
        }
        requestParams.put("ztype", "${itemObj!!.zType}")
        requestParams.put("prize_type", "${itemObj!!.prizeType}")
        requestParams.put("type", type)
        requestParams.put("content", content)
        requestParams.put("channel", ConstantTT.CHANEL_ID)
        if (!imageFilePath.isNullOrEmpty()) {
            val file = File(imageFilePath)
            if (file.exists()) {
                requestParams.put("image_file", imageFilePath)
                requestParams.put("typedata", myHttpRequest!!.getMimeType(file))
                requestParams.put("typeext", myHttpRequest!!.getExtension(file.name))
            }
        }
        myHttpRequest!!.request(
            true,
//            ServiceUtilTT.validAPIDGTH(context, API_USE_VOUCHER_ADD),
            if (imageFilePath.isNullOrEmpty()) ServiceUtilTT.validAPITT(
                context,
                API_VOUCHER_ADD_CHAT
            ) else ServiceUtilTT.validAPITT(context, Constant.API_VOUCHER_ADD_CHAT_IMAGE),
            requestParams,
            object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                    activity!!.runOnUiThread {
                        (activity as BaseActivity).showToast(R.string.msg_network_error)
                    }
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    if (isDetached) {
                        return
                    }
                    val json = JsonParserUtil.getJsonObject(responseString)
                    val jsonResult = JsonParserUtil.getJsonObject(json, "result")
                    val jsonData = JsonParserUtil.getJsonObject(jsonResult, "data")
//                    val imageUrl = JsonParserUtil.getString(jsonData, "image")
                    val imageUrl = JsonParserUtil.getString(jsonResult, "image")
                    val creatAt = JsonParserUtil.getString(jsonData, "created_at")
                    activity?.runOnUiThread {
                        (activity as BaseActivity).hideLoadingDialog()
                        if (imageUrl.isNullOrEmpty()) {
                            return@runOnUiThread
                        }
                        val userName = itemUser!!.fullname ?: itemUser!!.phone
                        val itemVoucherChat = ItemChat(
                            "",
                            itemObj!!.id,
                            itemObj!!.uniqueId,
                            itemUser!!.id.toString(),
                            userName,
                            itemUser!!.avatar,
                            "0",
                            TYPE_ADMIN,
                            content,
                            imageUrl,
                            creatAt
                        )
                        addItemChat(itemVoucherChat)
                        sendMessageChat(content, imageUrl, TYPE_ADMIN)
//                    getData()
                    }
                }
            })
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        stopService()
        super.onDestroyView()
    }

    private var port: String? = ""
    private val PORT: String = "wss://api.daugiatruyenhinh.com:8088/interactive?" // debug
    private var mSocketMangager: SocketMangager? = null
    private fun initSocket() {
//        port = PORT + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
        mSocketMangager = SocketMangager.getInstance()
        val listenner = object : SocketMangager.MySocketListenner {
            override fun onConnected(response: Response) {
                autoConnectCount = 0
                Loggers.e("Socket", "onConnected")
            }

            override fun onMessage(string: String) {
                autoConnectCount = 0
                val data = EncryptUtil.base64Decode(string)
//                data = resume
                Loggers.e("Socket", "onMessage :" + data)
                if (data.isNullOrEmpty()) {
                    return
                }
                activity?.runOnUiThread {
                    try {
                        handleMessage(data)
                    } catch (e: Exception) {
                    }
                }
            }

            override fun onClosed(code: Int, reason: String) {
                Loggers.e("Socket", "onClosed" + reason)
                if (code == ConstantTT.CLOSE_CONNECT) {
                    return
                }
            }

            override fun onClosing(code: Int, reason: String) {
                Loggers.e("Socket", "onClosing" + reason)
                if (code == ConstantTT.CLOSE_CONNECT) {
                    return
                }
                if (isDetached) {
                    return
                }
                tryConnect()
//                activity?.runOnUiThread {
//                    AppUtils.showDialog(activity, false, false, null, "Không thể kết nối đến máy chủ. Vui lòng kiểm tra và kết nối lại!", "Huỷ", "Kết nối", object : AppUtils.OnDialogButtonListener {
//                        override fun onLeftButtonClick() {
//                            activity?.finish()
//                        }
//
//                        override fun onRightButtonClick() {
//                            mSocketMangager?.reconnect()
//                        }
//                    })
//                }
            }

            override fun onFailure(t: Throwable, response: Response?) {
                Loggers.e(
                    "Socket",
                    "onFailure:" + t.message + " --- " + (if (response != null) (response.body.toString()) else "")
                )
                tryConnect()
            }
        }
        mSocketMangager?.addSocketlistenner(
            ChatConversationFragment::class.java.canonicalName,
            listenner
        )
        val item = ServiceUtilTT.itemInteractiveConfig
        if (SharedPreferencesManager.isTest(context) && !MyApplication.getInstance()
                .isEmpty(SharedPreferencesManager.getPortChat(context))
        ) {
            port = SharedPreferencesManager.getPortChat(context)
                .trim() + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
        } else if (item != null && !item.portSupport.isNullOrEmpty()) {
            port =
                item.portSupport.trim() + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
        } else {
//            port = mSocketMangager!!.url
//            if (port == null) {
//                port = PORT.trim() + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
//            }
        }
        if (!port.isNullOrEmpty()) {
            mSocketMangager?.run(port)
        }
    }

    private var socketManager: SocketIOManager? = null
    private fun initSocketIO() {
        if (socketManager != null) {
            socketManager?.close()
        }
        socketManager = SocketIOManager()
        socketManager?.onSocketListener = object : SocketIOManager.OnSocketListener {
            override fun onConnected() {
                Loggers.e("socketManager", "onConnected")
            }

            override fun onDisconnect(reason: String) {
                Loggers.e("socketManager", "onDisconnect: $reason")
            }

            override fun onFailure(message: String) {
                Loggers.e("socketManager", "onFailure: $message")
            }

            override fun onClosed(closeByUser: Boolean, reason: String) {
                Loggers.e("socketManager", "onClosed: userClosed = $closeByUser, reason = $reason")
            }
        }
        port = "https://socket.teenthanhchuong.com:3001"
        val item = ServiceUtilTT.itemInteractiveConfig
        if (SharedPreferencesManager.isTest(context) && !(SharedPreferencesManager.getPortChat(context).isNullOrEmpty())
        ) {
            port =
                SharedPreferencesManager.getPortChat(context)
        } else if (item != null && !item.portSupport.isNullOrEmpty()) {
            port = item.portSupport.trim()
        }
        socketManager?.connect(port!!)
        socketManager?.addEvent("chat_message", object : SocketIOManager.OnMessageListener {
            override fun onMessage(message: String) {
                Loggers.e("socketManager", "message:" + message)
                val jsonMessage = JsonParserUtil.getJsonObject(message)
                val msg = JsonParserUtil.getString(jsonMessage, "content")
                val imageUrl = JsonParserUtil.getString(jsonMessage, "image")
                val idVoucher = JsonParserUtil.getString(jsonMessage, "voucher_id")
                val uniqueId = JsonParserUtil.getString(jsonMessage, "unique_id")
                val idfrom = JsonParserUtil.getString(jsonMessage, "user_id")
                val cmd = JsonParserUtil.getString(jsonMessage, "cmd")
                try {
//                    if (idfrom == null || idfrom!!.equals(itemUser!!.id.toString())) {
//                        return
//                    }
                    if (!"user".equals(cmd) || itemUser!!.id.toString().equals(idfrom)) {
                        return
                    }
                    if (idVoucher == null || !idVoucher.equals(voucherId) || uniqueId == null || !uniqueId.equals(
                            uniqueIdVoucher
                        )
                    ) {
                        return
                    }
                } catch (e: Exception) {
                }
                val type = JsonParserUtil.getInt(jsonMessage, "type")
                if (type == TYPE_IS_TYPING) {
                    val name = JsonParserUtil.getString(jsonMessage, "user_name")
                    setIsTyping(name + " đang soạn tin", true)
                    return
                } else if (type == TYPE_CANCEL_TYPING) {
                    setIsTyping("", false)
                    return
                }
//                var type = TYPE_USER
//                if (!idfrom.equals(itemObj!!.userId)) {
//                    type = TYPE_ADMIN
//                }
                val userName = itemUser!!.fullname ?: itemUser!!.phone
                val itemVoucherChat = ItemChat(
                    "",
                    itemObj!!.id,
                    itemObj!!.uniqueId,
                    itemUser!!.id.toString(),
                    userName,
                    itemUser!!.avatar,
                    "0",
                    type,
                    msg,
                    imageUrl,
                    TextUtil.getCurrentDateString()
                )
                addItemChat(itemVoucherChat)
//                val creatAt = getCurrentDateString()
//                val itemVoucherChat = ItemChat(
//                    "0",
//                    "itemObj!!.id.toString()",
//                    "itemObj!!.uniqueId.toString()",
//                    "itemUser!!.id.toString()",
//                    "userName",
//                    "itemUser!!.avatar",
//                    "itemObj!!.status.toString()",
//                    TYPE_ADMIN,
//                    message,
//                    "imageUrl",
//                    creatAt
//                )
//                addItemChat(itemVoucherChat)
            }
        })
    }

    var autoConnectCount: Int = 0
    private fun tryConnect() {
        if (autoConnectCount >= 0) {
            activity?.runOnUiThread {
                AppUtils.showDialog(
                    activity,
                    false,
                    false,
                    null,
                    "Không thể kết nối đến máy chủ. Vui lòng kiểm tra và kết nối lại!",
                    "Huỷ",
                    "Kết nối",
                    object : AppUtils.OnDialogButtonListener {
                        override fun onLeftButtonClick() {
//                        activity?.finish()
                        }

                        override fun onRightButtonClick() {
                            mSocketMangager?.reconnect()
                        }
                    })
            }
        } else {
            autoConnectCount++
            mSocketMangager?.reconnect()
        }
    }

    private fun handleMessage(message: String) {
        val json = JsonParserUtil.getJsonObject(message)
        Loggers.e("Socket", "onMessageData :" + JsonParserUtil.getString(json, "data"))
//         json = JsonParserUtil.getJsonObject(startGame)
        if (json == null) {
            return
        }
        val service = JsonParserUtil.getString(json, ConstantTT.SERVICE)
        when (service) {
            ConstantTT.SERVICE_REGISTER -> {
//                dataSend = message
//                handleREGISTER(json)
            }
            ConstantTT.SERVICE_CHAT -> {
                val jsonMessageString = JsonParserUtil.getString(json, "message")
                val jsonMessage = JsonParserUtil.getJsonObject(jsonMessageString)
                val message = JsonParserUtil.getString(jsonMessage, "content")
                val uniqueId = JsonParserUtil.getString(jsonMessage, "unique_id")
                val imageUrl = JsonParserUtil.getString(jsonMessage, "image")
                val idVoucher = JsonParserUtil.getString(jsonMessage, "voucher_id")
                val jsonfrom = JsonParserUtil.getJsonObject(json, "from")
                val idfrom = JsonParserUtil.getString(jsonfrom, "ID")
                val key = JsonParserUtil.getString(json, "key")
                if (!key.equals("CHAT") || idfrom!!.equals(itemUser!!.id.toString())) {
                    return
                }
                val cmd = JsonParserUtil.getString(json, "cmd")
                try {
                    if (idVoucher == null || !idVoucher.equals(voucherId) || uniqueId == null || !uniqueId.equals(
                            uniqueIdVoucher
                        )
                    ) {
                        return
                    }
                } catch (e: Exception) {
                }
                val type = JsonParserUtil.getInt(jsonMessage, "type")
                if (type == TYPE_IS_TYPING) {
                    val name = JsonParserUtil.getString(jsonfrom, "Fullname")
                    setIsTyping(name + " đang soạn tin", true)
                    return
                } else if (type == TYPE_CANCEL_TYPING) {
                    setIsTyping("", false)
                    return
                }
//                var type = TYPE_USER
//                if (!idfrom.equals(itemObj!!.userId)) {
//                    type = TYPE_ADMIN
//                }
                val userName = itemUser!!.fullname ?: itemUser!!.phone
                val itemVoucherChat = ItemChat(
                    "",
                    itemObj!!.id,
                    itemObj!!.uniqueId,
                    itemUser!!.id.toString(),
                    userName,
                    itemUser!!.avatar,
                    "0",
                    type,
                    message,
                    imageUrl,
                    TextUtil.getCurrentDateString()
                )
                addItemChat(itemVoucherChat)
            }
        }
    }

    private fun sendMessageChat(message: String?, imageUrl: String?, type: Int) {
        val json = JSONObject()
        val jsonMessage = JSONObject()
        jsonMessage.put("content", message)
        jsonMessage.put("image", imageUrl)
        jsonMessage.put("voucher_id", voucherId)
        jsonMessage.put("unique_id", uniqueIdVoucher)
        jsonMessage.put("voucher_name", itemObj!!.voucherName)
        jsonMessage.put("type", type)
        jsonMessage.put("user_id", itemUser!!.id)
        jsonMessage.put("user_name", itemUser!!.fullname)
        jsonMessage.put("user_avatar", itemUser!!.avatar)
        jsonMessage.put("admin_id", "3")
        jsonMessage.put(ConstantTT.CMD, "user")
        jsonMessage.put("channel", ConstantTT.CHANEL_ID)
        json.put(ConstantTT.MESSAGE, jsonMessage.toString())
        json.put(ConstantTT.CMD, itemObj!!.userId)
        val jsonFrom = JSONObject()
        jsonFrom.put("ID", itemUser!!.id)
        jsonFrom.put("Avatar", itemUser!!.avatar)
        jsonFrom.put("Fullname", itemUser!!.fullname)
//        jsonFrom.put("last_update", Date().toString())
        json.put(ConstantTT.FROM, jsonFrom)
        json.put(ConstantTT.SERVICE, ConstantTT.SERVICE_CHAT)
        json.put(ConstantTT.KEY, ConstantTT.SERVICE_CHAT)
        Loggers.e("Send", json.toString())
        mSocketMangager?.send(EncryptUtil.base64Encode(json.toString()))
        socketManager?.sendMessage("chat_message", "${jsonMessage.toString()}")
    }

    private fun stopService() {
        socketManager?.close()
        mSocketMangager?.close()
        myHttpRequest?.cancel()
    }

    override fun onDetach() {
        super.onDetach()
        fragmentCallBackListenner?.onDetach()
    }

    private fun setIsTyping(msg: String?, isShow: Boolean = true) {
        activity?.runOnUiThread {
            textIsTyping?.text = msg
            layoutTyping?.visibility = if (isShow) View.VISIBLE else View.GONE
        }
    }

    private var keyboardHeightProvider: KeyboardHeightProvider? = null
    private fun initHeightProvider() {
        keyboardHeightProvider = KeyboardHeightProvider(requireActivity())
        keyboardHeightProvider?.addKeyboardListener(object :
            KeyboardHeightProvider.KeyboardListener {
            override fun onHeightChanged(height: Int) {
                if (!isDetached) {
//                    layoutInput?.setTranslationY((-height).toFloat())
//                    recyclerView?.setTranslationY((-height).toFloat())
                    if (height > 0) {
                        sendMessageChat("", null, TYPE_IS_TYPING)
                        activity?.runOnUiThread {
                            layoutVoucher?.visibility = View.GONE
                        }
                    } else {
                        layoutVoucher?.visibility = View.VISIBLE
                        sendMessageChat("", null, TYPE_CANCEL_TYPING)
                    }
                }
            }
        })

    }

    override fun onPause() {
        super.onPause()
        isSummitRead = true
        sendMessageChat("", null, TYPE_CANCEL_TYPING)
        getData()
        keyboardHeightProvider?.onPause()
        MyApplication.getInstance().dataManager.itemConversations = null
    }

    override fun onResume() {
        super.onResume()
        keyboardHeightProvider?.onResume()
        MyApplication.getInstance().dataManager.itemConversations = itemObj
        MyApplication.getInstance().dataManager.isChat = false
    }

    fun updateUI(item: ItemConversation?) {
        if (item == null) {
            return
        }
        itemObj = item
        voucherId = itemObj!!.voucherId
        uniqueIdVoucher = itemObj!!.uniqueId
        isLoading = false
        getData()
    }
//    var popupVoucher:PopupWindow? = null
//    val
}
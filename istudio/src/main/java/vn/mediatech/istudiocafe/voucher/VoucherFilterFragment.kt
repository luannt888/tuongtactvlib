package vn.mediatech.istudiocafe.voucher

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.fragment_voucher_filter.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.util.SpacesItemDecoration

class VoucherFilterFragment : Fragment() {
    var savedInstanceState: Bundle? = null
    var statusList: ArrayList<ItemBaseCategory>? = null
    var categoryList: ArrayList<ItemBaseCategory>? = null
    var brandList: ArrayList<ItemBaseCategory>? = null
    var onResultListener: OnResultListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_voucher_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        init()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelableArrayList(Constant.STATUS_LIST, statusList)
            putParcelableArrayList(Constant.CATEGORY_LIST, categoryList)
            putParcelableArrayList(Constant.BRAND_LIST, brandList)
        }
        super.onSaveInstanceState(outState)
    }

    fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        statusList = bundle.getParcelableArrayList(Constant.STATUS_LIST)
        categoryList = bundle.getParcelableArrayList(Constant.CATEGORY_LIST)
        brandList = bundle.getParcelableArrayList(Constant.BRAND_LIST)
        initBoxCategory()
        initBoxBrand()
        initBoxStatus()
    }

    fun setUnderline(textView: TextView) {
        /*val text = textView.text.toString()
        val span = SpannableString(text)
        span.setSpan(UnderlineSpan(), 0, text.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.text = span*/
    }

    fun initBoxCategory() {
        if (categoryList == null || categoryList!!.size == 0) {
            return
        }
        textBoxCategory.visibility = View.VISIBLE
        recyclerCategory.visibility = View.VISIBLE
        setUnderline(textBoxCategory)
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        val numberColumn = 2
        val itemDecoration = SpacesItemDecoration(spacePx, numberColumn)
        val layoutManager = StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.VERTICAL)
        val adapter = CheckboxAdapter(activity!!, categoryList!!, CheckboxAdapter.STYLE_CHECKBOX)
        activity?.runOnUiThread {
            recyclerCategory.addItemDecoration(itemDecoration)
            recyclerCategory.layoutManager = layoutManager
            recyclerCategory.adapter = adapter
            recyclerCategory.scheduleLayoutAnimation()
        }
    }

    fun initBoxBrand() {
        if (brandList == null || brandList!!.size == 0) {
            return
        }
        textBoxBrand.visibility = View.VISIBLE
        recyclerBrand.visibility = View.VISIBLE
        setUnderline(textBoxBrand)
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        val numberColumn = 2
        val itemDecoration = SpacesItemDecoration(spacePx, numberColumn)
        val layoutManager = StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.VERTICAL)
        val adapter = CheckboxAdapter(activity!!, brandList!!, CheckboxAdapter.STYLE_CHECKBOX)
        activity?.runOnUiThread {
            recyclerBrand.addItemDecoration(itemDecoration)
            recyclerBrand.layoutManager = layoutManager
            recyclerBrand.adapter = adapter
            recyclerBrand.scheduleLayoutAnimation()
        }
    }

    fun initBoxStatus() {
        if (statusList == null || statusList!!.size == 0) {
            return
        }
        textBoxStatus.visibility = View.VISIBLE
        recyclerStatus.visibility = View.VISIBLE
        setUnderline(textBoxStatus)
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        val numberColumn = 1
        val itemDecoration = SpacesItemDecoration(spacePx, numberColumn)
        val layoutManager = StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.VERTICAL)
        val adapter = CheckboxAdapter(activity!!, statusList!!, CheckboxAdapter.STYLE_RADIO_BUTTON)
        activity?.runOnUiThread {
            recyclerStatus.addItemDecoration(itemDecoration)
            recyclerStatus.layoutManager = layoutManager
            recyclerStatus.adapter = adapter
            recyclerStatus.scheduleLayoutAnimation()
        }
        adapter.onItemClickListener = object : CheckboxAdapter.OnItemClickListener {
            override fun onClick(itemObj: ItemBaseCategory, position: Int) {
                for (item in statusList!!) {
                    item.isChecked = false
                }
                itemObj.isChecked = !itemObj.isChecked
                Handler(Looper.getMainLooper()).postDelayed(Runnable {
                    adapter.notifyDataSetChanged()
                }, 200)
            }
        }
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            goBack(false)
        }
        buttonReset.setOnClickListener {
            if (categoryList != null) {
                for (item in categoryList!!) {
                    item.isChecked = false
                }
                recyclerCategory.adapter?.notifyDataSetChanged()
            }
            if (brandList != null) {
                for (item in brandList!!) {
                    item.isChecked = false
                }
                recyclerBrand.adapter?.notifyDataSetChanged()
            }
            if (statusList != null && statusList!!.size > 0) {
                for (item in statusList!!) {
                    item.isChecked = false
                }
                statusList!!.get(0).isChecked = true
                recyclerStatus.adapter?.notifyDataSetChanged()
            }
        }
        buttonApply.setOnClickListener {
            goBack(true)
        }
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                goBack(false)
            }
        })
    }

    fun goBack(success: Boolean) {
        onResultListener?.onResult(success, categoryList, brandList, statusList)
        if (activity is VoucherActivity) {
            (activity as VoucherActivity).goBack(false)
        }
    }

    interface OnResultListener {
        fun onResult(success: Boolean, categoryList: ArrayList<ItemBaseCategory>?, brandList: ArrayList<ItemBaseCategory>?, statusList: ArrayList<ItemBaseCategory>?)
    }

}
package vn.mediatech.istudiocafe.voucher

import android.Manifest
import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.webkit.WebViewClient
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_use_voucher.*
import okhttp3.Response
import org.json.JSONObject
import vn.mediatech.interactive.app.*
import vn.mediatech.interactive.service.SocketMangager
import vn.mediatech.interactive.slideImage.SlideImageAdapter
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Constant.*
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.app.MyFileProvider
import vn.mediatech.istudiocafe.fragment.ImageZoomPagerDialog
import vn.mediatech.istudiocafe.fragment.PlayerFragment
import vn.mediatech.istudiocafe.listener.OnCancelListener
import vn.mediatech.istudiocafe.listener.OnPlayerStateChangeListener
import vn.mediatech.istudiocafe.model.ItemPhoto
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.service.SocketIOManager
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.istudiocafe.util.EncryptUtil
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.TextUtil.getCurrentDateString
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemChat
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemChatAdapter
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemConversation
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class UseVoucherFragment : Fragment() {
    private var loadingDialog: Dialog? = null
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var itemObj: ItemVoucher? = null
    var myHttpRequest: MyHttpRequest? = null
    var myHttpRequestChat: MyHttpRequest? = null
    var itemUser: ItemUser? = null
    var itemList: ArrayList<ItemChat> = ArrayList()

    val STATUS_NO_PROGRESS = 1
    val STATUS_PROGRESSING = 2
    val STATUS_PROGRESSED = 3

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_use_voucher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.DATA, itemObj)
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
        (activity as? VoucherActivity)?.setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance()
            .loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)
        initHeightProvider()
//        toggleShowGuidle()
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        itemObj = bundle.getParcelable(Constant.DATA) ?: return
        if (savedInstanceState != null) {
            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
        if (itemUser == null) {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).goBack(false)
            }
            return
        }
        textVoucherName.text = itemObj!!.name
        initSocketIO()
//        initSocket()
        getData()
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun initWebviewGuide(content: String?) {
        if (content.isNullOrEmpty()) {
            webViewGuide.visibility = View.GONE
            return
        }
        val webSettings = webViewGuide.settings
        webSettings.builtInZoomControls = false
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            webSettings.setAppCacheEnabled(true)
        }
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.setSupportMultipleWindows(true)
        webSettings.loadsImagesAutomatically = true
        webSettings.mediaPlaybackRequiresUserGesture = false
        CookieManager.getInstance().setAcceptCookie(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webViewGuide.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            CookieManager.getInstance().setAcceptThirdPartyCookies(webViewGuide, true)
        }
        webViewGuide.webViewClient = WebViewClient()
        webViewGuide.setBackgroundColor(Color.TRANSPARENT)
        val guideContent =
            "<style>body{padding: 0px; margin: 0px; color: #ffffff;}</style>" + content
//        guideContent += "<br/><br/><img src=\"${itemObj!!.image}\" style=\"width: 100%; height: auto;\" />"
//        guideContent += "<br/><br/><div style=\"color: #989898; font-style: italic;\"> ${itemObj!!.example} </div>"
        webViewGuide?.loadDataWithBaseURL(null, guideContent, "text/html", "utf-8", null);
//       webViewGuide.visibility = View.VISIBLE
        layoutGuide?.visibility = View.VISIBLE
        rotateButtonToggleGuide(false)

    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        if (isDetached) {
            return
        }
        activity?.runOnUiThread {
            try {
                /*if (itemList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }*/
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("voucher_id", "${itemObj!!.id}")
        requestParams.put("unique_id", "${itemObj!!.uniqueId}")
        ServiceManager.getChatList(
            requireActivity(),
            requestParams,
            object : ServiceManager.OnChatListRequestListener {
                override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                    this@UseVoucherFragment.myHttpRequest = myHttpRequest
                }

                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    itemList: ArrayList<ItemChat>?,
                    status: Int?,
                    guide: String?
                ) {
                    if (isDetached) {
                        return
                    }
                    isLoading = false
                    if (!isSuccess) {
                        showErrorNetwork(message)
                        return
                    }
                    activity?.runOnUiThread {
                        try {
                            if (isFirsAddLayoutChat) {
                                initWebviewGuide(guide)
                                if (status != STATUS_PROGRESSED) {
                                    layoutInput.visibility = View.VISIBLE
                                    viewBottomLine.visibility = View.VISIBLE
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    if (itemList == null || itemList.size == 0) {
                        showDialogGuide()
                        showErrorNetwork(getString(R.string.msg_empty_data))
                        return
                    }
                    var isShowDialog = true
                    for (item in itemList) {
                        if (item.type == TYPE_USER) {
                            isShowDialog = false
                            break
                        }
                    }
                    if (isShowDialog) {
                        showDialogGuide()
                    }
//                    itemList.reverse()
                    this@UseVoucherFragment.itemList.clear()
                    this@UseVoucherFragment.itemList.addAll(itemList)
                    activity?.runOnUiThread {
                        try {
                            setData()
                            progressBar.visibility = View.GONE
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })
    }

    fun showDialogGuide() {
        if (!isDetached) {
            activity?.runOnUiThread {
                toggleShowGuidle()
            }
        }
    }

    var isFirsAddLayoutChat = true
    fun setData() {
        if (isFirsAddLayoutChat) {
            isFirsAddLayoutChat = false
            /*if (webViewGuide.height > 0) {
                toggleShowGuidle()
            }*/
        }
        /*   layoutChat.removeAllViews()
           itemList.reverse()
           for (i in 0 until itemList.size) {
               val itemVoucherChat = itemList.get(i)
               addItemChat(itemVoucherChat)
           }
           Handler(Looper.getMainLooper()).postDelayed(Runnable {
               scrollToBottom()
           }, 500)
           startReloadHistoryTimer()*/
        if (isDetached) {
            return
        }
        if (adapter != null) {
            adapter?.notifyDataSetChanged()
        } else {
            initAdapter()
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            scrollToBottom()
        }, 200)
    }

    fun addItemChat(item: ItemChat) {
        isScroll = true
        itemList.add(item)
        if (isDetached) {
            return
        }
        activity?.runOnUiThread {
            if (adapter != null) {
                adapter?.notifyDataSetChanged()
            } else {
                initAdapter()
            }
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                scrollToBottom()
            }, 200)
        }
    }

    var isScroll: Boolean = true
    var adapter: ItemChatAdapter? = null
    fun initAdapter() {
        isScroll = true
        adapter = ItemChatAdapter(requireContext(), itemList, false)
        adapter?.onItemClickListener = object : ItemChatAdapter.OnItemClickListener {
            override fun onClick(item: ItemConversation, position: Int) {
            }

            override fun onLoadImgFinish() {
                if (isScroll) {
                    isScroll = false
                    activity?.runOnUiThread {
                        Handler(Looper.getMainLooper()).postDelayed({ scrollToBottom() }, 500)
                    }
                }
            }
        }
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

//    fun addItemChat(itemVoucherChat: ItemVoucherChat) {
//        val layoutItem: View = layoutInflater.inflate(R.layout.layout_item_voucher_chat, layoutChat, false)
//        val layoutRoot = layoutItem.findViewById<LinearLayout>(R.id.layoutRoot)
//        val layoutContent = layoutItem.findViewById<LinearLayout>(R.id.layoutContent)
//        val imageThumbnail = layoutItem.findViewById<ImageView>(R.id.imageThumbnail)
//        val textMessage = layoutItem.findViewById<TextView>(R.id.textMessage)
//        val textDate = layoutItem.findViewById<TextView>(R.id.textDate)
//        if (itemVoucherChat.content.isNullOrEmpty() && itemVoucherChat.image.isNullOrEmpty()) {
//            return
//        }
//        if (itemVoucherChat.createdAt.isNullOrEmpty()) {
//            textDate.visibility = View.GONE
//        } else {
//            textDate.text = itemVoucherChat.createdAt
//            textDate.visibility = View.VISIBLE
//        }
//        if (itemVoucherChat.content.isNullOrEmpty()) {
//            textMessage.visibility = View.GONE
//        } else {
//            textMessage.text = itemVoucherChat.content
//            textMessage.visibility = View.VISIBLE
//        }
//
//        when (itemVoucherChat.type) {
//            TYPE_USER -> {
//                layoutContent.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_layout_voucher_chat_right)
//                layoutRoot.gravity = Gravity.END
//                layoutContent.gravity = Gravity.END
//            }
//            TYPE_ADMIN -> {
//                layoutContent.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_layout_voucher_chat_left)
//                layoutRoot.gravity = Gravity.START
//                layoutContent.gravity = Gravity.START
//            }
//        }
//        if (itemVoucherChat.image.isNullOrEmpty()) {
//            imageThumbnail.visibility = View.GONE
//        } else {
//            imageThumbnail.visibility = View.VISIBLE
//            MyApplication.getInstance().loadImage(requireContext(), imageThumbnail, itemVoucherChat.image)
//        }
//        layoutChat.addView(layoutItem)
//    }

    fun sendMsg() {
        val msg = editMessage.text.toString().trim()
        if (msg.isEmpty()) {
            GeneralUtils.showToast(activity, R.string.msg_comment_empty)
            return
        }
        editMessage.setText("")
        if (itemUser == null) {
            return
        }
        val userName = itemUser!!.fullname ?: itemUser!!.phone
        val itemVoucherChat = ItemChat(
            "0",
            itemObj!!.id.toString(),
            itemObj!!.uniqueId.toString(),
            itemUser!!.id.toString(),
            userName,
            itemUser!!.avatar,
            itemObj!!.status.toString(),
            TYPE_USER,
            msg,
            null,
            getCurrentDateString()
        )
        addItemChat(itemVoucherChat)
        sendMessageChat(msg, "", TYPE_USER)
        val requestParams = RequestParams()
        requestParams.put("content", msg)
        sendMsgToServer(requestParams)
    }

    fun sendMsgToServer(requestParams: RequestParams?,isImage:Boolean = false) {
        var params = requestParams
        if (params == null) {
            params = RequestParams()
        }
        params.put("voucher_id", "${itemObj!!.id}")
        params.put("type", "$TYPE_USER")
        params.put("unique_id", "${itemObj!!.uniqueId}")
        params.put("ztype", "${itemObj!!.zType}")
        params.put("prize_type", "${itemObj!!.prizeType}")
        params.put("channel", "${ConstantTT.CHANEL_ID}")
        ServiceManager.requestBase(
            requireActivity(),
//            ServiceUtilTT.validAPIDGTH(context, Constant.API_VOUCHER_ADD_CHAT),

            if(!isImage) ServiceUtilTT.validAPITT(context, Constant.API_VOUCHER_ADD_CHAT) else
            ServiceUtilTT.validAPITT(context, Constant.API_VOUCHER_ADD_CHAT_IMAGE),
            params,
            object : ServiceManager.OnBaseRequestListener {
                override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                    this@UseVoucherFragment.myHttpRequestChat = myHttpRequestChat
                }

                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    responseString: String?
                ) {
                    if (isDetached) {
                        return
                    }
                    activity?.runOnUiThread {
                        loadingDialog?.dismiss()
                        if (isSuccess) {
                            val json = JsonParserUtil.getJsonObject(responseString)
                            val jsonResult = JsonParserUtil.getJsonObject(json, "result")
//                            val jsonData = JsonParserUtil.getJsonObject(jsonResult, "data")
                            val jsonData = JsonParserUtil.getJsonObject(json, "result")
                            val imageUrl = JsonParserUtil.getString(jsonData, "image")
                            if (imageUrl.isNullOrEmpty() || imageUrl.equals("https://daugiatruyenhinh.com/upload/istudios/chats/files/")) {
                                return@runOnUiThread
                            }
                            val creatAt = JsonParserUtil.getString(jsonData, "created_at")
                            val content = JsonParserUtil.getString(jsonData, "content")
                            loadingDialog?.dismiss()
                            val userName = itemUser!!.fullname ?: itemUser!!.phone
//                        val itemVoucherChat = ItemVoucherChat(0, userName, itemUser!!.avatar, TYPE_USER, content, imageUrl, creatAt)
                            val itemVoucherChat = ItemChat(
                                "0",
                                itemObj!!.id.toString(),
                                itemObj!!.uniqueId.toString(),
                                itemUser!!.id.toString(),
                                userName,
                                itemUser!!.avatar,
                                itemObj!!.status.toString(),
                                TYPE_USER,
                                content,
                                imageUrl,
                                getCurrentDateString()
                            )
                            addItemChat(itemVoucherChat)
                            sendMessageChat(content, imageUrl, TYPE_USER)
//                        startReloadHistoryTimer(true)
                        }
                    }
                }
            })
    }

    fun scrollToBottom() {
//        scrollContent.fullScroll(View.FOCUS_DOWN)
        /*  try {
              scrollContent.smoothScrollTo(0, scrollContent.getChildAt(0).height, 0)
          } catch (e: Exception) {
          }*/
        //        Handler(Looper.getMainLooper()).postDelayed({
        if (adapter != null) {
            recyclerView?.getLayoutManager()?.scrollToPosition(adapter!!.getItemCount() - 1);
        }
//        }, 200)
    }

    //    START: CHOOSE IMAGE
    var popupMenuChooseImage: PowerMenu? = null
    fun showChooseImagePopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.select_from_gallery)))
        itemList.add(PowerMenuItem(getString(R.string.select_from_camera)))

        val screenSize = ScreenSize(requireActivity())
        val popupWidth = screenSize.width * 2 / 3

        val textView = TextView(activity)
        textView.textSize = 17f
        textView.gravity = Gravity.CENTER
        textView.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.color_primary_dark_tt
            )
        )
        textView.text = activity?.getString(R.string.send_image_title) ?: ""
        textView.setPadding(25, 15, 15, 15)
        textView.setBackgroundColor(Color.parseColor("#f8f8f8"))
        textView.setTypeface(Typeface.create("sans-serif-medium", Typeface.BOLD))

        popupMenuChooseImage = PowerMenu.Builder(requireContext())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOW_UP_CENTER)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireContext(), R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireContext(), R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    onMenuItemChooseImageClickListener(position, item?.title.toString())
                    popupMenuChooseImage!!.dismiss()
                }
            })
            .setHeaderView(textView)
            .build()
        popupMenuChooseImage!!.showAtCenter(buttonGallery)
    }

    fun onMenuItemChooseImageClickListener(position: Int, title: String) {
        if (title.equals(getString(R.string.select_from_gallery))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    Constant.REQUEST_CODE_GALLERY
                )
                return
            }
            openGallery()
            return
        }
        if (title.equals(getString(R.string.select_from_camera))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    Constant.REQUEST_CODE_CAMERA
                )
                return
            }
            openCamera()
        }
    }

    fun openGallery() {
//        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(intent, Constant.REQUEST_CODE_GALLERY)
    }

    private fun createImageFile(): File? {
        val fileName = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(fileName, ".jpg", storageDir)
    }

    var photoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(requireContext().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(
                    requireContext(), requireContext().packageName + ".provider",
                    photoFile
                )
                this.photoFile = photoFile
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(intent, Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY, Constant.REQUEST_CODE_CAMERA -> {
                for (permission in permissions) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(),
                            permission
                        )
                    ) {
                        GeneralUtils.showDialog(activity, getString(R.string.msg_accept_permission))
                        break
                    } else {
                        if (ActivityCompat.checkSelfPermission(
                                requireContext(),
                                permission
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                                openGallery()
                            } else if (requestCode == Constant.REQUEST_CODE_CAMERA) {
                                openCamera()
                            }
                        } else {
                            callPermissionSettings(requestCode)
                        }
                    }
                }
            }
        }
    }

    fun callPermissionSettings(requestCode: Int) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", requireContext().packageName, null)
        intent.data = uri
        startActivityForResult(intent, requestCode)
    }

    fun isPopupMenuChooseImage(): Boolean {
        if (popupMenuChooseImage == null || !popupMenuChooseImage!!.isShowing()) {
            return false
        }
        popupMenuChooseImage!!.dismiss()
        return true
    }
    //    END: CHOOSE IMAGE

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        buttonBack.setOnClickListener {
            GeneralUtils.hideKeyboard(activity, editMessage)
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).goBack(false)
            } else {
                activity?.onBackPressed()
            }

        }
        buttonToggleGuide.setOnClickListener {
            toggleShowGuidle()
        }
        buttonGallery.setOnClickListener {
            showChooseImagePopup()
        }
        buttonSend.setOnClickListener {
            sendMsg()
        }
        recyclerView.setOnTouchListener { v, event ->
            GeneralUtils.hideKeyboard(activity, editMessage)
            false
        }
        editMessage.setOnFocusChangeListener { v, hasFocus ->
            if (isShow) {
                toggleShowGuidle()
            }
//            if(hasFocus){
//                sendMessageChat("", null,TYPE_IS_TYPING)
//            } else {
//                sendMessageChat("", null,TYPE_CANCEL_TYPING)
//            }
            if (hasFocus && adapter != null) {
                Handler(Looper.getMainLooper()).postDelayed({
                    scrollToBottom()
                }, 500)
            }
        }
        editMessage.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed({
                if (isShow) {
                    toggleShowGuidle()
                }
                scrollToBottom()
            }, 500)
        }
        editMessage.addTextChangedListener({ text, start, count, after ->
            if (isShow) {
                toggleShowGuidle()
            }

        })
    }

    var timerReloadHistory: Timer? = null
    fun startReloadHistoryTimer(immediate: Boolean = false) {
//        cancelReloadHistoryTimer()
//        val delay = if (immediate) 0L else 20000L
//        timerReloadHistory = Timer()
//        timerReloadHistory!!.schedule(object : TimerTask() {
//            override fun run() {
//                getData()
//            }
//        }, delay, 20000L)
    }

    fun cancelReloadHistoryTimer() {
        try {
            timerReloadHistory?.cancel()
            timerReloadHistory?.purge()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        timerReloadHistory = null
    }

    var layoutToggleHeight = 0
    var isShow: Boolean = false
    fun toggleShowGuidle() {
        if (webViewGuide.visibility == View.GONE) {
            isShow = true
            rotateButtonToggleGuide(true)
            webViewGuide.visibility = View.VISIBLE
            GeneralUtils.hideKeyboard(activity, editMessage)
//            itemObj!!.galerryTutorial = arrayOf("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOuiXoTSsPOpf15KtOkfjDPrrU-vOvkZWHJA&usqp=CAU",
//                 "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVGLWDPE42_pepDHFj2n2xzQ5Zd6wpuluB8g&usqp=CAU","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOuiXoTSsPOpf15KtOkfjDPrrU-vOvkZWHJA&usqp=CAU")
            startPlayGalery(itemObj!!.galerryTutorial)
            initPlayer()
            Handler(Looper.getMainLooper()).postDelayed({
                if (layoutToggle == null) {
                    return@postDelayed
                }
                val lp = layoutToggle.layoutParams as LinearLayout.LayoutParams
                val layoutToggleHeight = Math.min(
                    layoutToggle.height,
                    (layoutRoot.height - (layoutInput.height + layoutActionbar.height + layoutTitleGuide.height)) * 8 / 10
                )
                lp.height = layoutToggleHeight
                layoutToggle?.layoutParams = lp
            }, 1000)

            return
        }
//        https://stackoverflow.com/questions/19765938/show-and-hide-a-view-with-a-slide-up-down-animation
        if (layoutToggleHeight == 0) {
            layoutToggleHeight = layoutToggle.height
        }
        val start = layoutToggle.height
        val end = if (start == 0) layoutToggleHeight else 0
        isShow = end > 0
        if (!isShow) {
            playerFragment?.pause()
        } else {
            if (playerFragment == null) {
                initPlayer()
            }
            GeneralUtils.hideKeyboard(activity, editMessage)
        }

        val anim = ValueAnimator.ofInt(start, end)
        anim.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
            override fun onAnimationUpdate(animation: ValueAnimator?) {
                if (animation == null || layoutToggle == null) {
                    return
                }
                val height: Int = animation.getAnimatedValue() as Int
                val lpWebviewGuide = layoutToggle.layoutParams as LinearLayout.LayoutParams
                lpWebviewGuide.height = height
                layoutToggle?.layoutParams = lpWebviewGuide
            }
        })
        anim.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {
                rotateButtonToggleGuide(isShow)
            }

            override fun onAnimationEnd(animation: Animator?) {
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationRepeat(animation: Animator?) {
            }

        })
        anim.duration = 300
        anim.start()
    }

    fun rotateButtonToggleGuide(isShow: Boolean) {
        val animId = if (isShow) R.anim.rotate_guide_show else R.anim.rotate_guide_hide
        val anim = AnimationUtils.loadAnimation(requireContext(), animId)
        buttonToggleGuide?.startAnimation(anim)
    }

    var fileCompressor: FileCompressor? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY -> {
                if (data == null) return
                data.let {
                    val selectedImage = data.data
                    if (selectedImage == null) {
                        return
                    }
                    if (fileCompressor == null) {
                        fileCompressor = FileCompressor(requireContext())
                    }
                    val realPath =
                        fileCompressor!!.getRealPathFromUri(requireActivity(), selectedImage)
                    if (realPath.isNullOrEmpty()) {
                        return
                    }
                    photoFile = fileCompressor!!.compressToFile(File(realPath))
//                    MyApplication.getInstance()
//                            ?.loadImage(requireContext(), imageThumbnail, photoFile, true)
                    uploadImageToServer(photoFile.toString())
                    photoFile = null
                }
            }
            Constant.REQUEST_CODE_CAMERA -> {
                if (photoFile == null) {
                    return
                }
                if (fileCompressor == null) {
                    fileCompressor = FileCompressor(requireContext())
                }
                photoFile = fileCompressor!!.compressToFile(photoFile)
//                MyApplication.getInstance()?.loadImage(requireContext(), imageThumbnail, photoFile, true)
                uploadImageToServer(photoFile.toString())
                photoFile = null
            }
        }
    }

    fun uploadImageToServer(filePath: String) {
        val file = File(filePath)
        if (!file.exists()) {
            GeneralUtils.showToast(activity, R.string.msg_process_error)
            return
        }
        if (myHttpRequestChat == null) {
            myHttpRequestChat = MyHttpRequest(requireContext())
        } else {
            myHttpRequestChat!!.cancel()
        }
        loadingDialog = GeneralUtils.showLoadingDialog(activity, true, object : OnCancelListener {
            override fun onCancel(isCancel: Boolean) {
                myHttpRequestChat?.cancel()
            }
        })
        val fileName = file.name
        val msg = editMessage.text.toString().trim()
        val requestParams = RequestParams()
        requestParams.put("image_file", filePath)
        requestParams.put("file_name", fileName)
        requestParams.put("mime_type", myHttpRequest!!.getMimeType(file))
        requestParams.put("extension", myHttpRequest!!.getExtension(fileName))
        requestParams.put("content", msg)
        sendMsgToServer(requestParams,true)
    }

    override fun onDestroyView() {
        stopService()
        cancelReloadHistoryTimer()
        playerFragment?.releasePlayer()
        myHttpRequest?.cancel()
        myHttpRequestChat?.cancel()
        super.onDestroyView()
    }

    override fun onPause() {
        super.onPause()
        timer1?.cancel()
        sendMessageChat("", null, TYPE_CANCEL_TYPING)
        keyboardHeightProvider?.onPause()
        cancelReloadHistoryTimer()
    }

    override fun onResume() {
        super.onResume()
        keyboardHeightProvider?.onResume()
        if (itemList.size > 0) {
            startReloadHistoryTimer(true)
        }
    }

    private var port: String? = ""
    private var PORT: String = "wss://api.daugiatruyenhinh.com:8088/interactive?" // debug

    //        private var PORT: String? = "" // debug
    private var mSocketMangager: SocketMangager? = null
    private fun initSocket() {
        mSocketMangager = SocketMangager.getInstance()
        val listenner = object : SocketMangager.MySocketListenner {
            override fun onConnected(response: Response) {
                Loggers.e("Socket", "onConnected")
            }

            override fun onMessage(string: String) {
                autoConnectCount = 0
                val data = EncryptUtil.base64Decode(string)
//                data = resume
                Loggers.e("Socket", "onMessage :" + data)
                if (data.isNullOrEmpty()) {
                    return
                }
                activity?.runOnUiThread {
                    try {
                        handleMessage(data)
                    } catch (e: Exception) {
                    }
                }
            }

            override fun onClosed(code: Int, reason: String) {
                Loggers.e("Socket", "onClosed" + reason)
                if (code == ConstantTT.CLOSE_CONNECT) {
                    return
                }
            }

            override fun onClosing(code: Int, reason: String) {
                Loggers.e("Socket", "onClosing" + reason)
                if (code == ConstantTT.CLOSE_CONNECT) {
                    return
                }
                if (isDetached) {
                    return
                }
                tryConnect()
            }

            override fun onFailure(t: Throwable, response: Response?) {
                Loggers.e(
                    "Socket",
                    "onFailure:" + t.message + " --- " + (if (response != null) (response.body.toString()) else "")
                )
                tryConnect()
            }
        }
//        mSocketMangager?.addSocketlistenner(UseVoucherFragment::class.java.canonicalName, listenner)
        mSocketMangager?.listenner = listenner
        port = mSocketMangager!!.url
        val item = ServiceUtilTT.itemInteractiveConfig
        if (SharedPreferencesManager.isTest(context) && !MyApplication.getInstance()
                .isEmpty(SharedPreferencesManager.getPortChat(context))
        ) {
            port =
                SharedPreferencesManager.getPortChat(context)
                    .trim() + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
        } else if (item != null && !item.portSupport.isNullOrEmpty()) {
            port =
                item.portSupport.trim() + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
        } else {
//            port = mSocketMangager!!.url
//            if (port == null) {
//                port =
//                    PORT.trim() + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
//            }
        }
        if (!port.isNullOrEmpty()) {
            mSocketMangager?.run(port)
        }
    }

    var autoConnectCount: Int = 0
    private fun tryConnect() {
        if (autoConnectCount >= 0) {
            activity?.runOnUiThread {
                AppUtils.showDialog(
                    activity,
                    false,
                    false,
                    null,
                    "Không thể kết nối đến máy chủ. Vui lòng kiểm tra và kết nối lại!",
                    "Huỷ",
                    "Kết nối",
                    object : AppUtils.OnDialogButtonListener {
                        override fun onLeftButtonClick() {
                            activity?.finish()
                        }

                        override fun onRightButtonClick() {
                            mSocketMangager?.reconnect()
                        }
                    })
            }
        } else {
            autoConnectCount++
            mSocketMangager?.reconnect()
        }
    }

    var currentMessage: String? = null
    var countDouble: Int = 0
    private fun handleMessage(message: String) {
        val json = JsonParserUtil.getJsonObject(message)
        Loggers.e("Socket", "onMessageData :" + JsonParserUtil.getString(json, "data"))
//         json = JsonParserUtil.getJsonObject(startGame)
        if (json == null) {
            return
        }
        val service = JsonParserUtil.getString(json, ConstantTT.SERVICE)
        when (service) {
            ConstantTT.SERVICE_REGISTER -> {
//                dataSend = message
//                handleREGISTER(json)
            }
            ConstantTT.SERVICE_CHAT -> {
                if (currentMessage != null && currentMessage.equals(message) && countDouble == 0) {
                    countDouble++
                    return
                }
                countDouble = 0
                currentMessage = message
                val jsonMessageString = JsonParserUtil.getString(json, "message")
                val jsonMessage = JsonParserUtil.getJsonObject(jsonMessageString)
                val msg = JsonParserUtil.getString(jsonMessage, "content")
                val imageUrl = JsonParserUtil.getString(jsonMessage, "image")
                val idVoucher = JsonParserUtil.getString(jsonMessage, "voucher_id")
                val uniqueId = JsonParserUtil.getString(jsonMessage, "unique_id")
                val jsonfrom = JsonParserUtil.getJsonObject(json, "from")
                val idfrom = JsonParserUtil.getString(jsonfrom, "ID")
                val key = JsonParserUtil.getString(json, "key")
                val cmd = JsonParserUtil.getString(json, "cmd")
                if (!key.equals("CHAT") || idfrom!!.equals(itemUser!!.id.toString()) || (cmd.isNullOrEmpty() || cmd.equals(
                        "admin"
                    ))
                ) {
                    return
                }
//                val cmd = JsonParserUtil.getString(json, "cmd")
                try {
                    if (idVoucher == null || !idVoucher.equals(itemObj!!.id.toString()) || uniqueId == null || !uniqueId.equals(
                            itemObj!!.uniqueId.toString()
                        )
                    ) {
                        return
                    }
                } catch (e: Exception) {
                }
                val type = JsonParserUtil.getInt(jsonMessage, "type")
                if (type == Constant.TYPE_IS_TYPING) {
                    val name = JsonParserUtil.getString(jsonfrom, "Fullname")
//                    setIsTyping(name + " đang soạn tin", true)
                    return
                } else if (type == Constant.TYPE_CANCEL_TYPING) {
                    setIsTyping("", false)
                    return
                } else if (type == Constant.TYPE_USER) {
                    setIsTyping("", false)
                    return
                }
                val userName = itemUser!!.fullname ?: itemUser!!.phone
                val creatAt = getCurrentDateString()
                val itemVoucherChat = ItemChat(
                    "0",
                    itemObj!!.id.toString(),
                    itemObj!!.uniqueId.toString(),
                    itemUser!!.id.toString(),
                    userName,
                    itemUser!!.avatar,
                    itemObj!!.status.toString(),
                    TYPE_ADMIN,
                    msg,
                    imageUrl,
                    creatAt
                )
                addItemChat(itemVoucherChat)
            }
        }
    }

    private fun sendMessageChat(message: String?, imageUrl: String?, type: Int) {
        val json = JSONObject()
        val jsonMessage = JSONObject()
        jsonMessage.put("content", message)
        jsonMessage.put("image", imageUrl)
        jsonMessage.put("voucher_id", itemObj!!.id)
        jsonMessage.put("unique_id", itemObj!!.uniqueId)
        jsonMessage.put("voucher_name", itemObj!!.name)
        jsonMessage.put("type", type)
        jsonMessage.put("user_id", itemObj!!.userId)
        jsonMessage.put("user_name", itemObj!!.userName)
        jsonMessage.put("user_avatar", itemObj!!.userAvatar)
        jsonMessage.put("ztype", itemObj!!.zType)
        jsonMessage.put("prize_type", "${itemObj!!.prizeType}")
        jsonMessage.put("admin_id", "3")
        jsonMessage.put(ConstantTT.CMD, "user")
        jsonMessage.put("channel", ConstantTT.CHANEL_ID)
        json.put(ConstantTT.MESSAGE, jsonMessage.toString())
//        json.put(InteractiveConstant.CMD, itemUser!!.id)
        json.put(ConstantTT.CMD, "user")
        val jsonFrom = JSONObject()
        jsonFrom.put("ID", itemUser!!.id)
        jsonFrom.put("Avatar", itemUser!!.avatar)
        jsonFrom.put("Fullname", itemUser!!.fullname)
        json.put(ConstantTT.FROM, jsonFrom)
        json.put(ConstantTT.SERVICE, ConstantTT.SERVICE_CHAT)
        json.put(ConstantTT.KEY, ConstantTT.SERVICE_CHAT)
        Loggers.e("Send", json.toString())
        mSocketMangager?.send(EncryptUtil.base64Encode(json.toString()))
//        socketManager?.sendMessage("chat_message","${message}")
        socketManager?.sendMessage("chat_message", "${jsonMessage.toString()}")
    }

    private var socketManager: SocketIOManager? = null
    private fun initSocketIO() {
        if (socketManager != null) {
            socketManager?.close()
        }
        socketManager = SocketIOManager()
        socketManager?.onSocketListener = object : SocketIOManager.OnSocketListener {
            override fun onConnected() {
                Loggers.e("socketManager", "onConnected")
            }

            override fun onDisconnect(reason: String) {
                Loggers.e("socketManager", "onDisconnect: $reason")
            }

            override fun onFailure(message: String) {
                Loggers.e("socketManager", "onFailure: $message")
            }

            override fun onClosed(closeByUser: Boolean, reason: String) {
                Loggers.e("socketManager", "onClosed: userClosed = $closeByUser, reason = $reason")
            }
        }
        port = "https://socket.teenthanhchuong.com:3001"
        val item = ServiceUtilTT.itemInteractiveConfig
        if (SharedPreferencesManager.isTest(context) && !(SharedPreferencesManager.getPortChat(context).isNullOrEmpty())
        ) {
            port =
                SharedPreferencesManager.getPortChat(context)
        } else if (item != null && !item.portSupport.isNullOrEmpty()) {
            port = item.portSupport.trim()
        }
        socketManager?.connect(port!!)
        socketManager?.addEvent("chat_message", object : SocketIOManager.OnMessageListener {
            override fun onMessage(message: String) {
                Loggers.e("socketManager", "message:" + message)
                if (currentMessage != null && currentMessage.equals(message) && countDouble == 0) {
                    countDouble++
                    return
                }
                countDouble = 0
                currentMessage = message
                val jsonMessage = JsonParserUtil.getJsonObject(message)
                val msg = JsonParserUtil.getString(jsonMessage, "content")
                val imageUrl = JsonParserUtil.getString(jsonMessage, "image")
                val idVoucher = JsonParserUtil.getString(jsonMessage, "voucher_id")
                val uniqueId = JsonParserUtil.getString(jsonMessage, "unique_id")
                val idfrom = JsonParserUtil.getString(jsonMessage, "user_id")
                val cmd = JsonParserUtil.getString(jsonMessage, "cmd")
                if (idfrom == null || idfrom!!.equals(itemUser!!.id.toString())) {
                    return
                }
//                val cmd = JsonParserUtil.getString(json, "cmd")
                try {
                    if (idVoucher == null || !idVoucher.equals(itemObj!!.id.toString()) || uniqueId == null || !uniqueId.equals(
                            itemObj!!.uniqueId.toString()
                        )
                    ) {
                        return
                    }
                } catch (e: Exception) {
                }
                val type = JsonParserUtil.getInt(jsonMessage, "type")
                if (type == Constant.TYPE_IS_TYPING) {
                    val name = JsonParserUtil.getString(jsonMessage, "user_name")
//                    setIsTyping(name + " đang soạn tin", true)
                    return
                } else if (type == Constant.TYPE_CANCEL_TYPING) {
                    setIsTyping("", false)
                    return
                } else if (type == Constant.TYPE_USER) {
                    setIsTyping("", false)
                    return
                }
                val userName = itemUser!!.fullname ?: itemUser!!.phone
                val creatAt = getCurrentDateString()
                val itemVoucherChat = ItemChat(
                    "0",
                    itemObj!!.id.toString(),
                    itemObj!!.uniqueId.toString(),
                    itemUser!!.id.toString(),
                    userName,
                    itemUser!!.avatar,
                    itemObj!!.status.toString(),
                    TYPE_ADMIN,
                    msg,
                    imageUrl,
                    creatAt
                )
                addItemChat(itemVoucherChat)
//                val creatAt = getCurrentDateString()
//                val itemVoucherChat = ItemChat(
//                    "0",
//                    "itemObj!!.id.toString()",
//                    "itemObj!!.uniqueId.toString()",
//                    "itemUser!!.id.toString()",
//                    "userName",
//                    "itemUser!!.avatar",
//                    "itemObj!!.status.toString()",
//                    TYPE_ADMIN,
//                    message,
//                    "imageUrl",
//                    creatAt
//                )
//                addItemChat(itemVoucherChat)
            }
        })
    }

    private fun stopService() {
        socketManager?.close()
        mSocketMangager?.close()
//        mSocketMangager?.clearSocketListenner(UseVoucherFragment::class.java.canonicalName)
        myHttpRequest?.cancel()
    }


    private var keyboardHeightProvider: KeyboardHeightProvider? = null
    var bottomMargin: Int = -1

    private var addKeyboarHeight = 0
    private fun initHeightProvider() {
        keyboardHeightProvider = KeyboardHeightProvider(requireActivity())
        keyboardHeightProvider?.addKeyboardListener(object :
            KeyboardHeightProvider.KeyboardListener {
            override fun onHeightChanged(height: Int) {
                if (!isDetached) {
//                    layoutInput?.setTranslationY((-height).toFloat())
//                    recyclerView?.setTranslationY((-height).toFloat())
                    if (height > 0) {
                        sendMessageChat("", null, TYPE_IS_TYPING)
                    } else {
                        sendMessageChat("", null, TYPE_CANCEL_TYPING)
                    }
                    Loggers.e(
                        "keyboardHeightProvider",
                        "bottomMargin = $bottomMargin, height = $height"
                    )
                    if (layoutInput == null) {
                        return
                    }
                    val lp = layoutInput?.layoutParams as RelativeLayout.LayoutParams
                    Loggers.e(
                        "keyboardHeightProvider",
                        "bottomMargin2 = ${lp.bottomMargin}, height = $height"
                    )
                    if (bottomMargin < 0) {
                        bottomMargin = lp.bottomMargin
                    }
                    if (height <= 0) {
                        addKeyboarHeight = height
                        if (addKeyboarHeight < 0) {
                            addKeyboarHeight *= -1
                        }
                        lp.bottomMargin = bottomMargin
                    } else {
//                        lp.bottomMargin = bottomMargin + height + 130//
                        lp.bottomMargin = bottomMargin + height + addKeyboarHeight
                    }

                    /* if (bottomMargin == -123 || bottomMargin == -130) {
                         bottomMargin = lp.bottomMargin
                     }
                     lp.bottomMargin = bottomMargin + height*/
                    Loggers.e(
                        "keyboardHeightProvider",
                        "bottomMargin = ${lp.bottomMargin}, height = $height, addKeyboarHeight = $addKeyboarHeight"
                    )
                    layoutInput?.layoutParams = lp
                }
            }
        })
    }

    var playerFragment: PlayerFragment? = null
    var playlink: String? = null

    //    var playlink: String? = "http:/mdtv.mediatech.vn/upload/video/vide"
    private fun initPlayer() {
        playlink = itemObj!!.linkTutorial
        if (playlink.isNullOrEmpty()) {
            frameLayout.visibility = View.GONE
            return
        }
        val screenSize = ScreenSize(requireActivity())
        val height = screenSize.width * 9 / 16
        val lp: LinearLayout.LayoutParams = frameLayout.layoutParams as LinearLayout.LayoutParams
        lp.height = height
        frameLayout.layoutParams = lp
        frameLayout.visibility = View.VISIBLE
        val fragmentManager = childFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        playerFragment = PlayerFragment()
        val bundle = Bundle()
        bundle.putString(Constant.DATA, playlink)
//        bundle.putString(Constant.YOUTUBE_URL, itemObj!!.youtubeUrl)
        bundle.putString(Constant.TYPE, Constant.TYPE_VIDEO)
        bundle.putBoolean(Constant.IS_TAB_SELECTED, true)
        bundle.putBoolean(Constant.IS_RUN_PLAY, true)
        bundle.putBoolean(Constant.IS_LIVE_STREAM, false)
        bundle.putBoolean(Constant.PLAY_WHEN_READY, true)
        bundle.putInt(Constant.FRAME_PLAYER_HEIGHT, height)
        bundle.putInt(Constant.TIME_START_POSITION, 0)
        playerFragment?.setArguments(bundle)
        playerFragment?.setOnPlayerStateChangeListener(object : OnPlayerStateChangeListener {
            override fun onStateEnd() {

            }

            override fun onStateReady() {
            }

            override fun onPlayerError() {
//                frameLayout.visibility = View.GONE
            }
        })
        //        fragmentTransaction.add(R.id.frameLayout, playerFragment);
        fragmentTransaction.add(frameLayout.id, playerFragment!!)
        fragmentTransaction.commit()
    }

    var timer1: Timer? = null
    var index = 0
    fun startPlayGalery(galeryArrayString: Array<String>?) {

        if (galeryArrayString == null || galeryArrayString!!.size == 0) {
            layoutSlideImg.visibility = View.GONE
            return
        }
        layoutSlideImg.visibility = View.VISIBLE
        Loggers.e("GALERRY", galeryArrayString!!.toString())
        val adapter =
            SlideImageAdapter(requireContext(), galeryArrayString.toCollection(ArrayList()))
        adapter.onClickItemListener = object : SlideImageAdapter.OnClickItemListener {
            override fun onClick(urlImage: String, position: Int) {
                val fragment = ImageZoomPagerDialog()
                val list = ArrayList<ItemPhoto>()
                for (string in galeryArrayString) {
                    list.add(ItemPhoto(requireContext().getString(R.string.box_guide), string, ""))
                }
                val bundle = Bundle()
                bundle.putParcelableArrayList(Constant.DATA, list)
                fragment.arguments = bundle
                fragment.show(childFragmentManager, "ImageZoom")
            }
        }
        buttonLeft.setOnClickListener {
            slideImgViewPager.setCurrentItem(Math.max(index - 2, 0), true)
        }
        buttonRight.setOnClickListener {
            slideImgViewPager.setCurrentItem(index, true)
        }
        slideImgViewPager.setAdapter(adapter)
        slideImgViewPager.visibility = View.VISIBLE
        slideImgViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                index = index + 1
                if (index > (galeryArrayString.size - 1)) {
                    index = 0
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })

        timer1 = Timer()
        timer1!!.schedule(timerTask {
            if (isDetached || !isShow) {
                return@timerTask
            }
            activity?.runOnUiThread {
                if (galeryArrayString.get(index).isNullOrEmpty()) {
                    return@runOnUiThread
                }
                Loggers.e(
                    "GALERRY",
                    "" + index + "/ " + galeryArrayString.get(index)
                )
//                AppUtils.loadImage(context, imageView, galeryArrayString.get(index))
                slideImgViewPager?.setCurrentItem(index, true)
            }
        }, 0, 4000)
    }

    private fun setIsTyping(msg: String?, isShow: Boolean = true) {
        textIsTyping?.text = msg
        layoutTyping?.visibility = if (isShow) View.VISIBLE else View.GONE
    }
}
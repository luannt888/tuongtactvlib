package vn.mediatech.istudiocafe.voucher

import android.app.Activity
import android.graphics.Outline
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.item_voucher_layout_user_info.view.*
import kotlinx.android.synthetic.main.item_voucher_listview.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.TextUtil
import java.util.*

class VoucherAdapter(val activity: Activity, val itemList: ArrayList<ItemVoucher>, val style: String, val
numberColumn: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        val STYLE_LIST = 1
        val STYLE_LISTBOX = 2
        val STYLE_GRID = 3
        val STYLE_HORIZONTAL = 4

        val STYLE_LIST_STRING = "listview"
        val STYLE_LISTBOX_STRING = "listbox"
        val STYLE_GRID_STRING = "gridview"
        val STYLE_HORIZONTAL_STRING = "horizontal"
    }

    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var onItemClickListener: OnItemClickListener? = null
    var showLayoutUser: Boolean = false

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int {
        var viewType = STYLE_LIST
        when (style) {
            STYLE_LIST_STRING -> viewType = STYLE_LIST
            STYLE_LISTBOX_STRING -> viewType = STYLE_LISTBOX
            STYLE_GRID_STRING -> viewType = STYLE_GRID
            STYLE_HORIZONTAL_STRING -> viewType = STYLE_HORIZONTAL
        }
        return viewType
    }

    fun initViewSize(viewType: Int) {
        val screenSize = ScreenSize(activity)
        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
            screenSize.width
        val spacePx: Int = activity.resources.getDimensionPixelSize(R.dimen.space_item)
        when (viewType) {
            STYLE_LIST -> {
                val whiteDistance = spacePx * (numberColumn + 1)
                imageWidth = (screenWidth - whiteDistance) * 4 / 13
                imageHeight = imageWidth
            }
            STYLE_LISTBOX -> {
                val whiteDistance = spacePx * (numberColumn + 1)
                imageWidth = screenWidth - whiteDistance
                imageHeight = imageWidth * 2 / 5
            }
            STYLE_GRID -> {
                val whiteDistance = spacePx * (numberColumn + 1)
                imageWidth = (screenWidth - whiteDistance) / numberColumn
                imageHeight = imageWidth * 5 / 8
            }
            STYLE_HORIZONTAL -> {
                imageWidth = screenWidth * 2 / 5
                imageHeight = imageWidth * 5 / 8
            }
        }
    }

    fun setFullSpan(view: View, isFullSpan: Boolean) {
        if (view.layoutParams is StaggeredGridLayoutManager.LayoutParams) {
            (view.layoutParams as StaggeredGridLayoutManager.LayoutParams).isFullSpan = isFullSpan
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        initViewSize(viewType)
        when (viewType) {
            STYLE_LIST -> {
                val view = LayoutInflater.from(activity).inflate(R.layout.item_voucher_listview,
                        parent, false)
                setFullSpan(view, false)
                return ListViewHolder(view)
            }
            STYLE_LISTBOX -> {
                val view = LayoutInflater.from(activity).inflate(R.layout.item_voucher_listboxview,
                        parent, false)
                setFullSpan(view, false)
                return ListBoxViewHolder(view)
            }
            STYLE_GRID -> {
                val view = LayoutInflater.from(activity).inflate(R.layout.item_voucher_gridview,
                        parent, false)
                setFullSpan(view, false)
                return GridViewHolder(view)
            }
            STYLE_HORIZONTAL -> {
                val view = LayoutInflater.from(activity).inflate(R.layout.item_voucher_gridview,
                        parent, false)
                setFullSpan(view, false)
                return GridViewHolder(view, true)
            }
        }
        //default
        val view = LayoutInflater.from(activity).inflate(R.layout.item_voucher_listview,
                parent, false)
        setFullSpan(view, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemObj: ItemVoucher = itemList.get(position)
        if (holder is ListViewHolder) {
            bindDataListView(holder, itemObj, position)
        } else if (holder is ListBoxViewHolder) {
            bindDataListBoxView(holder, itemObj, position)
        } else if (holder is GridViewHolder) {
            bindDataGridView(holder, itemObj, position)
        }
    }

    fun bindDataListView(holder: ListViewHolder, itemObj: ItemVoucher, position: Int) {
        holder.textName.text = itemObj.name
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image, true)
        MyApplication.getInstance().loadImageNoPlaceHolder(activity, holder.imageBrandLogo, itemObj.brandLogo)
        holder.textExprireDate.text = itemObj.expiredDate
        holder.textDiscountPrice.text = "Giá KM: " + TextUtil.formatMoney(itemObj.discountPrice
                ?: 0L)
        if (itemObj.discountPrice == itemObj.price) {
            holder.textPrice.visibility = View.GONE
            holder.textPercent.visibility = View.GONE
        } else {
            holder.textPrice.visibility = View.GONE
            holder.textPercent.visibility = View.GONE
            holder.textPrice.text = TextUtil.formatMoney(itemObj.price ?: 0L)
            holder.textPercent.text = itemObj.percent
            val firstPrice = holder.textPrice.text.toString()
            val firstPriceSpan = SpannableString(firstPrice)
            firstPriceSpan.setSpan(StrikethroughSpan(), 0, firstPrice.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            holder.textPrice.text = firstPriceSpan
            /*   holder.textPrice.visibility = View.VISIBLE
               holder.textPercent.visibility = View.VISIBLE
               holder.textPrice.text = TextUtil.formatMoney(itemObj.price ?: 0L)
               holder.textPercent.text = itemObj.percent
               val firstPrice = holder.textPrice.text.toString()
               val firstPriceSpan = SpannableString(firstPrice)
               firstPriceSpan.setSpan(StrikethroughSpan(), 0, firstPrice.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
               holder.textPrice.text = firstPriceSpan*/
        }
        if (!itemObj.voucherReceive.isNullOrEmpty()) {
            val str = itemObj.voucherReceive!!.replace("vào lúc", "<br/> Vào lúc")
            TextUtil.setHtmlTextView(str,holder.textFrom)
//            holder.textFrom.movementMethod = LinkMovementMethod.getInstance()
            holder.textFrom.visibility = View.VISIBLE
        } else {
            holder.textFrom.visibility = View.GONE
        }
        if (itemObj.statusName.isNullOrEmpty()) {
            holder.textStatus.visibility = View.GONE
        } else {
            val bgStatus: Int = getBgStatus(itemObj.statusType)
            if (bgStatus == -10) {
                holder.textStatus.visibility = View.GONE
            } else {
                holder.textStatus.visibility = View.VISIBLE
                holder.textStatus.text = itemObj.statusName
                holder.textStatus.background = ContextCompat.getDrawable(activity, bgStatus)
            }
        }
        binLayoutUserData(itemObj, holder.viewLine, holder.layoutUser, holder.imageAvatar, holder.textFullName)
    }

    fun bindDataListBoxView(holder: ListBoxViewHolder, itemObj: ItemVoucher, position: Int) {
        holder.textName.text = itemObj.name
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)
        MyApplication.getInstance().loadImage(activity, holder.imageBrandLogo, itemObj.brandLogo)
        holder.textExprireDate.text = itemObj.expiredDate
        holder.textDiscountPrice.text = TextUtil.formatMoney(itemObj.discountPrice ?: 0L)
        if (itemObj.discountPrice == itemObj.price) {
            holder.textPrice.visibility = View.GONE
            holder.textPercent.visibility = View.GONE
        } else {
            holder.textPrice.visibility = View.VISIBLE
            holder.textPercent.visibility = View.VISIBLE
            holder.textPrice.text = TextUtil.formatMoney(itemObj.price ?: 0L)
            holder.textPercent.text = itemObj.percent
            val firstPrice = holder.textPrice.text.toString()
            val firstPriceSpan = SpannableString(firstPrice)
            firstPriceSpan.setSpan(StrikethroughSpan(), 0, firstPrice.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            holder.textPrice.text = firstPriceSpan
        }
        if (itemObj.statusName.isNullOrEmpty()) {
            holder.textStatus.visibility = View.GONE
        } else {
            val bgStatus: Int = getBgStatus(itemObj.statusType)
            if (bgStatus == -10) {
                holder.textStatus.visibility = View.GONE
            } else {
                holder.textStatus.visibility = View.VISIBLE
                holder.textStatus.text = itemObj.statusName
                holder.textStatus.background = ContextCompat.getDrawable(activity, bgStatus)
            }
        }
        binLayoutUserData(itemObj, holder.viewLine, holder.layoutUser, holder.imageAvatar, holder.textFullName)
    }

    fun bindDataGridView(holder: GridViewHolder, itemObj: ItemVoucher, position: Int) {
        holder.textName.text = itemObj.name
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)
        MyApplication.getInstance().loadImageNoPlaceHolder(activity, holder.imageBrandLogo, itemObj.brandLogo)
        holder.textExprireDate.text = itemObj.expiredDate
        holder.textDiscountPrice.text = TextUtil.formatMoney(itemObj.discountPrice ?: 0L)
        if (itemObj.discountPrice == itemObj.price) {
            holder.textPrice.visibility = View.GONE
            holder.textPercent.visibility = View.GONE
        } else {
            holder.textPrice.visibility = View.VISIBLE
            holder.textPercent.visibility = View.VISIBLE
            holder.textPrice.text = TextUtil.formatMoney(itemObj.price ?: 0L)
            holder.textPercent.text = itemObj.percent
            val firstPrice = holder.textPrice.text.toString()
            val firstPriceSpan = SpannableString(firstPrice)
            firstPriceSpan.setSpan(StrikethroughSpan(), 0, firstPrice.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            holder.textPrice.text = firstPriceSpan
        }
        if (itemObj.statusName.isNullOrEmpty()) {
            holder.textStatus.visibility = View.GONE
        } else {
            val bgStatus: Int = getBgStatus(itemObj.statusType)
            if (bgStatus == -10) {
                holder.textStatus.visibility = View.GONE
            } else {
                holder.textStatus.visibility = View.VISIBLE
                holder.textStatus.text = itemObj.statusName
                holder.textStatus.background = ContextCompat.getDrawable(activity, bgStatus)
            }
        }
        binLayoutUserData(itemObj, holder.viewLine, holder.layoutUser, holder.imageAvatar, holder.textFullName)
    }

    fun binLayoutUserData(itemObj: ItemVoucher, viewLine: View, layoutUser: LinearLayout, imageAvatar: ImageView, textFullName: TextView) {
        if (!showLayoutUser) {
            return
        }
        if (itemObj.userName.isNullOrEmpty()) {
            viewLine.visibility = View.GONE
            layoutUser.visibility = View.GONE
        } else {
            viewLine.visibility = View.VISIBLE
            layoutUser.visibility = View.VISIBLE
            textFullName.text = itemObj.userName
            MyApplication.getInstance().loadAvatar(activity, imageAvatar, itemObj.userAvatar, true)
        }
    }

    fun getBgStatus(statusType: Int): Int {
        var bgStatus: Int = -10
        when (statusType) {
            Constant.VOUCHER_STATUS_USED -> bgStatus = R.drawable.ic_voucher_ribbon_used
            Constant.VOUCHER_STATUS_EXPIRED -> bgStatus = R.drawable.ic_voucher_ribbon_expired
            Constant.VOUCHER_STATUS_SELLED -> bgStatus = R.drawable.ic_voucher_ribbon_selled
            Constant.VOUCHER_STATUS_NEW -> bgStatus = R.drawable.ic_voucher_ribbon_new
            Constant.VOUCHER_STATUS_RECEIVED -> bgStatus = R.drawable.ic_voucher_ribbon_received
            Constant.VOUCHER_STATUS_SELLING -> bgStatus = R.drawable.ic_voucher_ribbon_selling
            Constant.VOUCHER_STATUS_DONATED -> bgStatus = R.drawable.ic_voucher_ribbon_donated
            Constant.VOUCHER_STATUS_WAITING_RECEIVE, Constant.VOUCHER_STATUS_WAITING_DONATE -> bgStatus = R.drawable.ic_voucher_ribbon_waiting
        }
        return bgStatus
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val textName = itemView.textName
        val layoutImage = itemView.layoutImage
        val imageThumbnail = itemView.imageThumbnail
        val imageBrandLogo = itemView.imageBrandLogo
        val textStatus = itemView.textStatus
        val textPercent = itemView.textPercent
        val textExprireDate = itemView.textExprireDate
        val textPrice = itemView.textPrice
        val textDiscountPrice = itemView.textDiscountPrice
        val viewLine = itemView.viewLine
        val layoutUser = itemView.layoutUser
        val imageAvatar = itemView.imageAvatar
        val textFullName = itemView.textFullName
        val textFrom = itemView.textFrom

        init {
            layoutRoot.setOnClickListener {
                onItemClickListener?.onClick(itemList.get(adapterPosition), adapterPosition)
            }
            val lp = layoutImage.layoutParams as LinearLayout.LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
        }
    }

    inner class ListBoxViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val textName = itemView.textName
        val layoutImage = itemView.layoutImage
        val imageThumbnail = itemView.imageThumbnail
        val imageBrandLogo = itemView.imageBrandLogo
        val textStatus = itemView.textStatus
        val textPercent = itemView.textPercent
        val textExprireDate = itemView.textExprireDate
        val textPrice = itemView.textPrice
        val textDiscountPrice = itemView.textDiscountPrice
        val viewLine = itemView.viewLine
        val layoutUser = itemView.layoutUser
        val imageAvatar = itemView.imageAvatar
        val textFullName = itemView.textFullName

        init {
            layoutRoot.setOnClickListener {
                onItemClickListener?.onClick(itemList.get(adapterPosition), adapterPosition)
            }
            val lp = layoutImage.layoutParams as LinearLayout.LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
        }
    }

    inner class GridViewHolder(itemView: View, isHorizontal: Boolean = false) : RecyclerView.ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val textName = itemView.textName
        val layoutImage = itemView.layoutImage
        val imageThumbnail = itemView.imageThumbnail
        val imageBrandLogo = itemView.imageBrandLogo
        val textStatus = itemView.textStatus
        val textPercent = itemView.textPercent
        val textExprireDate = itemView.textExprireDate
        val textPrice = itemView.textPrice
        val textDiscountPrice = itemView.textDiscountPrice
        val viewLine = itemView.viewLine
        val layoutUser = itemView.layoutUser
        val imageAvatar = itemView.imageAvatar
        val textFullName = itemView.textFullName

        init {
            layoutRoot.setOnClickListener {
                onItemClickListener?.onClick(itemList.get(adapterPosition), adapterPosition)
            }
            if (isHorizontal) {
                val lp = layoutRoot.layoutParams as ViewGroup.LayoutParams
                lp.width = RecyclerView.LayoutParams.WRAP_CONTENT
            }
            val lp = layoutImage.layoutParams as LinearLayout.LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
        }
    }

    fun setRound(view: View) {
        view.setOutlineProvider(object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 1f * activity.resources.getDimensionPixelSize(R.dimen.dimen_5)
//                val cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
//                        cornerRadiusDP, activity.resources.getDisplayMetrics())
//                if (isRoundBottomStyleListBox) {
//                    outline.setRoundRect(0, 0, view.width, (view.height + cornerRadius).toInt()
//                    , cornerRadius)
//                } else {
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
//                }
            }
        })
        view.setClipToOutline(true)
    }

    interface OnItemClickListener {
        fun onClick(itemObj: ItemVoucher, position: Int)
    }
}
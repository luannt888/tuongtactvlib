package vn.mediatech.istudiocafe.voucher

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.fragment_tab_home_tt.view.*
import kotlinx.android.synthetic.main.fragment_voucher.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnItemClickUserVoucherListener
import vn.mediatech.istudiocafe.listener.OnResultListener
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.util.*
import kotlin.collections.ArrayList

class VoucherFragment : Fragment() {
    var isViewCreated: Boolean = false
    var isLoading: Boolean = false
    var myHttpRequest: MyHttpRequest? = null
    var itemList: ArrayList<ItemVoucher> = ArrayList()
    var adapter: VoucherAdapter? = null
    var voucherType: Int = Constant.VOUCHER_STORE
    var  limit = 15
    var startInsert = 0
    var loadMoreAble: Boolean = false
    var savedInstanceState: Bundle? = null
    var itemUser: ItemUser? = null
    var keyword: String = ""
    var userId: Int = -1
    var page = 1;
    var onItemClickListener: VoucherAdapter.OnItemClickListener? = null
    var onItemClickVoucherListener: OnItemClickUserVoucherListener? = null
    lateinit var itemObj: ItemBaseCategory
    var requestParamsStore: RequestParams? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_voucher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        checkRefresh()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putInt(Constant.VOUCHER_TYPE, voucherType)
            putParcelable(Constant.DATA, itemObj)
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (isDetached) {
            return
        }
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        layoutRefresh.setColorSchemeResources(R.color.color_primary_tt)
    }

    var type_area: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        itemObj = bundle.getParcelable(Constant.DATA) ?: return
        voucherType = bundle.getInt(Constant.VOUCHER_TYPE, Constant.VOUCHER_STORE)
        type_area = bundle.getString(Constant.TYPE_VOUCHER_AREA, null)
        keyword = bundle.getString(Constant.KEY_WORD, "")
        userId = bundle.getInt(Constant.USER_ID, -1)
        if (voucherType == Constant.USER_VOUCHER) {
            if (userId == -1) {
                return
            }
        }
        if (savedInstanceState == null) {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        } else {
            itemUser = bundle.getParcelable(Constant.USERNAME)
            MyApplication.getInstance().dataManager.itemUser = itemUser
        }
        if (voucherType == Constant.SEARCH_VOUCHER) {
            if (keyword.isEmpty()) {
                return
            }
        }
        getData()
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemList.size <= 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                } else {
                    textNotify.visibility = View.GONE
                }
                layoutLoadMore.visibility = View.GONE
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        var api: String? = null
        val requestParams = RequestParams()
//        requestParams.put("type", "1")
        requestParams.put("page", "$page")
        requestParams.put("limit", "$limit")
        when (voucherType) {
            Constant.MY_VOUCHER -> {
                requestParams.put("userid", "${itemUser?.id}")
                requestParams.put("status", "${itemObj.id}")
                requestParams.put("channel", "${ConstantTT.CHANEL_ID}")
//                api = ServiceUtilTT.validAPIDGTH(context, Constant.API_MY_VOUCHER)
                api = ServiceUtilTT.validAPITT(context, Constant.API_MY_VOUCHER)
            }
            Constant.VOUCHER_STORE, Constant.SEARCH_VOUCHER -> {
                api = ServiceUtilTT.validAPIDGTH(context, Constant.API_VOUCHER_STORE)
                if (!keyword.isEmpty()) {
                    requestParams.put("keyword", keyword)
                }
                requestParams.addParams(requestParamsStore)
            }
            Constant.USER_VOUCHER -> {
                requestParams.put("userid", "$userId")
                requestParams.put("utype", "user")
                api = ServiceUtilTT.validAPIDGTH(context, Constant.API_VOUCHER_STORE)
            }
        }
        if (api.isNullOrEmpty() || isDetached || activity == null) {
            return
        }

        ServiceManager.getVoucherList(
            requireActivity(),
            api,
            requestParams,
            object : ServiceManager.OnVoucherRequestListener {
                override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                    this@VoucherFragment.myHttpRequest = myHttpRequest
                }

                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    itemList: ArrayList<ItemVoucher>?
                ) {
                    if (isDetached) {
                        return
                    }
                    isLoading = false
                    if (!isSuccess) {
                        activity?.runOnUiThread {
                            showErrorNetwork(message)
                        }
                        return
                    }
                    if (itemList == null || itemList.size == 0) {
                        loadMoreAble = false
                        activity?.runOnUiThread {
                            showErrorNetwork(requireContext().resources.getString(R.string.msg_empty_data))
                        }
                    } else {
                        loadMoreAble = itemList.size >= limit
                        startInsert = itemList.size
                        this@VoucherFragment.itemList.addAll(itemList)
                        try {
                            setData()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        page++
                    }
                    activity?.runOnUiThread {
                        try {
                            progressBar.visibility = View.GONE
                            layoutLoadMore.visibility = View.GONE
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })
    }

    fun setData() {
        if (adapter != null) {
            activity?.runOnUiThread {
                adapter?.notifyItemChanged(startInsert)
                if (limit == 0) {
                    recyclerView.scheduleLayoutAnimation()
//                recyclerView.notifyDataSetChanged()
                }
            }
            return
        }
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        var numberColumn = 1
        var orientation: Int = StaggeredGridLayoutManager.VERTICAL
        when (itemObj.style) {
            "gridview" -> numberColumn = 2
            "horizontal" -> orientation = StaggeredGridLayoutManager.HORIZONTAL
        }
        val itemDecoration = SpacesItemDecoration(spacePx, numberColumn)
        val layoutManager = StaggeredGridLayoutManager(numberColumn, orientation)
//        recyclerView.isNestedScrollingEnabled = false
        recyclerView.setHasFixedSize(false)
        recyclerView.itemAnimator = DefaultItemAnimator()
        activity?.runOnUiThread {
//            adapter = VoucherAdapter(requireContext(), itemList, itemObj.style, numberColumn)
            adapter = VoucherAdapter(requireActivity(), itemList, itemObj.style, numberColumn)
            adapter?.showLayoutUser = !(voucherType == Constant.MY_VOUCHER)
            recyclerView.addItemDecoration(itemDecoration)
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = adapter
            recyclerView.scheduleLayoutAnimation()
            adapter!!.onItemClickListener = object : VoucherAdapter.OnItemClickListener {
                override fun onClick(itemObj: ItemVoucher, position: Int) {
                    if (itemObj.statusType == Constant.VOUCHER_STATUS_WAITING_RECEIVE) {
                        GeneralUtils.showDonatedVoucherDialog(activity,
                            itemObj,
                            object : OnResultListener {
                                override fun onResult(isSuccess: Boolean, msg: String?) {
                                    if (isSuccess) {
                                        try {
                                            itemList.remove(itemObj)
                                            adapter?.notifyDataSetChanged()
                                            showErrorNetwork(getString(R.string.msg_empty_data))
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                        }
                                    }
                                }
                            })
                        return
                    } else if (itemObj.statusType == Constant.VOUCHER_STATUS_WAITING_DONATE) {
                        GeneralUtils.showDialogVoucher(
                            activity,
                            true,
                            true,
                            itemObj.image,
                            R.string.notification,
                            itemObj.message,
                            null,
                            null,
                            R.string.close,
                            null
                        )
                        return
                    }

                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        val bundle = Bundle()
                        bundle.putParcelable(Constant.DATA, itemObj)
                        bundle.putString(Constant.TYPE_VOUCHER_AREA, type_area)
                        val onResultListener = object : OnResultListener {
                            override fun onResult(isSuccess: Boolean, msg: String?) {
                                refreshData()
                            }
                        }
                        if (activity is VoucherActivity) {
                            (activity as? VoucherActivity)?.showDetailVoucherFragment(
                                bundle, onResultListener
                            )
                        } else {
                            onItemClickListener?.onClick(itemObj, position)
                        }
                        return@Runnable
                    }, 200)
                }
            }
        }
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                Loggers.e("VoucherFrg_onScrollStateChanged", "newState = $newState")
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//
//                }
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView2: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView2, dx, dy)
                val firstVisibleItem =
                    layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
//                Loggers.e("VoucherFrg_onScrolled", "dx = $dx, dy = $dy, firstVisibleItem = $firstVisibleItem, isSticky = $isSticky")
                /*if (dy > 0) {//scroll up
                   if (firstVisibleItem == 0) {

                   }
               } else {//scroll down

               }*/
                if (isLoading || !loadMoreAble) {
//                    Loggers.e("VoucherFrg", "addOnScrollListener onScrolled_stopGet")
                    return
                }
                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
                layoutRefresh.isEnabled = firstPos == 0

                val visibleItemCount = layoutManager.childCount
                val firstVisible = layoutManager.findFirstVisibleItemPositions(null)[0]
                val totalItem = layoutManager.itemCount
                val spanCount = layoutManager.spanCount + 1
                if (visibleItemCount + firstVisible >= totalItem - spanCount) {
                    layoutLoadMore.visibility = View.VISIBLE
//                    Loggers.e("VoucherFrg", "addOnScrollListener onScrolled_getData")
                    getData()
                }
            }
        })
    }

    var scrollView: StickyNestedScrollView? = null
    var isSticky: Boolean = false

    fun search(keyword: String) {
        this.keyword = keyword
        refreshData()
    }

    fun refreshData(itemObj: ItemBaseCategory) {
        this.itemObj = itemObj
        refreshData()
    }

    fun refreshData() {
//        recyclerView.scrollToPosition(0)
        myHttpRequest?.cancel()
        isLoading = false
        itemList.clear()
        layoutLoadMore?.visibility = View.GONE
        page = 1
        loadMoreAble = true
        adapter?.notifyDataSetChanged()
        progressBar?.visibility = View.VISIBLE
        layoutRefresh?.isRefreshing = false
        getData()
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        layoutRefresh.setOnRefreshListener {
            if (isLoading || isDetached) {
                return@setOnRefreshListener
            }
            refreshData()
        }
    }

    fun setNestedScrollingEnabled(enable: Boolean) {
        if (recyclerView == null || recyclerView.isNestedScrollingEnabled == enable) {
            return
        }
        isSticky = enable
        recyclerView.isNestedScrollingEnabled = isSticky
        /* if (recyclerView == null) {
             return
         }
         if (recyclerView?.isNestedScrollingEnabled == enable || enable == false) {
             return
         }
         Loggers.e("setNestedScrollingEnabled", "enableA = " + recyclerView.isNestedScrollingEnabled)
         recyclerView?.isNestedScrollingEnabled = true
         Loggers.e("setNestedScrollingEnabled", "enableB = " + recyclerView.isNestedScrollingEnabled)
 //        recyclerView?.invalidate()
         isSticky = enable*/
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        super.onDestroyView()
    }
}
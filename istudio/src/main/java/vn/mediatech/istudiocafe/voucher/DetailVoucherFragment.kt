package vn.mediatech.istudiocafe.voucher

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.fragment_detail_voucher.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.listener.OnItemClickUserVoucherListener
import vn.mediatech.istudiocafe.listener.OnResultListener
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.*
import java.util.*

class DetailVoucherFragment : Fragment() {
    private var loadingDialog: Dialog? = null
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var itemObj: ItemVoucher? = null
    var myHttpRequest: MyHttpRequest? = null
    var myHttpRequestRelate: MyHttpRequest? = null
    var isMyVoucher: Boolean = false
    var itemUser: ItemUser? = null
    var onResultListener: OnResultListener? = null
    var onItemClickUserVoucherListener: OnItemClickUserVoucherListener? = null
    var removeId: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail_voucher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.DATA, itemObj)
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
        (activity as? VoucherActivity)?.setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)
    }

    var areaType: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        itemObj = bundle.getParcelable(Constant.DATA) ?: return
        areaType = bundle.getString(Constant.TYPE_VOUCHER_AREA, null)
        if (savedInstanceState != null) {
            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
        getData()
//        setData()
    }

    fun checkShowBottomButton() {
        /* if (!isMyVoucher || itemUser == null) {
             hideBottomButton()
             getRelateData()
             return
         }*/
        viewBottomSpace.visibility = View.VISIBLE
        layoutBottomButton.visibility = View.VISIBLE
        when (itemObj!!.statusType) {
            Constant.VOUCHER_STATUS_USED -> {
                buttonUse.visibility = View.GONE
                buttonDonate.visibility = View.GONE
                buttonSell.text = getString(R.string.button_detail_used)
                buttonSell.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_voucher_white)
                buttonSell.visibility = View.VISIBLE
            }
            Constant.VOUCHER_STATUS_EXPIRED -> {
                buttonUse.visibility = View.GONE
                buttonDonate.visibility = View.GONE
                buttonSell.text = getString(R.string.button_expired)
                buttonSell.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_voucher_white)
                buttonSell.visibility = View.VISIBLE
                buttonSell.isEnabled = false
            }
            Constant.VOUCHER_STATUS_SELLED -> {
                buttonUse.visibility = View.GONE
                buttonDonate.visibility = View.GONE
                buttonSell.text = getString(R.string.button_selled)
                buttonSell.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_voucher_white)
                buttonSell.visibility = View.VISIBLE
                buttonSell.isEnabled = false
            }
            Constant.VOUCHER_STATUS_RECEIVED, Constant.VOUCHER_STATUS_NEW -> {
                buttonUse.visibility = View.VISIBLE
                buttonDonate.visibility = View.GONE
                buttonSell.visibility = View.GONE
                buttonSell.text = getString(R.string.button_sell_now)
                buttonSell.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_voucher_red)
                buttonSell.setTextColor(ContextCompat.getColor(requireContext(), R.color.white_tt))
            }
            Constant.VOUCHER_STATUS_SELLING -> {
                buttonUse.visibility = View.GONE
                buttonDonate.visibility = View.GONE
                buttonSell.text = getString(R.string.button_cancel_sell)
                buttonSell.background = ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.bg_button_voucher_white_yellow
                )
                buttonSell.visibility = View.VISIBLE
            }
            Constant.VOUCHER_STATUS_DONATED -> {
                buttonUse.visibility = View.GONE
                buttonDonate.visibility = View.GONE
                buttonSell.text = getString(R.string.button_donated)
                buttonSell.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_voucher_white)
                buttonSell.visibility = View.VISIBLE
                buttonSell.isEnabled = false
            }
            Constant.VOUCHER_STATUS_WAITING_RECEIVE, Constant.VOUCHER_STATUS_WAITING_DONATE -> {
                buttonUse.visibility = View.GONE
                buttonDonate.visibility = View.GONE
                buttonSell.text = getString(R.string.button_waiting)
                buttonSell.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_voucher_white)
                buttonSell.visibility = View.VISIBLE
                buttonSell.isEnabled = false
            }
        }
    }

    fun hideBottomButton() {
        activity?.runOnUiThread {
            viewBottomSpace.visibility = View.GONE
            layoutBottomButton.visibility = View.GONE
            progressBarRelate.visibility = View.VISIBLE
        }
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                textNotify.visibility = View.VISIBLE
                textNotify.text = message
                layoutContent.visibility = View.GONE
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("id", "${itemObj!!.id}")
        requestParams.put("unique_id", "${itemObj!!.uniqueId}")
        requestParams.put("area_type", "${areaType}")
        requestParams.put("ztype", itemObj!!.zType)
        if (itemObj!!.userId != null) {
            requestParams.put("userid", "${itemObj!!.userId}")
        }
        ServiceManager.getVoucherDetail(
            requireActivity(),
//            ServiceUtilTT.validAPIDGTH(context, Constant.API_VOUCHER_DETAIL),
            ServiceUtilTT.validAPITT(context, Constant.API_VOUCHER_DETAIL),
            requestParams,
            object : ServiceManager.OnVoucherDetailRequestListener {
                override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                    this@DetailVoucherFragment.myHttpRequest = myHttpRequest
                }

                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    itemObj: ItemVoucher?
                ) {
                    if (isDetached) {
                        return
                    }
                    isLoading = false
                    if (!isSuccess) {
                        activity?.runOnUiThread {
                            showErrorNetwork(message)
                        }
                        return
                    }
                    if (itemObj == null) {
                        activity?.runOnUiThread {
                            showErrorNetwork(requireContext().resources.getString(R.string.msg_empty_data))
                        }
                        return
                    }
                    isLoading = false
                    activity?.runOnUiThread {
                        progressBar.visibility = View.GONE
                    }
                    this@DetailVoucherFragment.itemObj = itemObj
                    activity?.runOnUiThread {
                        try {
                            setData()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })
    }

    fun setData() {
        if (itemObj == null) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val imageHeight = ScreenSize(requireActivity()).width * 5 / 8
        val lp = imageThumbnail.layoutParams as LinearLayout.LayoutParams
        lp.height = imageHeight
        MyApplication.getInstance().loadImage(requireContext(), imageThumbnail, itemObj!!.image)
        textTitle.text = itemObj!!.name ?: ""

        textDescription.text = TextUtil.getHtmlFormat(itemObj!!.description).toString().trim()
//        TextUtil.setHtmlTextView(itemObj!!.description?.trim(), textDescription)
        textExprireDate.text = itemObj!!.expiredDate ?: ""
        textDiscountPrice.text = getString(R.string.tittle_promotion_price) + TextUtil.formatMoney(
            itemObj!!.discountPrice ?: 0L
        )
        textPrice.text = TextUtil.formatMoney(itemObj!!.price ?: 0L)

        val firstPrice = textPrice.text.toString()
        val firstPriceSpan = SpannableString(firstPrice)
        firstPriceSpan.setSpan(
            StrikethroughSpan(),
            0,
            firstPrice.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        textPrice.text = firstPriceSpan

        if (itemObj!!.discountPrice == itemObj!!.price) {
            textPrice.visibility = View.GONE
            textPercent.visibility = View.GONE
        } else {
            textPrice.visibility = View.VISIBLE
            textPercent.visibility = View.VISIBLE
            textPercent.text = itemObj!!.percent
        }
        if (MyApplication.getInstance().isEmpty(itemObj!!.brandName)) {
            layoutBrand?.visibility = View.GONE
        }
        MyApplication.getInstance()
            .loadImage(requireContext(), imageBrand, itemObj!!.brandLogo, true)
        textBrandName.text = itemObj!!.brandName ?: ""
        textBrandAbout.text = itemObj!!.brandDescription ?: ""
//        TextUtil.setHtmlTextView(itemObj!!.brandAbout, textBrandAbout)

//        TextUtil.setHtmlTextView(itemObj!!.voucherValue, textVoucherValue)
//        TextUtil.setHtmlTextView(itemObj!!.guide, textGuide)
//        TextUtil.setHtmlTextView(itemObj!!.placeUse, textPlaceUse)
        TextUtil.setHtmlTextView(itemObj!!.voucherReceive, textVoucherValue)
//        textVoucherValue.text = itemObj!!.voucherReceive ?: ""
        TextUtil.setHtmlTextView(itemObj!!.voucherTutorial ?: "", textGuide)
//        textGuide.text = itemObj!!.voucherTutorial ?: ""
        textPlaceUse.text = itemObj!!.voucherLocation ?: ""

        if (itemUser != null) {
            isMyVoucher = itemUser!!.id == itemObj!!.userId
        } else {
            isMyVoucher = false
        }
        if (isMyVoucher || itemObj!!.userId == null || itemObj!!.userId!! <= 0) {
            layoutBoxUserInfo.visibility = View.GONE
        } else {
            layoutBoxUserInfo.visibility = View.VISIBLE
            textUserName.text = itemObj!!.userName ?: ""
            MyApplication.getInstance().loadImageCircle(
                requireContext(),
                imageAvatar,
                itemObj!!.userAvatar,
                R.drawable.ic_avatar
            )
        }

        textNotify.visibility = View.GONE
        layoutContent.visibility = View.VISIBLE
        progressBar.visibility = View.GONE

        checkShowBottomButton()
    }

    fun getRelateData() {
        val requestParams = RequestParams()
        requestParams.put("id", "${itemObj!!.id}")
        requestParams.put("unique_id", "${itemObj!!.uniqueId}")
        requestParams.put("area_type", "${areaType}")
        ServiceManager.getVoucherList(
            requireActivity(),
            ServiceUtilTT.validAPIDGTH(context, Constant.API_VOUCHER_DETAIL_RELATE),
            requestParams,
            object : ServiceManager.OnVoucherRequestListener {
                override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                    this@DetailVoucherFragment.myHttpRequestRelate = myHttpRequest
                }

                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    itemList: ArrayList<ItemVoucher>?
                ) {
                    if (isDetached) {
                        return
                    }
                    if (!isSuccess || itemList == null || itemList.size == 0) {
                        activity?.runOnUiThread {
                            textBoxRelateTitle?.visibility = View.GONE
                            recyclerViewRelate?.visibility = View.GONE
                            progressBarRelate?.visibility = View.GONE
                        }
                        return
                    }
                    try {
                        setRelateData(itemList)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
    }

    fun setRelateData(itemList: ArrayList<ItemVoucher>) {
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        val numberColumn = 1
        val itemDecoration = SpacesItemDecoration(spacePx, numberColumn, true)
        val layoutManager =
            StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.HORIZONTAL)
        val adapter = VoucherAdapter(
            requireActivity(),
            itemList,
            VoucherAdapter.STYLE_HORIZONTAL_STRING,
            numberColumn
        )
        activity?.runOnUiThread {
            adapter.showLayoutUser = true
            textBoxRelateTitle.visibility = View.VISIBLE
            recyclerViewRelate.visibility = View.VISIBLE
            progressBarRelate.visibility = View.GONE
            if (recyclerViewRelate.itemDecorationCount == 0) {
                recyclerViewRelate.addItemDecoration(itemDecoration)
            }
            recyclerViewRelate.layoutManager = layoutManager
            recyclerViewRelate.adapter = adapter
            recyclerViewRelate.scheduleLayoutAnimation()

            adapter.onItemClickListener = object : VoucherAdapter.OnItemClickListener {
                override fun onClick(itemObj: ItemVoucher, position: Int) {
                    this@DetailVoucherFragment.itemObj = itemObj
                    layoutContent.visibility = View.GONE
                    progressBar.visibility = View.VISIBLE
                    scrollContent.scrollTo(0, 0)
                    getData()
                }
            }
        }
    }

    fun showSellLayout() {
        val dialog = MyDialog(requireContext())
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = WindowManager.LayoutParams()
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimationLeftRight
        dialog.window?.setAttributes(lp)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_sell_voucher)
        val screenSize = ScreenSize(requireActivity())
        dialog.window?.setLayout(screenSize.width * 9 / 10, WindowManager.LayoutParams.WRAP_CONTENT)
        val editText: EditText = dialog.findViewById(R.id.editText)
        val textMessage: TextView = dialog.findViewById(R.id.textMessage)
        val buttonSend: TextView = dialog.findViewById(R.id.buttonSend)
        val layoutImage: RelativeLayout = dialog.findViewById(R.id.layoutImage)
        val imageThumbnail: ImageView = dialog.findViewById(R.id.imageThumbnail)
        val imageBrandLogo: ImageView = dialog.findViewById(R.id.imageBrandLogo)
        val textPrice: TextView = dialog.findViewById(R.id.textPrice)
        val textDiscountPrice: TextView = dialog.findViewById(R.id.textDiscountPrice)
        val textName: TextView = dialog.findViewById(R.id.textName)
        val textPercent: TextView = dialog.findViewById(R.id.textPercent)
        val textExprireDate: TextView = dialog.findViewById(R.id.textExprireDate)

        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
            screenSize.width
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        val whiteDistance = spacePx * 2
        val imageWidth = (screenWidth - whiteDistance) * 3 / 10
        val layoutParams = layoutImage.layoutParams as LinearLayout.LayoutParams
        layoutParams.width = imageWidth
        layoutParams.height = imageWidth

        val message = String.format(Locale.ENGLISH, getString(R.string.guide_sell), itemObj!!.name)
        textMessage.text = message

        textName.text = itemObj!!.name
        MyApplication.getInstance().loadImage(activity, imageThumbnail, itemObj!!.image)
        MyApplication.getInstance().loadImage(activity, imageBrandLogo, itemObj!!.brandLogo)
        textExprireDate.text = itemObj!!.expiredDate
        textDiscountPrice.text = TextUtil.formatMoney(itemObj!!.discountPrice ?: 0L)
        if (itemObj!!.discountPrice == itemObj!!.price) {
            textPrice.visibility = View.GONE
            textPercent.visibility = View.GONE
        } else {
            textPrice.visibility = View.VISIBLE
            textPrice.text = TextUtil.formatMoney(itemObj!!.price ?: 0L)
            /*textPercent.visibility = View.VISIBLE
            textPercent.text = itemObj!!.percent*/
            textPercent.visibility = View.GONE
            val firstPrice = textPrice.text.toString()
            val firstPriceSpan = SpannableString(firstPrice)
            firstPriceSpan.setSpan(
                StrikethroughSpan(),
                0,
                firstPrice.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            textPrice.text = firstPriceSpan
        }
        buttonSend.setOnClickListener {
            sellVoucher(editText, dialog)
        }
        try {
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun sellVoucher(editText: EditText, dialog: MyDialog) {
        val price = editText.text.toString().trim()
        if (price.isEmpty()) {
            GeneralUtils.showToast(activity, R.string.msg_price_empty)
            return
        }
        loadingDialog = GeneralUtils.showLoadingDialog(activity, true, null)
        val requestParams = RequestParams()
        requestParams.put("voucherid", "${itemObj!!.id}")
        requestParams.put("unique_id", "${itemObj!!.uniqueId}")
        requestParams.put("price", price)
        requestParams.put("unique_id", "${itemObj!!.uniqueId}")
        ServiceManager.requestBase(
            requireActivity(),
            ServiceUtilTT.validAPIDGTH(context, Constant.API_VOUCHER_SELL),
            requestParams,
            object : ServiceManager.OnBaseRequestListener {
                override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                }

                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    responseString: String?
                ) {
                    if (isDetached) {
                        return
                    }
                    activity?.runOnUiThread {
                        loadingDialog?.dismiss()
                        GeneralUtils.showDialog(activity, message)
                        if (isSuccess) {
                            dialog.dismiss()
                            buttonUse.visibility = View.GONE
                            buttonDonate.visibility = View.GONE
                            buttonSell.visibility = View.VISIBLE
                            buttonSell.text =
                                requireContext().getString(R.string.button_cancel_sell)
                            buttonSell.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.black_tt
                                )
                            )
                            buttonSell.background = ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.bg_button_voucher_white_yellow
                            )
                            buttonSell.isEnabled = true
                            itemObj?.statusType = Constant.VOUCHER_STATUS_SELLING
                        }
                    }
                }

            },
            true
        )
    }

    fun cancelSellVoucher() {
        loadingDialog = GeneralUtils.showLoadingDialog(activity, true, null)
        val requestParams = RequestParams()
        requestParams.put("voucherid", "${itemObj!!.id}")
        requestParams.put("unique_id", "${itemObj!!.uniqueId}")
        ServiceManager.requestBase(
            requireActivity(),
            ServiceUtilTT.validAPIDGTH(context, Constant.API_VOUCHER_CANCEL_SELL),
            requestParams,
            object : ServiceManager.OnBaseRequestListener {
                override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                }

                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    responseString: String?
                ) {
                    if (isDetached) {
                        return
                    }
                    activity?.runOnUiThread {
                        loadingDialog?.dismiss()
                        GeneralUtils.showDialog(activity, message)
                        if (isSuccess) {
                            buttonUse.visibility = View.VISIBLE
                            buttonDonate.visibility = View.VISIBLE
                            buttonSell.visibility = View.VISIBLE
                            buttonSell.text = requireContext().getString(R.string.button_sell_now)
                            buttonSell.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.white_tt
                                )
                            )
                            buttonSell.background = ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.bg_button_voucher_red
                            )
                            buttonSell.isEnabled = true
                            itemObj?.statusType = Constant.VOUCHER_STATUS_RECEIVED
                        }
                    }
                }

            },
            true
        )
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        buttonBack.setOnClickListener {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).goBack(false)
            } else {
                activity?.onBackPressed()
            }
        }
        buttonUse.setOnClickListener {
//            val bundle = Bundle()
//            bundle.putParcelable(Constant.DATA, itemObj)
//            if (activity is VoucherActivity) {
//                (activity as? VoucherActivity)?.showUseVoucherFragment(bundle)
//            } else {
//                onItemClickUserVoucherListener?.onClickUseVoucher(itemObj, 1)
//            }
            GeneralUtils.openMessenger(activity)
        }
        buttonDonate.setOnClickListener {
            val fragment = DonateVoucherFragment()
            donateVoucherFragment = fragment
            val bundle = Bundle()
            bundle.putParcelable(Constant.DATA, itemObj)
            fragment.onResultListener = object : OnResultListener {
                override fun onResult(isSuccess: Boolean, msg: String?) {
                    if (isSuccess) {
//                        hideBottomButton()
                        removeId = itemObj!!.id
                        activity?.runOnUiThread {
//                            (activity as BaseActivity).showToast("$msg")
                            buttonUse.visibility = View.GONE
                            buttonDonate.visibility = View.GONE
                            buttonSell.visibility = View.VISIBLE
                            buttonSell.text = requireContext().getString(R.string.button_waiting)
                            buttonSell.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.black_tt
                                )
                            )
                            buttonSell.background = ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.bg_button_voucher_white
                            )
                            buttonSell.isEnabled = false
                        }
                    }
                }
            }
            GeneralUtils.showBottomSheetDialog(
                requireActivity().supportFragmentManager,
                fragment,
                bundle
            )
        }
        buttonSell.setOnClickListener {
            when (itemObj!!.statusType) {
                Constant.VOUCHER_STATUS_USED -> {
                    val bundle = Bundle()
                    bundle.putParcelable(Constant.DATA, itemObj)
                    (activity as? VoucherActivity)?.showUseVoucherFragment(bundle)
                }
                Constant.VOUCHER_STATUS_RECEIVED, Constant.VOUCHER_STATUS_NEW -> {
                    showSellLayout()
                }
                Constant.VOUCHER_STATUS_SELLING -> {
                    GeneralUtils.showDialog(activity,
                        true,
                        R.string.notification,
                        R.string.msg_cancel_sell,
                        R.string.close,
                        R.string.button_cancel_sell,
                        object : OnDialogButtonListener {
                            override fun onLeftButtonClick() {
                            }

                            override fun onRightButtonClick() {
                                cancelSellVoucher()
                            }
                        })
                }
            }
        }
        buttonViewMore.setOnClickListener {
            if (itemObj!!.userId == null) {
                GeneralUtils.showToast(activity, R.string.msg_empty_data)
                return@setOnClickListener
            }
            val bundle = Bundle()
            bundle.putInt(Constant.USER_ID, itemObj!!.userId!!)
            bundle.putString(Constant.USERNAME, itemObj!!.userName)
            (activity as? VoucherActivity)?.showUserVoucherFragment(bundle)
        }
        buttonBrandViewMore.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(Constant.ID, itemObj!!.brandId ?: "-1")
            bundle.putString(Constant.NAME, itemObj!!.brandName)
            (activity as? VoucherActivity)?.showDetailBrandFragment(bundle)
        }
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        myHttpRequestRelate?.cancel()
        if (removeId == null) {
            onResultListener?.onResult(true, "$removeId")
        } else {
            onResultListener?.onResult(false, "$removeId")
        }
        super.onDestroyView()
    }

    var donateVoucherFragment: DonateVoucherFragment? = null
    fun dismissDonateVoucherFragment() {
        donateVoucherFragment?.dismiss()
    }
}
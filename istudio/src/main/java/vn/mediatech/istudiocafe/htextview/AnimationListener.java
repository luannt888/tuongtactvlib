package vn.mediatech.istudiocafe.htextview;

public interface AnimationListener {
    void onAnimationEnd(HTextView hTextView);
}

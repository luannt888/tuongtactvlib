package vn.mediatech.istudiocafe.util;

import android.text.Html;
import android.text.Spanned;
import android.webkit.WebView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

import vn.mediatech.istudiocafe.app.Loggers;

public class TextUtil {
    public static Spanned getHtmlFormat(String text) {
        if (text == null) {
            text = "";
        }
        Spanned textSpanned;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            textSpanned = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            textSpanned = Html.fromHtml(text);
        }
        return textSpanned;
    }

    public static void setHtmlTextView(String text, TextView textView) {
        if (textView == null) {
            return;
        }
        textView.setText(getHtmlFormat(text + ""));
    }

    public static String getStringHtmlColor(String mString, String hexColor) {
        if (hexColor == null || mString == null || !hexColor.startsWith("#")) {
            return mString;
        }
        String string = "<span style=\"color: " + hexColor + "\">" + mString + "</span>";
        Loggers.e("HTML Color", string);
        return string;
    }

    public static String getStringHtmlStype(String mString, String hexColor, boolean isBold, boolean isUnderline) {
        if (mString == null) {
            return mString;
        }
        String string = mString;
        if (isBold) {
            string = "<b>" + string + "</b>";
        }
        if (isUnderline) {
            string = "<u>" + string + "</u>";
        }
        if (hexColor != null && hexColor.startsWith("#")) {
            string = "<span style=\"color: " + hexColor + "\">" + string + "</span>";
        }
        Loggers.e("HTML Color", string);
        return string;
    }

    public static void setWebviewContent(String message, WebView webView) {
        webView.loadDataWithBaseURL(null, message, "text/html", "utf-8", null);
    }

    public static String formatMoney(int number) {
//        NumberFormat format = NumberFormat.getCurrencyInstance();
//        format.setMaximumFractionDigits(0);
//        format.setCurrency(Currency.getInstance("VND"));
//        return format.format(number);
        NumberFormat formatter = new DecimalFormat("#,###");
        String formattedNumber = formatter.format(number);
        return  formattedNumber + " Đ";
    }

    public static String formatMoney(long number) {
//        NumberFormat format = NumberFormat.getCurrencyInstance();
//        format.setMaximumFractionDigits(0);
//        format.setCurrency(Currency.getInstance("VND"));
//        return format.format(number);
        NumberFormat formatter = new DecimalFormat("#,###");
        String formattedNumber = formatter.format(number);
        return  formattedNumber + " Đ";
    }

    public static String getCurrentDateString() {
        Date currentDate = new Date();
        String dateCurrent = new SimpleDateFormat("dd/MM/yyyy' 'HH:mm", Locale.ENGLISH).format(currentDate);
        return dateCurrent;
    }
}
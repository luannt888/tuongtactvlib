package vn.mediatech.istudiocafe.util

import android.app.Activity
import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.Headers
import okhttp3.Headers.Companion.toHeaders
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.service.fcm.MyFirebaseMessagingManager
import vn.mediatech.istudiocafe.voucher.ItemBaseCategory
import vn.mediatech.istudiocafe.voucher.ItemBrand
import vn.mediatech.istudiocafe.voucher.ItemVoucher
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemChat
import java.util.*
import kotlin.collections.ArrayList

class ServiceManager {
    companion object {
        fun getBaseCategoryVoucher(activity: Activity, api: String, listener: OnBaseCategoryRequestListener?) {
            val myHttpRequest = MyHttpRequest(activity)
            val requestParams = RequestParams()
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser != null) {
                requestParams.put("userid", "${itemUser.id}")
                requestParams.put("access_token", itemUser.accessToken)
            }
            listener?.onPreRequest(myHttpRequest)
            myHttpRequest.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    listener?.onResponse(false, activity.getString(R.string.msg_network_error), null)
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    if (jsonObject == null) {
                        listener?.onResponse(false, activity.getString(R.string.msg_empty_data), null)
                        return
                    }
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    if (errorCode != Constant.SUCCESS) {
                        listener?.onResponse(false, message, null)
                        return
                    }
                    val resultStr = JsonParser.getString(jsonObject, Constant.RESULT)
                    val gson = Gson()
                    val gsonType = object : TypeToken<ArrayList<ItemBaseCategory>>() {}.getType()
                    var list: ArrayList<ItemBaseCategory> = ArrayList()
                    try {
                        list = gson.fromJson(resultStr, gsonType)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    listener?.onResponse(true, message, list)
                }
            })
        }

        fun getVoucherList(activity: Activity, api: String, requestParams: RequestParams?, listener: OnVoucherRequestListener?) {
            val myHttpRequest = MyHttpRequest(activity)
            var params = requestParams
            if (params == null) {
                params = RequestParams()
            }
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser != null) {
//                params.put("userid", "${itemUser.id}")
                params.put("access_token", itemUser.accessToken)
            }
            listener?.onPreRequest(myHttpRequest)
            myHttpRequest.request(false, api, params, object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    listener?.onResponse(false, activity.getString(R.string.msg_network_error), null)
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    if (jsonObject == null) {
                        listener?.onResponse(false, activity.getString(R.string.msg_empty_data), null)
                        return
                    }
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    if (errorCode != Constant.SUCCESS) {
                        listener?.onResponse(false, message, null)
                        return
                    }
                    val resultStr = JsonParser.getString(jsonObject, Constant.RESULT)
                    val gson = Gson()
                    val gsonType = object : TypeToken<ArrayList<ItemVoucher>>() {}.getType()
                    var list: ArrayList<ItemVoucher> = ArrayList()
                    try {
                        list = gson.fromJson(resultStr, gsonType)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    listener?.onResponse(true, message, list)
                }
            })
        }

        fun getVoucherDetail(activity: Activity, api: String, requestParams: RequestParams?, listener: OnVoucherDetailRequestListener?) {
            val myHttpRequest = MyHttpRequest(activity)
            var params = requestParams
            if (params == null) {
                params = RequestParams()
            }
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser != null) {
//                params.put("userid", "${itemUser.id}")
                params.put("access_token", itemUser.accessToken)
            }
            listener?.onPreRequest(myHttpRequest)
            myHttpRequest.request(false, api, params, object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    listener?.onResponse(false, activity.getString(R.string.msg_network_error), null)
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    if (jsonObject == null) {
                        listener?.onResponse(false, activity.getString(R.string.msg_empty_data), null)
                        return
                    }
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    if (errorCode != Constant.SUCCESS) {
                        listener?.onResponse(false, message, null)
                        return
                    }
                    val resultStr = JsonParser.getString(jsonObject, Constant.RESULT)
                    val gson = Gson()
                    var itemObj: ItemVoucher? = null
                    try {
                        itemObj = gson.fromJson(resultStr, ItemVoucher::class.java)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    listener?.onResponse(true, message, itemObj)
                }
            })
        }

        fun requestBase(activity: Activity, api: String, requestParams: RequestParams?, listener: OnBaseRequestListener?, isPostMethod: Boolean = true) {
            val myHttpRequest = MyHttpRequest(activity)
            var params = requestParams
            if (params == null) {
                params = RequestParams()
            }
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser != null) {
                params.put("user_id", "${itemUser.id}")
                params.put("access_token", itemUser.accessToken)
            }
            listener?.onPreRequest(myHttpRequest)
            myHttpRequest.request(isPostMethod, api, params, object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    listener?.onResponse(false, activity.getString(R.string.msg_network_error), null)
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    if (jsonObject == null) {
                        listener?.onResponse(false, activity.getString(R.string.msg_empty_data), null)
                        return
                    }
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    listener?.onResponse(errorCode == Constant.SUCCESS, message, responseString)
                }
            })
        }

        fun getChatList(activity: Activity, requestParams: RequestParams?, listener: OnChatListRequestListener?) {
            val myHttpRequest = MyHttpRequest(activity)
            var params = requestParams
            if (params == null) {
                params = RequestParams()
            }
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser != null) {
                params.put("user_id", "${itemUser.id}")
                params.put("access_token", itemUser.accessToken)
            }
            listener?.onPreRequest(myHttpRequest)
//            myHttpRequest.request(false, ServiceUtilTT.validAPIDGTH(activity,Constant.API_VOUCHER_CHAT_LIST), params, object : MyHttpRequest.ResponseListener {
            myHttpRequest.request(false, ServiceUtilTT.validAPITT(activity,Constant.API_VOUCHER_CHAT_LIST), params, object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    listener?.onResponse(false, activity.getString(R.string.msg_network_error), null)
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    if (jsonObject == null) {
                        listener?.onResponse(false, activity.getString(R.string.msg_empty_data), null)
                        return
                    }
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                    val message = JsonParser.getString(jsonObject, "message")
                    if (errorCode != Constant.SUCCESS) {
                        listener?.onResponse(false, message, null)
                        return
                    }
//                    val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
//                    val status = JsonParser.getInt(resultObj, "status")
//                    val guide = JsonParser.getString(resultObj, "guide")
//                    val dataStr = JsonParser.getString(resultObj, "data")
                    val status = JsonParser.getInt(jsonObject, "status")
                    val guide = JsonParser.getString(jsonObject, "guide")
                    val dataStr = JsonParser.getString(jsonObject, Constant.RESULT)
                    val gson = Gson()
                    val gsonType = object : TypeToken<ArrayList<ItemChat>>() {}.getType()
                    var list: ArrayList<ItemChat> = ArrayList()
                    try {
                        list = gson.fromJson(dataStr, gsonType)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    listener?.onResponse(true, message, list, status, guide)
                }
            })
        }

        fun getBrandDetail(activity: Activity, requestParams: RequestParams?, listener: OnBrandDetailRequestListener?) {
            val myHttpRequest = MyHttpRequest(activity)
            /*var params = requestParams
            if (params == null) {
                params = RequestParams()
            }*/
            listener?.onPreRequest(myHttpRequest)
            myHttpRequest.request(false, ServiceUtilTT.validAPIDGTH(activity,Constant.API_BRAND_DETAIL), requestParams, object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    listener?.onResponse(false, activity.getString(R.string.msg_network_error), null)
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    if (jsonObject == null) {
                        listener?.onResponse(false, activity.getString(R.string.msg_empty_data), null)
                        return
                    }
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    if (errorCode != Constant.SUCCESS) {
                        listener?.onResponse(false, message, null)
                        return
                    }
                    val resultStr = JsonParser.getString(jsonObject, Constant.RESULT)
                    val gson = Gson()
                    var itemObj: ItemBrand? = null
                    try {
                        itemObj = gson.fromJson(resultStr, ItemBrand::class.java)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    listener?.onResponse(true, message, itemObj)
                }
            })
        }

        fun setNeedUpdateFcmTokenForUser(activity: Activity, needUpdate: Boolean) {
            val prefs = activity.getSharedPreferences(MyFirebaseMessagingManager.FCM, Context.MODE_PRIVATE)
            val editor = prefs.edit()
            editor.putBoolean(MyFirebaseMessagingManager.PROPERTY_NEED_UPDATE_TOKEN, needUpdate)
            editor.apply()
        }

        fun updateFcmTokenUser(activity: Activity) {
            val itemUser = MyApplication.getInstance().dataManager.itemUser ?: return
            val prefs = activity.getSharedPreferences(MyFirebaseMessagingManager.FCM, Context.MODE_PRIVATE)
            val needUpdate = prefs.getBoolean(MyFirebaseMessagingManager.PROPERTY_NEED_UPDATE_TOKEN, true)
            val deviceId = prefs.getString(MyFirebaseMessagingManager.PROPERTY_ID, null)
            val fcmToken = prefs.getString(MyFirebaseMessagingManager.PROPERTY_FCM_TOKEN, null)
            if (!needUpdate || deviceId.isNullOrEmpty() || fcmToken.isNullOrEmpty()) {
                return
            }
            val myHttpRequest = MyHttpRequest(activity)
            val requestParams = RequestParams()
            requestParams.put("access_token", itemUser.accessToken)
            requestParams.put("device_id", "$deviceId")
            val headers:HashMap<String, String> = HashMap()
            headers.put("Authorization","Bearer "+ itemUser.accessToken)

            myHttpRequest.request(true, ServiceUtilTT.validAPITT(activity,Constant.API_UPDATE_FCM_TOKEN_USER), requestParams,
                null, object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                    if (errorCode == Constant.SUCCESS) {
                        try {
                            val editor = prefs.edit()
                            editor.putBoolean(MyFirebaseMessagingManager.PROPERTY_NEED_UPDATE_TOKEN, false)
                            editor.apply()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })
        }

        fun getNewsList(activity: Activity, api: String, requestParams: RequestParams?, listener: OnNewsRequestListener?) {
            val myHttpRequest = MyHttpRequest(activity)
            listener?.onPreRequest(myHttpRequest)
            myHttpRequest.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    listener?.onResponse(false, null, activity.getString(R.string.msg_network_error), null)
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    if (jsonObject == null) {
                        listener?.onResponse(false, null, activity.getString(R.string.msg_empty_data), null)
                        return
                    }
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    if (errorCode != Constant.SUCCESS) {
                        listener?.onResponse(false, null, message, null)
                        return
                    }
                    val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
                    val dataArr = JsonParser.getJsonArray(resultObj, Constant.DATA)
                    val list: ArrayList<ItemNews>? = JsonParser.parseItemNewsList(activity, dataArr)
                    listener?.onResponse(true, errorCode, message, list)
                }
            })
        }
    }

    interface OnBaseRequestListener {
        fun onPreRequest(myHttpRequest: MyHttpRequest?)
        fun onResponse(isSuccess: Boolean, message: String?, responseString: String? = null)
    }

    interface OnBaseCategoryRequestListener {
        fun onPreRequest(myHttpRequest: MyHttpRequest?)
        fun onResponse(isSuccess: Boolean, message: String?, itemList: ArrayList<ItemBaseCategory>?)
    }

    interface OnVoucherRequestListener {
        fun onPreRequest(myHttpRequest: MyHttpRequest?)
        fun onResponse(isSuccess: Boolean, message: String?, itemList: ArrayList<ItemVoucher>?)
    }

    interface OnVoucherDetailRequestListener {
        fun onPreRequest(myHttpRequest: MyHttpRequest?)
        fun onResponse(isSuccess: Boolean, message: String?, itemObj: ItemVoucher?)
    }

    interface OnBrandDetailRequestListener {
        fun onPreRequest(myHttpRequest: MyHttpRequest?)
        fun onResponse(isSuccess: Boolean, message: String?, itemObj: ItemBrand?)
    }

    interface OnChatListRequestListener {
        fun onPreRequest(myHttpRequest: MyHttpRequest?)
        fun onResponse(isSuccess: Boolean, message: String?, itemList: ArrayList<ItemChat>?, status: Int? = null, guide: String? = null)
    }

    interface OnNewsRequestListener {
        fun onPreRequest(myHttpRequest: MyHttpRequest?)
        fun onResponse(isSuccess: Boolean, status: Int?, message: String?, itemList: ArrayList<ItemNews>?)
    }
}
package vn.mediatech.istudiocafe.util;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import vn.mediatech.istudiocafe.app.Constant;

public class SpacesItemDecorationGridMultipleViewType extends RecyclerView.ItemDecoration {
    private int space;
    private int spanCount;
    private boolean isHorizoltalListview;

    public SpacesItemDecorationGridMultipleViewType(int space, int numberColumn,
                                                    boolean isHorizoltalListview) {
        this.space = space;
        this.spanCount = numberColumn;
        this.isHorizoltalListview = isHorizoltalListview;
        if (spanCount <= 0) {
            spanCount = 1;
        }
    }

    public SpacesItemDecorationGridMultipleViewType(int space, int numberColumn) {
        this.space = space;
        this.spanCount = numberColumn;
        this.isHorizoltalListview = false;
        if (spanCount <= 0) {
            spanCount = 1;
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
//        int column = position % spanCount;
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        int viewType = -1;
        int spanIndex = -1;
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            viewType = layoutManager.getItemViewType(view);
            StaggeredGridLayoutManager.LayoutParams lp =
                    (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            spanIndex = lp.getSpanIndex();
        }
//        Loggers.e("getItemOffsetsGrid_" + spanCount, "position = " + position + " _ column = "
//        + column + " _ viewType = " + viewType + " _ spanIndex = " + spanIndex);
        outRect.bottom = space;
//        Loggers.e("getItemOffsetsGrid_" + position, "position = " + position + " _ viewType =" +
//                " " + viewType + " _ spanIndex = " + spanIndex);
        if (viewType == Constant.STYLE_GRIDVIEW || viewType == Constant.STYLE_PRODUCT) {
            boolean isTopSpace = false;
//            Loggers.e("getItemOffsetsGrid_" + position, "position = " + position + " _ viewType =" +
//                    " " + viewType + " _ spanIndex = " + spanIndex);
            if (spanIndex == 0) {
                outRect.left = space;
                outRect.right = space;
                if (position == 0 || position == 1) {
                    isTopSpace = true;
                }
            } else {
                outRect.left = 0;
                outRect.right = space;
                if (position == 1 || position == 2) {
                    isTopSpace = true;
                }
            }
            if(isTopSpace) {
                outRect.top = space;
            }
        } else {
            outRect.left = space;
            outRect.right = space;
            if (position == 0) {
                outRect.top = space;
            }
        }
    }
}
package vn.mediatech.istudiocafe.util

import android.app.Activity
import android.app.Dialog
import android.content.*
import android.content.res.Configuration
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.method.ScrollingMovementMethod
import android.util.DisplayMetrics
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ShareCompat
import androidx.core.view.marginBottom
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_main_tt.*
import kotlinx.android.synthetic.main.layout_loading_tt.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.DetailNewsActivity
import vn.mediatech.istudiocafe.activity.DetailVideoActivity
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.app.MyApplication.OnLoadImageBitmapListener
import vn.mediatech.istudiocafe.customView.TypeWriterTextView
import vn.mediatech.istudiocafe.fragment.MyDialogFragment
import vn.mediatech.istudiocafe.listener.OnCancelListener
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.listener.OnResultListener
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.voucher.ItemVoucher
import vn.mediatech.istudiocafe.voucher.OnDialogButtonVoucherListener
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class GeneralUtils {
    companion object {
        fun isEmpty(data: String?): Boolean {
            return data == null || data.trim { it <= ' ' }.isEmpty()
        }

        fun coppyToClipboard(activity: Activity?, str: String) {
            if (activity == null) {
                return
            }
            val clipboard =
                activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("label", str)
            clipboard.setPrimaryClip(clip)
        }

        fun isNetworkConnect(context: Context?): Boolean {
            if (context == null) {
                return false
            }
            val connMgr = context.applicationContext
                .getSystemService(Activity.CONNECTIVITY_SERVICE) as ConnectivityManager
                ?: return true
            val networkInfo = connMgr.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }

        fun loadImageBitmapListener(
            context: Context?, url: String?,
            onLoadImageBitmapListener: OnLoadImageBitmapListener?
        ) {
            if (context == null || url.isNullOrEmpty()) {
                onLoadImageBitmapListener?.onResourceReady(null)
                return
            }
            val options = RequestOptions()
                .priority(Priority.HIGH)
            Glide.with(context!!).asBitmap().load(url).apply(options)
                .into(object : CustomTarget<Bitmap?>() {

                    override fun onLoadCleared(placeholder: Drawable?) {
                        onLoadImageBitmapListener?.onResourceReady(null)
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        onLoadImageBitmapListener?.onResourceReady(null)
                    }

                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap?>?
                    ) {
                        onLoadImageBitmapListener?.onResourceReady(resource)
                    }
                })
        }

        fun loadImageCircle(
            context: Context?, imageView: ImageView?, url: String?,
            placeholderDrawableId: Int
        ) {
            if (context == null || imageView == null || url.isNullOrEmpty()) {
                return
            }
            val options = if (placeholderDrawableId <= 0) {
                RequestOptions().transform(CircleCrop())
                    .priority(Priority.NORMAL)
            } else {
                RequestOptions().transform(CircleCrop())
                    .placeholder(placeholderDrawableId)
                    .error(placeholderDrawableId)
                    .priority(Priority.NORMAL)
            }
            Glide.with(context).load(url).apply(options).into(imageView!!)
        }

        fun showKeyboardFocus(activity: Activity?, editText: EditText?) {
            if (activity == null || editText == null) {
                return
            }
            editText?.requestFocus()
            val inputMethodManager =
                activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
        }

        fun hideKeyboard(context: Context?) {
            if (context == null) {
                return
            }
            if (isShowKeyboard(context)) {
                val imm =
                    context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
            }
        }

        fun loadImage(context: Context?, imageView: ImageView?, url: String?) {
            if (imageView == null || isEmpty(url) || context == null) {
                return
            }
            val options: RequestOptions = RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
            Glide.with(context).load(url).apply(options).into(imageView)
        }

        fun loadImagefitImageview(
            activity: Activity?,
            url: String?,
            imageView: ImageView?
        ) {
            if (url == null || url.trim { it <= ' ' } == "" || imageView == null || activity == null) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
            Glide.with(activity).asBitmap().load(url).apply(options).into(
                object : CustomTarget<Bitmap?>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap?>?
                    ) {
                        val lp: ViewGroup.LayoutParams = imageView.layoutParams
                        lp.width = imageView.measuredWidth
                        lp.height = lp.width * resource.height / resource.width
                        imageView.layoutParams = lp
                        imageView.setImageBitmap(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {

                    }
                }
            )
        }

        fun loadImagefitImageview(
            activity: Activity?,
            drawableRes: Int?,
            imageView: ImageView?
        ) {
            if (drawableRes == null || imageView == null || activity == null) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
            Glide.with(activity).asBitmap().load(drawableRes).apply(options)
                .into(object : CustomTarget<Bitmap?>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap?>?
                    ) {
                        val lp: ViewGroup.LayoutParams = imageView.layoutParams
                        lp.width = imageView.measuredWidth
                        lp.height = lp.width * resource.height / resource.width
                        imageView.layoutParams = lp
                        imageView.setImageBitmap(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {

                    }
                })
        }

        fun getDateTime(strDate: String?): Date {
//        val strDate = "2013-05-15T10:00:00-0700"
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
            val dateFormat2 = SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.ENGLISH)
            val date = try {
                dateFormat.parse(strDate)
            } catch (e: Exception) {
                try {
                    dateFormat2.parse(strDate)
                } catch (e: Exception) {
                    Date()
                }
            }
            return date
        }

        fun getTimeInMilliSeconds(strDate: String?): Long {
            try {
                if (strDate.isNullOrEmpty()) {
                    return -1L
                }
//        val strDate = "2013-05-15T10:00:00-0700"
                val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
                return dateFormat.parse(strDate).time
            } catch (e: Exception) {
                e.printStackTrace()
                return 0L
            }
        }

        fun getDateString(context: Context, strDate: String?): String {
            try {
                val date = getDateTime(strDate)
                val currentDate = Date()
                val dateStr = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(date)
                val dateCurrent = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(currentDate)
                return if (dateStr.equals(dateCurrent)) context.getString(R.string.today) else dateStr
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
        }

        fun getCurrentDateStringFormatServer(): String {
            val currentDate = Date()
            val dateCurrent =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).format(currentDate)
            return dateCurrent
        }

        fun getDateStringFormatServer(milliseconds: Long): String {
            val currentDate = Date(milliseconds)
            val dateCurrent =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).format(currentDate)
            return dateCurrent
        }

        fun getTimeString(strDate: String?): String {
            try {
                val date = getDateTime(strDate)
                return SimpleDateFormat("HH:mm", Locale.ENGLISH).format(date)
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
        }

        fun getTextHtml(str: String?): String {
            try {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    return Html.fromHtml(str, Html.FROM_HTML_MODE_LEGACY).toString()
                } else {
                    return Html.fromHtml(str).toString()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
        }

        fun moneyFormat(amount: String?): String {
            if (amount.isNullOrEmpty()) {
                return "0 Đ"
            }
            val formatter = DecimalFormat("###,###,###")
            return try {
                formatter.format(amount.toDouble()) + " Đ"
            } catch (e: Exception) {
                e.printStackTrace()
                "0 Đ"
            }
        }

        fun changeColorHTMLString(string: String?, hexColorString: String?): String {
            if (string.isNullOrEmpty()) {
                return ""
            }

//            val str = "&lt;font color='$hexColorString'&gt;" + string + "&lt;/font&gt;"
            val str = "<font color='$hexColorString'>" + string + "</font>"
            return str
        }

        fun getHtmlFormat(text: String?): Spanned? {
            val data = text ?: ""
            val textSpanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(data, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(data)
            }
            return textSpanned
        }

        fun setHtmlTextView(text: String?, textView: TextView?) {
            if (text.isNullOrEmpty() || textView == null) {
                return
            }
            textView.text = getHtmlFormat(text + "")
        }


        fun convertSecondsToMmSs(seconds: Long): String {
            val s = seconds % 60
            val m = seconds / 60 % 60
            return String.format("%02d:%02d", m, s)
        }

        fun convertDpToPixel(dp: Float, context: Context): Float {
            return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }

        fun isShowKeyboard(context: Context): Boolean {
            val imm by lazy { context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager }
            val windowHeightMethod =
                InputMethodManager::class.java.getMethod("getInputMethodWindowVisibleHeight")
            val height = windowHeightMethod.invoke(imm) as Int
            return height > 0
        }

        fun showDialogIncludeImage(
            activity: Activity?,
            cancelable: Boolean,
            cancelableOutside: Boolean,
            title: String?,
            message: String?,
            image: String?,
            video: String?,
            leftButtonTitle: String?,
            rightButtonTitle: String?,
            timeSkipSecond: Int?,
            onDialogButtonListener: OnDialogButtonListener?
        ): MyDialogFragment? {
            if (activity == null || (title == null && message == null && image == null && video == null)) {
                return null
            }
            val dialog = MyDialogFragment()
            val bundle = Bundle()
            bundle.putString(Constant.TITLTE, title)
            bundle.putString(Constant.MESSAGE, message)
            bundle.putString(Constant.VIDEO, video)
            bundle.putString(Constant.LEFT_BUTTON, leftButtonTitle)
            bundle.putString(Constant.RIGHT_BUTTON, rightButtonTitle)
            bundle.putBoolean(Constant.CANCELABLE, cancelable)
            bundle.putBoolean(Constant.CANCEL_TOUCH_OUTSITE, cancelableOutside)
            if (timeSkipSecond != null) {
                bundle.putInt(Constant.TIME_SKIP, timeSkipSecond)
            }

            dialog.onDialogButtonListener = onDialogButtonListener
            if (!image.isNullOrEmpty()) {
                loadImageBitmapListener(activity, image, object : OnLoadImageBitmapListener {
                    override fun onResourceReady(resource: Bitmap?) {
                        bundle.putParcelable(Constant.BITMAP, resource)
                        dialog.arguments = bundle
                        if (activity.isFinishing || activity.isDestroyed) {
                            return
                        }
                        activity.runOnUiThread {
                            dialog.show(
                                (activity as FragmentActivity).supportFragmentManager,
                                "DIALOG"
                            )
                        }
                    }
                })
                return dialog
            }

//            bundle.putString(Constant.IMAGE, image)
            dialog.arguments = bundle
            dialog.show((activity as FragmentActivity).supportFragmentManager, "DIALOG")
            return dialog
        }

        fun showDialogIncludeImage(
            activity: Activity?,
            cancelable: Boolean,
            cancelableOutside: Boolean,
            title: String?,
            message: String?,
            image: String?,
            leftButtonTitle: String?,
            rightButtonTitle: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ): Dialog? {
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return null
            }
            val dialog = Dialog(activity)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val lp = WindowManager.LayoutParams()
            //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER
            lp.windowAnimations = R.style.DialogAnimationLeftRight
            dialog.window?.setAttributes(lp)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(cancelable)
            dialog.setCanceledOnTouchOutside(cancelableOutside)
            dialog.setContentView(R.layout.layout_dialog_popup)
            val screenSize = ScreenSize(activity)
            dialog.window?.setLayout(
                screenSize.width * 9 / 10,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
            val imageView: ImageView = dialog.findViewById(R.id.imageView)
            val textTitle: TextView = dialog.findViewById(R.id.textTitle)
            val textMessage: TextView = dialog.findViewById(R.id.textMessage)
            val buttonLeft: TextView = dialog.findViewById(R.id.buttonLeft)
            val buttonRight: TextView = dialog.findViewById(R.id.buttonRight)
            val frameLayoutDialogtt: FrameLayout = dialog.findViewById(R.id.frameLayoutDialogtt)
            //        View viewDivider = dialog.findViewById(R.id.viewDivider);
            layoutRoot.clipToOutline = true
            layoutRoot.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View, outline: Outline) {
                    val cornerRadiusDP = 15f
                    outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
                }
            }
            if (!title.isNullOrEmpty()) {
                textTitle.text = title
                textTitle.visibility = View.VISIBLE
            }
            if (!image.isNullOrEmpty()) {
                imageView.visibility = View.VISIBLE
                loadImagefitImageview(activity, image, imageView)
            }

            if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
                setHtmlTextView(message, textMessage)
                textMessage.movementMethod = LinkMovementMethod.getInstance()
            }
            if (leftButtonTitle.isNullOrEmpty()) {
                buttonLeft.visibility = View.GONE
                //            viewDivider.setVisibility(View.GONE);
            } else {
                buttonLeft.text = leftButtonTitle
                buttonLeft.visibility = View.VISIBLE
            }
            if (rightButtonTitle.isNullOrEmpty()) {
                buttonRight.visibility = View.GONE
                //            viewDivider.setVisibility(View.GONE);
            } else {
                buttonRight.text = rightButtonTitle
                buttonRight.visibility = View.VISIBLE
            }
            buttonLeft.setOnClickListener {
                dialog.dismiss()
                if (onDialogButtonListener != null) {
                    onDialogButtonListener.onLeftButtonClick()
                }
            }
            buttonRight.setOnClickListener {
                dialog.dismiss()
                if (onDialogButtonListener != null) {
                    onDialogButtonListener.onRightButtonClick()
                }
            }
            try {
                dialog.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return dialog
        }

        fun showDialogImage(
            activity: Activity?,
            cancelable: Boolean,
            cancelableOutside: Boolean,
            image: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ): Dialog? {
            if (activity == null || activity.isFinishing || activity.isDestroyed || image.isNullOrEmpty()) {
                return null
            }
            var dialog: Dialog? = null
            loadImageBitmapListener(activity, image, object : OnLoadImageBitmapListener {
                override fun onResourceReady(bitmap: Bitmap?) {
                    if (bitmap == null) {
                        return
                    }
                    if (activity.isFinishing || activity.isDestroyed) {
                        return
                    }
                    activity.runOnUiThread {
                        dialog = Dialog(activity)
                        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        val layoutParams = WindowManager.LayoutParams()
                        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        layoutParams.gravity = Gravity.CENTER
                        layoutParams.windowAnimations = R.style.DialogAnimationLeftRight
                        dialog?.window?.setAttributes(layoutParams)
                        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog?.setCancelable(cancelable)
                        dialog?.setCanceledOnTouchOutside(cancelableOutside)
                        dialog?.setContentView(R.layout.layout_dialog_image)
                        val screenSize = ScreenSize(activity)
                        val width = screenSize.width * 19 / 20
                        dialog?.window?.setLayout(
                            width,
                            WindowManager.LayoutParams.WRAP_CONTENT
                        )
                        val layoutRoot: View = dialog!!.findViewById(R.id.layoutRoot)
                        val imageView: ImageView = dialog!!.findViewById(R.id.imageView)
                        val buttonClose: ImageView = dialog!!.findViewById(R.id.buttonClose)
                        val lp: ViewGroup.LayoutParams = imageView.layoutParams
                        val widthFr = width - layoutRoot.paddingStart - layoutRoot.paddingEnd
                        lp.width = widthFr
                        lp.height = lp.width * bitmap.height / bitmap.width
                        imageView.layoutParams = lp
                        imageView.setImageBitmap(bitmap)

                        imageView.setOnClickListener {
                            dialog?.dismiss()
                            if (onDialogButtonListener != null) {
                                onDialogButtonListener.onLeftButtonClick()
                            }
                        }
                        buttonClose.setOnClickListener {
                            dialog?.dismiss()
                            if (onDialogButtonListener != null) {
                                onDialogButtonListener.onRightButtonClick()
                            }
                        }
                        try {
                            dialog?.show()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })
            return dialog
        }

        fun showDialog(
            activity: Activity?,
            cancelable: Boolean,
            cancelableOutside: Boolean,
            title: String?,
            message: String?,
            leftButtonTitle: String?,
            rightButtonTitle: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ): Dialog? {
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return null
            }
            val dialog = Dialog(activity)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val lp = WindowManager.LayoutParams()
            //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER
            lp.windowAnimations = R.style.DialogAnimationLeftRight
            dialog.window?.setAttributes(lp)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(cancelable)
            dialog.setCanceledOnTouchOutside(cancelableOutside)
            dialog.setContentView(R.layout.layout_dialog_tt)
            val screenSize = ScreenSize(activity)
            dialog.window?.setLayout(
                screenSize.width * 9 / 10,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
            val textTitle: TextView = dialog.findViewById(R.id.textTitle)
            val textMessage: TextView = dialog.findViewById(R.id.textMessage)
            val buttonLeft: TextView = dialog.findViewById(R.id.buttonLeft)
            val buttonRight: TextView = dialog.findViewById(R.id.buttonRight)
            //        View viewDivider = dialog.findViewById(R.id.viewDivider);
            layoutRoot.clipToOutline = true
            layoutRoot.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View, outline: Outline) {
                    val cornerRadiusDP = 15f
                    outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
                }
            }
            if (!title.isNullOrEmpty()) {
                textTitle.text = title
            }
            if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
                setHtmlTextView(message, textMessage)
                textMessage.movementMethod = LinkMovementMethod.getInstance()
            }
            if (leftButtonTitle.isNullOrEmpty()) {
                buttonLeft.visibility = View.GONE
                //            viewDivider.setVisibility(View.GONE);
            } else {
                buttonLeft.text = leftButtonTitle
                buttonLeft.visibility = View.VISIBLE
            }
            if (rightButtonTitle.isNullOrEmpty()) {
                buttonRight.visibility = View.GONE
                //            viewDivider.setVisibility(View.GONE);
            } else {
                buttonRight.text = rightButtonTitle
                buttonRight.visibility = View.VISIBLE
            }
            buttonLeft.setOnClickListener {
                dialog.dismiss()
                if (onDialogButtonListener != null) {
                    onDialogButtonListener.onLeftButtonClick()
                }
            }
            buttonRight.setOnClickListener {
                dialog.dismiss()
                if (onDialogButtonListener != null) {
                    onDialogButtonListener.onRightButtonClick()
                }
            }
            try {
                dialog.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return dialog
        }

        fun showDialog(activity: Activity?, cancelable: Boolean, msg: String): Dialog? {
            return showDialog(activity, cancelable, cancelable, null, msg, null, null, null)
        }

        fun showDialog(activity: Activity?, message: String?): Dialog? {
            if (activity == null || message == null) {
                return null
            }
            return showDialog(
                activity,
                true,
                activity.getString(vn.mediatech.istudiocafe.R.string.notification),
                message,
                activity.getString(
                    vn.mediatech.istudiocafe.R.string.close
                ),
                null,
                null
            )
        }

        fun showDialog(
            activity: Activity?,
            message: String?,
            onDialogButtonListener: vn.mediatech.istudiocafe.listener.OnDialogButtonListener?
        ) {
            if (activity == null) {
                return
            }
            showDialog(
                activity,
                true,
                activity.getString(vn.mediatech.istudiocafe.R.string.notification),
                message,
                activity.getString(
                    vn.mediatech.istudiocafe.R.string.close
                ),
                null,
                onDialogButtonListener
            )
        }

        fun showDialog(
            activity: Activity?,
            cancelable: Boolean,
            message: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ) {
            if (activity == null) {
                return
            }
            showDialog(
                activity,
                cancelable,
                activity.getString(vn.mediatech.istudiocafe.R.string.notification),
                message,
                activity.getString(
                    vn.mediatech.istudiocafe.R.string.close
                ),
                null,
                onDialogButtonListener
            )
        }

        fun showDialog(
            activity: Activity?, cancelable: Boolean, titleId: Int, messageId: Int,
            leftButtonTitleId: Int, rightButtonTitleId: Int,
            dialogButtonListener: vn.mediatech.istudiocafe.listener.OnDialogButtonListener?
        ) {

            if (activity == null) {
                return
            }
            var message: String? = null
            if (messageId != 0) {
                message = activity.getString(messageId)
            }
            showDialog(
                activity,
                cancelable,
                titleId,
                message,
                leftButtonTitleId,
                rightButtonTitleId,
                dialogButtonListener
            )
        }

        fun showDialog(
            activity: Activity?, cancelable: Boolean, titleId: Int, message: String?,
            leftButtonTitleId: Int, rightButtonTitleId: Int,
            dialogButtonListener: OnDialogButtonListener?
        ) {
            if (activity == null) {
                return
            }
            var title: String? = null
            var leftButtonTitle: String? = null
            var rightButtonTitle: String? = null
            if (titleId != 0) {
                title = activity.getString(titleId)
            }
            if (leftButtonTitleId != 0) {
                leftButtonTitle = activity.getString(leftButtonTitleId)
            }
            if (rightButtonTitleId != 0) {
                rightButtonTitle = activity.getString(rightButtonTitleId)
            }
            showDialog(
                activity,
                cancelable,
                title,
                message,
                leftButtonTitle,
                rightButtonTitle,
                dialogButtonListener
            )
        }

        fun showDialog(
            activity: Activity?, cancelable: Boolean, title: String?, message: String?,
            leftButtonTitle: String?, rightButtonTitle: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ): Dialog? {
            return showDialog(
                activity,
                cancelable,
                false,
                title,
                message,
                leftButtonTitle,
                rightButtonTitle,
                onDialogButtonListener
            )
        }

        fun showLoadingDialog(
            activity: Activity?,
            cancelable: Boolean,
            onCancelListener: OnCancelListener?
        ): Dialog? {
            return showLoadingDialog(activity, cancelable, null, onCancelListener)
        }

        fun showLoadingDialog(
            activity: Activity?,
            cancelable: Boolean,
            message: String?,
            onCancelListener: OnCancelListener?
        ): Dialog? {
            var loadingDialog: Dialog? = null
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return null
            }
            if (loadingDialog != null) {
                loadingDialog?.dismiss()
                loadingDialog?.setCancelable(cancelable)
                loadingDialog?.setCanceledOnTouchOutside(false)
                if (message.isNullOrEmpty()) {
                    loadingDialog.textMessage?.visibility = View.GONE
                } else {
                    loadingDialog.textMessage?.text = message
                    loadingDialog.textMessage?.visibility = View.VISIBLE
                }
                loadingDialog?.show()
                return loadingDialog
            }
            loadingDialog = Dialog(activity)
            if (loadingDialog?.window != null) {
                loadingDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
            loadingDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loadingDialog?.setCancelable(cancelable)
            loadingDialog?.setCanceledOnTouchOutside(false)
            loadingDialog?.setContentView(vn.mediatech.istudiocafe.R.layout.layout_loading_tt)
            if (message.isNullOrEmpty()) {
                loadingDialog.textMessage?.visibility = View.GONE
            } else {
                loadingDialog.textMessage?.text = message
                loadingDialog.textMessage?.visibility = View.VISIBLE
            }
            loadingDialog?.setOnCancelListener {
                if (onCancelListener != null) {
                    onCancelListener.onCancel(true)
                }
            }
            loadingDialog?.show()
            return loadingDialog
        }

        fun showDonatedVoucherDialog(
            activity: Activity?,
            itemObj: ItemVoucher,
            onResultListener: OnResultListener? = null
        ) {
            showDialogVoucher(activity,
                true,
                false,
                itemObj.image,
                R.string.notification,
                itemObj.message,
                R.string.receive,
                null,
                R.string.decline,
                object : OnDialogButtonVoucherListener {
                    override fun onLeftButtonClick() {
                        updateDonatedStatus(activity, itemObj, "yes", onResultListener)
                    }

                    override fun onCenterButtonClick() {
                    }

                    override fun onRightButtonClick() {
                        updateDonatedStatus(activity, itemObj, "no", onResultListener)
                    }
                })
        }

        fun updateDonatedStatus(
            activity: Activity?,
            itemObj: ItemVoucher,
            type: String,
            onResultListener: OnResultListener? = null
        ) {

            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return
            }
            var loadingDialog: Dialog? = null
            val requestParams = RequestParams()
            requestParams.put("voucherid", "${itemObj.id}")
            requestParams.put("unique_id", "${itemObj.uniqueId}")
            requestParams.put("type", type)
            ServiceManager.requestBase(
                activity,
                Constant.API_VOUCHER_DONATED_CONFIRM,
                requestParams,
                object : ServiceManager.OnBaseRequestListener {
                    override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                        loadingDialog = showLoadingDialog(activity, true, null, null)
                    }

                    override fun onResponse(
                        isSuccess: Boolean,
                        message: String?,
                        responseString: String?
                    ) {
                        if (activity == null || activity.isFinishing || activity.isDestroyed) {
                            return
                        }
                        activity.runOnUiThread {
                            loadingDialog?.dismiss()
                            if (!isSuccess) {
                                if (message != null && !message.equals(activity.getString(R.string.msg_network_error))) {
                                    onResultListener?.onResult(true, message)
                                }
                                showDialogVoucher(activity,
                                    true,
                                    false,
                                    null,
                                    R.string.notification,
                                    message,
                                    null,
                                    null,
                                    R.string.close,
                                    object : OnDialogButtonVoucherListener {
                                        override fun onLeftButtonClick() {
                                        }

                                        override fun onCenterButtonClick() {
                                        }

                                        override fun onRightButtonClick() {
                                        }

                                    })
                                return@runOnUiThread
                            }
                            onResultListener?.onResult(true, message)
                            showDialogVoucher(activity,
                                true,
                                false,
                                null,
                                R.string.notification,
                                message,
                                null,
                                R.string.view_voucher,
                                R.string.close,
                                object : OnDialogButtonVoucherListener {
                                    override fun onLeftButtonClick() {
                                    }

                                    override fun onCenterButtonClick() {
                                        val itemUser =
                                            MyApplication.getInstance().dataManager.itemUser
                                        if (itemUser != null) {
                                            itemObj.userId = itemUser.id
                                        }
                                        val bundle = Bundle()
                                        bundle.putParcelable(Constant.DATA, itemObj)
                                        if (activity is VoucherActivity) {
                                            activity.dismissDonateVoucherFragment()
                                            activity.showDetailVoucherFragment(
                                                bundle,
                                                object : OnResultListener {
                                                    override fun onResult(
                                                        isSuccess: Boolean,
                                                        msg: String?
                                                    ) {
                                                        activity.refreshData()
                                                    }
                                                })
                                        } else {
                                            bundle.putInt(
                                                Constant.VOUCHER_TYPE,
                                                Constant.DETAIL_VOUCHER
                                            )
                                            gotoActivity(
                                                activity,
                                                VoucherActivity::class.java,
                                                bundle
                                            )
                                        }
                                    }

                                    override fun onRightButtonClick() {
                                        if (activity is VoucherActivity) {
                                            activity.refreshData()
                                        }
                                    }

                                })
                        }
                    }
                },
                false
            )
        }

        //    END: SHOW DONATED VOUCHER DIALOG
        fun showDialogVoucher(
            activity: Activity?,
            cancelable: Boolean,
            cancelableOutside: Boolean,
            imageUrl: String?,
            title: Int?,
            message: String?,
            leftButtonTitle: Int?,
            centerButtonTitle: Int?,
            rightButtonTitle: Int?,
            listener: OnDialogButtonVoucherListener?
        ): MyDialog? {
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return null
            }
            val titleStr = if (title != null) activity.getString(title) else null
            val leftButtonTitleStr =
                if (leftButtonTitle != null) activity.getString(leftButtonTitle) else null
            val centerButtonTitleStr =
                if (centerButtonTitle != null) activity.getString(centerButtonTitle) else null
            val rightButtonTitleStr =
                if (rightButtonTitle != null) activity.getString(rightButtonTitle) else null
            return showDialogVoucher(
                activity,
                cancelable,
                cancelableOutside,
                imageUrl,
                titleStr,
                message,
                leftButtonTitleStr,
                centerButtonTitleStr,
                rightButtonTitleStr,
                listener
            )
        }

        fun showDialogVoucher(
            activity: Activity?,
            cancelable: Boolean,
            cancelableOutside: Boolean,
            imageUrl: String?,
            title: String?,
            message: String?,
            leftButtonTitle: String?,
            centerButtonTitle: String?,
            rightButtonTitle: String?,
            listener: OnDialogButtonVoucherListener?
        ): MyDialog? {
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return null
            }
            val dialog = MyDialog(activity)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val lp = WindowManager.LayoutParams()
            //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER
            lp.windowAnimations = R.style.DialogAnimationLeftRight
            dialog.window?.setAttributes(lp)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(cancelable)
            dialog.setCanceledOnTouchOutside(cancelableOutside)
            dialog.setContentView(R.layout.layout_voucher_notification)
            val screenSize = ScreenSize(activity)
            dialog.window?.setLayout(
                screenSize.width * 9 / 10,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
            val textTitle: TextView = dialog.findViewById(R.id.textTitle)
            val textMessage: TextView = dialog.findViewById(R.id.textMessage)
            val image: ImageView = dialog.findViewById(R.id.image)
            val buttonLeft: TextView = dialog.findViewById(R.id.buttonLeft)
            val buttonCenter: TextView = dialog.findViewById(R.id.buttonCenter)
            val buttonRight: TextView = dialog.findViewById(R.id.buttonRight)
            //        View viewDivider = dialog.findViewById(R.id.viewDivider);
            layoutRoot.clipToOutline = true
            layoutRoot.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View, outline: Outline) {
                    val cornerRadiusDP = 15f
                    outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
                }
            }
            if (!title.isNullOrEmpty()) {
                textTitle.text = title
            }
            if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
                TextUtil.setHtmlTextView(message, textMessage)
                textMessage.movementMethod = LinkMovementMethod.getInstance()
            }
            if (!leftButtonTitle.isNullOrEmpty()) {
                buttonLeft.text = leftButtonTitle
                buttonLeft.visibility = View.VISIBLE
            }
            if (!centerButtonTitle.isNullOrEmpty()) {
                buttonCenter.text = centerButtonTitle
                buttonCenter.visibility = View.VISIBLE
            }
            if (!rightButtonTitle.isNullOrEmpty()) {
                buttonRight.text = rightButtonTitle
                buttonRight.visibility = View.VISIBLE
            }
            if (!imageUrl.isNullOrEmpty()) {
                image.visibility = View.VISIBLE
                val imageHeight = ScreenSize(activity).width * 5 / 8
                val lparams = image.layoutParams as LinearLayout.LayoutParams
                lparams.height = imageHeight
                MyApplication.getInstance().loadImage(activity, image, imageUrl)
            }
            buttonLeft.setOnClickListener {
                dialog.dismiss()
                listener?.onLeftButtonClick()
            }
            buttonCenter.setOnClickListener {
                dialog.dismiss()
                listener?.onCenterButtonClick()
            }
            buttonRight.setOnClickListener {
                dialog.dismiss()
                listener?.onRightButtonClick()
            }
            try {
                dialog.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return dialog
        }

        var buttonMenu: View? = null
        fun showTextBubleDialog(
            activity: Activity?,
            msg: String?,
            timeCloseMs: Long? = 0,
            isTyping: Boolean = false,
            timDelayTyping: Long = 10,
            iconMenuClickListener: View.OnClickListener? = null,
            timeHideText: Long? = 0,
            isTouchCancel: Boolean = true
        ): Dialog? {
            if (activity == null) {
                return null
            }
            if (activity.isDestroyed || activity.isFinishing) {
                return null
            }
            val dialog = MyDialog(activity)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val lp = WindowManager.LayoutParams()
            lp.gravity = Gravity.CENTER
            dialog.window?.attributes = lp
            dialog.window?.setDimAmount(0.1F)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(isTouchCancel)
            dialog.setCanceledOnTouchOutside(isTouchCancel)
            dialog.setContentView(R.layout.layout_dialog_bubble_tt)
            val screenSize = ScreenSize(activity)
            dialog.window!!.setLayout(
                screenSize.width * 10 / 10,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            val layoutRoot = dialog.findViewById<View>(R.id.layoutRoot)
            val textBubleMessageDialog =
                dialog.findViewById<TypeWriterTextView>(R.id.textBubleMessageDialog)
            val buttonMenuDialog = dialog.findViewById<ImageView>(R.id.buttonMenuDialog)
            val layoutTextBubble = dialog.findViewById<RelativeLayout>(R.id.layoutTextBubble)
            val params = textBubleMessageDialog.layoutParams as ViewGroup.LayoutParams
            val width = ScreenSize(activity).width * 2 / 3
            params.width = width
            val px10 = activity.resources.getDimensionPixelSize(R.dimen.dimen_10)
            val px20 = activity.resources.getDimensionPixelSize(R.dimen.dimen_40)
            if (msg.isNullOrEmpty()) {
                textBubleMessageDialog?.visibility = View.GONE
            }
            textBubleMessageDialog.setPadding(px10, px10, px20, px10)
            if (isTyping) {
                textBubleMessageDialog?.setText("");
                textBubleMessageDialog?.visibility = View.VISIBLE
                textBubleMessageDialog?.setCharacterDelay(timDelayTyping);
                textBubleMessageDialog?.setMovementMethod(ScrollingMovementMethod())
                textBubleMessageDialog?.animateText(msg);
            } else {
                AppUtils.setHtmlTextView(msg, textBubleMessageDialog)
//            textBubleMessageDialog?.setText(msg);
                textBubleMessageDialog?.visibility = View.VISIBLE
            }

            if (buttonMenu != null) {
                val params = buttonMenuDialog.layoutParams as ConstraintLayout.LayoutParams
                params.bottomMargin = buttonMenu!!.marginBottom + convertDpToPixel(
                    Constant.HEIGHT_TAB_BAR_BOTTOM_DP.toFloat(),
                    activity
                ).toInt()
            }
            textBubleMessageDialog.setOnClickListener {
                if (isTouchCancel) {
                    dialog?.dismiss()
                }
            }
//            layoutRoot.setOnClickListener {
//                if (isTouchCancel) {
//                    dialog?.dismiss()
//                }
//            }
            buttonMenuDialog.setOnClickListener {
                iconMenuClickListener?.onClick(it)
                if (isTouchCancel) {
                    dialog?.dismiss()
                }
            }
            try {
                dialog.show()
                if (timeCloseMs != null && timeCloseMs > 0L) {
                    Handler(Looper.getMainLooper()).postDelayed({
                        dialog?.dismiss()
                    }, timeCloseMs)
                }
                if (timeHideText != null && timeHideText > 0L) {
                    Handler(Looper.getMainLooper()).postDelayed({
                        textBubleMessageDialog?.visibility = View.GONE
                        layoutTextBubble?.visibility = View.GONE
                    }, timeHideText)
                }
                buttonMenu?.visibility = View.INVISIBLE
                dialog?.getWindow()?.getDecorView()
                    ?.setSystemUiVisibility(activity.window!!.decorView!!.systemUiVisibility!!)
            } catch (e: Exception) {
            }

            dialog.setOnDismissListener {
                buttonMenu?.visibility = View.VISIBLE
            }
            return dialog;
        }
        fun showIconVoiceDialog(
            activity: Activity?,
            msg: String?,
            timeCloseMs: Long? = 0,
            iconMenuClickListener: View.OnClickListener? = null
        ): Dialog? {
            return  showTextBubleDialog(activity,msg,timeCloseMs,true,10,iconMenuClickListener,2000L,false)
        }
        fun showFragment(
            fragmentManager: FragmentManager?,
            fragment: Fragment?, layoutId: Int,
            isReplace: Boolean = false,
            isAddBackstack: Boolean = false,
        ) {
            if (fragment == null || fragmentManager == null) {
                return
            }
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.fragment_right_to_left,
                R.anim.fragment_right_to_left_exit,
                R.anim.fragment_left_to_right,
                R.anim.fragment_left_to_right
            )
            if (isAddBackstack) {
                fragmentTransaction.addToBackStack("")
            }
            if (isReplace) {
                fragmentTransaction.replace(layoutId, fragment)
            } else {
                fragmentTransaction.add(layoutId, fragment)
            }
            fragmentTransaction.commit()
        }

        fun showBottomSheetDialog(
            fragmentManager: FragmentManager?,
            fragment: Fragment,
            bundle: Bundle?
        ) {
            if (fragment !is BottomSheetDialogFragment || fragmentManager == null) {
                return
            }
            if (bundle != null) {
                fragment.setArguments(bundle)
            }
            Handler(Looper.getMainLooper()).postDelayed({
                fragment.show(fragmentManager, fragment.getTag())
            }, Constant.DELAY_HANDLE)
        }

        @SuppressWarnings("deprecation")
        fun clearCookies(context: Context?) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                CookieManager.getInstance().removeAllCookies(null)
                CookieManager.getInstance().flush()
            } else if (context != null) {
                val cookieSyncManager = CookieSyncManager.createInstance(context)
                cookieSyncManager.startSync()
                val cookieManager: CookieManager = CookieManager.getInstance()
                cookieManager.removeAllCookie()
                cookieManager.removeSessionCookie()
                cookieSyncManager.stopSync()
                cookieSyncManager.sync()
            }
        }

        fun gotoActivity(activity: Activity?, intent: Intent?) {
            if (intent != null && activity != null) {
                activity.startActivity(intent)
            }
        }

        fun gotoActivity(activity: Activity?, cla: Class<*>?) {
            if (cla == null || activity == null) {
                return
            }
            val intent = Intent(activity, cla)
            activity.startActivity(intent)
        }

        fun gotoActivity(activity: Activity?, cla: Class<*>?, bundle: Bundle?) {
            if (cla == null || activity == null) {
                return
            }
            val intent = Intent(activity, cla)
            if (bundle != null) {
                intent.putExtras(bundle)
            }
            activity.startActivity(intent)
        }
        fun gotoActivityForResult(
            activity: Activity?,
            cla: Class<*>?,
            bundle: Bundle?,
            requestCode: Int
        ) {
            if (cla == null || activity == null) {
                return
            }
            val intent = Intent(activity, cla)
            if (bundle != null) {
                intent.putExtras(bundle)
            }
            activity.startActivityForResult(intent, requestCode)
        }

        fun gotoActivityForResult(activity: Activity?, cla: Class<*>?, requestCode: Int) {
            if (cla == null || activity == null) {
                return
            }
            val intent = Intent(activity, cla)
            activity.startActivityForResult(intent, requestCode)
        }

        fun goDetailNewsActivity(activity: Activity?, itemObj: ItemNews) {
            if (activity == null) {
                return
            }
            val bundle = Bundle()
            bundle.putParcelable(Constant.DATA, itemObj)
            var cla: Class<*>? = DetailNewsActivity::class.java
            when (itemObj.contentType) {
                Constant.TYPE_NEWS -> cla = DetailNewsActivity::class.java
                Constant.TYPE_VIDEO -> cla = DetailVideoActivity::class.java
            }
            gotoActivity(activity, cla, bundle)
        }

        fun hideKeyboard(activity: Activity?, editText: EditText?) {
            if (activity == null) {
                return
            }
            val inputMethodManager =
                activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                editText?.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }

        fun showToast(activity: Activity?, message: String) {
            if (activity == null) {
                return
            }
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        }

        fun showToast(activity: Activity?, messageId: Int) {
            if (activity == null) {
                return
            }
            Toast.makeText(activity, messageId, Toast.LENGTH_SHORT).show()
        }


        fun shareApp(activity: Activity?, msg: String?, title: String? = null) {
            if (activity == null) {
                return
            }
            if (msg == null || msg.isEmpty()) {
                showToast(activity, vn.mediatech.istudiocafe.R.string.msg_empty_data)
                return
            }
            var mTitle = ""
            if (title.isNullOrEmpty()) {
                mTitle = activity.getString(vn.mediatech.istudiocafe.R.string.select_share_app)
            } else {
                mTitle = title
            }

            ShareCompat.IntentBuilder.from(activity)
                .setType("text/plain")
                .setText(msg)
                .setChooserTitle(mTitle)
                .startChooser()
        }

        fun openMyVoucher(activity: Activity?) {
            if (activity == null) {
                return
            }
            if (MyApplication.getInstance().dataManager.itemUser == null) {
                showToast(activity, "Vui lòng đăng nhập để xem kho quà!")
            }
            if (activity is VoucherActivity) {
                activity.showMyVoucherFragment()
                return
            }
            val intent = Intent(activity, VoucherActivity::class.java)
            val bundle = Bundle()
            bundle.putInt(Constant.VOUCHER_TYPE, Constant.MY_VOUCHER)
            intent.putExtras(bundle)
            activity.startActivity(intent)
        }

        var enableShowDialogConfirm = true
        var isShowLogin = false
        fun showDialogConfirm(
            activity: Activity?,
            itemUser: ItemUser?,
            dialogButtonListener: vn.mediatech.istudiocafe.listener.OnDialogButtonListener?
        ): Dialog? {
            if (activity == null || activity.isDestroyed || activity.isFinishing || !enableShowDialogConfirm || isShowLogin || itemUser == null) {
                if (isShowLogin) {
                    isShowLogin = false
                }
                dialogButtonListener?.onLeftButtonClick()
                return null
            }
            if (!Constant.REQUIRE_CONFIRM || Constant.USER_STATUS_REAL.equals(itemUser.userStatus)) {
                dialogButtonListener?.onLeftButtonClick()
                return null
            }
            val title = "TuongTac.tv XIN CHÀO"
            val msg = ("Để " + TextUtil.getStringHtmlStype(
                "Xác thực tài khoản",
                "#FFFFFF",
                true,
                false
            )
                    + ", vui lòng làm theo các bước sau:" + "<br/>" +
                    "   - Bước 1: Nhấn vào nút \"" + TextUtil.getStringHtmlStype(
                "Xác thực",
                "#FFFFFF",
                true,
                false
            ) + "\" phía bên dưới." + "<br/>" +
                    "   - Bước 2: Gõ nội dung \"" + TextUtil.getStringHtmlStype(
                "Xác thực ID " + itemUser.id,
                "#FFFFFF",
                true,
                false
            ) + "\" vào Chatbox FacebookMessenger")
            enableShowDialogConfirm = false
            val dialog = AppUtils.showDialog(
                activity,
                false,
                false,
                title,
                msg,
                activity.getString(R.string.title_ignore),
                activity.getString(R.string.title_confirm),
                object : AppUtils.OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        dialogButtonListener?.onLeftButtonClick()
                        enableShowDialogConfirm = true
                    }

                    override fun onRightButtonClick() {
                        enableShowDialogConfirm = true
                        dialogButtonListener?.onRightButtonClick()
                    }
                })
            dialog!!.setOnDismissListener {
                enableShowDialogConfirm = true
            }
            return dialog
        }

        fun openMessenger(activity: Activity?) {
            if (activity == null || activity.isDestroyed || activity.isFinishing) {
                return
            }
//            LoadingDialogUtils.showLoadingDialog(activity, true, null)
            try {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse("fb://messaging/1356792611058221"))
                activity.startActivity(i)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                val i =
                    Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.facebook.orca"))
                activity.startActivity(i)
            }
        }

        fun convertNonAccentVietnamese(str: String?): String? {
            if (str == null) {
                return null
            }
            var str = str
            str = str.replace("à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ".toRegex(), "a")
            str = str.replace("è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ".toRegex(), "e")
            str = str.replace("ì|í|ị|ỉ|ĩ".toRegex(), "i")
            str = str.replace("ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ".toRegex(), "o")
            str = str.replace("ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ".toRegex(), "u")
            str = str.replace("ỳ|ý|ỵ|ỷ|ỹ".toRegex(), "y")
            str = str.replace("đ".toRegex(), "d")
            str = str.replace("À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ".toRegex(), "A")
            str = str.replace("È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ".toRegex(), "E")
            str = str.replace("Ì|Í|Ị|Ỉ|Ĩ".toRegex(), "I")
            str = str.replace("Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ".toRegex(), "O")
            str = str.replace("Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ".toRegex(), "U")
            str = str.replace("Ỳ|Ý|Ỵ|Ỷ|Ỹ".toRegex(), "Y")
            str = str.replace("Đ".toRegex(), "D")
            return str
        }

        fun isDarkThemeOn(context: Context?): Boolean {
            if (context == null) {
                return false
            }
            return context.resources.configuration.uiMode and
                    Configuration.UI_MODE_NIGHT_MASK == UI_MODE_NIGHT_YES
        }

        fun setStatusBar(activity: Activity?) {
            if (activity == null) {
                return
            }
//                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            activity.getWindow().getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (!isDarkThemeOn(activity)) {
////                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//                    activity.getWindow().getDecorView().setSystemUiVisibility(
//                        activity.getWindow().getDecorView()
//                            .getSystemUiVisibility() and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
//                    ) //set status text  light
//                } else {
//                    activity.window.getDecorView()
//                        .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
//                }
//            }
        }
    }

}
package vn.mediatech.istudiocafe.util;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class SpacesItemDecorationListBox2 extends RecyclerView.ItemDecoration {
    private int space;
    private int spanCount;
    private boolean isHorizoltalListview;

    public SpacesItemDecorationListBox2(int space, int numberColumn, boolean isHorizoltalListview) {
        this.space = space;
        this.spanCount = numberColumn;
        this.isHorizoltalListview = isHorizoltalListview;
        if (spanCount <= 0) {
            spanCount = 1;
        }
    }

    public SpacesItemDecorationListBox2(int space, int numberColumn) {
        this.space = space;
        this.spanCount = numberColumn;
        this.isHorizoltalListview = false;
        if (spanCount <= 0) {
            spanCount = 1;
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
//        int spanCount = 2;
        if (spanCount == 1) {
            if (isHorizoltalListview) {
                if (position == 0) {
                    outRect.left = space;
                }
                outRect.right = space;
                outRect.top = space;
                outRect.bottom = space;
            } else {//Listview 1 column
                if (position == 0) {
                    outRect.top = space;
                }
                outRect.left = space;
                outRect.bottom = space;
                outRect.right = space;
            }
            return;
        }
        if (position >= 0) {
            int column = position % spanCount; // item column
            outRect.left = space - column * space / spanCount; // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * space / spanCount; // (column + 1) * ((1f / spanCount) * spacing)
            if (position < spanCount) { // top edge
                outRect.top = space;
            }
            outRect.bottom = space; // item bottom
        } else {
            outRect.left = 0;
            outRect.right = 0;
            outRect.top = 0;
            outRect.bottom = 0;
        }
       /* outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;
        outRect.top = space;*/
    }
}
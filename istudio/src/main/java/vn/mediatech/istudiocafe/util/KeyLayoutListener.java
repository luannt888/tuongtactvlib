package vn.mediatech.istudiocafe.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;

public class KeyLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
    @NonNull
    private Activity mActivity;
    private int mScreenHeight;
    private Rect mOutRect = new Rect();
    private int mWindowVisibleDisplayFrameBottom;
    private OnGetKeyHeightListener onGetKeyHeightListener;

    public KeyLayoutListener(@NonNull Activity activity) {
        mActivity = activity;
        mScreenHeight = getScreenHeight();
    }

    @Override
    public void onGlobalLayout() {
        Window window = mActivity.getWindow();
        if (window == null)
            return;
        mOutRect.setEmpty();
        window.getDecorView().getWindowVisibleDisplayFrame(mOutRect);
        int offset = mWindowVisibleDisplayFrameBottom - mOutRect.bottom;
        if (offset > mScreenHeight / 4) {
            if (onGetKeyHeightListener != null) {
                onGetKeyHeightListener.onGet(offset);
            }
        } else {
            mWindowVisibleDisplayFrameBottom = mOutRect.bottom;
        }
    }

    private int getScreenHeight() {
        WindowManager wm = (WindowManager) mActivity.getSystemService(Context.WINDOW_SERVICE);
        if (wm == null) {
            return mActivity.getResources().getDisplayMetrics().heightPixels;
        }
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            wm.getDefaultDisplay().getRealSize(point);
        } else {
            wm.getDefaultDisplay().getSize(point);
        }
        return point.y;
    }

    public void setOnGetKeyHeightListener(OnGetKeyHeightListener onGetKeyHeightListener) {
        this.onGetKeyHeightListener = onGetKeyHeightListener;
    }

    public interface OnGetKeyHeightListener {
        void onGet(int keyBoardHeight);
    }

}

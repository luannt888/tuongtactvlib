package vn.mediatech.istudiocafe.util;

import android.app.Activity;
import android.net.Uri;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.app.MyApplication;

public class ShareFacebookUtil {
    private Activity activity;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    public ShareFacebookUtil(Activity activity) {
        this.activity = activity;
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                /*activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "shareok", Toast.LENGTH_SHORT).show();
                    }
                });*/
            }

            @Override
            public void onCancel() {
                /*activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "onCancel", Toast.LENGTH_SHORT).show();
                    }
                });*/
            }

            @Override
            public void onError(FacebookException error) {
                /*activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "onError", Toast.LENGTH_SHORT).show();
                    }
                });*/
            }
        });
    }

    /*public void shareItemNews(ItemNews itemObj) {
        if (itemObj == null || MyApplication.getInstance().isEmpty(itemObj.getUrl())) {
            ((BaseActivity) activity).showToast(R.string.msg_empty_data);
            return;
        }
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(itemObj.getUrl()))
//                    .setQuote("this is setQuote : " + itemObj.getName())
//                    .setContentTitle("this is title 123")
//                    .setContentDescription("this is desc")
//                    .setShareHashtag(new ShareHashtag.Builder().setHashtag("#riderok").build())
                    .build();
            shareDialog.show(linkContent);
            ServiceUtil.addShare(activity, itemObj.getId());
        } else {
            Toast.makeText(activity, activity.getString(R.string.msg_process_error), Toast.LENGTH_SHORT).show();
        }
    }*/
    public void shareFacebook(String url) {
        if ( MyApplication.getInstance().isEmpty(url)) {
            ((BaseActivity) activity).showToast(R.string.msg_empty_data);
            return;
        }
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(url))
//                    .setQuote("this is setQuote : " + itemObj.getName())
//                    .setContentTitle("this is title 123")
//                    .setContentDescription("this is desc")
//                    .setShareHashtag(new ShareHashtag.Builder().setHashtag("#riderok").build())
                    .build();
            shareDialog.show(linkContent);
            ServiceUtilTT.addShare(activity, url);
        } else {
            Toast.makeText(activity, activity.getString(R.string.msg_process_error), Toast.LENGTH_SHORT).show();
        }
    }
}
package vn.mediatech.istudiocafe.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.glide.slider.library.slidertypes.BaseSliderView
import vn.mediatech.istudiocafe.R

open class MySliderView(context: Context?) : BaseSliderView(context) {
    override fun getView(): View {
        val v = LayoutInflater.from(context).inflate(R.layout.my_slider_view, null)
        val target: AppCompatImageView = v.findViewById(R.id.glide_slider_image)
        val description: AppCompatTextView = v.findViewById(R.id.glide_slider_description)
        description.text = getDescription()
        bindEventAndShow(v, target)
        return v
    }
}
package vn.mediatech.istudiocafe.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.view.WindowInsetsController
import android.view.WindowManager

@Suppress("DEPRECATION")
class MyDialog : Dialog {
    private var ctx: Context

    constructor(context: Context) : super(context) {
        ctx = context
    }

    constructor(context: Context, themeResId: Int) : super(context, themeResId) {
        ctx = context
    }

    protected constructor(context: Context, cancelable: Boolean, cancelListener: DialogInterface.OnCancelListener?) : super(context, cancelable, cancelListener) {
        ctx = context
    }

    override fun show() {
        // Set the dialog to not focusable.
        window!!.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        super.show()
        if (Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.R) {
            window!!.decorView.systemUiVisibility = (ctx as Activity).window.decorView.systemUiVisibility
        } else {
            window!!.insetsController?.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_SWIPE
        }

        // Set the dialog to focusable again.
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
    }
}
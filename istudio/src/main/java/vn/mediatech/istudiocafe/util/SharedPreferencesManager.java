package vn.mediatech.istudiocafe.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import vn.mediatech.istudiocafe.app.Constant;

import static vn.mediatech.istudiocafe.service.fcm.MyFirebaseMessagingManager.FCM;
import static vn.mediatech.istudiocafe.service.fcm.MyFirebaseMessagingManager.PROPERTY_ID;

public class SharedPreferencesManager {
    private static String KEY_FONT_SIZE = "KEY_FONT_SIZE";
    private static String KEY_DARK_MODE = "KEY_DARK_MODE";
    private static String KEY_THEME = "KEY_THEME";

    public static void saveUser(Context context, String username, String token) {
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constant.USERNAME, username);
        editor.putString(Constant.ACCESS_TOKEN, token);
        editor.apply();
        if(token == null){
            clearInteractiveSharedPreferences(context);
        }
    }

    public static int getDefaultWebviewFontSize(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(KEY_FONT_SIZE, 100);
    }

    public static String getAccessToken(Context context) {
        if (context == null) {
            return null;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        return sharedPreferences.getString(Constant.ACCESS_TOKEN, null);
    }
    public static void saveAccessToken(Context context, String accessToken) {
        saveString(context,accessToken,Constant.ACCESS_TOKEN);
    }

    public static void saveDefaultWebviewFontSize(Context context, int percent) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(KEY_FONT_SIZE, percent);
        editor.apply();
    }

    public static void saveString(Context context, String string, String key) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, string);
        editor.apply();
    }

    public static String getString(Context context, String key) {
        if (context == null) {
            return null;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        return sharedPreferences.getString(key, null);
    }

    public static void saveInt(Context context, int i, String key) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, i);
        editor.apply();
    }

    public static SharedPreferences getSharedPreferences(Context context, String config) {
        return context.getSharedPreferences(config,
                Context.MODE_PRIVATE);
    }

    public static int getInt(Context context, String key) {
        if (context == null) {
            return 0;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        return sharedPreferences.getInt(key, 0);
    }

    public static boolean getBoolean(Context context, String key) {
        if (context == null) {
            return false;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        return sharedPreferences.getBoolean(key, false);
    }
    public static boolean isContain(Context context, String key) {
        if (context == null) {
            return false;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        return sharedPreferences.contains(key);
    }
    public static void saveBoolean(Context context, boolean value, String key) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }
    public static void clearInteractiveSharedPreferences(Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(context, Constant.CONFIG);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("PORT_GAME");
        editor.remove("PORT_CHAT");
        editor.remove("LIVE_STREAM");
        editor.remove("IS_TEST");
        editor.remove("BASE_URL");
        editor.remove("CHANNEL_ID");
        editor.apply();
    }
    public static void savePortGame(Context context, String port) {
        saveString(context, port, "PORT_GAME");
    }

    public static String getPortGame(Context context) {
        return getString(context, "PORT_GAME");
    }

    public static void savePortChat(Context context, String port) {
        saveString(context, port, "PORT_CHAT");
    }

    public static String getPortChat(Context context) {
        return getString(context, "PORT_CHAT");
    }

    public static void saveUrlStream(Context context, String url) {
        saveString(context, url, "LIVE_STREAM");
    }

    public static String getUrlStream(Context context) {
        return getString(context, "LIVE_STREAM");
    }

    public static void saveIsTest(Context context, boolean isTest) {
        saveBoolean(context, isTest, "IS_TEST");
    }

    public static boolean isTest(Context context) {
        return getBoolean(context, "IS_TEST");
    }

    public static void saveBaseURL(Context context, String url) {
        saveString(context, url, "BASE_URL");
    }

    public static String getBaseUrlGame(Context context) {
        return getString(context, "BASE_URL");
    }
    public static void saveChannelID(Context context, String channelID) {
        saveString(context, channelID, "CHANNEL_ID");
    }

    public static String getChannelID(Context context) {
        return getString(context, "CHANNEL_ID");
    }
    public static String getPopupID(Context context) {
        return getString(context, "ID_POPUP");
    }

    public static void savePopupID(Context context, String popupID) {
        saveString(context, popupID, "ID_POPUP");
    }

    public static int getPopupCount(Context context) {
        return getInt(context, "ID_POPUP_COUNT");
    }

    public static void savePopupCount(Context context, int count) {
        saveInt(context, count, "ID_POPUP_COUNT");
    }

    public static boolean isDarkMode(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(KEY_DARK_MODE, true);
    }

    public static void setDarkMode(Context context, boolean isBackgroundRadio) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(KEY_DARK_MODE, isBackgroundRadio);
        editor.apply();
    }
    public static String getDeviceId(Context context ) {
        SharedPreferences prefs = getSharedPreferences(context, FCM);
        return prefs.getString(PROPERTY_ID, null);
    }
}

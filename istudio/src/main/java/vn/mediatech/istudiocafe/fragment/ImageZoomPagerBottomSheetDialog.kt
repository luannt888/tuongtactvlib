package vn.mediatech.istudiocafe.fragment

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_image_zoom_pager_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.adapter.ViewPagerAdapter
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemPhoto
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ServiceUtilTT

class ImageZoomPagerBottomSheetDialog : BottomSheetDialogFragment() {
    var itemObj: ItemNews? = null
    val fragmentList: ArrayList<Fragment> = ArrayList()
    val itemList: ArrayList<ItemPhoto> = ArrayList()
    var myHttpRequest: MyHttpRequest? = null
    var page: Int = 1
    val limit = 20

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN)
        } else {

        }
        return inflater.inflate(R.layout.fragment_image_zoom_pager_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setOnShowListener {
            val bottomSheetInternal: View? = (it as BottomSheetDialog).findViewById(R.id.design_bottom_sheet)
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetInternal!!)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.isDraggable = false
            bottomSheetBehavior.skipCollapsed = true
            setFullScreen(true)

        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            initUI()
            initData()
            initControl()
        }, 300)
    }

    override fun onCancel(dialog: DialogInterface) {
        dismissDialog()
        super.onCancel(dialog)
    }

    override fun onDismiss(dialog: DialogInterface) {
        dismissDialog()
        super.onDismiss(dialog)
    }

    fun dismissDialog() {
        setFullScreen(false)
        myHttpRequest?.cancel()
    }

    fun setFullScreen(isFullScreen: Boolean) {
        if (isFullScreen) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN)
            } else {

            }

        } else {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            } else {

            }
        }
    }

    fun initUI() {
        MyApplication.getInstance().loadImageNoPlaceHolder(activity, layoutFragmentRoot, R.drawable.bg_main_tt)
    }

    fun initData() {
        itemObj = arguments?.getParcelable(Constant.DATA)
        if (itemObj == null) {
            dismiss()
            return
        }
        getData()
    }

    fun showErrorNetwork(message: String) {
        activity?.runOnUiThread {
            if (itemList.size <= 0) {
                textNotify.visibility = View.VISIBLE
                textNotify.text = message
            }
            progressBar.visibility = View.GONE
        }
    }

    fun getData() {
        if (!MyApplication.getInstance().isNetworkConnect) {
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
        progressBar.visibility = View.VISIBLE
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
//        val api = String.format(Locale.ENGLISH, Constant.API_GELLERY_LIST, itemObj!!.id, page, limit)
        val requestParams = RequestParams()
        requestParams.put("id", "${itemObj!!.id}")
        requestParams.put("content_type", "${itemObj!!.contentType}")
        myHttpRequest!!.request(false, ServiceUtilTT.validAPI(Constant.API_NEWS_DETAIL), requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    progressBar.visibility = View.GONE
                }
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            showErrorNetwork(message ?: requireActivity().getString(R.string.msg_empty_data))
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
        val dataObj = JsonParser.getJsonObject(resultObj, Constant.DATA)
        itemObj = JsonParser.parseItemNews(requireActivity(), dataObj, null)
        val galleryArr = JsonParser.getJsonArray(itemObj!!.gallery)
        val list = JsonParser.parseItemPhotoList(galleryArr)
        if (list == null || list.size == 0) {
            if (itemList.size > 0) {
                return
            }
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        itemList.addAll(list)
        initPager()
    }

    fun initPager() {
        val tabTitleList: ArrayList<String> = ArrayList()
        for (i in 0 until itemList.size) {
            val item = itemList.get(i)
            if (item.image.isNullOrEmpty()) {
                continue
            }
            item.time = itemObj!!.timeAgo
            val fragment = ImageZoomFragment()
            val bundle = Bundle()
            bundle.putParcelable(Constant.DATA, item)
            fragment.arguments = bundle
            fragmentList.add(fragment)
        }
        activity?.runOnUiThread {
            val viewPagerAdapter = ViewPagerAdapter(childFragmentManager, fragmentList, tabTitleList)
            viewPager.adapter = viewPagerAdapter
            viewPager.offscreenPageLimit = fragmentList.size
            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    handleOnPageSelected(position)
                }

                override fun onPageScrollStateChanged(state: Int) {
                }
            })
            buttonPrevios.visibility = View.VISIBLE
            buttonNext.visibility = View.VISIBLE
            handleOnPageSelected(0)
        }
    }

    var selectedFragment: ImageZoomFragment? = null
    fun handleOnPageSelected(position: Int) {
        selectedFragment?.resetZoom()
        selectedFragment = fragmentList.get(position) as ImageZoomFragment

        buttonPrevios.isEnabled = !(position == 0)
        buttonNext.isEnabled = !(position == fragmentList.size - 1)

        if (buttonPrevios.isEnabled) {
            buttonPrevios.clearColorFilter()
        } else {
            buttonPrevios.setColorFilter(ContextCompat.getColor(requireActivity(), R.color.gray), android.graphics.PorterDuff.Mode.SRC_ATOP);
        }
        if (buttonNext.isEnabled) {
            buttonNext.clearColorFilter()
        } else {
            buttonNext.setColorFilter(ContextCompat.getColor(requireActivity(), R.color.gray), android.graphics.PorterDuff.Mode.SRC_ATOP);
        }
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            dismiss()
        }
        buttonPrevios.setOnClickListener {
            viewPager.currentItem = viewPager.currentItem - 1
        }
        buttonNext.setOnClickListener {
            viewPager.currentItem = viewPager.currentItem + 1
        }
    }
}
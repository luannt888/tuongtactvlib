package vn.mediatech.istudiocafe.fragment

import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_setup.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.listener.OnResultListener
import vn.mediatech.istudiocafe.util.GeneralUtils
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.SharedPreferencesManager
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import java.util.*

class SetUpDialogFragment : BottomSheetDialogFragment() {
    var bottomSheetDialog: BottomSheetDialog? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null
    var onResultListener: OnResultListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_setup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        onShowDismissListener?.onShow()
        init()
        return
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()
    }

    var data: String? = null
    fun initData() {
        checkTest.isChecked = SharedPreferencesManager.isTest(context)
        editPortGame.setText(
            if (SharedPreferencesManager.getPortGame(context)
                    .isNullOrEmpty()
            ) "" else SharedPreferencesManager.getPortGame(context)
        )
        editPortChat.setText(
            if (SharedPreferencesManager.getPortChat(context)
                    .isNullOrEmpty()
            ) "" else SharedPreferencesManager.getPortChat(context)
        )
        editUrlLiveStream.setText(
            if (SharedPreferencesManager.getUrlStream(context)
                    .isNullOrEmpty()
            ) "" else SharedPreferencesManager.getUrlStream(context)
        )
        editBaseUrl.setText(
            if (SharedPreferencesManager.getBaseUrlGame(context)
                    .isNullOrEmpty()
            ) "" else SharedPreferencesManager.getBaseUrlGame(context)
        )

        editChannelId.setText(
            if (SharedPreferencesManager.getChannelID(context)
                    .isNullOrEmpty()
            ) "" else SharedPreferencesManager.getChannelID(context)
        )
    }

    fun initControl() {
        buttonConfirm?.setOnClickListener {
            SharedPreferencesManager.saveIsTest(context, checkTest.isChecked)
            if (!editPortGame.text.toString().isEmpty()) {
                SharedPreferencesManager.savePortGame(context, editPortGame.text.toString().trim())
            }
            if (!editUrlLiveStream.text.toString().isEmpty()) {
                SharedPreferencesManager.saveUrlStream(
                    context,
                    editUrlLiveStream.text.toString().trim()
                )
            }
            if (!editPortChat.text.toString().isEmpty()) {
                SharedPreferencesManager.savePortChat(context, editPortChat.text.toString().trim())
            }
            if (!editBaseUrl.text.toString().isEmpty()) {
                SharedPreferencesManager.saveBaseURL(context, editBaseUrl.text.toString().trim())
            }
            if (!editChannelId.text.toString().isEmpty()) {
                SharedPreferencesManager.saveChannelID(
                    context,
                    editChannelId.text.toString().trim()
                )
                ConstantTT.CHANEL_ID = editChannelId.text.toString().trim()
            }
            onResultListener?.onResult(true, "")
            dismiss()
        }
        buttonBack?.setOnClickListener {
            dismiss()
        }
        buttonServer.setOnClickListener {
            val item = ServiceUtilTT.itemInteractiveConfig
            if (item == null) {
                GeneralUtils.showToast(activity, "Không có dư liệu")
                return@setOnClickListener
            }
            editPortGame.setText(
                if (item.portSocket.isNullOrEmpty()) "" else item.portSocket
            )
            editPortChat.setText(
                if (item.portSupport.isNullOrEmpty()) "" else item.portSupport
            )
            editUrlLiveStream.setText(
                if (item.urlLivestream.isNullOrEmpty()) "" else item.urlLivestream
            )
            editBaseUrl.setText(
                if (item.baseUrl.isNullOrEmpty()) "" else item.baseUrl
            )
        }
        buttonUser.setOnClickListener {
            editPortGame.setText(
                if (SharedPreferencesManager.getPortGame(context)
                        .isNullOrEmpty()
                ) "" else SharedPreferencesManager.getPortGame(context)
            )
            editPortChat.setText(
                if (SharedPreferencesManager.getPortChat(context)
                        .isNullOrEmpty()
                ) "" else SharedPreferencesManager.getPortChat(context)
            )
            editUrlLiveStream.setText(
                if (SharedPreferencesManager.getUrlStream(context)
                        .isNullOrEmpty()
                ) "" else SharedPreferencesManager.getUrlStream(context)
            )
            editBaseUrl.setText(
                if (SharedPreferencesManager.getBaseUrlGame(context)
                        .isNullOrEmpty()
                ) "" else SharedPreferencesManager.getBaseUrlGame(context)
            )

            editChannelId.setText(
                if (SharedPreferencesManager.getChannelID(context)
                        .isNullOrEmpty()
                ) "" else SharedPreferencesManager.getChannelID(context)
            )
        }
        buttonDefault.setOnClickListener {
            editPortGame.setText("wss://api.daugiatruyenhinh.com:8088/interactive?")
            editPortChat.setText("wss://api.daugiatruyenhinh.com:8088/interactive?")
//            editUrlLiveStream.setText("rtmp://rtmp.mediatech.vn/mdtvlive/group4toadam?vhost=__defaultVhost__")
            editUrlLiveStream.setText("rtmp://lms-public-ingest.swiftfederation.com/mdtvlive/group4toadam")
            editBaseUrl.setText("http://api.daugiatruyenhinh.com/")
            editChannelId.setText(ConstantTT.CHANEL_ID)
        }
    }


    override fun onDismiss(dialog: DialogInterface) {
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

}
package vn.mediatech.istudiocafe.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_main_tt.*
import kotlinx.android.synthetic.main.fragment_notification.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnScrolledRecyckerViewListener

class NotificationFragment : BottomSheetDialogFragment() {
    lateinit var bottomSheetDialog: BottomSheetDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setOnShowListener {
            /*val bottomSheetInternal: View? = (it as BottomSheetDialog).findViewById(R.id.design_bottom_sheet)
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetInternal!!)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.isDraggable = false
            bottomSheetBehavior.skipCollapsed = true*/
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initUI()
        initControl()
    }

    fun initUI() {
        val fragment = NewsFragment()
        val bundle = Bundle()
        bundle.putInt(Constant.TYPE, Constant.TYPE_NOTIFICATION)
        fragment.arguments = bundle
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, fragment)
        fragmentTransaction.commit()
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            fragment.checkRefresh()
        }, 200)

        fragment.onScrolledListener = object : OnScrolledRecyckerViewListener {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager: StaggeredGridLayoutManager = recyclerView.layoutManager as StaggeredGridLayoutManager
                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
                bottomSheetDialog.behavior.isDraggable = if (firstPos < 0 || firstPos == 0) true else false
            }
        }
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            dismiss()
        }
    }
}
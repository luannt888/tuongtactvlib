package vn.mediatech.istudiocafe.fragment

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.text.method.HideReturnsTransformationMethod
import android.text.method.KeyListener
import android.text.method.PasswordTransformationMethod
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.activity_main_tt.*
import kotlinx.android.synthetic.main.fragment_tab_person_tt.*
import kotlinx.android.synthetic.main.layout_home_actionbar_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.activity.user.UserActivity
import vn.mediatech.istudiocafe.activity.user.UserEnterNewPasswordActivity
import vn.mediatech.istudiocafe.activity.user.UserLoginActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.*
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.shareUser.ListUserShareFragment
import vn.mediatech.istudiocafe.util.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class TabPersonFragment : Fragment() {
    private var loadingDialog: Dialog? = null
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var popupMenuChangeAvatar: PowerMenu? = null
    var popupMenuChangeGender: PowerMenu? = null
    var currentKeyListenerFullName: KeyListener? = null
    var currentKeyListenerAddress: KeyListener? = null
    var myHttpRequestUploadImage: MyHttpRequest? = null
    var myHttpRequestUpdateProfile: MyHttpRequest? = null
    var itemUser: ItemUser? = null
    lateinit var fileCompressor: FileCompressor
    var onShowListener: OnShowListener? = null
    var onCreateViewListener: OnCreateViewListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tab_person_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isViewCreated = true
        onShowListener?.onViewCreated()
        onCreateViewListener?.onCreated()
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        if ((activity is MainActivity && (activity as MainActivity).isTabPersonFragmentShowing) || activity is UserActivity) {
            buttonBack.visibility = View.VISIBLE
        } else {
            buttonBack.visibility = View.GONE
        }
    }

    fun initData() {
        fileCompressor = FileCompressor(requireActivity())
        if (activity is BaseActivity) {
            val lp: RelativeLayout.LayoutParams =
                buttonBack.layoutParams as RelativeLayout.LayoutParams
            lp.topMargin = (activity as BaseActivity).getStatusBarHeight()
        }
        currentKeyListenerFullName = textPersonFullName.keyListener
        currentKeyListenerAddress = textPersonAddress.keyListener
//        textPersonFullName.keyListener = null//disable edittable
//        textPersonAddress.keyListener = null//disable edittable
//        textPersonShareId.keyListener = null//disable edittable
        textPersonGender.isEnabled = false
        textPersonBirthday.isEnabled = false
        textPersonAddress.isEnabled = false
        textPersonShareId.isEnabled = false
        textPersonPassword.isEnabled = false
        textPersonBirthday.text = BIRTH_DAY_FORMAT
        initLogin()
    }

    fun initLogin() {
        if (!isViewCreated) {
            return
        }
        itemUser = MyApplication.getInstance().dataManager.itemUser
        textPhone.visibility = View.GONE
        textLogout.visibility = View.GONE
        if (itemUser == null) {
            layoutPersonInfo.visibility = View.GONE
            buttonEdit.visibility = View.GONE
            buttonCancelEdit.visibility = View.GONE
            textLogin.text = getString(R.string.login)
            imageAvatar.setImageResource(R.mipmap.ic_avatar)
        } else {
            textLogin.text = itemUser!!.fullname
            if (!itemUser!!.phone.isNullOrEmpty()) {
                textPhone.visibility = View.VISIBLE
                textPhone.text = itemUser!!.phone
            }
            layoutPersonInfo.visibility = View.VISIBLE
            layoutForumInfo.visibility = View.GONE
            textLogout.visibility = View.VISIBLE
            buttonEdit.visibility = View.VISIBLE
            initPersonInfo()
        }
        if (activity is MainActivity) {
            (activity as? MainActivity)?.updateUIChangeAcount()
        }
    }

    fun initPersonInfo() {
        MyApplication.getInstance()
            .loadImageCircleClearCache(activity, imageAvatar, itemUser!!.avatar, R.mipmap.ic_avatar)
        textPersonPassword.transformationMethod = PasswordTransformationMethod.getInstance()
        buttonShowPassword.setImageResource(R.drawable.ic_show_password)
        textPersonUserName.text = itemUser!!.userNameForum
        textPersonId.text = "" + itemUser!!.id
        textPersonEmail.text = itemUser!!.email
        textPersonFullName.setText(if (itemUser!!.fullname.isNullOrEmpty()) getString(R.string.no_info) else itemUser!!.fullname)
//        textPersonAddress.setText(if (itemUser!!.address.isNullOrEmpty()) getString(R.string.no_info) else itemUser!!.address)
//        textPersonJob.setText(if (itemUser!!.job.isNullOrEmpty()) getString(R.string.no_info) else itemUser!!.job)
        if (!itemUser!!.address.isNullOrEmpty() && !getString(R.string.no_info).equals(itemUser!!.address)) {
            textPersonAddress.setText(itemUser!!.address)
        }
        if (!itemUser!!.job.isNullOrEmpty() && !getString(R.string.no_info).equals(itemUser!!.address)) {
            textPersonJob.setText(itemUser!!.job)
        }
        textPersonBirthday.text =
            if (itemUser!!.birthday.isNullOrEmpty()) getString(R.string.no_info) else itemUser!!.birthday
        textPersonShareId.setText(if (itemUser!!.shareId.isNullOrEmpty()) getString(R.string.no_info) else itemUser!!.shareId)

        textPersonChip.text = "${itemUser!!.chip} chip"
        textUserForum.text = "Tên đăng nhập: ${itemUser!!.userNameForum}"
        textPassForum.text = "Mật khẩu: ${itemUser!!.passwordForum}"
        textPersonPassword.setText("${itemUser!!.passwordForum}")
        val genderId = itemUser!!.gender
        var gender = getString(R.string.no_info)
        if (!genderId.isNullOrEmpty()) {
            gender = genderId
        }
        textPersonGender.text = gender
        /*var gender = itemUser!!.gender
        when (gender) {
            getString(R.string.male) -> genderId = 1
            getString(R.string.female) -> genderId = 2
            getString(R.string.other) -> genderId = 3
        }*/
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            if (activity is MainActivity) {
                if ((activity as MainActivity).isTabPersonFragmentShowing) {
                    (activity as? MainActivity)?.removePersonFragment()
                } else {
                    activity?.onBackPressed()
                }
            } else {
                activity?.onBackPressed()
            }

        }
        textLogin.setOnClickListener {
            if (itemUser != null) {
                return@setOnClickListener
            }
            GeneralUtils.gotoActivityForResult(
                activity,
                UserLoginActivity::class.java,
                Constant.CODE_LOGIN
            )
        }
        textLogout.setOnClickListener {
            GeneralUtils.showDialog(activity,
                true,
                R.string.notification,
                R.string.logout_msg,
                R.string.logout,
                R.string.close,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        itemUser = null
                        MyApplication.getInstance().dataManager.itemUser = null
                        initLogin()
                        GeneralUtils.clearCookies(context)
                        SharedPreferencesManager.saveUser(activity, null, null)
                        Handler(Looper.getMainLooper()).postDelayed(Runnable {
                            FirebaseAuth.getInstance().signOut()
                            if (AccessToken.getCurrentAccessToken() == null) {
                                return@Runnable
                            }
                            GraphRequest(AccessToken.getCurrentAccessToken(),
                                "/me/permissions/",
                                null,
                                HttpMethod.DELETE,
                                {
                                    LoginManager.getInstance().logOut()
                                }).executeAndWait()
                        }, 200)
                    }

                    override fun onRightButtonClick() {}

                })
        }
        layoutPersonChangePassword.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                val bundle = Bundle()
                bundle.putBoolean(Constant.IS_CHANGE_PASSWORD_SCREEN, true)
                GeneralUtils.gotoActivity(
                    activity,
                    UserEnterNewPasswordActivity::class.java,
                    bundle
                )
            }, 200)
        }
        textPersonBirthday.setOnClickListener {
            GeneralUtils.hideKeyboard(activity, textPersonFullName)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showBirthdayPicker()
            }, 200)
        }
        textPersonGender.setOnClickListener {
            GeneralUtils.hideKeyboard(activity, textPersonFullName)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showChangeGenderPopup()
            }, 200)
        }
        imageAvatar.setOnClickListener {
            if (itemUser == null) {
                GeneralUtils.gotoActivityForResult(
                    activity,
                    UserLoginActivity::class.java,
                    Constant.CODE_LOGIN
                )
            } else {
                showChangeAvatarPopup()
            }
        }
        buttonCancelEdit.setOnClickListener {
            initPersonInfo()
            initItemInfoEdittable(false, R.color.gray)
            buttonCancelEdit.visibility = View.GONE
            if (itemUser!!.birthday.isNullOrEmpty()) {
                textPersonBirthday.text = BIRTH_DAY_FORMAT
            }
            buttonEdit.text = getString(R.string.edit_profile)
            GeneralUtils.hideKeyboard(activity, textPersonAddress)
        }
        buttonEdit.setOnClickListener {
            val title = buttonEdit.text.toString()
            val enable = title.equals(getString(R.string.edit_profile))
            val bgEditText =
                R.color.yellow
//                if (title.equals(getString(R.string.edit_profile))) R.color.yellow else R.color.gray
            initItemInfoEdittable(enable, bgEditText)
            if (title.equals(getString(R.string.update))) {
                GeneralUtils.hideKeyboard(activity, textPersonFullName)
                updateProfile()
                return@setOnClickListener
            }
            buttonEdit.text = getString(R.string.update)
            buttonCancelEdit.visibility = View.VISIBLE
        }
        buttonForumInfo.setOnClickListener {
            layoutForumInfo.visibility =
                if (layoutForumInfo.visibility == View.VISIBLE) View.GONE else View.VISIBLE
        }
        buttonShowPassword.setOnClickListener {
            if (textPersonPassword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                buttonShowPassword.setImageResource(R.drawable.ic_hide_password)
                textPersonPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                buttonShowPassword.setImageResource(R.drawable.ic_show_password)
                textPersonPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }

        buttonShareID.setOnClickListener {
            val linkAndroid =
                "https://play.google.com/store/apps/details?id=vn.mediatech.istudiocafe&hl=vi"
            val linkIOS = "https://apps.apple.com/vn/app/istudio/id1540378720?l=vi"
            val str =
//                "Mã giới thiệu của tôi là " + itemUser!!.id + ". Vui lòng nhập mã giới thiệu của tôi vào app TuongTac.tv để nhận quà nhé! Link tải app Android: " + linkAndroid + ", app IOS " + linkIOS
                "Hãy tham gia tương tác cùng tôi tại gameshow Đấu Giá Truyền Hình, một sản phấm của TuongTac.tv. Nhập mã giới thiệu " + itemUser!!.id + " để nhận quà nhé!"
            GeneralUtils.shareApp(activity, str)
        }
        layoutPersonMyVoucher.setOnClickListener {
            GeneralUtils.openMyVoucher(activity)
        }
        layoutPersonShareNewUser.setOnClickListener {
            val linkAndroid =
                "https://play.google.com/store/apps/details?id=vn.mediatech.istudiocafe&hl=vi"
            val linkIOS = "https://apps.apple.com/vn/app/istudio/id1540378720?l=vi"
            val str =
                "Mã giới thiệu của tôi là:" + itemUser!!.id + ". Vui lòng nhập mã giới thiệu của tôi vào app TuongTac.tv để nhận quà nhé!. Link tải app Android: " + linkAndroid + ", app IOS " + linkIOS
            GeneralUtils.shareApp(activity, str)
        }
        layoutPersonListUserShare.setOnClickListener {
            val fragment = ListUserShareFragment()
            fragment.show(childFragmentManager, "")
        }
    }

    fun initItemInfoEdittable(enable: Boolean, bgEditText: Int) {
        textPersonFullName.setTextColor(ContextCompat.getColor(requireActivity(), bgEditText))
        textPersonBirthday.setTextColor(ContextCompat.getColor(requireActivity(), bgEditText))
        textPersonGender.setTextColor(ContextCompat.getColor(requireActivity(), bgEditText))
        textPersonAddress.setTextColor(ContextCompat.getColor(requireActivity(), bgEditText))
        textPersonPassword.setTextColor(ContextCompat.getColor(requireActivity(), bgEditText))
        textPersonJob.setTextColor(ContextCompat.getColor(requireActivity(), bgEditText))
        textPersonShareId.setTextColor(ContextCompat.getColor(requireActivity(), bgEditText))
        textPersonAddress.setHintTextColor(ContextCompat.getColor(requireActivity(), bgEditText))
        textPersonShareId.setHintTextColor(ContextCompat.getColor(requireActivity(), bgEditText))
        textPersonJob.setHintTextColor(ContextCompat.getColor(requireActivity(), bgEditText))

//        if (enable) {
//            textPersonFullName.keyListener = currentKeyListenerFullName
//            textPersonAddress.keyListener = currentKeyListenerAddress
//            textPersonPassword.keyListener = currentKeyListenerFullName
//            textPersonJob.keyListener = currentKeyListenerFullName
//            textPersonShareId.keyListener = currentKeyListenerFullName
//        } else {
//            textPersonFullName.keyListener = null
//            textPersonAddress.keyListener = null
//            textPersonPassword.keyListener = null
//            textPersonJob.keyListener = null
//            textPersonShareId.keyListener = null
//        }
        textPersonBirthday.isEnabled = enable
        textPersonGender.isEnabled = enable
        textPersonPassword.isEnabled = enable
        textPersonAddress.isEnabled = enable
        textPersonJob.isEnabled = enable
        if (itemUser!!.shareId.isNullOrEmpty()) {
            textPersonShareId.isEnabled = enable
        }
    }

    fun updateProfile() {
        if (itemUser == null) {
            initLogin()
            return
        }

        val fullName = textPersonFullName.text.toString().trim()
//        if (fullName.isEmpty()) {
//            textPersonFullName.requestFocus()
//            (activity as BaseActivity).showToast(R.string.msg_fullname_empty)
//            return
//        }
        val address = textPersonAddress.text.toString().trim()
        val gender = textPersonGender.text.toString()
        var birthday = textPersonBirthday.text.toString()
        birthday = if (birthday.equals(BIRTH_DAY_FORMAT)) "" else birthday
        val job = textPersonJob.text.toString()
        val password = textPersonPassword.text.toString().trim()
//        var genderId = ""
//        if (!gender.isEmpty()) {
//            when (gender) {
//                getString(R.string.male) -> genderId = 1
//                getString(R.string.female) -> genderId = 2
//                getString(R.string.other) -> genderId = 3
//            }
//        }

        loadingDialog =
            GeneralUtils.showLoadingDialog(activity, true, null, object : OnCancelListener {
                override fun onCancel(isCancel: Boolean) {
                    myHttpRequestUpdateProfile?.cancel()
                }
            })
        if (myHttpRequestUpdateProfile == null) {
            myHttpRequestUpdateProfile = MyHttpRequest(requireActivity())
        } else {
            myHttpRequestUpdateProfile!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("fullname", fullName)
        requestParams.put("birthday", birthday)
        requestParams.put("address", address)
        requestParams.put("access_token", itemUser!!.accessToken)
        requestParams.put("gender", "$gender")
        requestParams.put("job", "$job")
        requestParams.put("password", "$password")
        requestParams.put("username", "${itemUser!!.userNameForum}")
        ServiceUtilTT.updateUserProfile(
            requireActivity(),
            requestParams,
            object : OnBaseRequestListener {
                override fun onResponse(isSuccess: Boolean, message: String?, response: String?) {
                    requireActivity().runOnUiThread {
                        loadingDialog?.dismiss()
                        if(!message.isNullOrEmpty()){
                        GeneralUtils.showToast(activity, message!!)}
                    }
                    if (isSuccess) {
                        itemUser!!.fullname = fullName
                        itemUser!!.address = address
                        itemUser!!.birthday = birthday
                        itemUser!!.gender = gender
                        itemUser!!.job = job
                        requireActivity().runOnUiThread {
                            buttonCancelEdit.visibility = View.GONE
                            buttonEdit.text = getString(R.string.edit_profile)
                            textLogin.text = fullName
                        }
                    }
                }
            })
    }

    var datePickerDialog: DatePickerDialog? = null
    val BIRTH_DAY_FORMAT = "yyyy-MM-dd"
    fun showBirthdayPicker() {
        if (datePickerDialog != null) {
            datePickerDialog!!.show()
            return
        }
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val date = calendar.get(Calendar.DAY_OF_MONTH)
        datePickerDialog =
            DatePickerDialog(requireActivity(), object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(
                    datePicker: DatePicker?,
                    year: Int,
                    monthOfYear: Int,
                    dayOfMonth: Int
                ) {
                    if (datePicker != null) {
                        if (!datePicker.isShown) {
                            return
                        }
                    }
                    val calendarSelected = Calendar.getInstance()
                    calendarSelected[Calendar.YEAR] = year
                    calendarSelected[Calendar.MONTH] = monthOfYear
                    calendarSelected.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    val sdf = SimpleDateFormat(BIRTH_DAY_FORMAT, Locale.ENGLISH)
                    val dateDisplay = sdf.format(calendarSelected.time)
                    textPersonBirthday.text = dateDisplay
                }

            }, year, month, date)
        val distance = year - 13//from 13 years old and up
        val calendarMaxDate = Calendar.getInstance()
        calendarMaxDate.set(Calendar.YEAR, distance)
        calendarMaxDate.set(Calendar.MONTH, 11)
        calendarMaxDate.set(Calendar.DAY_OF_MONTH, 31)
//        calendarMaxDate.add(Calendar.YEAR, -12)
        val calendarMinDate = Calendar.getInstance()
        calendarMinDate.set(Calendar.YEAR, (distance - 100))// distance from min to max = 100 year
        calendarMinDate.set(Calendar.MONTH, 0)
        calendarMinDate.set(Calendar.DAY_OF_MONTH, 1)
        datePickerDialog!!.datePicker.maxDate = calendarMaxDate.timeInMillis
        datePickerDialog!!.datePicker.minDate = calendarMinDate.timeInMillis
        datePickerDialog!!.show()
    }

    fun showChangeAvatarPopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.select_from_gallery)))
        itemList.add(PowerMenuItem(getString(R.string.select_from_camera)))

        configPopup(
            POPUP_TYPE_AVATAR,
            popupMenuChangeAvatar,
            itemList,
            getString(R.string.change_avatar),
            imageAvatar,
            MenuAnimation.SHOWUP_TOP_LEFT,
            object : OnSimpleListviewListener {
                override fun onItemClick(position: Int, title: String?) {
                    if (title.equals(getString(R.string.select_from_gallery))) {
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                Constant.REQUEST_CODE_GALLERY
                            )
                            return
                        }
                        openGallery()
                        return
                    }
                    if (title.equals(getString(R.string.select_from_camera))) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                arrayOf(Manifest.permission.CAMERA),
                                Constant.REQUEST_CODE_CAMERA
                            )
                            return
                        }
                        openCamera()
                    }
                }
            })
    }

    fun isPopupMenuChangeAvatarShowing(): Boolean {
        if (popupMenuChangeAvatar == null || !popupMenuChangeAvatar!!.isShowing()) {
            return false
        }
        popupMenuChangeAvatar!!.dismiss()
        return true
    }

    fun showChangeGenderPopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.male)))
        itemList.add(PowerMenuItem(getString(R.string.female)))
        itemList.add(PowerMenuItem(getString(R.string.other)))
        configPopup(
            POPUP_TYPE_GENDER,
            popupMenuChangeGender,
            itemList,
            getString(R.string.please_select),
            textPersonGender,
            MenuAnimation.SHOWUP_TOP_RIGHT,
            object : OnSimpleListviewListener {
                override fun onItemClick(position: Int, title: String?) {
                    textPersonGender.text = title
                }
            })
    }

    fun isPopupMenuChangeGenderShowing(): Boolean {
        if (popupMenuChangeGender == null || !popupMenuChangeGender!!.isShowing()) {
            return false
        }
        popupMenuChangeGender!!.dismiss()
        return true
    }

    val POPUP_TYPE_AVATAR = 1
    val POPUP_TYPE_GENDER = 2
    fun configPopup(
        popupType: Int,
        _popupMenu: PowerMenu?,
        itemList: ArrayList<PowerMenuItem>,
        titlePopup: String?,
        viewAnchor: View,
        menuAnimation: MenuAnimation,
        onSimpleListviewListener: OnSimpleListviewListener?
    ) {
        var popupMenu = _popupMenu
        if (popupMenu != null) {
            popupMenu.showAsDropDown(viewAnchor)
            return
        }
        val textView = TextView(activity)
        if (!titlePopup.isNullOrEmpty()) {
            textView.textSize = 17f
            textView.setTextColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.color_primary_dark_tt
                )
            )
            textView.text = titlePopup
            textView.setPadding(25, 15, 15, 15)
            textView.setBackgroundColor(Color.parseColor("#f8f8f8"))
            textView.setTypeface(Typeface.create("sans-serif-medium", Typeface.BOLD))
        } else {
            textView.visibility = View.GONE
        }

        val screenSize = ScreenSize(requireActivity())
        val popupWidth = screenSize.width * 2 / 3
//        val popupHeight = screenSize.height / 2
        popupMenu = PowerMenu.Builder(requireActivity())
            .addItemList(itemList)
            .setAnimation(menuAnimation)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    onSimpleListviewListener?.onItemClick(position, item?.title.toString())
                    popupMenu!!.dismiss()
                }
            })
            .setHeaderView(textView)
            .build()
        popupMenu!!.showAsDropDown(viewAnchor)
        if (popupType == POPUP_TYPE_AVATAR) {
            popupMenuChangeAvatar = popupMenu
        } else if (popupType == POPUP_TYPE_GENDER) {
            popupMenuChangeGender = popupMenu
        }
    }

    fun openGallery() {
//        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(intent, Constant.REQUEST_CODE_GALLERY)
    }

    private fun createImageFile(): File? {
        val timeStamp = SimpleDateFormat("ddMMyyyyHHmmss").format(Date())
        val mFileName = "AVT_" + timeStamp + "_"
        val storageDir: File? =
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(mFileName, ".jpg", storageDir)
    }

    var photoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(
                    requireActivity(), requireActivity().packageName + ".provider",
                    photoFile
                )
                this.photoFile = photoFile
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(intent, Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    fun callPermissionSettings(requestCode: Int) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", requireActivity().packageName, null)
        intent.data = uri
        startActivityForResult(intent, requestCode)
    }

    fun uploadImageToServer(filePath: String) {
        if (myHttpRequestUploadImage == null) {
            myHttpRequestUploadImage = MyHttpRequest(requireActivity())
        } else {
            myHttpRequestUploadImage!!.cancel()
        }
        loadingDialog = GeneralUtils.showLoadingDialog(activity, false, null, null)
        myHttpRequestUploadImage!!.uploadImage(
            ServiceUtilTT.validAPIDGTH(
                context,
                Constant.API_USER_CHANGE_AVATAR
            ), filePath, object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                    requireActivity().runOnUiThread {
                        loadingDialog?.dismiss()
                    }
                }

                override fun onSuccess(statusCode: Int, responseStr: String?) {
                    if (isDetached) {
                        return
                    }
                    requireActivity().runOnUiThread {
                        loadingDialog?.dismiss()
                    }
                    if (responseStr.isNullOrEmpty()) {
                        showDialogUI(getString(R.string.msg_empty_data))
                        return
                    }
                    val responseString = responseStr.trim()
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    if (jsonObject == null) {
                        showDialogUI(getString(R.string.msg_empty_data))
                        return
                    }
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    if (errorCode != Constant.SUCCESS) {
                        showDialogUI(message)
                        return
                    }
                    val avatar = JsonParser.getString(jsonObject, Constant.RESULT)
                    itemUser?.avatar = avatar
                    (activity as? MainActivity)?.updateUIChangeAcount()
                    message?.let {
                        requireActivity().runOnUiThread {
//                    MyApplication.getInstance().loadImage(requireActivity(), imageAvatar, avatar, true)
                            GeneralUtils.showToast(activity, it)
                        }
                    }
                }
            })

        /*val requestOption = RequestOptions().override(500, 500)
        Glide.with(requireActivity()).asBitmap()
                .load(filePath)
                .apply(requestOption)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        Loggers.e("uploadImageToServer", "onResourceReady ${resource.width} x ${resource.height}")
                        MyApplication.getInstance().loadImage(requireActivity(), imageAvatar, resource, true)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }
                })*/
    }

    fun showDialogUI(message: String?) {
        requireActivity().runOnUiThread { GeneralUtils.showDialog(activity, message) }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY, Constant.REQUEST_CODE_CAMERA -> {
                for (permission in permissions) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(),
                            permission
                        )
                    ) {
                        GeneralUtils.showDialog(activity, getString(R.string.msg_accept_permission))
                        break
                    } else {
                        if (ActivityCompat.checkSelfPermission(
                                requireActivity(),
                                permission
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                                openGallery()
                            } else if (requestCode == Constant.REQUEST_CODE_CAMERA) {
                                openCamera()
                            }
                        } else {
                            callPermissionSettings(requestCode)
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            Constant.CODE_LOGIN -> {
                MyApplication.getInstance().dataManager.isNeedRefreshLeftMenu = true
                initData()
                MyApplication.getInstance().dataManager.isNeedRefreshLeftMenu = false
                ServiceManager.setNeedUpdateFcmTokenForUser(requireActivity(), true)
                ServiceManager.updateFcmTokenUser(requireActivity())
            }
            Constant.REQUEST_CODE_GALLERY -> {
                data?.let {
                    val selectedImage = data.data
                    if (selectedImage == null) {
                        return
                    }
                    /*val filePathColumn = arrayOf(MediaStore.MediaColumns.DATA)
                    val cursor = requireActivity().contentResolver
                            .query(selectedImage!!, filePathColumn, null, null, null)
                    if (cursor != null) {
                        cursor.moveToFirst()
                        val columnIndex = cursor
                                .getColumnIndex(filePathColumn[0])
                        val filePath = cursor.getString(columnIndex)
                        cursor.close()
                        uploadImageToServer(filePath)
                    }*/
                    val realPath =
                        fileCompressor.getRealPathFromUri(requireActivity(), selectedImage)
                    if (realPath.isNullOrEmpty()) {
                        return
                    }
                    if (fileCompressor == null && context != null) {
                        fileCompressor = FileCompressor(context)
                    }
                    photoFile = fileCompressor.compressToFile(File(realPath))
                    MyApplication.getInstance()
                        .loadImage(requireActivity(), imageAvatar, photoFile, true)
                    uploadImageToServer(photoFile.toString())
                    photoFile = null
                }
            }
            Constant.REQUEST_CODE_CAMERA -> {
                if (photoFile == null) {
                    return
                }
                photoFile = fileCompressor.compressToFile(photoFile)
                MyApplication.getInstance()
                    .loadImage(requireActivity(), imageAvatar, photoFile, true)
                uploadImageToServer(photoFile.toString())
                photoFile = null
            }
        }
    }

    /*fun getRealPathFromUri(contentUri: Uri): String? {
        var cursor: Cursor? = null
        return try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = requireActivity().getContentResolver().query(contentUri, proj, null, null, null)
            assert(cursor != null)
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(column_index)
        } finally {
            cursor?.close()
        }
    }*/

    override fun onDestroy() {
        myHttpRequestUploadImage?.cancel()
        myHttpRequestUpdateProfile?.cancel()
        super.onDestroy()
    }
}
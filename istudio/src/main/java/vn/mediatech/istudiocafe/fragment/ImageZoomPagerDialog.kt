package vn.mediatech.istudiocafe.fragment

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_image_zoom_pager_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.adapter.ViewPagerAdapter
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemPhoto
import vn.mediatech.istudiocafe.service.MyHttpRequest

class ImageZoomPagerDialog : BottomSheetDialogFragment() {
    val fragmentList: ArrayList<Fragment> = ArrayList()
    var itemList: ArrayList<ItemPhoto>? = ArrayList()
    var myHttpRequest: MyHttpRequest? = null
    var page: Int = 1
    val limit = 20

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
//            requireContext().window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN)
//        } else {
//
//        }
        val view = inflater.inflate(R.layout.fragment_image_zoom_pager_tt, container, false)
       val progressBar =  view.findViewById(R.id.progressBar) as ProgressBar
        progressBar.visibility = View.GONE
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setOnShowListener {
            val bottomSheetInternal: View? = (it as BottomSheetDialog).findViewById(R.id.design_bottom_sheet)
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetInternal!!)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.isDraggable = false
            bottomSheetBehavior.skipCollapsed = true
//            setFullScreen(true)

        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            initUI()
            initData()
            initControl()
        }, 300)
    }

    override fun onCancel(dialog: DialogInterface) {
        dismissDialog()
        super.onCancel(dialog)
    }

    override fun onDismiss(dialog: DialogInterface) {
        dismissDialog()
        super.onDismiss(dialog)
    }

    fun dismissDialog() {
//        setFullScreen(false)
        myHttpRequest?.cancel()
    }

    fun setFullScreen(isFullScreen: Boolean) {
        if (isFullScreen) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                activity?.window?.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN)
            } else {

            }

        } else {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            } else {

            }
        }
    }

    fun initUI() {
//        progressBar.visibility = View.GONE
        MyApplication.getInstance().loadImageNoPlaceHolder(activity, layoutFragmentRoot, R.drawable.bg_main_tt)
    }

    fun initData() {
        itemList = arguments?.getParcelableArrayList(Constant.DATA)
        if (itemList == null) {
            dismiss()
            return
        }
        initPager()
    }

    fun showErrorNetwork(message: String) {
        activity?.runOnUiThread {
            if (itemList!!.size <= 0) {
                textNotify.visibility = View.VISIBLE
                textNotify.text = message
            }
            progressBar.visibility = View.GONE
        }
    }


    fun initPager() {
        val tabTitleList: ArrayList<String> = ArrayList()
        for (i in 0 until itemList!!.size) {
            val item = itemList!!.get(i)
            if (item.image.isNullOrEmpty()) {
                continue
            }
            val fragment = ImageZoomFragment()
            val bundle = Bundle()
            bundle.putParcelable(Constant.DATA, item)
            fragment.arguments = bundle
            fragmentList.add(fragment)
        }
        activity?.runOnUiThread {
            if (fragmentList!!.size == 1) {
                buttonNext.visibility = View.GONE
                buttonPrevios.visibility = View.GONE
            }
            val viewPagerAdapter = ViewPagerAdapter(childFragmentManager, fragmentList, tabTitleList)
            viewPager.adapter = viewPagerAdapter
            viewPager.offscreenPageLimit = fragmentList.size
            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    handleOnPageSelected(position)
                }

                override fun onPageScrollStateChanged(state: Int) {
                }
            })
//            buttonPrevios.visibility = View.VISIBLE
//            buttonNext.visibility = View.VISIBLE
            handleOnPageSelected(0)
        }
    }

    var selectedFragment: ImageZoomFragment? = null
    fun handleOnPageSelected(position: Int) {
        selectedFragment?.resetZoom()
        selectedFragment = fragmentList.get(position) as ImageZoomFragment

        buttonPrevios.isEnabled = !(position == 0)
        buttonNext.isEnabled = !(position == fragmentList.size - 1)

        if (buttonPrevios.isEnabled) {
            buttonPrevios.clearColorFilter()
        } else {
            buttonPrevios.setColorFilter(ContextCompat.getColor(requireContext(), R.color.gray), android.graphics.PorterDuff.Mode.SRC_ATOP);
        }
        if (buttonNext.isEnabled) {
            buttonNext.clearColorFilter()
        } else {
            buttonNext.setColorFilter(ContextCompat.getColor(requireContext(), R.color.gray), android.graphics.PorterDuff.Mode.SRC_ATOP);
        }
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            dismiss()
        }
        buttonPrevios.setOnClickListener {
            viewPager.currentItem = viewPager.currentItem - 1
        }
        buttonNext.setOnClickListener {
            viewPager.currentItem = viewPager.currentItem + 1
        }
    }
}
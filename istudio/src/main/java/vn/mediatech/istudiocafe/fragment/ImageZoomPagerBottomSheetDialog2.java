package vn.mediatech.istudiocafe.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.adapter.ViewStatePagerAdapter;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.MyApplication;

public class ImageZoomPagerBottomSheetDialog2 extends BottomSheetDialogFragment {
    private View rootView;
    private ViewPager viewPager;
    private ArrayList<Fragment> fragmentList;
    private ImageView buttonBack;
    private DialogInterface.OnDismissListener onDismissListener;
    private boolean hasDismiss;

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    private void dismiss(DialogInterface dialog) {
        if (hasDismiss) {
            return;
        }
        hasDismiss = true;
        if (onDismissListener != null) {
            setFullScreen(false);
            onDismissListener.onDismiss(dialog);
        }
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        dismiss(dialog);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        dismiss(dialog);
    }

    public boolean isShowing() {
        return getDialog() != null && getDialog().isShowing();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_image_zoom_pager_tt, container, false);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);//hide statusbar
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
                View bottomSheetInternal = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetInternal);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetBehavior.setDraggable(false);
                bottomSheetBehavior.setSkipCollapsed(true);
                setFullScreen(true);
            }
        });
        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (onDismissListener != null) {
                    onDismissListener.onDismiss(dialogInterface);
                }
            }
        });
        initData();
    }

    private void setFullScreen(boolean isFullScreen) {
        if (isFullScreen) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    private void initData() {
        viewPager = rootView.findViewById(R.id.viewPager);
        buttonBack = rootView.findViewById(R.id.buttonBack);
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        String currentImage = bundle.getString(Constant.IMAGE);
        ArrayList<String> imageList = bundle.getStringArrayList(Constant.DATA);
        if (imageList == null || imageList.size() == 0 || MyApplication.getInstance().isEmpty(currentImage)) {
            return;
        }
        int position = 0;
        fragmentList = new ArrayList<>();
        for (int i = 0; i < imageList.size(); i++) {
            if (imageList.get(i).equals(currentImage)) {
                position = i;
            }
            ImageZoomFragment fragment = new ImageZoomFragment();
            Bundle bundleImage = new Bundle();
            bundleImage.putString(Constant.DATA, imageList.get(i));
            fragment.setArguments(bundleImage);
            fragmentList.add(fragment);
        }
        ViewStatePagerAdapter adapter = new ViewStatePagerAdapter(getChildFragmentManager(), fragmentList, null);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position, false);
        setTextCurrentPosition(position);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTextCurrentPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void setTextCurrentPosition(int position) {
    }
}

package vn.mediatech.istudiocafe.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_book_event.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.DetailEventActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnCancelListener
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.TextUtil
import java.util.*

class BookEventFragment : Fragment() {
    var isLoading: Boolean = false
    var myHttpRequest: MyHttpRequest? = null
    var preOrientation = 0
    var itemUser: ItemUser? = null
    var itemObj: ItemNews? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        preOrientation = resources.configuration.orientation
        return inflater.inflate(R.layout.fragment_book_event, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
    }

    fun initData() {
        itemObj = arguments?.getParcelable(Constant.DATA)
        if (itemObj == null) {
            (activity as DetailEventActivity).removeBookEventFragment()
            return
        }
        setData()
    }

    fun setData() {
        itemUser = MyApplication.getInstance().dataManager.itemUser
        textPersonFullName.text = itemUser!!.fullname
        textPersonEmail.text = itemUser!!.email
        textPersonPhone.text = itemUser!!.phone
        textPersonAddress.text = itemUser!!.address

        var price = TextUtil.formatMoney(itemObj!!.basePrice)
        price = String.format(Locale.ENGLISH, getString(R.string.ticket_price), price)
//        textPrice.text = price
        textPriceInfo.animateText(price)
        checkQuantity(0)
    }

    fun checkQuantity(number: Int) {
        var quantity = textQuantity.text.toString().toInt()
        quantity += number
        val minQuantity = 1
//        if (quantity < minQuantity || quantity > itemObj!!.maxQuantity!!) {
        if (quantity < minQuantity || quantity > 20) {
            return
        }
//        textQuantity.text = "$quantity"
        textQuantity.animateText("$quantity")
        val totalPrice = itemObj!!.basePrice * quantity
//        textTotalPrice.text = TextUtil.formatMoney(totalPrice)
        textTotalPrice.animateText(TextUtil.formatMoney(totalPrice))
    }

    fun buyEvent() {
        getData()
    }

    fun showErrorNetwork(message: String) {
        activity?.runOnUiThread {
            (activity as BaseActivity).showDialog(message)
        }
    }

    fun getData() {
        if (isLoading) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
        activity?.runOnUiThread {
            (activity as BaseActivity).showLoadingDialog(true, R.string.msg_progress_waiting, object : OnCancelListener {
                override fun onCancel(isCancel: Boolean) {
                    myHttpRequest?.cancel()
                }
            })
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val quantity = textQuantity.text.toString().toInt()
        val totalPrice = itemObj!!.basePrice * quantity

        val requestParams = RequestParams()
        requestParams.put("event_id", "${itemObj!!.id}")
        requestParams.put("quantity", "$quantity")
        requestParams.put("price", "${itemObj!!.basePrice}")
        requestParams.put("total_amount", "$totalPrice")
        requestParams.put("access_token", itemUser!!.accessToken)
        myHttpRequest!!.request(true, ServiceUtilTT.validAPI(Constant.API_EVENT_BOOKING), requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                activity?.runOnUiThread { (activity as BaseActivity).hideLoadingDialog() }
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread { (activity as BaseActivity).hideLoadingDialog() }
                handleData(responseString)
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
        activity?.runOnUiThread {
            (activity as BaseActivity?)?.showDialog(false, message, object : OnDialogButtonListener {
                override fun onLeftButtonClick() {
                    if (errorCode == Constant.SUCCESS) {
                        (activity as DetailEventActivity).removeBookEventFragment()
                    }
                }

                override fun onRightButtonClick() {
                }
            })
        }
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            (activity as DetailEventActivity).removeBookEventFragment()
        }
        buttonIncrease.setOnClickListener {
            checkQuantity(1)
        }
        buttonDecrease.setOnClickListener {
            checkQuantity(-1)
        }
        buttonOrder.setOnClickListener {
            buyEvent()
        }
    }

    override fun onDestroy() {
        myHttpRequest?.cancel()
        super.onDestroy()
    }
}
package vn.mediatech.istudiocafe.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.rtmp.RtmpDataSource;
import com.google.android.exoplayer2.extractor.ts.DefaultTsPayloadReaderFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.DefaultHlsExtractorFactory;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.DefaultTimeBar;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoSize;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;

import vn.goplay.ytbextractor.VideoMeta;
import vn.goplay.ytbextractor.YoutubeExtractor;
import vn.goplay.ytbextractor.YtFile;
import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.activity.MainActivity;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.Loggers;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnPlayerControllerListener;
import vn.mediatech.istudiocafe.listener.OnPlayerStateChangeListener;
import vn.mediatech.istudiocafe.model.ItemSimpleParam;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.util.MyDialog;

@SuppressLint("SourceLockedOrientationActivity")
public class PlayerFragment extends Fragment implements Player.Listener,
        PlayerControlView.VisibilityListener {
    private String type, linkPlay = "", linkPlayFirst = "";
    private View rootView;
    private ProgressBar progressBar;
    private ImageButton buttonFullScreen;
    private DefaultTimeBar timeBar;
    private PlayerView playerView;
    //    private FrameLayout frameWebview;
    private RtmpDataSource.Factory rtmpDataSourceFactory = new RtmpDataSource.Factory();
    private DataSource.Factory mediaDataSourceFactory;
    private SimpleExoPlayer player;
    private DefaultTrackSelector trackSelector;
    private static final CookieManager DEFAULT_COOKIE_MANAGER;
    private boolean isTabSelected = false, isLiveStream = false;
    private OnFullScreenButtonListener onFullScreenButtonListener;
    private boolean isAutoPlay, playWhenReady = true;
    private int heightPlayer = 0;
    private boolean isPausedFragment;
    private long startPosition = -1;
    private OnPlayerStateChangeListener onPlayerStateChangeListener;

    public String getImageThumbnailAudioUrl() {
        return imageThumbnailAudioUrl;
    }

    private String imageThumbnailAudioUrl;
    private boolean isAudio, isShowPlaceHolder;
    //    private int mainTabPosition = -1;
    private ImageButton buttonSelectQuality;
    private boolean isPlayerVideoBox;
    private boolean isHideTimebar;
    //    private PlayerView playerViewFullscreen;
    private ImageView buttonPlayCenter;
    private String currentTabTag;
    private RecyclerView recyclerViewListChannelPlayer;

    public boolean isAudio() {
        return isAudio;
    }

    public String getCurrentTabTag() {
        return currentTabTag;
    }

    public void setCurrentTabTag(String currentTabTag) {
        this.currentTabTag = currentTabTag;
    }

    public void setAudio(boolean audio) {
        isAudio = audio;
    }

    public void setOnPlayerStateChangeListener(OnPlayerStateChangeListener onPlayerStateChangeListener) {
        this.onPlayerStateChangeListener = onPlayerStateChangeListener;
    }

    public void setImageThumbnailAudioUrl(String imageThumbnailAudioUrl) {
        this.imageThumbnailAudioUrl = imageThumbnailAudioUrl;
    }

    public void setStartPosition(long startPosition) {
        this.startPosition = startPosition;
    }

    public long getStartPosition() {
        return startPosition;
    }

    public void setTabSelected(boolean tabSelected) {
        isTabSelected = tabSelected;
    }

    public void setLiveStream(boolean liveStream) {
        isLiveStream = liveStream;
        if (timeBar == null) {
            return;
        }
        if (isLiveStream) {
            timeBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
            return;
        }
        timeBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }

    public void showProgressbar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    public void setLinkPlay(String linkPlay) {
        this.linkPlay = linkPlay;
    }

    public void setOnFullScreenButtonListener(OnFullScreenButtonListener onFullScreenButtonListener) {
        this.onFullScreenButtonListener = onFullScreenButtonListener;
    }

    public String getLinkPlay() {
        return linkPlay;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        rootView = inflater.inflate(R.layout.fragment_player_istudio, container, false);
        return rootView;
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(Constant.TAB_TAG, currentTabTag);
        outState.putString(Constant.DATA, linkPlay);
        outState.putString(Constant.YOUTUBE_URL, youtubeUrl);
        outState.putString(Constant.TYPE, type);
        outState.putBoolean(Constant.IS_AUDIO, isAudio);
        outState.putBoolean(Constant.IS_RUN_PLAY, isAutoPlay);
        outState.putBoolean(Constant.PLAY_WHEN_READY, playWhenReady);
        outState.putLong(Constant.TIME_START_POSITION, startPosition);
        outState.putBoolean(Constant.IS_TAB_SELECTED, isTabSelected);
        outState.putBoolean(Constant.IS_LIVE_STREAM, isLiveStream);
        outState.putBoolean(Constant.IS_PLAYER_VIDEO_BOX, isPlayerVideoBox);
        outState.putInt(Constant.FRAME_PLAYER_HEIGHT, heightPlayer);
        outState.putString(Constant.IMAGE, imageThumbnailAudioUrl);
        outState.putBoolean(Constant.IS_HIDE_PLAYER_TIMEBAR, isHideTimebar);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = savedInstanceState != null ? savedInstanceState : getArguments();
        if (bundle == null) {
            return;
        }
//        mainTabPosition = bundle.getInt(Constant.TAB_POSITION);
        currentTabTag = bundle.getString(Constant.TAB_TAG);
        linkPlay = bundle.getString(Constant.DATA);
        linkPlayFirst = bundle.getString(Constant.DATA);
        youtubeUrl = bundle.getString(Constant.YOUTUBE_URL, null);
        type = bundle.getString(Constant.TYPE);
        isAudio = bundle.getBoolean(Constant.IS_AUDIO, false);
        isShowPlaceHolder = bundle.getBoolean(Constant.IS_SHOW_IMG_ART, false);
        isAutoPlay = bundle.getBoolean(Constant.IS_RUN_PLAY);
        playWhenReady = bundle.getBoolean(Constant.PLAY_WHEN_READY, true);
        startPosition = bundle.getLong(Constant.TIME_START_POSITION);
        isTabSelected = bundle.getBoolean(Constant.IS_TAB_SELECTED);
        isLiveStream = bundle.getBoolean(Constant.IS_LIVE_STREAM);
        isPlayerVideoBox = bundle.getBoolean(Constant.IS_PLAYER_VIDEO_BOX, false);
        heightPlayer = bundle.getInt(Constant.FRAME_PLAYER_HEIGHT);
        imageThumbnailAudioUrl = bundle.getString(Constant.IMAGE);
        isHideTimebar = bundle.getBoolean(Constant.IS_HIDE_PLAYER_TIMEBAR, false);
        buttonPlayCenter = rootView.findViewById(R.id.buttonPlayCenter);
        if (isHideTimebar) {
            View viewPlayerControllerBottomShadow =
                    rootView.findViewById(R.id.viewPlayerControllerBottomShadow);
            viewPlayerControllerBottomShadow.setVisibility(View.VISIBLE);
            LinearLayout layoutTimebar = rootView.findViewById(R.id.layoutTimebar);
            layoutTimebar.setVisibility(View.INVISIBLE);
            if (seekBarVolume == null) {
                seekBarVolume = rootView.findViewById(R.id.seekBarVolume);
            }
            seekBarVolume.setVisibility(View.GONE);
            ImageView imageSeekBarVolume = rootView.findViewById(R.id.imageSeekBarVolume);
            imageSeekBarVolume.setVisibility(View.GONE);
            if (buttonPlayCenter != null) {
                buttonPlayCenter.setVisibility(View.VISIBLE);
                buttonPlayCenter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (player != null) {
                            boolean isSelected = !buttonPlayCenter.isSelected();
                            player.setPlayWhenReady(isSelected);
                            buttonPlayCenter.setSelected(isSelected);
                        }
                    }
                });
            }
        }
        /*if (MyApplication.getInstance().isEmpty(type)) {
            return rootView;
        }*/
        if (startPosition < 0) {
            startPosition = 0;
        }
        initUI();
        initFullscreenDialog();
        if (isAutoPlay) {
            initData();
        }
        initControl();
        initVolumeControl();
        initQuality();

        //START: TEST NEW
        /*playerViewFullscreen = new PlayerView(getActivity());
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams
        .MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        playerViewFullscreen.setLayoutParams(layoutParams);
        playerViewFullscreen.setVisibility(View.GONE);
        playerViewFullscreen.setBackgroundColor(Color.BLACK);*/
        //END: TEST NEW
    }

    public RecyclerView getRecyclerViewListChannelPlayer() {
        return recyclerViewListChannelPlayer;
    }

    private void initUI() {
//        frameWebview = rootView.findViewById(R.id.frameWebview);
        recyclerViewListChannelPlayer = rootView.findViewById(R.id.recyclerViewListChannelPlayer);
        playerView = rootView.findViewById(R.id.playerView);
//        playerView.getVideoSurfaceView().
        progressBar = rootView.findViewById(R.id.progressBar);
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }
        buttonFullScreen = rootView.findViewById(R.id.buttonFullScreen);
        timeBar = rootView.findViewById(R.id.exo_progress);
        if (isLiveStream) {
            timeBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
        buttonSelectQuality = rootView.findViewById(R.id.buttonSelectQuality);
        loadImageArtwork();
        showImagePlacholder(true);
//        playLink();
        /*if (isPlayerVideoBox) {
            LinearLayout layoutSeekbarVolume = rootView.findViewById(R.id.layoutSeekbarVolume);
            RelativeLayout.LayoutParams lpSeekbarVolume = (RelativeLayout.LayoutParams)
            layoutSeekbarVolume.getLayoutParams();
            lpSeekbarVolume.setMarginStart(getResources().getDimensionPixelOffset(R.dimen
            .dimen_50));
            layoutSeekbarVolume.setLayoutParams(lpSeekbarVolume);
        }*/
//        LinearLayout layoutSeekbarVolume = rootView.findViewById(R.id.layoutSeekbarVolume);
//        layoutSeekbarVolume.setRotation(-90);
    }

    public void loadImageArtwork() {
        if (getActivity() == null) {
            return;
        }
        if ((isAudio) && !MyApplication.getInstance().isEmpty(imageThumbnailAudioUrl)) {
//            playerView.setUseArtwork(false);
//            playerView.setDefaultArtwork(new ColorDrawable(Color.TRANSPARENT));
            playerView.setUseArtwork(true);
            MyApplication.getInstance().loadImageListener(getActivity(), imageThumbnailAudioUrl,
                    new MyApplication.OnLoadImageListener() {
                        @Override
                        public void onResourceReady(Drawable resource) {
                            if (playerView != null) {
                                playerView.setDefaultArtwork(resource);
                                ImageView artworkThumbnail =
                                        playerView.findViewById(R.id.exo_artwork);
                                if (artworkThumbnail != null) {
                                    artworkThumbnail.setVisibility(View.VISIBLE);
                                    artworkThumbnail.setImageDrawable(resource);
                                }
                            }
                        }
                    });
        } else {
            playerView.setUseArtwork(false);
        }
    }

    private ImageView imgPlacholder;
    private Drawable imgPlacholderDrawable;

    public void setAndLoadImagePlacholder(String urlImage) {
        if (MyApplication.getInstance().isEmpty(urlImage) || getActivity() == null) {
            return;
        }
        isShowPlaceHolder = true;
        this.imageThumbnailAudioUrl = urlImage;
        MyApplication.getInstance().loadImageListenerNoPlaceholderNocache(getActivity(), imageThumbnailAudioUrl,
                new MyApplication.OnLoadImageListener() {
                    @Override
                    public void onResourceReady(Drawable resource) {
                        imgPlacholderDrawable = resource;
                    }
                });
    }

    public void showImagePlacholder(boolean isShow) {
        if (getActivity() == null) {
            return;
        }
        imgPlacholder = rootView.findViewById(R.id.img_placeholder);
        if (imgPlacholder == null) {
            return;
        }
        if (!isShow) {
            imgPlacholder.setVisibility(View.GONE);
            return;
        }
        if ((isShowPlaceHolder) && !MyApplication.getInstance().isEmpty(imageThumbnailAudioUrl)) {

            if (MyApplication.getInstance().isNetworkConnect()) {
                MyApplication.getInstance().loadImageListenerNoPlaceholderNocache(getActivity(), imageThumbnailAudioUrl,
                        new MyApplication.OnLoadImageListener() {
                            @Override
                            public void onResourceReady(Drawable resource) {
                                imgPlacholderDrawable = resource;
                                if (getActivity() == null || getActivity().isDestroyed() || imgPlacholder == null || getActivity().isFinishing()) {
                                    return;
                                }
                                getActivity().runOnUiThread(() -> {
                                            imgPlacholder.setImageDrawable(resource);
                                            imgPlacholder.setVisibility(View.VISIBLE);
                                        }
                                );
                            }
                        });

            } else {
                if (imgPlacholderDrawable != null) {
                    imgPlacholder.setImageDrawable(imgPlacholderDrawable);
                    imgPlacholder.setVisibility(View.VISIBLE);
                }
            }
//            MyApplication.getInstance().loadImageNoPlaceHolderNoCache(getContext(), imgPlacholder, imageThumbnailAudioUrl);

        } else {
            imgPlacholder.setVisibility(View.GONE);
        }
    }

    private MyDialog dialogQuality;
    private ArrayList<ItemSimpleParam> itemLinkList;

    public void setItemLinkList(ArrayList<ItemSimpleParam> itemLinkList) {
        this.itemLinkList = itemLinkList;
    }

    private void initQuality() {
//        buttonSelectQuality.setVisibility(View.VISIBLE);
        /*buttonSelectQuality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*if (dialogQuality != null) {
                    dialogQuality.show();
                    return;
                }*//*
                if (itemLinkList == null || itemLinkList.size() == 0) {
//                    buttonSelectQuality.setVisibility(View.GONE);
                    return;
                }
                dialogQuality =
                        ((BaseActivity) getActivity()).showQuality(getString(R.string
                        .select_quality), itemLinkList, 0, new OnItemDialogClickListener() {
                    @Override
                    public void onClick(ItemSimpleParam itemSimpleParam, int selectedPosition) {
                        releasePlayer();
                        linkPlay = itemSimpleParam.getValue();
                        playLink();
                    }
                });
                dialogQuality.show();
            }
        });*/
//        if (itemLinkList == null || itemLinkList.size() == 0) {
//            buttonSelectQuality.setVisibility(View.GONE);
//        }
    }

    private void initData() {
        /*mediaDataSourceFactory = buildDataSourceFactory(true);
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }*/
        if (isTabSelected) {
//            playLink();
            checkPlayLink();
        }
    }

    private SeekBar seekBarVolume;

    public void checkSeekBarVolumeProgress() {
        if (seekBarVolume != null) {
            AudioManager audioManager =
                    (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            seekBarVolume.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            int volumeLevel = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            seekBarVolume.setProgress(volumeLevel);
        }
    }

    private void initVolumeControl() {
        if (seekBarVolume == null) {
            seekBarVolume = rootView.findViewById(R.id.seekBarVolume);
        }
        checkSeekBarVolumeProgress();
        AudioManager audioManager =
                (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        seekBarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int newVolume, boolean b) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, newVolume, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void initializePlayer() {
        playerView.setControllerVisibilityListener(this);
//        playerView.requestFocus();
        AdaptiveTrackSelection.Factory adaptiveTrackSelectionFactory = new AdaptiveTrackSelection.Factory();
        trackSelector = new DefaultTrackSelector(getActivity(), adaptiveTrackSelectionFactory);
        DefaultRenderersFactory renderersFactory = new DefaultRenderersFactory(getActivity());
        player =
                new SimpleExoPlayer.Builder(getActivity(), renderersFactory).setTrackSelector(trackSelector).build();
        player.addListener(this);
        playerView.setUseController(true);
        playerView.setPlayer(player);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
//        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
//        player.setPlayWhenReady(true);
        player.setPlayWhenReady(playWhenReady);
        playWhenReady = true;
    }

    private MediaSource buildMediaSource(Uri uri, String overrideExtension) {
        @C.ContentType int type = Util.inferContentType(uri, overrideExtension);
        MediaItem mediaItem = new MediaItem.Builder().setUri(uri).build();
        switch (type) {
            case C.TYPE_SS:
                return new SsMediaSource.Factory(new DefaultSsChunkSource.Factory(mediaDataSourceFactory), buildDataSourceFactory()).createMediaSource(mediaItem);
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(new DefaultDashChunkSource.Factory(mediaDataSourceFactory), buildDataSourceFactory()).createMediaSource(mediaItem);
            case C.TYPE_HLS:
//                return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
                if (!isAudio) {
                    return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(mediaItem);
                } else {
                    DefaultHlsExtractorFactory defaultHlsExtractorFactory =
                            new DefaultHlsExtractorFactory(DefaultTsPayloadReaderFactory.FLAG_IGNORE_H264_STREAM, true);
                    return new HlsMediaSource.Factory(mediaDataSourceFactory).setExtractorFactory(defaultHlsExtractorFactory).createMediaSource(mediaItem);
                }
//                return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                if (uri.getScheme() != null && uri.getScheme().equals("rtmp")) {
                    return new ProgressiveMediaSource.Factory(rtmpDataSourceFactory).createMediaSource(mediaItem);
                }
                return new ProgressiveMediaSource.Factory(mediaDataSourceFactory).createMediaSource(mediaItem);
            default: {
                return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(mediaItem);
            }
        }
    }

    public DataSource.Factory buildDataSourceFactory() {
//        String exoPlayerUserAgent = Util.getUserAgent(getActivity(), "ExoPlayer");
        DefaultHttpDataSource.Factory factory = new DefaultHttpDataSource.Factory();
        factory.setUserAgent(Constant.WEB_USER_AGENT);
        factory.setConnectTimeoutMs(45000);
        factory.setReadTimeoutMs(45000);
        factory.setAllowCrossProtocolRedirects(true);
        return factory;
    }

    @Override
    public void onVideoSizeChanged(VideoSize videoSize) {
        int resizeMode;
        if (videoSize.width > videoSize.height) {
            resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL;
        } else {
            resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT;
        }
        playerView.setResizeMode(resizeMode);
    }

    @Override
    public void onIsLoadingChanged(boolean isLoading) {
    }

    @Override
    public void onPlaybackStateChanged(int state) {
//        Loggers.e("PlayerFragment_A", "playbackState = " + state);
        switch (state) {
            case Player.STATE_READY:
                /*String wh = player.getVideoFormat().width + " x " + player.getVideoFormat()
                .height;
                Loggers.e("My_STATE_READY", wh + "");*/
                progressBar.setVisibility(View.GONE);
                if (buttonPlayCenter != null) {
                    buttonPlayCenter.setSelected(playWhenReady);
                }
                if (onPlayerStateChangeListener != null) {
                    onPlayerStateChangeListener.onStateReady();
                }
                loadImageArtwork();
                showImagePlacholder(false);
                break;
            case Player.STATE_ENDED:
                if (buttonPlayCenter != null) {
                    buttonPlayCenter.setSelected(false);
                }
                if (onPlayerStateChangeListener != null) {
                    onPlayerStateChangeListener.onStateEnd();
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isLiveStream && !isYoutubeError) {
                            handleGetYoutubeError();
                        }
                    }
                });
//                startPosition = 0;
                break;
            case Player.STATE_BUFFERING:
                progressBar.setVisibility(View.VISIBLE);
                if (buttonPlayCenter != null) {
                    buttonPlayCenter.setSelected(true);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onPlayerError(PlaybackException error) {
        if (isDetached() || !isAdded()) {
            return;
        }
        if (onPlayerStateChangeListener != null) {
            onPlayerStateChangeListener.onPlayerError();
        }
        error.printStackTrace();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showImagePlacholder(true);
                progressBar.setVisibility(View.GONE);
                if (isLiveStream && !isYoutubeError) {
                    handleGetYoutubeError();
                }
            }
        });
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    private OnPlayerControllerListener onPlayerControllerListener;

    public void setOnPlayerControllerListener(OnPlayerControllerListener onPlayerControllerListener) {
        this.onPlayerControllerListener = onPlayerControllerListener;
    }

    @Override
    public void onVisibilityChange(int visibility) {
        if (onPlayerControllerListener != null) {
            if (visibility == View.VISIBLE) {
                onPlayerControllerListener.onShow();
            } else if (visibility == View.GONE) {
                onPlayerControllerListener.onHide();
            }
        }
    }

    private String videoTitle = "";

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public void playLink() {
       /* if (4 % 2 == 0) {
            return;
        }*/
//        linkPlay = "http://cdn.hntv.mediatech.vn/hntvlive/tv1live480.m3u8?vhost=__defaultVhost__";
        if (playerView == null || getActivity() == null || getActivity().isFinishing() || getActivity().isDestroyed() || isDetached()) {
            return;
        }
        if (!isTabSelected) {
            return;
        }
//        linkPlay = "rtmp://rtmp.mediatech.vn/mdtvlive/mdtvlive_1_2?vhost=__defaultVhost__";
        if (MyApplication.getInstance().isEmpty(linkPlay)) {
//            ((BaseActivity) getActivity()).showDialog(getString(R.string.msg_video_not_found));
            return;
        }
        Loggers.e("MY_CHECK_PLAYEFRAGMENT", "B_ " + " isTabselected = " + isTabSelected + " _ " +
                "link=" + linkPlay + "");
//        Loggers.e("My_Check_LiveStreamFrg_playLink", "linkPlay = " + linkPlay);
        if (myHttpRequestYoutube != null && !myHttpRequestYoutube.isHasLoaded()) {
            return;
        }
        if (!isLiveStream && player != null && !player.getPlayWhenReady()) {
            long currentPositionTime = player.getCurrentPosition();
            if (currentPositionTime > 0) {
                player.setPlayWhenReady(true);
                return;
            }
        }
        releasePlayer();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showProgressbar();
            }
        });
        if (myHttpRequestYoutube != null && !myHttpRequestYoutube.isHasLoaded()) {
            return;
        }
        if (player == null) {
            initializePlayer();
        }
        mediaDataSourceFactory = buildDataSourceFactory();
//        MediaSource mediaSource = buildMediaSource(Uri.parse(linkPlay), "");
        MediaSource mediaSource;

        if (!MyApplication.getInstance().isEmpty(linkOnlyVideo) && !MyApplication.getInstance().isEmpty(linkOnlyAudio)) {
            Loggers.e("PlayerFragment_linkOnlyVideoHD", "" + linkOnlyVideo);
            Loggers.e("PlayerFragment_linkOnlyAudioHD", "" + linkOnlyAudio);
            MediaSource videosource = buildMediaSource(Uri.parse(linkOnlyVideo), "");
            MediaSource audiosource = buildMediaSource(Uri.parse(linkOnlyAudio), "");
            mediaSource = new MergingMediaSource(videosource, audiosource);
        } else {
            Loggers.e("PlayerFragment_linkPlay", "" + linkPlay);
            mediaSource = buildMediaSource(Uri.parse(linkPlay), "");
        }

        if (isYoutubeError()) {
//            buttonSelectQuality.setVisibility(View.VISIBLE);
        } else {
//            buttonSelectQuality.setVisibility(View.GONE);
        }

        if (isAutoPlay) {
            player.seekTo(startPosition);
            player.setMediaSource(mediaSource, false);
        } else {
            player.setMediaSource(mediaSource, true);
        }
        player.prepare();
//        playerView.setUseController(true);
        playerView.requestFocus();
    }

    public void pause() {
        if (player != null) {
            Loggers.e("PlayerFrg", "pause");
            player.setPlayWhenReady(false);
        }
    }

    public void releasePlayer() {
        updateStartPosition();
        cancelGetLinkVodYoutube();
        if (player != null) {
            try {
                Loggers.e("PlayerFrg", "releasePlayer");
                player.release();
//                player.seekTo(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            player.clearVideoSurface();
//            playerView.removeAllViewsInLayout();

            player = null;
//                trackSelector = null;
        }
    }

    private Dialog mFullScreenDialog;
    private boolean mExoPlayerFullscreen;

    private void initFullscreenDialog() {
        mFullScreenDialog = new Dialog(getActivity(),
                android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen) {
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    closeFullscreenDialog();
                    if (onFullScreenButtonListener == null) {
                        return;
                    }
                    if (mExoPlayerFullscreen) {
                        onFullScreenButtonListener.onExpand();
                    } else {
                        onFullScreenButtonListener.onMinimize();
                    }
                }
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        /*buttonFullScreen.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.mipmap
        .ic_fullscreen_minimize));
        mExoPlayerFullscreen = true;
        playerView.setVisibility(View.GONE);
        playerViewFullscreen.setVisibility(View.VISIBLE);
        PlayerView.switchTargetView(player, playerView, playerViewFullscreen);
        if (6 % 2 == 0) {
            return;
        }*/
        if (mFullScreenDialog == null) {
            initFullscreenDialog();
        }
        if (playerView == null || buttonFullScreen == null) {
            return;
        }
        ((ViewGroup) playerView.getParent()).removeView(playerView);
        mFullScreenDialog.addContentView(playerView,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        buttonFullScreen.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                R.mipmap.ic_fullscreen_minimize));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
//        int currentTabPosition = ((MainActivity) getActivity()).getCurrentPagerPosition();
//        if (currentTabPosition != Constant.TAB_TV_POSITION && currentTabPosition != Constant
//        .TAB_RADIO_POSITION) {

//        if (getActivity() instanceof DetailVideoActivity || getActivity() instanceof
//        DetailVideoBoxActivity) {
//            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        } else {
        boolean flagHandler = false;
        if (getActivity() instanceof MainActivity) {
            /*String currentTagTab = ((MainActivity) getActivity()).getCurrentTabTag();
            if (currentTagTab == null || !currentTagTab.equals(tabTag)) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else {
                ((MainActivity) getActivity()).setStopAutoRotate(false);
                flagHandler = true;
            }*/
            String currentTabTagMain = ((MainActivity) getActivity()).getCurrentTabTag();
            Loggers.e("PlayerFrg_openFullscreenDialog", currentTabTagMain + " - " + currentTabTag);

            /*int currentTabPosition = ((MainActivity) getActivity()).getCurrentPagerPosition();
            if (currentTabPosition != mainTabPosition) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else {
                ((MainActivity) getActivity()).setStopAutoRotate(false);
                flagHandler = true;
            }*/
        } else {
            flagHandler = true;
        }
        flagHandler = true;
        if (flagHandler) {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() == null) {
                        return;
                    }
                    if (getActivity() instanceof MainActivity) {
                        if (((MainActivity) getActivity()).isStopAutoRotate()) {
                            return;
                        }
                    }
                }
            }, 2000);
        }
    }

    private void closeFullscreenDialog() {
        if (mFullScreenDialog == null) {
            initFullscreenDialog();
        }
        if (playerView == null || buttonFullScreen == null) {
            return;
        }
        ((ViewGroup) playerView.getParent()).removeView(playerView);
        ((FrameLayout) rootView.findViewById(R.id.framePlayer)).addView(playerView);
        mExoPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
        buttonFullScreen.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                R.mipmap.ic_fullscreen_expand));
//        int currentTabPosition = ((MainActivity) getActivity()).getCurrentPagerPosition();
//        if (currentTabPosition != Constant.TAB_TV_POSITION && currentTabPosition != Constant
//        .TAB_RADIO_POSITION) {

        boolean flagHandler = false;
        if (getActivity() instanceof MainActivity) {
            /*String currentTagTab = ((MainActivity) getActivity()).getCurrentTabTag();
            if (currentTagTab == null || !currentTagTab.equals(tabTag)) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                ((MainActivity) getActivity()).setStopAutoRotate(false);
                flagHandler = true;
            }*/
            String currentTabTagMain = ((MainActivity) getActivity()).getCurrentTabTag();
            Loggers.e("PlayerFrg_closeFullscreenDialog", currentTabTagMain + " - " + currentTabTag);
            /*int currentTabPosition = ((MainActivity) getActivity()).getCurrentPagerPosition();
            if (currentTabPosition != mainTabPosition) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                ((MainActivity) getActivity()).setStopAutoRotate(false);
                flagHandler = true;
            }*/
        } else {
            flagHandler = true;
        }
        flagHandler = true;
        if (flagHandler) {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() == null) {
                        return;
                    }
                    if (getActivity() instanceof MainActivity) {
                        if (((MainActivity) getActivity()).isStopAutoRotate()) {
                            return;
                        }
                    }
                }
            }, 2000);
        }
    }

    public interface OnFullScreenButtonListener {
        void onExpand();

        void onMinimize();

        void onScrollToTop();
    }

    private void initControl() {
        buttonFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mExoPlayerFullscreen) {
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    openFullscreenDialog();
                } else {
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    closeFullscreenDialog();
                }
                if (onFullScreenButtonListener == null) {
                    return;
                }
                if (mExoPlayerFullscreen) {
                    onFullScreenButtonListener.onExpand();
                } else {
                    onFullScreenButtonListener.onMinimize();
                }
            }
        });
    }

    public SimpleExoPlayer getPlayer() {
        return player;
    }

    public PlayerView getPlayerView() {
        return playerView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private boolean allowRunBackground = false;

    public void setAllowRunBackground(boolean allowRunBackground) {
        this.allowRunBackground = allowRunBackground;
    }

    @Override
    public void onPause() {
        isPausedFragment = true;
        if (player != null) {
            if (!allowRunBackground) {
                Loggers.e("My_Check_PlayerFrg", "onPause");
                player.setPlayWhenReady(false);
            }
        }
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).removeKeepScreenOn();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        isPausedFragment = false;
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).keepScreenOn();
        }
        super.onResume();
    }

    public void resumePlayer() {
        if (player != null) {
            player.setPlayWhenReady(true);
        }
    }

    private void updateStartPosition() {
        if (player != null) {
//            if (startPosition == -1) {
            startPosition = player.getCurrentPosition();
            long endPosition = player.getDuration();
//            Loggers.e("MY_CHECK_DETAIL_VIDEO_playVideo_updateStartPosition", "startPosition = "
//            + startPosition + " endPosition = " + endPosition);
//            Loggers.e("MY_CHECK_DETAIL_VIDEO_playVideo_updateStartPosition_B", "startPosition =
//            " + startPosition / 1000 + " endPosition = " + endPosition / 1000);
            if (startPosition / 1000 >= endPosition / 1000) {
                startPosition = 0;
            }
//            }
        }
    }

    private String youtubeUrl;
    private MyHttpRequest myHttpRequestYoutube;
    private boolean isYoutubeError;

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public boolean isYoutubeError() {
        return isYoutubeError;
    }

    public void checkPlayLink() {
        linkOnlyVideo = "";
        linkOnlyAudio = "";
        isYoutubeError = true;
        showProgressbar();
        if (MyApplication.getInstance().isEmpty(youtubeUrl)) {
            Loggers.e("PlayerFragment_checkPlayLink", "youtubeUrl empty");
            playLink();
            return;
        }
        Loggers.e("PlayerFragment_checkPlayLink", "youtubeUrl = " + youtubeUrl);
        isYoutubeError = false;
//        buttonSelectQuality.setVisibility(View.GONE);
        if (isLiveStream) {
            getLinkStreamYoutube();
        } else {
            getLinkVodYoutube();
        }
    }

    private void getLinkStreamYoutube() {
        if (myHttpRequestYoutube != null && !myHttpRequestYoutube.isHasLoaded()) {
            return;
        }
        if (myHttpRequestYoutube == null) {
            myHttpRequestYoutube = new MyHttpRequest(getActivity());
        } else {
            myHttpRequestYoutube.cancel();
        }

        myHttpRequestYoutube.request(false, youtubeUrl, null, true,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (getActivity() == null || isDetached()) {
                            return;
                        }
                        handleGetYoutubeError();
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        handleGetLinkStreamYoutube(responseString);
                    }
                });
    }

    private void handleGetLinkStreamYoutube(String responseString) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            handleGetYoutubeError();
            return;
        }
        //START: CHECK YOUTUBE IS LIVE
        String[] isLiveNowArr = responseString.split("\"isLiveNow");
        if (isLiveNowArr.length <= 1) {
            handleGetYoutubeError();
            return;
        }
        String[] isLiveNowArrArr2 = isLiveNowArr[1].split(",");
        String isLiveNow = isLiveNowArrArr2[0];
        isLiveNow = isLiveNow.replace("\\", "");
        isLiveNow = isLiveNow.replace("\":", "");
        isLiveNow = isLiveNow.replace("\"", "");
        isLiveNow = isLiveNow.replace(":", "").trim();
        if (isLiveNow.equals("false")) {
            handleGetYoutubeError();
            return;
        }
        //END: CHECK YOUTUBE IS LIVE

        //START: GET YOUTUBE STREAM LINK
        String[] hlsManifestUrlArr = responseString.split("\"hlsManifestUrl");
        if (hlsManifestUrlArr.length <= 1) {
            handleGetYoutubeError();
            return;
        }
        String[] hlsManifestUrlArr2 = hlsManifestUrlArr[1].split("\"\\}");
        String[] hlsManifestUrlArr3 = hlsManifestUrlArr2[0].split("\",");
        String hlsManifestUrl = hlsManifestUrlArr3[0];
        hlsManifestUrl = hlsManifestUrl.replace("\\", "");
        hlsManifestUrl = hlsManifestUrl.replace("\":\"", "");
        hlsManifestUrl = hlsManifestUrl.replace("\"},", "");
        hlsManifestUrl = hlsManifestUrl.replace("\"", "");
        linkPlay = hlsManifestUrl.trim();
        //END: GET YOUTUBE STREAM LINK

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                buttonSelectQuality.setVisibility(View.GONE);
                if (player != null && !player.getPlayWhenReady()) {
                    return;
                }
                playLink();
            }
        });
    }

    public void handleGetYoutubeError() {
        Loggers.e("PlayerFragment_handleGetLinkVODYoutube", "handleGetYoutubeError");
        showProgressbar();
        isYoutubeError = true;
        youtubeUrl = "";
        if (itemLinkList != null && itemLinkList.size() > 0) {
            linkPlay = itemLinkList.get(0).getValue();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    buttonSelectQuality.setVisibility(View.VISIBLE);
                    if (player != null && !player.getPlayWhenReady()) {
                        return;
                    }
                    playLink();
                }
            });
        } else {
            linkPlay = linkPlayFirst;
            playLink();
        }
    }

    private YoutubeExtractor youTubeExtractor;
    private String linkOnlyVideo = "", linkOnlyAudio = "";

    private void getLinkVodYoutube() {
        linkOnlyVideo = "";
        linkOnlyAudio = "";
        if (youTubeExtractor != null) {
            cancelGetLinkVodYoutube();
        }
        /*youTubeExtractor = new YouTubeExtractor(getActivity()) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(SparseArray<YtFile> ytFiles) {
                super.onPostExecute(ytFiles);
            }

            @Override
            protected void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta videoMeta) {
                handleGetLinkVODYoutube(ytFiles, videoMeta);
            }
        };
        youTubeExtractor.extract(youtubeUrl);*/

        youTubeExtractor = new YoutubeExtractor(requireContext());
        youTubeExtractor.setOnAsyncTaskListener(new YoutubeExtractor.OnAsyncTaskListener() {
            @Override
            public void onExtractionComplete(@org.jetbrains.annotations.Nullable SparseArray<YtFile> ytFiles, @org.jetbrains.annotations.Nullable VideoMeta videoMeta) {
                handleGetLinkVODYoutube(ytFiles, videoMeta);
            }
        });
        youTubeExtractor.start(youtubeUrl);
    }

    private void handleGetLinkVODYoutube(SparseArray<YtFile> ytFiles, VideoMeta videoMeta) {
        if (ytFiles == null || ytFiles.size() == 0) {
            handleGetYoutubeError();
            return;
        }
        int itagHD = 22;// => 1280x720p
        int itagSD = 18;// => 640x360p
        int itagOnlyVideoHD = 136;
        int itagOnlyAudioHD = 140;
        int itagOnlyVideo480 = 135;
        int itagOnlyAudio480 = 140;

        YtFile ytFile = ytFiles.get(itagHD);
        if (ytFile != null) {
            String url = ytFile.getUrl();
            if (!MyApplication.getInstance().isEmpty(url)) {
                linkPlay = url.trim();
                handlePlayLinkYoutube();
                return;
            }
        }

        ytFile = ytFiles.get(itagSD);
        if (ytFile != null) {
            String url = ytFile.getUrl();
            if (!MyApplication.getInstance().isEmpty(url)) {
                linkPlay = url.trim();
                handlePlayLinkYoutube();
                return;
            }
        }

        ytFile = ytFiles.get(itagOnlyVideo480);
        if (ytFile != null) {
            linkOnlyVideo = ytFile.getUrl();
            if (!MyApplication.getInstance().isEmpty(linkOnlyVideo)) {
                linkOnlyVideo = linkOnlyVideo.trim();
                ytFile = ytFiles.get(itagOnlyAudio480);
                if (ytFile != null) {
                    linkOnlyAudio = ytFile.getUrl();
                    if (!MyApplication.getInstance().isEmpty(linkOnlyAudio)) {
                        linkOnlyAudio = linkOnlyAudio.trim();
                        linkPlay = linkOnlyVideo;
                        handlePlayLinkYoutube();
                        return;
                    }
                }
            }
        }

        ytFile = ytFiles.get(itagOnlyVideoHD);
        if (ytFile != null) {
            linkOnlyVideo = ytFile.getUrl();
            if (!MyApplication.getInstance().isEmpty(linkOnlyVideo)) {
                linkOnlyVideo = linkOnlyVideo.trim();
                ytFile = ytFiles.get(itagOnlyAudioHD);
                if (ytFile != null) {
                    linkOnlyAudio = ytFile.getUrl();
                    if (!MyApplication.getInstance().isEmpty(linkOnlyAudio)) {
                        linkOnlyAudio = linkOnlyAudio.trim();
                        linkPlay = linkOnlyVideo;
                        handlePlayLinkYoutube();
                        return;
                    }
                }
            }
        }
        linkOnlyVideo = "";
        linkOnlyAudio = "";
        handleGetYoutubeError();

        /*for (int i = 0; i < ytFiles.size(); i++) {
            YtFile ytFile = ytFiles.valueAt(i);
            if (ytFile == null) {
                continue;
            }
            String url = ytFile.getUrl();
            Format format = ytFile.getFormat();
            String fm = "itag = " + format.getItag() + " _ getExt = " + format.getExt() + " _
            getAudioBitrate = " + format.getAudioBitrate() + " _ getAudioCodec = " + format
            .getAudioCodec() + " _ getFps = " + format.getFps() + " _ getHeight = " + format
            .getHeight() + " _ getVideoCodec = " + format.getVideoCodec();
            Loggers.e("My_test_" + i, "----------------");
            Loggers.e("My_test_" + i, "url = " + url);
            Loggers.e("My_test_" + i, "" + fm);
        }*/
    }

    private void handlePlayLinkYoutube() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                buttonSelectQuality.setVisibility(View.GONE);
                if (player != null && !player.getPlayWhenReady()) {
                    return;
                }
                playLink();
            }
        });
    }

    public void cancelGetLinkVodYoutube() {
        if (youTubeExtractor != null) {
            try {
                youTubeExtractor.cancel(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (getActivity() == null) {
            return;
        }
        boolean isLandscape =
                getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        if (getActivity() instanceof MainActivity) {
            String currentTabTagMain = ((MainActivity) getActivity()).getCurrentTabTag();
            Loggers.e("PlayerFrg_onConfigChange", currentTabTagMain + " - " + currentTabTag);
            if (currentTabTagMain == null || !currentTabTagMain.equals(currentTabTag)) {
                return;
            }
            /*String currentTagTab = ((MainActivity) getActivity()).getCurrentTabTag();
            if (currentTagTab == null || !currentTagTab.equals(tabTag)) {
//            Loggers.e("onConfigurationChanged_B", currentTagTab + " = " + tabTag);
                return;
            }*/
            /*int currentTabPosition = ((MainActivity) getActivity()).getCurrentPagerPosition();
            if (currentTabPosition != mainTabPosition) {
                return;
            }*/
        }
//        Loggers.e("onConfigurationChanged_C", currentTagTab + " = " + tabTag);
        if (isLandscape) {
            openFullscreenDialog();
        } else {
            closeFullscreenDialog();
        }
    }

    @Override
    public void onDetach() {
        releasePlayer();
        if (myHttpRequestYoutube != null) {
            myHttpRequestYoutube.cancel();
        }
        super.onDetach();
    }
}
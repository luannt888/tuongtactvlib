package vn.mediatech.istudiocafe.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.glide.slider.library.SliderLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main_tt.*
import kotlinx.android.synthetic.main.fragment_tab_home_tt.*
import kotlinx.android.synthetic.main.fragment_tab_home_tt.progressBar
import kotlinx.android.synthetic.main.layout_header_recycler_view_tt.view.*
import kotlinx.android.synthetic.main.my_slider_view.view.*
import org.json.JSONObject
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.GameActivity
import vn.mediatech.istudiocafe.activity.LiveStreamActivity
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.activity.user.UserLoginActivity
import vn.mediatech.istudiocafe.adapter.ItemWebGameAdapter
import vn.mediatech.istudiocafe.adapter.ItemWinnerAdapter
import vn.mediatech.istudiocafe.adapter.NewsAdapter
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnCancelListener
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemPhoto
import vn.mediatech.istudiocafe.model.ItemWebGame
import vn.mediatech.istudiocafe.model.ItemWiner
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.voicecontrol.model.ItemCase
import vn.mediatech.voicecontrol.model.ItemCaseAnswer
import java.text.DecimalFormat
import java.text.NumberFormat

class TabHomeFragment : Fragment() {
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var isLoading: Boolean = false
    var myHttpRequestSlide: MyHttpRequest? = null
    var myHttpRequest: MyHttpRequest? = null
    var isOnPause = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tab_home_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isViewCreated = true
        if (savedInstanceState != null) {
            init()
        }
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        layoutRefresh.setColorSchemeColors(
            ContextCompat.getColor(
                requireActivity(),
                R.color.bg_color_actionbar_tt
            )
        )
    }

    fun initData() {
//        getSlideData()
//        getDataWiner()
        getData()
    }

    fun showErrorNetwork(message: String) {
        activity?.runOnUiThread {
            textNotify.text = message
            textNotify.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            isLoading = false
        }
    }


    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
        progressBar.visibility = View.VISIBLE
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }

        myHttpRequest!!.request(
            false,
            ServiceUtilTT.validAPITT(context, Constant.API_APP_HOME),
            null, object :
//                ServiceUtilTT.validAPIDGTH(context, Constant.API_APP_HOME),
//                null, object :
                MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                    isLoading = false
                    activity?.runOnUiThread {
                        showErrorNetwork(getString(R.string.msg_network_error))
                    }
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    if (isDetached) {
                        return
                    }
                    activity?.runOnUiThread { progressBar.visibility = View.GONE }
                    isLoading = false
                    handleData(responseString)
                }
            })
    }


    val KEY_HANDLE_SLIDER = "KEY_HANDLE_SLIDER"
    val KEY_HANDLE_HOME_LAYOUT = "KEY_HANDLE_HOME_LAYOUT"
    var itemNewList: ArrayList<ItemNews> = ArrayList()
    fun handleData(responseStr: String?) {
        BackgroundExecutor.execute(object :
            BackgroundExecutor.Task(KEY_HANDLE_HOME_LAYOUT, 0L, KEY_HANDLE_HOME_LAYOUT) {
            override fun execute() {
                if (responseStr.isNullOrEmpty()) {
                    showErrorNetwork(getString(R.string.msg_empty_data))
                    return
                }
                val responseString = responseStr.trim()
                val jsonObject = JsonParser.getJsonObject(responseString);
                if (jsonObject == null) {
                    showErrorNetwork(getString(R.string.msg_empty_data))
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                if (errorCode != Constant.SUCCESS) {
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    showErrorNetwork(message ?: getString(R.string.msg_empty_data))
                    return
                }
                val resultArr = JsonParser.getJsonArray(jsonObject, Constant.RESULT)
                if (resultArr == null) {
                    showErrorNetwork(getString(R.string.msg_empty_data))
                    return
                }
//                val strGame = "{\n" +
//                        "  \"type\": 4,\n" +
//                        "  \"name\": \"Chơi game nhận quà\",\n" +
//                        "  \"card_type\": \"game\",\n" +
//                        "  \"data\": [\n" +
//                        "    {\n" +
//                        "      \"Name\": \"FLAPPY BIRD\",\n" +
//                        "      \"Title\": \"Flapybird\",\n" +
//                        "      \"type\": \"flappy_bird\",\n" +
////                        "      \"url\": \"https://api.daugiatruyenhinh.com/html/flappy%20bird/index.html?\",\n" +
//                        "      \"url\": \"https://tuongtac.tv/webgame/flappy-bird?\",\n" +
//                        "      \"image\": \"https://lh3.googleusercontent.com/proxy/RSfNhnr4WCXuINiU6E87eU_YbNYV2S7Wl_Fbz_JlILfXBLIXR3tlAqMyqY0B2E_gBRw4-5RMXwTggjI0AI3JesRHwOpyPNOUjAlb52vq5P7fCfHlZYoqsSU5I_4u4tT3zBJUHwKc_f2ajvR4o8VSxYa_1uHnU4w043_QEB0jlVxt=s0-d\",\n" +
//                        "      \"id\": 1\n" +
//                        "    },\n" +
//                        "    {\n" +
//                        "      \"Name\": \"FRUIT NINJA\",\n" +
//                        "      \"Title\": \"FRUIT NINJA\",\n" +
//                        "      \"type\": \"fruit_ninja\",\n" +
////                        "      \"url\": \"https://api.daugiatruyenhinh.com/html/html5-fruit-ninja/index.html?\",\n" +
//                        "      \"url\": \"https://tuongtac.tv/webgame/fruit-ninja?\",\n" +
//                        "      \"image\": \"https://w7.pngwing.com/pngs/1000/66/png-transparent-fruit-ninja-halfbrick-studios-video-game-subway-surfer-game-electronics-food-thumbnail.png\",\n" +
//                        "      \"id\": 2\n" +
//                        "    },\n" +
//                        "    {\n" +
//                        "      \"Name\": \"TOWER BUILDING\",\n" +
//                        "      \"Title\": \"TOWER BUILDING\",\n" +
//                        "      \"type\": \"tower\",\n" +
//                        "      \"url\": \"https://tuongtac.tv/webgame/tower?\",\n" +
////                        "      \"url\": \"https://api.daugiatruyenhinh.com/html/tower_game/index.html?\",\n" +
//                        "      \"image\": \"https://d3frsattnbx5l6.cloudfront.net/1539285157804-towergame-icon-512.png\",\n" +
//                        "      \"id\": 1\n" +
//                        "    },\n" +
//                        "    {\n" +
//                        "      \"Name\": \"KNIFE HIT\",\n" +
//                        "      \"Title\": \"KNIFE HIT\",\n" +
//                        "      \"type\": \"knife\",\n" +
//                        "      \"url\": \"https://tuongtac.tv/webgame/knife?\",\n" +
////                        "      \"url\": \"https://api.daugiatruyenhinh.com/html/knife/index.html\",\n" +
//                        "      \"image\": \"https://lh3.googleusercontent.com/WdFoQ-GkKtvKVpfe-1AiEr0O4aPhpCPpozYIR_zOcpLV52xcsk6G4ZVeJKdKdxM6ag\",\n" +
//                        "      \"id\": 4\n" +
//                        "    }\n" +
//                        "  ]\n" +
//                        "}"
//                val jsonGame = JsonParser.getJsonObject(strGame)
//                resultArr.put(jsonGame)
                for (i in 0 until resultArr.length()) {
                    val jsonObj = resultArr.getJSONObject(i) ?: continue
                    UiThreadExecutor.runTask(KEY_HANDLE_HOME_LAYOUT, Runnable {
                        initHomeLayout1(jsonObj)
                    }, 0)
                }

            }
        })

        activity?.runOnUiThread {
            Handler(Looper.getMainLooper()).postDelayed({
                (activity as? MainActivity)?.initITV()
                if (itemNewList.size > 0) {
                    var question: String = "Hôm nay có một số tin tức mới sau:"
                    var itemCaseNewList: ArrayList<ItemCaseAnswer> = ArrayList()
                    var count = 0
                    for (item in itemNewList) {
                        count = count + 1
                        val index = count
                        question = question + "<br/>" + " $index." + item.name
                        itemCaseNewList.add(
                            ItemCaseAnswer(
                                arrayOf(index.toString(), item.name!!),
                                "OK, tôi sẽ mở ngay",
                                null,
                                item.id,
                                item.contentType
                            )
                        )

                    }
                    question = question + "<br/>" + "Bạn có muốn xem tin gì không"
                    (activity as? BaseActivity)?.voiceControl?.itemCase =
                        ItemCase(System.currentTimeMillis().toString(), question, itemCaseNewList)
                }
            }, 500)
        }
    }

    fun initSlide(itemList: ArrayList<ItemNews>?) {
        val lpHomeContent = layoutHomeContent.layoutParams as LinearLayout.LayoutParams
        if (itemList == null || itemList.size == 0) {
            sliderLayout.visibility = View.GONE
            lpHomeContent.topMargin = 150
            return
        } else {
            lpHomeContent.topMargin = 0
        }
        sliderLayout.visibility = View.VISIBLE

        val screenSize = ScreenSize(requireActivity())
        val lp: LinearLayout.LayoutParams = sliderLayout.layoutParams as LinearLayout.LayoutParams
        lp.height = screenSize.width * 9 / 16
//        lp.height = screenSize.width * 11 / 10
        sliderLayout.layoutParams = lp
        for (itemObj in itemList) {
            val sliderView: MySliderView = object : MySliderView(activity) {
                override fun getView(): View {
                    val view = super.getView()
                    val layoutDescrition = view.glide_slider_description_layout
                    layoutDescrition.removeAllViews()
                    layoutDescrition.setBackgroundColor(Color.TRANSPARENT)
                    return view
                }
            }
            sliderView.image(itemObj.image)
                .setProgressBarVisible(true)
                .setOnSliderClickListener {
                    handleSlideClicked(itemObj)
                }
            sliderLayout.addSlider(sliderView)
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion)
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
        sliderLayout.pagerIndicator.setDefaultIndicatorColor(
            ContextCompat.getColor(
                requireActivity(), R
                    .color.indicator_selected
            ), ContextCompat.getColor(
                requireActivity(), R.color
                    .indicator_unselected
            )
        )
        sliderLayout.setDuration(5000)
        sliderLayout.stopCyclingWhenTouch(false)
    }

    fun handleSlideClicked(itemObj: ItemNews) {
        if (itemObj.contentType.isNullOrEmpty()) {
            return
        }
        if (Constant.TYPE_LIVE.equals(itemObj.contentType!!.trim())) {
            (activity as? MainActivity)?.moveToTab(2)
            return
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            GeneralUtils.goDetailNewsActivity(activity, itemObj)
        }, 200)
    }

    fun initHomeLayout(type: String?, jsonObj: JSONObject?) {
        val dataArray = JsonParser.getJsonArray(jsonObj, Constant.DATA)
        if (activity == null) {
            return
        }
        val itemList = JsonParser.parseItemNewsList(requireActivity(), dataArray)
        if (itemList == null || itemList.size == 0) {
            return
        }
        val title = JsonParser.getString(jsonObj, "name")
        val layoutInflater: LayoutInflater = activity?.getSystemService(
            Context
                .LAYOUT_INFLATER_SERVICE
        ) as LayoutInflater
        val layout: View = layoutInflater.inflate(
            R.layout.layout_header_recycler_view_tt,
            layoutHomeContent, false
        )
        if (!title.isNullOrEmpty()) {
            layout.textTitle.text = title
            layout.layoutCategoryHeader.visibility = View.VISIBLE
        }

        val recyclerView = layout.recyclerView
        recyclerView.setHasFixedSize(true)
        var numberColumn = 1
        var style: Int = -1
        var layoutManager: StaggeredGridLayoutManager? = null
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        var itemDecoration: RecyclerView.ItemDecoration? = null
        var isShowButtonInfo = true
        var isProduct = false
        when (type) {
            Constant.TYPE_MENU -> {
                val lpRecyclerView: LinearLayout.LayoutParams =
                    recyclerView.layoutParams as LinearLayout.LayoutParams
                lpRecyclerView.width = LinearLayout.LayoutParams.WRAP_CONTENT

                numberColumn = 4
                style = Constant.STYLE_HOME_MENU
                itemDecoration = SpacesItemDecoration(spacePx, 1, true)
                layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL)
            }
            Constant.TYPE_MENU_GRID -> {
                isProduct = true
                numberColumn = 2
                style = Constant.STYLE_PRODUCT
                itemDecoration = SpacesItemDecorationGridMultipleViewType(spacePx, numberColumn)
                layoutManager =
                    StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.VERTICAL)
            }
            Constant.TYPE_HORIZONTAL -> {
                numberColumn = 1
                style = Constant.STYLE_HORIZONTAL
                itemDecoration = SpacesItemDecoration(spacePx, 1, true)
                layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL)
                isShowButtonInfo = false
            }
            Constant.TYPE_LIST -> {
                numberColumn = 1
                style = Constant.STYLE_LISTVIEW
                itemDecoration = SpacesItemDecoration(spacePx, numberColumn)
                layoutManager =
                    StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.VERTICAL)
            }
            Constant.TYPE_GALLERY -> {
                numberColumn = 1
                style = Constant.STYLE_GALLERY_VERTICAL
                itemDecoration = SpacesItemDecoration(spacePx, 1, true)
                layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL)
            }
        }
        if (style == -1) {
            return
        }
        val adapter = NewsAdapter(requireActivity(), itemList, style, numberColumn)
        adapter.isShowButtonInfo = isShowButtonInfo
        recyclerView.addItemDecoration(itemDecoration!!)
        activity?.runOnUiThread {
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = adapter
            recyclerView.scheduleLayoutAnimation()
            adapter.onItemClickListener = object : NewsAdapter.OnItemClickListener {
                override fun onClick(
                    itemObject: ItemNews,
                    position: Int,
                    holder: RecyclerView.ViewHolder
                ) {
                    if (isProduct) {
                        Handler(Looper.getMainLooper()).postDelayed(Runnable {
                            (activity as BaseActivity).goDetailProductActivity(itemObject, holder)
                        }, 200)
                        return
                    }

                    if (type.equals("menu")) {
                        when (itemObject.id) {
                            "${Constant.TYPE_MENU_BOOK_EVENT}" -> (activity as MainActivity).viewPager.currentItem =
                                1
                            "${Constant.TYPE_MENU_PRODUCT}" -> (activity as MainActivity).viewPager.currentItem =
                                2
                            "${Constant.TYPE_MENU_CALL_STAFF}" -> (activity as MainActivity).showCallStaffFragment()
                            "${Constant.TYPE_MENU_SHARE}" -> {
                                (activity as BaseActivity).shareLinkApp()
                            }
                        }
                        return
                    }

                    if (type.equals(Constant.TYPE_GALLERY)) {
                        (activity as BaseActivity).showImageZoomPagerBottomSheetDialog(itemObject)
                        return
                    }

                    if (!itemObject.isLive!!) {
                        Handler(Looper.getMainLooper()).postDelayed(Runnable {
                            GeneralUtils.goDetailNewsActivity(activity, itemObject)
                        }, 200)
                        return
                    }
                    //handle go livestream screen
                    if (itemObject.liveStream.isNullOrEmpty()) {
                        (activity as BaseActivity).showDialog(getString(R.string.msg_empty_data))
                        return
                    }
                    val bundle = Bundle()
                    bundle.putParcelable(Constant.DATA, itemObject)
                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        (activity as BaseActivity).gotoActivity(
                            LiveStreamActivity::class.java,
                            bundle
                        )
                    }, 200)
                }

                override fun onLongClick(itemObject: ItemNews, position: Int, view: View) {
                }

                override fun onCommentClick(itemObject: ItemNews, position: Int) {
                }

                override fun onShareClick(itemObject: ItemNews, position: Int) {
                    (activity as BaseActivity?)!!.shareApp(itemObject)
                }

                override fun onOrderClick(
                    itemObject: ItemNews,
                    position: Int,
                    holder: RecyclerView.ViewHolder
                ) {
                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        (activity as BaseActivity).goDetailProductActivity(itemObject, holder)
                    }, 200)
                }
            }

            val cardType = JsonParser.getString(jsonObj, "card_type")
            layoutHomeContent.addView(layout)
        }
    }

    fun initHomeLayout1(jsonObj: JSONObject?) {
        var type = JsonParser.getInt(jsonObj, "type")
        var cardType = JsonParser.getString(jsonObj, "card_type")
        val dataArray = JsonParser.getJsonArray(jsonObj, Constant.DATA)
        if (activity == null || cardType == null) {
            return
        }
        if (type == 0) {
            type = Constant.STYLE_HORIZONTAL
        }
        val itemList = JsonParser.parseItemNewsList(requireActivity(), dataArray)
        if (itemList == null || itemList.size == 0) {
            return
        }
        if (cardType.equals(Constant.CARD_TYPE_BANNER)) {
            initSlide(itemList)
            return
        }
        val title = JsonParser.getString(jsonObj, "name")
        val layoutInflater: LayoutInflater = activity?.getSystemService(
            Context
                .LAYOUT_INFLATER_SERVICE
        ) as LayoutInflater
        val layout: View = layoutInflater.inflate(
            R.layout.layout_header_recycler_view_tt,
            layoutHomeContent, false
        )
        if (!title.isNullOrEmpty()) {
            layout.textTitle.text = title
            layout.layoutCategoryHeader.visibility = View.VISIBLE
        }

        val recyclerView = layout.recyclerView
        recyclerView.setHasFixedSize(true)
        var numberColumn = 1
        if (type == Constant.STYLE_GRIDVIEW) {
            numberColumn = 2
        }
        var layoutManager: StaggeredGridLayoutManager? = null
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        var itemDecoration: RecyclerView.ItemDecoration? = null
        var isShowButtonInfo = true
        var isProduct = false
        var style = Constant.STYLE_HORIZONTAL
        itemDecoration = SpacesItemDecoration(spacePx, numberColumn, true)
        layoutManager =
            StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.HORIZONTAL)
        when (cardType) {
            Constant.CARD_TYPE_NEWS, Constant.CARD_TYPE_VIDEO -> {
                for (item in itemList) {
                    if (cardType.equals(Constant.CARD_TYPE_VIDEO)) {
                        item.contentType = Constant.TYPE_VIDEO
                    } else if (cardType.equals(Constant.CARD_TYPE_NEWS)) {
                        item.contentType = Constant.TYPE_NEWS
                    }
                }
                if (cardType.equals(Constant.CARD_TYPE_NEWS)) {
                    type = Constant.STYLE_LISTVIEW
                    itemNewList.addAll(itemList)
                }
                when (type) {
                    Constant.STYLE_HORIZONTAL -> {
                        style = type
                        itemDecoration = SpacesItemDecoration(spacePx, numberColumn, true)
                        layoutManager = StaggeredGridLayoutManager(
                            numberColumn,
                            StaggeredGridLayoutManager.HORIZONTAL
                        )
                    }

                    Constant.STYLE_LISTVIEW -> {
                        style = type
                        itemDecoration = SpacesItemDecoration(spacePx, numberColumn, false)
                        layoutManager = StaggeredGridLayoutManager(
                            numberColumn,
                            StaggeredGridLayoutManager.VERTICAL
                        )
                    }

                    Constant.STYLE_LISTVIEW_BOX -> {
                        style = type
                        itemDecoration = SpacesItemDecoration(spacePx, numberColumn, false)
                        layoutManager = StaggeredGridLayoutManager(
                            numberColumn,
                            StaggeredGridLayoutManager.VERTICAL
                        )
                    }

                    Constant.STYLE_GRIDVIEW -> {
                        style = type
                        itemDecoration =
                            SpacesItemDecorationGridMultipleViewType(spacePx, numberColumn)
                        layoutManager = StaggeredGridLayoutManager(
                            numberColumn,
                            StaggeredGridLayoutManager.VERTICAL
                        )
                    }
                }
                isShowButtonInfo = false
            }
            Constant.CARD_TYPE_TOP_USER -> {
                val result = JsonParser.getString(jsonObj, Constant.DATA)
                val type = object : TypeToken<ArrayList<ItemWiner>>() {}.getType()
                val gson = Gson()
                val itemWinnerList: ArrayList<ItemWiner>? = try {
                    gson.fromJson(result, type)
                } catch (e: Exception) {
                    null
                }
                if (itemWinnerList == null || itemWinnerList.size == 0) {
                    return
                }
                activity?.runOnUiThread {
                    val layoutInflater: LayoutInflater = activity?.getSystemService(
                        Context
                            .LAYOUT_INFLATER_SERVICE
                    ) as LayoutInflater
                    val layout: View = layoutInflater.inflate(
                        R.layout.layout_header_recycler_view_tt,
                        layoutHomeContent, false
                    )
                    layout.textTitle.text = title
                    layout.layoutCategoryHeader.visibility = View.VISIBLE
                    val recyclerView = layout.recyclerView
                    recyclerView.setHasFixedSize(true)
                    val itemMenuAdapter =
                        ItemWinnerAdapter(requireActivity(), itemWinnerList, 1, 1, 5)
                    val layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    recyclerView?.layoutManager = layoutManager
                    recyclerView?.adapter = itemMenuAdapter
                    if (layoutHomeContent.childCount >= 1) {
                        layoutHomeContent.addView(layout, 0)
                    } else{
                        layoutHomeContent.addView(layout)
                    }

                    itemMenuAdapter.onItemClickListener =
                        object : ItemWinnerAdapter.OnItemClickListener {
                            override fun onClick(itemObject: ItemWiner, position: Int) {
                                val fragment = ImageZoomPagerDialog()
                                val list = ArrayList<ItemPhoto>()
                                list.add(
                                    ItemPhoto(
                                        itemObject.fullName,
                                        itemObject.avatar,
                                        itemObject.showDate
                                    )
                                )
                                val bundle = Bundle()
                                bundle.putParcelableArrayList(Constant.DATA, list)
                                fragment.arguments = bundle
                                fragment.show(childFragmentManager, "ImageZoom")
                            }
                        }
                }
                return
            }
            Constant.CARD_TYPE_GAME -> {
                val result = JsonParser.getString(jsonObj, Constant.DATA)
                val type = object : TypeToken<ArrayList<ItemWebGame>>() {}.getType()
                val gson = Gson()
                val itemWebgameList: ArrayList<ItemWebGame>? = try {
                    gson.fromJson(result, type)
                } catch (e: Exception) {
                    null
                }
                if (itemWebgameList == null || itemWebgameList.size == 0) {
                    return
                }
                activity?.runOnUiThread {
                    val layoutInflater: LayoutInflater = activity?.getSystemService(
                        Context
                            .LAYOUT_INFLATER_SERVICE
                    ) as LayoutInflater
                    val layout: View = layoutInflater.inflate(
                        R.layout.layout_header_recycler_view_tt,
                        layoutHomeContent, false
                    )
                    layout.textTitle.text = title
                    layout.layoutCategoryHeader.visibility = View.VISIBLE
                    val recyclerView = layout.recyclerView
                    recyclerView.setHasFixedSize(true)
                    val adapter =
                        ItemWebGameAdapter(requireActivity(), itemWebgameList, 1, 1, 4)
                    val layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    recyclerView?.layoutManager = layoutManager
                    recyclerView?.adapter = adapter
                    if (layoutHomeContent.childCount >= 2) {
                        layoutHomeContent.addView(layout, 1)
                    } else{
                    layoutHomeContent.addView(layout)
                }
                    adapter.onItemClickListener =
                        object : ItemWebGameAdapter.OnItemClickListener {
                            override fun onClick(itemObject: ItemWebGame, position: Int) {
                                val itemUser = MyApplication.getInstance().dataManager.itemUser
                                if (itemUser != null && !itemUser.accessToken.isNullOrEmpty()) {
                                    GameActivity.startGameActivity(activity, itemObject)
                                } else {
                                    val msg =
                                        "Bạn vui lòng đăng nhập tài khoản để được chơi game nhận quà!"
                                    GeneralUtils.showDialog(
                                        activity,
                                        false,
                                        "Thông báo",
                                        msg,
                                        "Huỷ",
                                        "Đăng nhập",
                                        object : OnDialogButtonListener {
                                            override fun onLeftButtonClick() {
                                                currentItemGame = null
                                            }

                                            override fun onRightButtonClick() {
                                                currentItemGame = itemObject
                                                val intent =
                                                    Intent(activity, UserLoginActivity::class.java)
                                                resultLauncher.launch(intent)
                                            }
                                        })
                                }
                            }
                        }
                }
                return
            }
        }
        val adapter = NewsAdapter(requireActivity(), itemList, style, numberColumn)
        adapter.isShowButtonInfo = isShowButtonInfo
        recyclerView.addItemDecoration(itemDecoration!!)
        activity?.runOnUiThread {
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = adapter
            recyclerView.scheduleLayoutAnimation()
            adapter.onItemClickListener = object : NewsAdapter.OnItemClickListener {
                override fun onClick(
                    itemObject: ItemNews,
                    position: Int,
                    holder: RecyclerView.ViewHolder
                ) {
                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        GeneralUtils.goDetailNewsActivity(activity, itemObject)
                    }, 200)
                }

                override fun onLongClick(itemObject: ItemNews, position: Int, view: View) {
                }

                override fun onCommentClick(itemObject: ItemNews, position: Int) {
                }

                override fun onShareClick(itemObject: ItemNews, position: Int) {
                    (activity as BaseActivity?)!!.shareApp(itemObject)
                }

                override fun onOrderClick(
                    itemObject: ItemNews,
                    position: Int,
                    holder: RecyclerView.ViewHolder
                ) {
                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        (activity as BaseActivity).goDetailProductActivity(itemObject, holder)
                    }, 200)
                }
            }

//            val cardType = JsonParser.getString(jsonObj, "card_type")
//            if (orderBoxVideo == -1 && !cardType.isNullOrEmpty() && cardType.equals("live")) {
//                orderBoxVideo = layoutHomeContent.childCount
//            }
            layoutHomeContent.addView(layout)
        }
    }

    var currentItemGame: ItemWebGame? = null;
    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                ServiceManager.setNeedUpdateFcmTokenForUser(requireActivity(), true)
                ServiceManager.updateFcmTokenUser(requireActivity())
                if (activity is MainActivity) {
                    (activity as? MainActivity)?.updateUIChangeAcount()
                }
                if (currentItemGame != null) {
                    GameActivity.startGameActivity(activity, currentItemGame)
                    currentItemGame = null
                }
            }
        }


    fun getApiEvent() {
    }

    fun checkUpdateLive(
        adapter: NewsAdapter,
        currentItemList: ArrayList<ItemNews>?,
        newList: ArrayList<ItemNews>?
    ) {
        var itemList = currentItemList
        if (itemList == null) {
            if (newList == null) {
                return
            }
            itemList = ArrayList()
        }
        for (i in itemList.size - 1 downTo 0) {
            val obj = itemList.get(i)
            if (obj.isLive == null || !obj.isLive!!) {
                continue
            }
            itemList.removeAt(i)
        }
        if (newList == null) {
            activity?.runOnUiThread {
                adapter.notifyDataSetChanged()
            }
            return
        }
        itemList.addAll(0, newList)
        activity?.runOnUiThread {
            adapter.notifyDataSetChanged()
        }
    }

    fun checkUpdateBottomCart() {
        val itemCartList = MyApplication.getInstance().dataManager.itemCartList
        if (itemCartList.size == 0) {
            if (layoutNumberProduct?.visibility == View.GONE) {
                return
            }
            val anim = AnimationUtils.loadAnimation(activity, R.anim.slide_in_down)
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    layoutNumberProduct.visibility = View.GONE
                }

                override fun onAnimationRepeat(p0: Animation?) {
                }
            })
            layoutNumberProduct.startAnimation(anim)
            return
        }
        var quantity = 0
        for (item in itemCartList) {
            quantity += item.quantity
        }

        val numberFormat: NumberFormat = DecimalFormat("00")
        var quantityStr = numberFormat.format(quantity)
        quantityStr = if (quantity > 99) "99+" else quantityStr
//        textNumberProduct.text = quantityStr
        if (layoutNumberProduct.visibility != View.VISIBLE) {
            val anim = AnimationUtils.loadAnimation(activity, R.anim.slide_in_up)
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    layoutNumberProduct.visibility = View.VISIBLE
                    textNumberProduct.animateText(quantityStr)
                }

                override fun onAnimationRepeat(p0: Animation?) {
                }
            })
            layoutNumberProduct.visibility = View.VISIBLE
            layoutNumberProduct.startAnimation(anim)
            return
        }
        textNumberProduct.animateText(quantityStr)
    }

    override fun onPause() {
        sliderLayout?.stopAutoCycle()
        isOnPause = true
        super.onPause()
    }

    override fun onResume() {
        sliderLayout?.startAutoCycle()
        isOnPause = false
        checkUpdateBottomCart()
        super.onResume()
    }

    fun initControl() {
        textNotify.setOnClickListener {
            getData()
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser == null) {
                (activity as MainActivity).autoLogin()
            }
        }
        layoutNumberProduct.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                (activity as MainActivity).showCartFragment(object : OnCancelListener {
                    override fun onCancel(isCancel: Boolean) {
                        checkUpdateBottomCart()
                    }
                })
            }, 200)
        }
        layoutRefresh.setOnRefreshListener {
            layoutRefresh.isRefreshing = false
            progressBar.visibility = View.VISIBLE
            layoutHomeContent.removeAllViews()
            sliderLayout.removeAllSliders()
            sliderLayout.visibility = View.GONE
//            getData()
            initData()
        }
    }

    override fun onDetach() {
        myHttpRequest?.cancel()
        myHttpRequestSlide?.cancel()
        BackgroundExecutor.cancelAll(KEY_HANDLE_SLIDER, true)
        BackgroundExecutor.cancelAll(KEY_HANDLE_HOME_LAYOUT, true)
        super.onDetach()
    }

}
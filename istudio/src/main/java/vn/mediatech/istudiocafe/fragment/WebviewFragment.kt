package vn.mediatech.istudiocafe.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnScrollChangedListener
import android.view.inputmethod.EditorInfo
import android.webkit.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_webview_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnCreateViewListener
import vn.mediatech.istudiocafe.listener.OnShowListener
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class WebviewFragment : Fragment() {
    //    private val REQUEST_SELECT_FILE = 1002
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var isInit: Boolean = false
    var isLoading: Boolean = false
    var myHttpRequest: MyHttpRequest? = null
    var itemUser: ItemUser? = null
    var savedInstanceState: Bundle? = null
    val URL_DEFAUL = "https://tuongtac.tv"
    var urlForum: String? = URL_DEFAUL
    var urlLogin: String? = null

    companion object {
        var isCheckUserChange = false
    }

    //    var URL_DEFAUL = "http://210.211.126.222:3000/"
//    var URL_DEFAUL = "https://api.daugiatruyenhinh.com/html/flappy%20bird/index.html?"
    var isLogin = false
    var isOnscreen = false
    var onShowListener: OnShowListener? = null
    var onCreateViewListener: OnCreateViewListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_webview_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        onShowListener?.onViewCreated()
        onCreateViewListener?.onCreated()
//        if (savedInstanceState != null) {
//            init()
//        }
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    private fun initControl() {

        buttonGo.setOnClickListener {
            loadUrl()
        }
        buttonBack.setOnClickListener {
            if (webView.canGoBack()) {
                webView?.goBack()
            }
        }
        editSearch.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                (activity as? BaseActivity)?.hideKeyboard(editSearch)
                loadUrl()
                true
            } else false
        }

        layoutRefresh.setOnRefreshListener {
            layoutRefresh.isRefreshing = false
            loadUrl(urlForum!!)
        }
        layoutRefresh?.getViewTreeObserver()?.addOnScrollChangedListener(OnScrollChangedListener {
            if (webView == null) {
                return@OnScrollChangedListener
            }
            if (webView.getScrollY() == 0) layoutRefresh.setEnabled(true) else layoutRefresh.setEnabled(
                false
            )
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    private fun loadUrl() {
        loadUrl(editSearch.text.toString().trim())
    }

    private fun loadUrl(link: String) {
        var url = link
        val googleSearch = "https://www.google.com/search?q="
        if (!url.isEmpty()) {
            if (!url.startsWith("http")) {
                url = googleSearch + url
            }
            Loggers.e("WEBVIEW_URL", url)

            editSearch?.setText(url)
            webView?.loadUrl(url)
            webView?.requestFocus(View.FOCUS_UP)
        }
    }

    fun initUI() {
        layoutSearch?.visibility = View.GONE
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun initData() {
        val bundle = arguments ?: return
//        if (savedInstanceState != null) {
//            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
//        } else {
//            itemUser = MyApplication.getInstance().dataManager.itemUser
//        }
        val topSpace = bundle.getBoolean(Constant.IS_SHOW_TOP_SPACE, false)
        if (topSpace) {
            if (activity is MainActivity) {
//            val lp: ViewGroup.LayoutParams = layoutFragmentRoot.layoutParams
//            layoutActionbar
                val topSpacePadding = (activity as MainActivity).getStatusBarHeight()
                Loggers.e("TabNewsFrg", "topSpacePadding = $topSpacePadding")
                layoutFragmentRoot.setPadding(0, topSpacePadding, 0, 0)
            }
        }
        initWebView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
//        webView?.setInitialScale(140)
        webView?.clearCache(true)
        webView?.clearHistory()
        val webSettings: WebSettings = webView.getSettings()
        webSettings.setSupportZoom(false)
        webSettings.builtInZoomControls = false
        //        webSettings.setLoadWithOverviewMode(true);
        //        webSettings.setUseWideViewPort(true);
        //        webSettings.setLoadWithOverviewMode(true);
        //        webSettings.setUseWideViewPort(true);
        webSettings.setAppCacheEnabled(true)
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.setSupportMultipleWindows(true)
        webSettings.allowFileAccess = true
        webSettings.loadsImagesAutomatically = true
        webSettings.mediaPlaybackRequiresUserGesture = false
        webSettings.allowFileAccess = true
        CookieManager.getInstance().setAcceptCookie(true)
        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
        //        String info = "<style>video, img{width: 100%; height: auto;} body{text-align:justify; padding: 0px; margin: 0px;}</style>" + itemObj.getInfo();
        //        String info = "<style>video, img{width: 100%; height: auto;} body{text-align:justify; padding: 0px; margin: 0px;}</style>" + itemObj.getInfo();
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                progressBar?.visibility = View.VISIBLE
                super.onPageStarted(view, url, favicon)
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (request != null) {
                    Loggers.e("WEBVIEW_URL_OverrideUrlLoading", request!!.url.toString())
                    urlForum = request!!.url.toString()
                    activity?.runOnUiThread {
                        editSearch?.setText(request!!.url.toString())
                    }
                }
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progressBar?.visibility = View.GONE
                editSearch?.setText(url)
                super.onPageFinished(view, url)
            }

//            override fun onReceivedSslError(
//                view: WebView?,
//                handler: SslErrorHandler?,
//                error: SslError?
//            ) {
//                handler?.proceed()
//            }
        }
//                webView.setWebViewClient(WebViewClient())
        webView.webChromeClient = MyWebChromeClient()

//        isInit = true
//        checkUrlForum()

        Handler(Looper.getMainLooper()).postDelayed({ //
            //            webView.loadUrl("https://forum.mediatech.vn/");
            itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser != null && !itemUser!!.urlForum.isNullOrEmpty()) {
                urlForum = itemUser!!.urlForum!!
                urlLogin = itemUser!!.urlForum!!
                isLogin = true
            } else {
                urlForum = URL_DEFAUL
                isLogin = false
            }
            itemUser?.let {urlForum = urlForum +"?token=${it.accessToken}"  }
            loadUrl(urlForum!!)
        }, 100)
        isInit = true

    }

    var myFilePathCallback: ValueCallback<Array<Uri>>? = null
    fun backUrl(): Boolean {
        if (webView != null && webView.canGoBack()) {
            webView?.goBack()
            return true
        }
        return false
    }

    private fun getCustomHeaders(): Map<String, String>? {
        val headers: MutableMap<String, String> = HashMap()
        headers["YOURHEADER"] = "VALUE"
        return headers
    }

    override fun onResume() {
        super.onResume()
        onShowListener?.onResume()
        isOnscreen = true
        if (isCheckUserChange) {
            isCheckUserChange = false
            checkUrlForum()
        }
    }

    override fun onPause() {
        super.onPause()
        onShowListener?.onPause()
        isOnscreen = false
//        webView?.onPause();
//        webView?.pauseTimers();
    }

    override fun onStop() {
        super.onStop()
        onShowListener?.onStop()
    }

    override fun onDestroyView() {
        webView?.destroy()
        super.onDestroyView()
    }

    fun checkUrlForum() {
//        if (!isInit) {
//            return
//        }
////        clearCookies(requireContext())
////        webView?.clearCache(true)
//        itemUser = MyApplication.getInstance().dataManager.itemUser
//        if(itemUser != null){
////            var urlGame = "https://api.daugiatruyenhinh.com/html/flappy%20bird/index.html?name=${itemUser!!.fullname}&id=${itemUser!!.id}&avt=${itemUser!!.avatar}"
//            var urlGame = URL_DEFAUL + "name=${itemUser!!.fullname}&id=${itemUser!!.id}&avt=${itemUser!!.avatar}"
//            loadUrl(urlGame)
//        }
        if (!isInit) {
            return
        }
        itemUser = MyApplication.getInstance().dataManager.itemUser
        if (itemUser == null) {
            if (isLogin) {
                urlForum = URL_DEFAUL
                isLogin = false
                loadReset()
            }
            return
        }
        if (itemUser != null && !itemUser!!.urlForum.isNullOrEmpty()) {
            if (!isLogin) {
                urlForum = itemUser!!.urlForum!!
                urlLogin = itemUser!!.urlForum!!
                isLogin = true
                loadReset()
            } else {
                if (!itemUser!!.urlForum!!.equals(urlLogin)) {
                    urlForum = itemUser!!.urlForum!!
                    urlLogin = itemUser!!.urlForum!!
                    loadReset()
                }
            }
        }
    }

    private fun loadReset() {
        webView.clearCache(true)
        webView.clearHistory()
        clearCookies(context)
        loadUrl(urlForum!!)
    }

    //    START: UPLOAD_FILE
    private var uploadMessage: ValueCallback<Array<Uri>>? = null
    private val REQUEST_SELECT_FILE = 3001
    private val REQUEST_PERMISSION_CAMERA_CODE = 2001
    private val REQUEST_PERMISSION_AUDIO_CODE = 2002
    private val TYPE_IMAGE = 1
    private val TYPE_VIDEO = 2
    private val TYPE_AUDIO = 3
    private var takeMediaCameraUri: Uri? = null

    inner class MyWebChromeClient : WebChromeClient() {
        override fun onPermissionRequest(request: PermissionRequest?) {
            request?.grant(request.resources)
            super.onPermissionRequest(request)
        }

        override fun onPermissionRequestCanceled(request: PermissionRequest?) {
            super.onPermissionRequestCanceled(request)
        }

        override fun onShowFileChooser(
            webView: WebView?,
            filePathCallback: ValueCallback<Array<Uri>>?,
            fileChooserParams: FileChooserParams?
        ): Boolean {
            if (fileChooserParams == null) {
                return true
            }
            val acceptTypeList = fileChooserParams.acceptTypes
            var intent = fileChooserParams.createIntent()
            for (acceptType in acceptTypeList) {
//                Loggers.e("onShowFileChooser", "acceptType = $acceptType")
                intent = checkIntentView(acceptType)
                if (intent != null) {
                    break
                }
            }
            if (intent == null) {
                return false
            }
            if (uploadMessage != null) {
                uploadMessage!!.onReceiveValue(null)
                uploadMessage = null
            }
            uploadMessage = filePathCallback
            try {
                startActivityForResult(intent, REQUEST_SELECT_FILE)
            } catch (e: Exception) {
                e.printStackTrace()
                uploadMessage = null
                (requireActivity() as? BaseActivity)?.showToast(R.string.msg_not_found_task)
                return false
            }
            return true
        }
    }

    fun checkIntentView(acceptType: String): Intent? {
        var mediaType = 0
        if (acceptType.contains("image")) {
            mediaType = TYPE_IMAGE
        } else if (acceptType.contains("camera")) {
            mediaType = TYPE_VIDEO
        } else if (acceptType.contains("audio")) {
            mediaType = TYPE_AUDIO
        }
        if (!checkPermission(mediaType)) {
            return null
        }
        var intent: Intent
        if (acceptType.contains("image")) {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "image/*"
            val takeCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            takeMediaCameraUri = createMediaCameraUri(TYPE_IMAGE)
            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, takeMediaCameraUri)
            val galleryIntent =
                Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            val intentArray = arrayOf(takeCameraIntent, galleryIntent)
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
            intent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        } else if (acceptType.contains("camera")) {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "video/*"
            val takeCameraIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            takeMediaCameraUri = createMediaCameraUri(TYPE_VIDEO)
            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, takeMediaCameraUri)
            val galleryIntent =
                Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            val intentArray = arrayOf(takeCameraIntent, galleryIntent)
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
            intent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        } else if (acceptType.contains("audio")) {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "audio/*"
            val takeCameraIntent = Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION)
            takeMediaCameraUri = createMediaCameraUri(TYPE_AUDIO)
            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, takeMediaCameraUri)
            val galleryIntent = Intent()
            galleryIntent.type = "audio/*"
            galleryIntent.action = Intent.ACTION_GET_CONTENT
            val intentArray = arrayOf(takeCameraIntent, galleryIntent)
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
            intent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        } else {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "*/*"
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
        }
        return intent
    }

    private fun createMediaCameraUri(mediaType: Int): Uri? {
        val sdf = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
        var fileName = "_${sdf.format(Date())}"
        fileName = if (mediaType == TYPE_IMAGE) {
            "Img_$fileName.jpg"
        } else if (mediaType == TYPE_VIDEO) {
            "Video_$fileName.mp4"
        } else if (mediaType == TYPE_AUDIO) {
            "Audio_$fileName.m4a"
        } else {
            "Noname_$fileName"
        }
        val file = File(fileName)
        return Uri.fromFile(file)
    }

    private fun checkPermission(mediaType: Int): Boolean {
        val permissionWriteStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val permissionCamera = Manifest.permission.CAMERA
        val permissionRecordAudio = Manifest.permission.RECORD_AUDIO
        val permissionCameraList =
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
        when (mediaType) {
            TYPE_IMAGE, TYPE_VIDEO -> {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        permissionWriteStorage
                    ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                        requireContext(),
                        permissionCamera
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(permissionCameraList, REQUEST_PERMISSION_CAMERA_CODE)
                    return false
                }
            }
            TYPE_AUDIO -> {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        permissionRecordAudio
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(permissionCameraList, REQUEST_PERMISSION_AUDIO_CODE)
                    return false
                }
            }
            else -> {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        permissionWriteStorage
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(permissionCameraList, REQUEST_PERMISSION_CAMERA_CODE)
                    return false
                }
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_CAMERA_CODE || requestCode == REQUEST_PERMISSION_AUDIO_CODE) {
            if (!checkGrantPermission(grantResults, permissions as Array<String>)) {
                (activity as? BaseActivity)?.showToast(R.string.msg_require_accept_permission)
            }
        }
    }

    private fun checkGrantPermission(grantResults: IntArray, permissions: Array<String>): Boolean {
        for (i in grantResults.indices) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

//    END: UPLOAD_FILE

    @SuppressWarnings("deprecation")
    fun clearCookies(context: Context?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null)
            CookieManager.getInstance().flush()
        } else if (context != null) {
            val cookieSyncManager = CookieSyncManager.createInstance(context)
            cookieSyncManager.startSync()
            val cookieManager: CookieManager = CookieManager.getInstance()
            cookieManager.removeAllCookie()
            cookieManager.removeSessionCookie()
            cookieSyncManager.stopSync()
            cookieSyncManager.sync()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SELECT_FILE) {
            if (uploadMessage == null) {
                return
            }
            val intent = data ?: Intent()
            if (takeMediaCameraUri != null && intent.data == null) {
                intent.data = takeMediaCameraUri
            }
            uploadMessage!!.onReceiveValue(
                WebChromeClient.FileChooserParams.parseResult(
                    resultCode,
                    intent
                )
            )
            uploadMessage = null
            takeMediaCameraUri = null

            /*if (myFilePathCallback == null) return;
            myFilePathCallback?.onReceiveValue(
                WebChromeClient.FileChooserParams.parseResult(
                    resultCode,
                    data
                )
            );
            myFilePathCallback = null;*/
        }
    }
}
package vn.mediatech.istudiocafe.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.ScrollingMovementMethod
import android.view.*
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_main_tt.*
import vn.mediatech.interactive.service.SocketMangager
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.adapter.ItemMenuInteractiveAdapter
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.group.ListGroupFragment
import vn.mediatech.istudiocafe.group.ListGroupFragment.OnGroupListener
import vn.mediatech.istudiocafe.listener.*
import vn.mediatech.istudiocafe.model.ItemInteractiveConfig
import vn.mediatech.istudiocafe.model.ItemMenuInteractive
import vn.mediatech.istudiocafe.util.GeneralUtils
import vn.mediatech.istudiocafe.util.ServiceManager.Companion.setNeedUpdateFcmTokenForUser
import vn.mediatech.istudiocafe.util.ServiceManager.Companion.updateFcmTokenUser
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.voucher.MyVoucherFragment
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.util.*

class MainInteractiveFragment : Fragment() {
    private var webviewFragment: WebviewFragment? = null
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var topSpace = false
    var onCreateViewListener: OnCreateViewListener? = null
    var onShowListener: OnShowListener? = null
    var appId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        onCreateViewListener?.onCreated()
        onShowListener?.onViewCreated()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putBoolean(Constant.IS_SHOW_TOP_SPACE, topSpace)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (isDetached) {
            return
        }
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
    }

    var isCheckAutoLogin: Boolean = false
    var enableInitInteractive: Boolean = true
    fun initData() {
        appId = arguments?.getString(Constant.APP_ID)
//            ?: throw java.lang.Exception("APP_ID has not been set. Please use: bundle.putString(Constant.APP_ID, appId_value) for init.")
        /* if (appId.isNullOrEmpty()) {
             t
         }*/

        if (MyApplication.getContext() == null) {
            MyApplication.setsContext(requireActivity().applicationContext)
        }
//        initAdapter()
//        val bundle = savedInstanceState ?: arguments ?: return
        if (appId != null) {
            startGetConfigInteractive()
        }
        initInteractiveLiveStreamFragment()
        if (!isCheckAutoLogin) {
            var itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser == null) {
                ServiceUtilTT.autoLogin(requireContext(), object : OnLoginFormListener {
                    override fun onLogin(hasLogin: Boolean) {
                        if (isDetached) {
                            return
                        }
                        setNeedUpdateFcmTokenForUser(
                            requireActivity(), true
                        )
                        updateFcmTokenUser(requireActivity())
                        isCheckAutoLogin = true
                        requireActivity().runOnUiThread {
                            checkShowInterActive()
                        }
                    }
                })
            } else {
                isCheckAutoLogin = true
            }
        }
//        Handler(Looper.getMainLooper()).postDelayed(Runnable {
//            initInteractiveLiveStreamFragment()
//        }, 200)

    }

    var selectIndex: Int = -1;
    var interactiveIndex: Int = -1;
    var currentMenuID = -1;
    var adapter: ItemMenuInteractiveAdapter? = null
    private fun initAdapter() {
        val itemList = ArrayList<ItemMenuInteractive>()
        val item = ServiceUtilTT.itemInteractiveConfig
        if (item == null || item.menu == null || item.menu.size == 0) {
            return
        }
        itemList.addAll(item.menu)
//        itemList.add(
//            ItemMenuInteractive(
//                Constant.ID_MENU_FOCUS,
//                "",
//                R.drawable.ic_menu_interactive,
//                "Tiêu điểm"
//            )
//        )
        if (currentFragment is InteractiveLiveStreamFragment) {
            if (!isShowNews) {
                currentMenuID = Constant.ID_MENU_INTERACTIVE
            } else {
                currentMenuID = Constant.ID_MENU_FOCUS
            }
        }
        for (i in 0 until itemList.size) {
            if (currentMenuID == itemList.get(i).id) {
                selectIndex = i
                break
            }
        }

        adapter = ItemMenuInteractiveAdapter(requireContext(), itemList, 1, 1, 1)
        adapter?.selectIndex = selectIndex
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerViewMenu.layoutManager = layoutManager
        recyclerViewMenu.adapter = adapter
        recyclerViewMenu.scheduleLayoutAnimation()
        adapter?.onItemClickListener = object : ItemMenuInteractiveAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemMenuInteractive, position: Int) {
                showViewOverlay(false)
                if (selectIndex == position) {
                    recyclerViewMenu.visibility = View.INVISIBLE
                    return
                }
                when (itemObject.id) {
                    Constant.ID_MENU_NEWS -> {
                        currentMenuID = itemObject.id
                        val fragment = NewsFragment()
                        val bundle = Bundle()
                        bundle.putBoolean(Constant.IS_SHOW_TOP_SPACE, true)
                        bundle.putInt(Constant.TYPE, Constant.TAB_TYPE_NEWS)
                        fragment.arguments = bundle
                        fragment.checkRefresh()
                        showFragment(fragment)
                        recyclerViewMenu.visibility = View.INVISIBLE
                    }
                    Constant.ID_MENU_VIDEO -> {
                        currentMenuID = itemObject.id
                        val fragment = NewsFragment()
                        val bundle = Bundle()
                        bundle.putBoolean(Constant.IS_SHOW_TOP_SPACE, true)
                        bundle.putInt(Constant.TYPE, Constant.TAB_TYPE_VIDEO)
                        fragment.arguments = bundle
                        fragment.checkRefresh()
                        showFragment(fragment)
                        recyclerViewMenu.visibility = View.INVISIBLE
                    }
                    Constant.ID_MENU_INTERACTIVE -> {
                        currentMenuID = itemObject.id
                        recyclerViewMenu.visibility = View.INVISIBLE
                        isShowNews = false
                        showInteractive()
                    }
                    Constant.ID_MENU_FOCUS -> {
                        currentMenuID = itemObject.id
                        recyclerViewMenu.visibility = View.INVISIBLE
                        isShowNews = true
                        showInteractive()
                    }
                    Constant.ID_MENU_FORUM -> {
                        currentMenuID = itemObject.id
                        webviewFragment = WebviewFragment()
                        val bundle = Bundle()
                        bundle.putBoolean(Constant.IS_SHOW_TOP_SPACE, false)
                        webviewFragment?.arguments = bundle
                        webviewFragment?.onCreateViewListener = object : OnCreateViewListener {
                            override fun onCreated() {
                                webviewFragment?.checkRefresh()
                            }
                        }
                        showFragment(webviewFragment)
                        recyclerViewMenu.visibility = View.INVISIBLE
                    }
                    Constant.ID_MENU_GIT_STORE -> {
                        return
                    }
                    Constant.ID_MENU_MY_GIT -> {
                        currentMenuID = itemObject.id
                        val itemUser = MyApplication.getInstance().dataManager.itemUser
                        if (itemUser == null) {
                            GeneralUtils.showDialog(activity,
                                true,
                                R.string.notification,
                                R.string.msg_require_login,
                                R.string.close,
                                0,
                                object : OnDialogButtonListener {
                                    override fun onLeftButtonClick() {
                                    }

                                    override fun onRightButtonClick() {
                                    }
                                })
                            return
                        }
                        interactiveLiveStreamFragment?.closeSocket()
                        val fragment = MyVoucherFragment()
                        showFragment(fragment)
//                        GeneralUtils.openMyVoucher(activity)
                        recyclerViewMenu.visibility = View.INVISIBLE
                    }

                    Constant.ID_MENU_PERSONAL -> {
                        currentMenuID = itemObject.id
                        interactiveLiveStreamFragment?.closeSocket()
                        val fragment = TabPersonFragment()
                        showFragment(fragment)
                        fragment.onCreateViewListener = object : OnCreateViewListener {
                            override fun onCreated() {
                                fragment.checkRefresh()
                            }
                        }
//                    GeneralUtils.gotoActivity(activity, UserActivity::class.java)
                        recyclerViewMenu.visibility = View.INVISIBLE
                    }
                    Constant.ID_MENU_SHARE -> {
                        val itemUser = MyApplication.getInstance().dataManager.itemUser
                        if (itemUser == null) {
                            GeneralUtils.showToast(
                                activity,
                                "Vui lòng đăng nhập để sử dụng tính năng này!"
                            )
                            return
                        }
                        val linkAndroid =
                            "https://play.google.com/store/apps/details?id=vn.mediatech.istudiocafe&hl=vi"
                        val linkIOS = "https://apps.apple.com/vn/app/istudio/id1540378720?l=vi"
                        var link =
                            if (ConstantTT.CHANEL_ID.equals(
                                    ConstantTT.CHANEL_DEFAULT
                                )
                            ) "Link tải app https://daugiatruyenhinh.com/api/v1.0/tuongtactv/dowload" else ""
                        val str =
                            "Hãy tham gia tương tác cùng tôi tại gameshow Đấu Giá Truyền Hình, một sản phẩm của TuongTac.tv. Nhập mã giới thiệu " + itemUser!!.id + " để nhận quà nhé!. " + link
                        GeneralUtils.shareApp(activity, str)
                        recyclerViewMenu.visibility = View.INVISIBLE
                        return
                    }
                    Constant.ID_MENU_GROUP -> {
                        val itemUser = MyApplication.getInstance().dataManager.itemUser
                        if (itemUser == null) {
                            GeneralUtils.showToast(
                                activity,
                                "Vui lòng đăng nhập để sử dụng tính năng này!"
                            )
                            return
                        }
                        if (!Constant.USER_STATUS_REAL.equals(itemUser.userStatus)) {
                            GeneralUtils.showToast(
                                activity,
                                "Vui lòng xác thực tài khoản để sử dụng tính năng này!"
                            )
                            return
                        }
                        val fragment = ListGroupFragment()
                        val fragmentTransaction = childFragmentManager.beginTransaction()
                        fragment.show(
                            fragmentTransaction,
                            "LIST_GROUP"
                        )
                        fragment.onGroupListener = object : OnGroupListener {
                            override fun onRequireConnectSocket() {
                                interactiveLiveStreamFragment?.reconnectSocket()
                            }
                        }
                        recyclerViewMenu.visibility = View.INVISIBLE
                        return
                    }
                    else -> {
                        return
                    }
                }
                selectIndex = position
            }

            override fun onClickOut(itemObject: ItemMenuInteractive, position: Int) {
                showViewOverlay(false)
                recyclerViewMenu.visibility = View.INVISIBLE
            }
        }
    }

    private fun showViewOverlay(isShow: Boolean, isHideRecyclerView: Boolean = false) {
        if (viewOverlay == null || recyclerViewMenu == null) {
            return
        }
        viewOverlay?.clearAnimation()
        if (isShow) {
            viewOverlay?.visibility = View.VISIBLE
            val anim: Animation = AlphaAnimation(0f, 1f)
            anim.interpolator = DecelerateInterpolator()
            anim.duration = 400
            viewOverlay?.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.transparent_alpha
                )
            )
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation?) {
                }

                override fun onAnimationEnd(animation: Animation?) {
                    viewOverlay.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.transparent_alpha
                        )
                    )
                    viewOverlay.visibility = View.VISIBLE
                }

                override fun onAnimationRepeat(animation: Animation?) {
                }
            })
            viewOverlay.startAnimation(anim)
            return
        }
        val anim: Animation = AlphaAnimation(1f, 0f)
        anim.interpolator = DecelerateInterpolator()
        anim.duration = 400
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
                if (isHideRecyclerView) {
                    recyclerViewMenu?.visibility = View.INVISIBLE
                }
            }

            override fun onAnimationEnd(animation: Animation?) {
                viewOverlay.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.transparent
                    )
                )
                viewOverlay.visibility = View.GONE
            }

            override fun onAnimationRepeat(animation: Animation?) {
            }

        })
        viewOverlay.startAnimation(anim)
    }

    var isTrue = true
    var onLongclickButtonMenu: View.OnLongClickListener? = null
    fun initControl() {
        buttonMenu.setOnClickListener {
            clickMenu()
        }
        viewOverlay.setOnClickListener {
            if (recyclerViewMenu.visibility == View.VISIBLE) {
                showViewOverlay(false, true)
            }
        }
        textBubleMessage.setOnClickListener {
            textBubleMessage.visibility = View.GONE
        }
        buttonMenu.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(v: View?): Boolean {
//                showTextBubleChat(
//                    "\uD83C\uDF81 Bạn đã dự đoán sản phẩm <u>Voucher giảm giá mua ví tại Ốc Sên shop có giá là bao nhiêu?</u> với giá 68.000 đ giá thực tế của sản phẩm là 119.100 đ bạn đã xếp hàng 4 / tổng số 69 người chơi.",
//                    20000
//                )
//                showTextBubleDialog(
//                    "\uD83C\uDF81 Bạn đã dự đoán sản phẩm <u>Voucher giảm giá mua ví tại Ốc Sên shop có giá là bao nhiêu?</u> với giá 68.000 đ giá thực tế của sản phẩm là 119.100 đ bạn đã xếp hàng 4 / tổng số 69 người chơi.",
//                    20000, false
//                )
                if (recyclerViewMenu.visibility == View.VISIBLE) {
                    showViewOverlay(false, true)
                }
                onLongclickButtonMenu?.onLongClick(v)
                return true
            }
        })
    }

    private fun clickMenu() {
        if (recyclerViewMenu.visibility == View.VISIBLE) {
            showViewOverlay(false, true)
            return
        } else {
            initAdapter()
            showViewOverlay(true)
            recyclerViewMenu.visibility = View.VISIBLE
        }
    }

    fun showTextBubleChat(msg: String?, timeCloseMs: Long? = 0) {
        if (msg.isNullOrEmpty() || textBubleMessage == null) {
            return
        }
        activity?.runOnUiThread {
            textBubleMessage?.setText("");
            textBubleMessage?.visibility = View.VISIBLE
            textBubleMessage?.setCharacterDelay(20);
            textBubleMessage?.setMovementMethod(ScrollingMovementMethod())
            textBubleMessage?.animateText(msg);
            if (timeCloseMs != null && timeCloseMs > 0L) {
                Handler(Looper.getMainLooper()).postDelayed({
                    textBubleMessage?.visibility = View.GONE
                }, timeCloseMs)
            }
        }
    }


    var interactiveLiveStreamFragment: InteractiveLiveStreamFragment? = null
    fun startGetConfigInteractive() {
        ServiceUtilTT.startTimerRequestConfig(
            requireContext(),
            appId,
            object : OnGetConfigInteractiveListener {
                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    item: ItemInteractiveConfig?
                ) {
                    if (isSuccess && item != null) {
                        activity?.runOnUiThread {
                            initInteractiveLiveStreamFragment()
                        }
                    }

                }
            },
            10000
        )
    }

    var isShowNews: Boolean = false
    private fun showInteractive() {
        if (currentFragment != null && currentFragment is InteractiveLiveStreamFragment) {
            (currentFragment as InteractiveLiveStreamFragment).showNews(isShowNews)
        } else {
            interactiveLiveStreamFragment = null
            enableInitInteractive = true
            initInteractiveLiveStreamFragment()
        }
    }

    /*fun initInteractiveLiveStreamFragment() {
        if (!isTabSelected || !ServiceUtilTT.isGetConfigFirst || activity == null || requireActivity().isDestroyed || requireActivity().isFinishing) {
            return
        }
        val item = ServiceUtilTT.itemInteractiveConfig
        if (item == null) {
            return
        }
        activity?.runOnUiThread {
            if (item.menu == null || item.menu.size == 0) {
                buttonMenu.visibility = View.INVISIBLE
            } else {
                buttonMenu.visibility = View.VISIBLE
            }
            if (interactiveLiveStreamFragment == null) {
                if (!enableInitInteractive) {
                    return@runOnUiThread
                }
                enableInitInteractive = false
                InteractiveLiveStreamFragment.isShowNews = isShowNews
                interactiveLiveStreamFragment = InteractiveLiveStreamFragment()
                val bundle = Bundle()
                bundle.putBoolean(Constant.IS_RUN_PLAY, true)
                updateConfigInteractive(item)
                interactiveLiveStreamFragment!!.arguments = bundle
                interactiveLiveStreamFragment?.onShowListener =
                    object : OnShowListener {
                        override fun onViewCreated() {
                            interactiveLiveStreamFragment?.checkRefresh()
                        }

                        override fun onResume() {
                            progressBarMain?.visibility = View.GONE
                        }

                        override fun onPause() {
                        }

                        override fun onStop() {
                        }
                    }
                showFragment(interactiveLiveStreamFragment)
                return@runOnUiThread
            }
            updateConfigInteractive(item)
        }
    }*/
    fun initInteractiveLiveStreamFragment() {
        if (!isTabSelected || activity == null || requireActivity().isDestroyed || requireActivity().isFinishing) {
            return
        }
        val item = ServiceUtilTT.itemInteractiveConfig
        activity?.runOnUiThread {
            if (interactiveLiveStreamFragment == null) {
                if (!enableInitInteractive) {
                    return@runOnUiThread
                }
                enableInitInteractive = false
                InteractiveLiveStreamFragment.isShowNews = isShowNews
                interactiveLiveStreamFragment = InteractiveLiveStreamFragment()
                val bundle = Bundle()
                bundle.putBoolean(Constant.IS_RUN_PLAY, true)
                updateConfigInteractive(item)
                interactiveLiveStreamFragment!!.arguments = bundle
                interactiveLiveStreamFragment?.onShowListener =
                    object : OnShowListener {
                        override fun onViewCreated() {
                            interactiveLiveStreamFragment?.checkRefresh()
                        }

                        override fun onResume() {
                            progressBarMain?.visibility = View.GONE
                        }

                        override fun onPause() {
                        }

                        override fun onStop() {
                        }
                    }
                showFragment(interactiveLiveStreamFragment)
                return@runOnUiThread
            }
            updateConfigInteractive(item)
        }
    }

    fun updateConfigInteractive(item: ItemInteractiveConfig?) {
        if (item == null) {
            return
        }
        interactiveLiveStreamFragment?.updatePlayer(item.urlLivestream)
        interactiveLiveStreamFragment?.updatePort(item.portSocket)
        interactiveLiveStreamFragment?.updateImagePlacholder(item.imagePlaceholderUrl)
        interactiveLiveStreamFragment?.updateShareFacebook(item.urlShare)
        if (!item.baseUrl.isNullOrEmpty()) {
            ConstantTT.BASE_URL = item.baseUrl.trim()
        }
        val isShowNews = Constant.TYPE_VISIBLE.equals(item.typeShowNews)
//        interactiveLiveStreamFragment?.showNews(isShowNews)
        if (item.menu == null || item.menu.size == 0) {
            buttonMenu?.visibility = View.INVISIBLE
        } else {
            buttonMenu?.visibility = View.VISIBLE
        }
    }

    fun playPlayer(isPlay: Boolean) {
        if (isPlay) {
            interactiveLiveStreamFragment?.resumePlay()
        } else {
            interactiveLiveStreamFragment?.pauseFragment()
        }

    }

    private fun stopTimerGetLive() {
        ServiceUtilTT.stopRequestConfig()
    }

    var currentFragment: Fragment? = null
    fun showFragment(fragment: Fragment?) {
        if (fragment == null || isDetached) {
            return
        }
//        if (currentFragment != null && fragment.javaClass == currentFragment!!.javaClass) {
//            return
//        }
        if (currentFragment != null && currentFragment is InteractiveLiveStreamFragment) {
            interactiveLiveStreamFragment = null
        }
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(
            R.anim.fragment_right_to_left,
            R.anim.fragment_right_to_left_exit,
            R.anim.fragment_left_to_right,
            R.anim.fragment_left_to_right
        )
        currentFragment = fragment
        fragmentTransaction.replace(R.id.frameLayoutContainer, fragment)
        fragmentTransaction.commit()
    }

    fun removeFragment(fragment: Fragment?) {
        if (fragment == null || isDetached) {
            return
        }
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.remove(fragment)
        fragmentTransaction.commit()
    }

    fun showAddFragment(fragment: Fragment?, isAddBackstack: Boolean = false) {
        if (fragment == null || isDetached) {
            return
        }
        if (currentFragment != null && fragment.javaClass == currentFragment!!.javaClass) {
            return
        }
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(
            R.anim.fragment_right_to_left,
            R.anim.fragment_right_to_left_exit,
            R.anim.fragment_left_to_right,
            R.anim.fragment_left_to_right
        )
        currentFragment = fragment
        if (isAddBackstack) {
            fragmentTransaction.addToBackStack("")
        }
        fragmentTransaction.add(R.id.frameLayoutContainer, fragment)
        fragmentTransaction.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val list = childFragmentManager.fragments
        if (list.size > 0) {
            for (item in list) {
                item.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    fun canBack(): Boolean {
        showViewOverlay(false)
        recyclerViewMenu?.visibility = View.INVISIBLE
        if ((currentFragment is InteractiveLiveStreamFragment && !isShowNews) || !isOnScreen) {
            return false;
        } else {
            if (currentFragment is WebviewFragment) {
                val isBack = (currentFragment as WebviewFragment).backUrl()
                if (isBack) {
                    return true
                }
            }

            if (currentFragment is MyVoucherFragment) {
                val isBack = (currentFragment as MyVoucherFragment).back()
                if (isBack) {
                    return true
                }
            }
            if (currentFragment is InteractiveLiveStreamFragment && isShowNews) {
                isShowNews = false
                showInteractive()
                return true
            }
            interactiveLiveStreamFragment = null
            enableInitInteractive = true
            initInteractiveLiveStreamFragment()
            return true
        }
    }

    var isOnScreen: Boolean = false
    override fun onResume() {
        super.onResume()
        isOnScreen = true
        onShowListener?.onResume()
        GeneralUtils.buttonMenu = buttonMenu
        if (!appId.isNullOrEmpty() && ServiceUtilTT.isGetConfigFirst) {
            startGetConfigInteractive()
        }
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onPause() {
        super.onPause()
        isOnScreen = false
//        stopTimerGetLive()
        showViewOverlay(false, true)
        onShowListener?.onPause()
        interactiveLiveStreamFragment?.closeSocket()

    }

    override fun onStop() {
        super.onStop()
        if (appId != null) {
            stopTimerGetLive()
        }
        onShowListener?.onStop()
    }

    fun closeSocket() {
        SocketMangager.getInstance().close()
    }

    fun checkShowInterActive() {
        interactiveLiveStreamFragment?.checkShowInterActive()
    }
}
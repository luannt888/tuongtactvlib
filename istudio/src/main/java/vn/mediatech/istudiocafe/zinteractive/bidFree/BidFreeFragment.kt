package vn.mediatech.interactive.bidFree

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_bid_free.*
import kotlinx.android.synthetic.main.fragment_bid_free.buttonClose
import kotlinx.android.synthetic.main.fragment_bid_free.textNotify
import kotlinx.android.synthetic.main.fragment_bid_free.textTimeCountDown
import kotlinx.android.synthetic.main.fragment_bid_free.textTitle
import kotlinx.android.synthetic.main.fragment_bid_guess_price.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.interactive.slideImage.SlideImageAdapter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class BidFreeFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var itemObj: ItemBidProduct? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var onClickListener: OnClickButtonBidListenner? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bid_free, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED

            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                    com.google.android.material.R.id.design_bottom_sheet)
                    ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }
    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
    }
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(ConstantTT.DATA, itemObj)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()
    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        itemObj = bundle?.getParcelable(ConstantTT.DATA)!!
        if (itemObj == null) {
            dismiss()
            return
        }
        AppUtils.loadImage(context, imgAvata, itemObj!!.imageUrl)
        textTitle.text = itemObj!!.name
        textPriceInfo.text = String.format(Locale.ENGLISH, getString(R.string.price_zero_title), AppUtils.moneyFormat(itemObj!!.priceSale))
//        holder.layoutRoot.setOnClickListener {
//            onItemClickListener?.onClick(itemObj,position)
//        }

        Handler(Looper.getMainLooper()).postDelayed({
            startPlayGalery(itemObj!!.galleryUrl, imgAvata)
        }, 3000)
        if (!itemObj!!.playerMessage.isNullOrEmpty()) {
            setTextNotify(itemObj!!.playerMessage)
        }
        buttonBid.setOnClickListener {
            setTextNotify("")
            onClickListener?.onClick(itemObj!!)
        }
    }


    fun initControl() {
        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
//        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        timer?.cancel()
        timer1?.cancel()
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

    var timer: Timer? = null
    fun startCountUp(timeStartSecond: Long) {
        var timeCount = timeStartSecond
        timer?.cancel()
        timer = Timer()
        timer!!.schedule(timerTask {
            activity?.runOnUiThread {
                if (timeCount >= 0) {
                    textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(timeCount)
                }
                timeCount = timeCount + 1
            }
        }, 0, 1000)
    }

    fun startCountDown(timeSeconds: Long) {
        timer?.cancel()
        var timeCount = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            if (timeCount >= 0) {
                val time = timeCount
                activity?.runOnUiThread {
                    textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(time)
                    textTimeCountDown?.setTextColor(Color.YELLOW)
                    timeCount = timeCount - 1
                }
            } else {
                try {
                    activity?.runOnUiThread {
                        dismiss()
                    }
                } catch (e: Exception) {
                }
            }
        }, 0, 1000)
    }

    private fun setupBackPressListener() {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                activity?.finish()
                true
            } else
                false
        }
    }

    fun setTextNotify(msg: String?) {
        if (textNotify != null) {
            textNotify?.visibility = View.VISIBLE
            AppUtils.setHtmlTextView(msg, textNotify)
        }
    }

    interface OnClickButtonBidListenner {
        fun onClick(item: ItemBidProduct)
    }

    var timer1: Timer? = null
    fun startPlayGalery(galeryArrayString: Array<String>?, imageView
    : ImageView?) {

        if (galeryArrayString == null || galeryArrayString!!.size == 0) {
            return
        }
        Loggers.e("GALERRY", galeryArrayString!!.toString())
        val adapter = SlideImageAdapter(requireContext(), galeryArrayString.toCollection(ArrayList()))
        slideImgViewPager.setAdapter(adapter)
        slideImgViewPager.visibility = View.VISIBLE
        var index = 0
        timer1 = Timer()
        timer1!!.schedule(timerTask {
            activity?.runOnUiThread {
                Loggers.e("GALERRY", "" + index + "/ " + galeryArrayString.get(index))
//                AppUtils.loadImage(context, imageView, galeryArrayString.get(index))
                slideImgViewPager.setCurrentItem(index, true)
                index = index + 1
                if (index > (galeryArrayString.size - 1)) {
                    index = 0
                }
                if (galeryArrayString.get(index).isNullOrEmpty()) {
                    return@runOnUiThread
                }
            }
        }, 0, 4000)
    }
}
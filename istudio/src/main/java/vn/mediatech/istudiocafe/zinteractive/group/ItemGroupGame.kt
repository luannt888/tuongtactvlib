package vn.mediatech.interactive.group

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
//<<<<<<< HEAD
class ItemGroupGame(
        @SerializedName("Index") val index: Int?,
        @SerializedName("Name") val name: String?,
        @SerializedName("Avt") val avatar: String?,
        @SerializedName("MemberCount") val menberCount: String?,
        @SerializedName("MemberDetail") var memberDetail: String?,
        @SerializedName("Note") val note: String?,
        @SerializedName("Description") val description: String?

) : Parcelable
//=======
/*
Index: 1,
Name: "Hội cựu chiến binh",
Avt: "https://daugiatruyenhinh.com/upload/avatar/group/ttnga_vmor.jpg",
MemberCount: 4,
MemberDetail: null,
Note: null,
Description: null*/

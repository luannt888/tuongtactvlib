package vn.mediatech.interactive.vote

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_bid_guess_price.*
import kotlinx.android.synthetic.main.fragment_vote.*
import kotlinx.android.synthetic.main.fragment_vote.buttonClose
import kotlinx.android.synthetic.main.fragment_vote.layoutRoot
import kotlinx.android.synthetic.main.fragment_vote.textTimeCountDown
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.app.JsonParserUtil
import vn.mediatech.istudiocafe.zinteractive.fragment.InteractiveFragment
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class VoteFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var itemObj: ItemVote? = null
    var isSuccess = false
    var bottomSheetDialog: BottomSheetDialog? = null
    var onClickListener: VoteAdapter.OnItemClickListener? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_vote, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                    com.google.android.material.R.id.design_bottom_sheet)
                    ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(ConstantTT.DATA, itemObj)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        val height = requireArguments().getInt(ConstantTT.HEIGHT)
        try {
            if (height > 0) {
                val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
                param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                param?.height = height
            }
        } catch (e: Exception) {
        }
        try {
            itemObj = bundle?.getParcelable(ConstantTT.DATA)!!
        } catch (e: Exception) {
            dismiss()
        }
        if (itemObj == null) {
            dismiss()
            return
        }
        updateUI(itemObj)
    }

    var adapter: VoteAdapter? = null
    fun updateUI(item: ItemVote?) {
        if (item == null) {
            activity?.onBackPressed()
            return
        }
        this.itemObj = item
        textQuestion.text = item.voteTitle
        if (item!!.voteDetail.isNullOrEmpty()) {
            activity?.onBackPressed()
            return
        }
        val gson = Gson()
        var arr = JsonParserUtil.getJsonArray(item.voteDetail)
        if (arr == null || arr.length() == 0) {
            activity?.onBackPressed()
            return
        }
        val list: ArrayList<ItemVoteAnswer> = ArrayList()
        for (i in 0 until arr.length()) {
            val str = arr.get(i).toString()
            val item = try {
                gson.fromJson(str, ItemVoteAnswer::class.java)
            } catch (e: Exception) {
                null
            }
            if (item != null) {
                list.add(item)
                if (itemObj!!.status != null && itemObj!!.status!!.trim().equals("1")) {
                    if (!itemObj!!.currentVote.isNullOrEmpty() && itemObj!!.currentVote!!.trim().equals(item.voteID!!.trim())) {
                        itemObj!!.indexSelected = list.size - 1
                    }
                }
            }
        }

        if (list.size == 0) {
            activity?.onBackPressed()
            return
        }
        adapter = VoteAdapter(requireActivity(), list, 1, 1, 1)
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView?.layoutManager = layoutManager
        recyclerView?.adapter = adapter
        /*if (!itemIQResult.chooseReply.isNullOrEmpty()) {
            for (i in 0 until list.size) {
                if (list.get(i).title.equals(itemIQResult.chooseReply)) {
                    adapter?.indexTrue = i
                    break
                }
            }
        }*/
        adapter?.setSelected(item.indexSelected)
        adapter?.onItemClickListener = object : VoteAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemVoteAnswer, position: Int) {
                onClickListener?.onClick(itemObject, position)
            }
        }
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
//        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        timer?.cancel()
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

    var timer: Timer? = null
    fun startCountUp(timeStartSecond: Long) {
        var timeCount = timeStartSecond
        timer?.cancel()
        timer = Timer()
        timer!!.schedule(timerTask {
            activity?.runOnUiThread {
                textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(timeCount)
                timeCount = timeCount + 1
            }
        }, 0, 1000)
    }

    fun startCountDown(timeSeconds: Long) {
        if (timeSeconds <= 0) {
            return
        }
        timer?.cancel()
        var timeCount = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            activity?.runOnUiThread {
                if (timeCount >= 0) {
                    textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(timeCount)
                    if (timeCount <= 10) {
                        textTimeCountDown?.setTextColor(Color.RED)
                    } else {
                        textTimeCountDown?.setTextColor(Color.WHITE)
                    }
                    timeCount = timeCount - 1
                } else {
                    dismiss()
                }
            }
        }, 0, 1000)
    }

    private fun setupBackPressListener() {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                activity?.finish()
                true
            } else
                false
        }
    }

    var interactiveFragment: InteractiveFragment? = null


}
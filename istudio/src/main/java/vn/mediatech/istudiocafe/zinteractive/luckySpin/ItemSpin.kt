package vn.mediatech.interactive.luckySpin

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemSpin(
        @SerializedName("maxplayer") val maxPlayer: String?,
        @SerializedName("timecount") val timeCount: Long?,
        @SerializedName("msg") val msg: String?,
        @SerializedName("type") val type: String?,
        @SerializedName("status") val status: String?,
        @SerializedName("diemdanh") val isDiemdanh: Boolean?,
) : Parcelable
/*
"maxplayer":4,
"timecount":20,
"msg":"Nhanh tay bấm ĐỒNG Ý để tham gia quay ngay bây giờ!",
"type":"QUICK"*/

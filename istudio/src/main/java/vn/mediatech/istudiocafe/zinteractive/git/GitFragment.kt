package vn.mediatech.interactive.git

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_bid_guess_price.*
import kotlinx.android.synthetic.main.fragment_git.*
import kotlinx.android.synthetic.main.fragment_git.buttonClose
import kotlinx.android.synthetic.main.fragment_git.layoutRoot
import kotlinx.android.synthetic.main.item_tab_imageview.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import java.util.*
import kotlin.collections.ArrayList

class GitFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var data: String? = null
    var isSuccess = false
    var bottomSheetDialog: BottomSheetDialog? = null
    var onClickListener: OnClickListenner? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_git, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED

            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                    com.google.android.material.R.id.design_bottom_sheet)
                    ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
//            bottomSheet.setBackgroundColor(Color.parseColor("#7A000000"))
//            bottomSheet.setBackgroundColor(Color.parseColor("#000000"))
//            bottomSheet.setBackgroundColor(Color.parseColor("#00FFFFFF"))
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }
    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
    }
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putString(ConstantTT.DATA, data)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()
    }

    var itemList: ArrayList<ItemGitCategory>? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        val height = requireArguments().getInt(ConstantTT.HEIGHT)
        try {
            if (height > 0) {
                val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
                param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                param?.height = height
            }
        } catch (e: Exception) {
        }
        itemList = bundle?.getParcelableArrayList(ConstantTT.DATA)
        if (itemList == null || itemList!!.size == 0) {
            return
        }
        fragmentList.clear()
        tabTitleList.clear()
        for (i in 0 until itemList!!.size) {
            val fragment = GitCategoryFragment()
            val bundle = Bundle()
            bundle.putParcelable(ConstantTT.DATA, itemList!!.get(i))
            bundle.putInt(ConstantTT.POSITION, i)
            fragment.arguments = bundle
            fragment.onClickListener = object : ItemGitAdapter.OnItemClickListener {
                override fun onClick(itemObject: ItemGit, position: Int) {
                    dismiss()
                    onClickListener?.onClick(itemObject, position)
                }
            }
            fragmentList.add(fragment)
            val title = if (itemList!!.get(i).name.isNullOrBlank()) ("Gift " + i) else itemList!!.get(i).name
            tabTitleList.add(title!!)
        }
//        (fragmentList.get(0) as? GitCategoryFragment)?.checkRefresh()
        initPager()
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    fun updateFragment(item: ItemGitCategory, position: Int) {
        itemList?.set(position, item)
        (fragmentList.get(position) as? GitCategoryFragment)?.updateUI(item)
    }

    var fragmentList: ArrayList<Fragment> = ArrayList()
    var tabTitleList: ArrayList<String> = ArrayList()
    fun initPager() {
        val viewPagerAdapter = ViewStatePagerAdapter(childFragmentManager, fragmentList, tabTitleList)
        viewPager.adapter = viewPagerAdapter
        viewPager.offscreenPageLimit = fragmentList.size
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                (fragmentList.get(position) as? GitCategoryFragment)?.checkRefresh()
                onClickListener?.onSelectPage(itemList!!.get(position), position)

            }

            override fun onPageScrollStateChanged(state: Int) {
            }

        })
        tabLayout.setupWithViewPager(viewPager)
        setupTabIcons()
    }

    private fun setupTabIcons() {
        for (i in 0 until tabLayout.tabCount) {
            var view = LayoutInflater.from(context).inflate(R.layout.item_tab_imageview, null)
            tabLayout.getTabAt(i)!!.setCustomView(view)
            AppUtils.loadImage(context, view.imgAvatar, itemList!!.get(i).icon)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
//        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

    interface OnClickListenner {
        fun onClick(item: ItemGit, position: Int)
        fun onSelectPage(itemGitCategory: ItemGitCategory?, position: Int)
    }
}
package vn.mediatech.istudiocafe.zinteractive.emotion

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.nio.file.attribute.DosFileAttributes

@Parcelize
//<<<<<<< HEAD
class ItemEmotionResult(
        @SerializedName("Icon_Id") var id: Int?,
        @SerializedName("Icon_Name") val name: String?,
        @SerializedName("Icon_Png") val png: String?,
        @SerializedName("type") val type: String?,
        @SerializedName("Player_Count") val playerCount: Int?,
        @SerializedName("Total_Rate") val totalRate: Int?,
        @SerializedName("Rate_avg") val rateAvg: Double?,
        @SerializedName("Icon_Sprite") val sprite: ItemSprite?,
//         var sprite: ItemSprite?,
//        @SerializedName("Icon_Seq") val seq: ItemSeq?,
) : Parcelable
//=======
/*
Icon_Id": 51,
"Icon_Name": "Like",
"Icon_Png": "https://daugiatruyenhinh.com/upload/istudios/emoji/51_51_Like.seq2",
"Icon_Seq": "https://daugiatruyenhinh.com/upload/istudios/emoji/51_51_Like.seq2",
"Icon_Sprite": "https://daugiatruyenhinh.com/upload/istudios/emoji/51_51_Like.seq2",
"Channel": "tuongtac.tv",
"Player_Count": 10,
"Total_Rate": 48,
"Rate_avg": 4.8*/

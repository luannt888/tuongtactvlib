package vn.mediatech.interactive.slideImage

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide


class SlideImageAdapter constructor(context: Context, imageList: ArrayList<String>) : PagerAdapter() {
    private val mContext: Context
    private val mImageList = imageList
    var onClickItemListener: OnClickItemListener? = null
    override fun getCount(): Int {
        return mImageList.size
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = ImageView(mContext)
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER)
        imageView.setBackgroundColor(Color.parseColor("#00000000"))
        Glide.with(mContext).load(mImageList.get(position)).into(imageView)
        container.addView(imageView, 0)
        imageView.setOnClickListener {
            onClickItemListener?.onClick(mImageList.get(position), position)
        }
        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as ImageView)
    }

    init {
        mContext = context
    }

    interface OnClickItemListener {
        fun onClick(urlImage: String, position: Int)
    }
}
package vn.mediatech.istudiocafe.zinteractive.app;

import android.content.Context;

import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.util.SharedPreferencesManager;

public class ConstantTT {

    public static  String BASE_URL = "https://api.daugiatruyenhinh.com/";
    public static final int POST = 1;
    public static final int GET = 2;
    public static final int SUCCESS = 200;
    public static final String CODE = "status";
    public static final String RESULT = "result";
    public static final String DATA = "data";
    public static final String NAME = "name";
    public static final String GAME_ONLINE = "game_online";
    public static final String DATA_GAME = "dataGame";
    public static final String POSITION = "position";
    public static final String HEIGHT = "height";
    public static final String TYPE = "type";
    public static final String USER_ID = "USER_ID";
    public static final String USER = "USER";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_TOKEN = "USER_TOKEN";
    public static final String USER_AVATAR = "USER_AVATAR";
    public static final String GROUP_ID = "GROUP_ID";
    public static final String GROUP_NAME = "GROUP_NAME";
    public static final String USER_STATUS = "USER_STATUS";
    public static final String SHOW_NEWS = "SHOW_NEWS";
    public static final String ROLE = "ROLE";
    public static final String APP_ID = "appId";
    public static final String CHANEL_DEFAULT = "mediatech";
    public static String CHANEL_ID = CHANEL_DEFAULT;
    public static final String ACTION_INTERACTIVE = "action_interactive";

    //Socket Config

    public static final int CLOSE_CONNECT = 1001;
    // Constant service interactive
    public static final String PORT = "PORT";
    public static final String SHARE = "share";
    public static final String SERVICE_BID = "BID";
    public static final String TIME = "TIME";

    public static final String SERVICE_SPIN = "SPIN";
    public static final String SERVICE_VOTE2 = "VOTE2";
    public static final String SERVICE_IQ = "IQ";
    public static final String SERVICE_SETTING = "SETTING";
    public static final String SERVICE_REGISTER = "REGISTER";
    public static final String SERVICE_DISCONNECT = "DISCONNECT";
    public static final String SERVICE_GUESS_PRICE = "DOANGIA";
    public static final String SERVICE_CHAT = "CHAT";
    public static final String SERVICE_GIT = "EMOTION";
    public static final String SERVICE_GROUP = "GROUP";
    public static final String SERVICE_INVITE = "INVITE";
    public static final String SERVICE_NOTIFY = "NOTIFY";
    public static final String SERVICE_DIEMDANH = "DIEMDANH";

    public static final String BID_PRODUCT = "bid_product";
    public static final String BID_MULTI_PRODUCT = "Bid_Multil_Products";

    public static final String MESSAGE = "message";
    // key json interactive
    public static final String STATUS = "status";
    public static final String ERR_CODE = "errCode";
    public static final String SERVICE = "service";
    public static final String KEY = "key";
    public static final String CMD = "cmd";
    public static final String CONNECTION_COUNT = "connection_count";
    public static final String FROM = "from";
    public static final String SETTING = "setting";

    //Menu Constant
    public static final String MENU_START_INTERACTIVE = "Start_Interactive";
    public static final String MENU_CHAT = "CHAT";
    public static final String MENU_ISTUDIO_VOTE = "IStudio_Vote";
    public static final String MENU_ISTUDIO_OFFER = "IStudio_Offer";
    public static final String MENU_BID_FREE = "Bid_Free";
    public static final String MENU_BID_MULTIL = "Bid_Multil";
    public static final String MENU_IQ = "IQ";
    public static final String MENU_EMOTION = "Emotion";
    public static final String MENU_EMOTION2 = "Emotion2";
    public static final String MENU_VOTE = "Vote";
    public static final String MENU_VOTE2 = "Vote2";
    public static final String MENU_GIFT = "Gift";
    public static final String MENU_VOICE = "Voice";
    public static final String MENU_ADS = "Ads";
    public static final String MENU_VIDEO_CALL = "Video_Call";
    public static final String MENU_DOAN_GIA = "DoanGia";
    public static final String MENU_GUIDE = "guide";
    public static final String MENU_GROUP = "Group";
    public static final String MENU_INVITE = "Invite";
    public static final String MENU_RATE = "Rate";
    public static final String MENU_WEBGAME = "Webgame";


    //BID_GAME_FREE

    public static final String BID_MESSAGE_CALLBACK = "BID-MESSAGE-CALLBACK";
    public static final String BID_GAME_FREE_COUNTDOWN = "BID-GAME-FREE-COUNTDOWN";
    public static final String START_GAME_FREE_BID = "START-GAME-FREE-BID";
    public static final String STOP_GAME_FREE_BID = "STOP-GAME-FREE-BID";
    public static final String BID_GAME_FREE = "BID-GAME-FREE";

    public static final String UPDATE_GAME_MUTIL_BID = "UPDATE-GAME-MUTIL-BID";
    public static final String NEXT_PRODUCT_MUTIL_BID = "NEXT-PRODUCT-MUTIL-BID";
    public static final String START_GAME_MUTIL_BID = "START-GAME-MUTIL-BID";
    public static final String STOP_GAME_MUTIL_BID = "STOP-GAME-MUTIL-BID";
    public static final String DISPLAY_WINNER_MULTIL_BID = "DISPLAY-WINNER-MULTIL-BID";
    public static final String BID_GAME_MUTIL_BID = "BID-GAME-MUTIL-BID";


    //    BidMulti
    public static final String SERVICE_BIDMULTI = "MUTILBID";
    public static final String START = "START";
    public static final String BID = "BID";
    public static final String STOP = "STOP";
    public static final String WIN = "WIN";
    public static final String NEXT = "NEXT";

    //IQ Game
    public static final String PLAYER_ANSWER_TIMEOUT = "PLAYER-ANSWER-TIMEOUT";
    public static final String START_GAME_IQ = "START-GAME-IQ";
    public static final String STOP_GAME_IQ = "STOP-GAME-IQ";
    public static final String SHOW_TOP_IQ = "SHOW-TOP";
    public static final String IQ_FINISH = "IQ-FINISH";
    public static final String SHOW_CHART_GAME_IQ = "SHOW-CHART-GAME-IQ";
    public static final String PLAYER_ANSWER = "PLAYER-ANSWER";
    public static final String IQ_MESSAGE = "IQ-MESSAGE";


    //Note Bid
    public static final String NOTE_BID_FREE = "free";
    public static final String NOTE = "note";
    public static final String NOTE_BID_MUTIL = "normal";

    //SPIN KEY
    public static final String SPIN_REQUEST_QUICK = "SPIN-REQUEST-QUICK";
    public static final String SPIN_ACCEPT_QUICK = "SPIN-ACCEPT-QUICK";
    public static final String SPIN_ACCEPT = "SPIN-ACCEPT";
    public static final String SPIN_RESPONSE = "SPIN-RESPONSE";

    //GUESS PRICE KEY

    public static final String DOANGIA_START = "DOANGIA-START";
    public static final String DOANGIA_SEND = "DOANGIA-SEND";
    public static final String DOANGIA_FINISH = "DOANGIA-FINISH";
    public static final String DOANGIA_STOP = "DOANGIA-STOP";


    //VOTE2 KEY

    public static final String VOTE2_START = "VOTE2-START";
    public static final String VOTE2_UPDATE_CLOCK = "VOTE2-UPDATE-CLOCK";
    public static final String VOTE2_RESPONSE = "VOTE2-RESPONSE";
    public static final String VOTE2_STOP = "VOTE2-STOP";
    public static final String VOTE2_SEND = "VOTE2-SEND";

    //CHAT KEY

    public static final String CHAT_SEND = "CHAT-SEND";
    public static final String SOCKET_CLIENT = "#SOCKET-CLIENT";

    //GIT KEY

    public static final String EMOTION_SEND = "EMOTION-SEND";
    public static final String EMOTION_APPROVED = "EMOTION-APPROVED";


    //GROUP KEY
    public static final String GROUP_START = "GROUP-START";
    public static final String GROUP_STOP = "GROUP-STOP";
    public static final String GROUP_FINISH = "GROUP-FINISH";

    //INVITE KEY
    public static final String INVITE_START = "INVITE-START";
    public static final String INVITE_STOP = "INVITE-STOP";
    public static final String INVITE_FINISH = "INVITE-FINISH";

    //RATE KEY EMOTION2
    //Service
    //EMOTION-SEND
    //EMOTION-START
    //EMOTION-FINISH
    //EMOTION-STOP

    public static final String SERVICE_EMOTION2 = "EMOTION2";
    public static final String EMOTION2_START = "EMOTION-START";
    public static final String EMOTION2_STOP = "EMOTION-STOP";
    public static final String EMOTION2_FINISH = "EMOTION-FINISH";
    public static final String EMOTION2_SEND = "EMOTION-SEND";
//Service

    public static final String SERVICE_WEBGAME = "WEBGAME";
    public static final String WEBGAME_START = "WEBGAME-START";
    public static final String WEBGAME_FINISH = "WEBGAME-FINISH";
    public static final String WEBGAME_STOP = "WEBGAME-STOP";
    public static final String WEBGAME_FLAPPY_BIRD = "FLAPPY BIRD";
    public static final String WEBGAME_TOWER = "TOWER BUILDING";
    public static final String WEBGAME_FRUIJT = "FRUIT NINJA";
    public static final String WEBGAME_KNIFE = "KNIFE HIT";
    //TYPE EFFECT FIREWORK

    public static final int TYPE_EFFECT_WIN_ICON = 1;
    public static final int TYPE_EFFECT_WIN = 2;
    public static final int TYPE_EFFECT_FALSE = 3;


    //API
    public static final String API_GET_GUIDE_PLAY_GAME = "https://daugiatruyenhinh.com/api/v2.1/istudio/app/tutorial";

    public static String  validApiGameTT(Context context, String api) {
        String baseUrl;
        baseUrl = ConstantTT.BASE_URL;
        if(context != null && SharedPreferencesManager.isTest(context)){
        String baseUrlSave = SharedPreferencesManager.getBaseUrlGame(context);
        if (baseUrlSave != null && !(baseUrlSave.trim().equals(""))) {
            baseUrl = baseUrlSave;
        }
        }
        return baseUrl + api;
    }
}
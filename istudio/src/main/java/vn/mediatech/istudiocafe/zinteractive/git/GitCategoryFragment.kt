package vn.mediatech.interactive.git

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_iq.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import vn.mediatech.istudiocafe.zinteractive.app.SpacesItemDecoration
import java.util.*
import kotlin.concurrent.timerTask

class GitCategoryFragment : Fragment() {
    private var isViewCreated: Boolean = false
    private var isTabSelected: Boolean = false
    var itemObj: ItemGitCategory? = null
    var isSuccess = false
    var bottomSheetDialog: BottomSheetDialog? = null
    var onClickListener: ItemGitAdapter.OnItemClickListener? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_git_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        onShowDismissListener?.onShow()
//        checkRefresh()

        val bundle = savedInstanceState ?: arguments
        if (bundle!!.getInt(ConstantTT.POSITION) == 0) {
            init()
        }
        return
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(ConstantTT.DATA, itemObj)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()

    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        try {
            itemObj = bundle?.getParcelable(ConstantTT.DATA)!!
        } catch (e: Exception) {
        }
        if (itemObj == null) {
            return
        }
        updateUI(itemObj!!)
    }

    var adapter: ItemGitAdapter? = null
    fun updateUI(item: ItemGitCategory?) {
        this.itemObj = item
        if (item == null) {
            return
        }
        val list: ArrayList<ItemGit>? = item.data
        if (list == null || list.size == 0) {
            return
        }
        val numberColumn = 4
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.dimen_15)
        val layoutManager = GridLayoutManager(context, numberColumn, GridLayoutManager.VERTICAL, false)
        val itemDecoration: RecyclerView.ItemDecoration = SpacesItemDecoration(spacePx, numberColumn)
        adapter = ItemGitAdapter(requireActivity(), list, spacePx, 1, numberColumn)
        recyclerView?.layoutManager = layoutManager
        recyclerView.addItemDecoration(itemDecoration)
        recyclerView?.adapter = adapter
        /*if (!itemIQResult.chooseReply.isNullOrEmpty()) {
            for (i in 0 until list.size) {
                if (list.get(i).title.equals(itemIQResult.chooseReply)) {
                    adapter?.indexTrue = i
                    break
                }
            }
        }*/
        adapter?.onItemClickListener = object : ItemGitAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemGit, position: Int) {
                onClickListener?.onClick(itemObject, position)
            }
        }
    }

    fun initControl() {
    }


    var timer: Timer? = null
    fun startCountUp(timeStartSecond: Long) {
        var timeCount = timeStartSecond
        timer?.cancel()
        timer = Timer()
        timer!!.schedule(timerTask {
            activity?.runOnUiThread {
                textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(timeCount)
                timeCount = timeCount + 1
            }
        }, 0, 1000)
    }

    fun startCountDown(timeSeconds: Long) {
        timer?.cancel()
        var timeCount = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            if (timeCount >= 0) {
                activity?.runOnUiThread {
                    textTimeCountDown?.text = timeCount.toInt().toString()
                    if (textTimeCountDown.visibility == View.GONE) {
                        textTimeCountDown.visibility = View.VISIBLE
                    }
                    val progress = ((timeCount / timeSeconds.toFloat()) * 100)
                    if (progress >= 0 && progress <= 100) {
                        progressTime.progress = progress.toInt()
                    }
                    if (timeCount <= 10) {
                        textTimeCountDown?.setTextColor(Color.RED)
                    } else {
                        textTimeCountDown?.setTextColor(Color.WHITE)
                    }
                    timeCount = timeCount - 1
                }
            }
        }, 0, 1000)
    }

    private fun setupBackPressListener() {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                activity?.finish()
                true
            } else
                false
        }
    }



}
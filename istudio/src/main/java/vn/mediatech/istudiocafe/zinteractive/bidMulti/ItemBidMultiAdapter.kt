package vn.mediatech.interactive.bidMulti

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_bid_multi.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.app.AppUtils.Companion.moneyFormat
import vn.mediatech.interactive.app.JsonParserUtil
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.interactive.slideImage.SlideImageAdapter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class ItemBidMultiAdapter(
        val context: Context, val itemList: ArrayList<ItemBidMulti>, val type: Int, val style: Int, val
        numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0


    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return position
    }

    fun initViewSize(viewType: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
//        imageWidth = screenWidth / 4
//        imageHeight = imageWidth * 7 / 10
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_bid_multi,
                parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemBidMulti = itemList.get(position)
        if (holder is MyViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: MyViewHolder, itemObj: ItemBidMulti, position: Int) {
        if (!itemObj.status.equals("1")) {
            holder.layoutRoot.visibility = View.GONE
            return
        }
        if (itemObj.galleryUrl != null && itemObj.galleryUrl.size > 0) {
            val json = JsonParserUtil.getJsonObject(itemObj.galleryUrl!!.get(0))
            val img = JsonParserUtil.getString(json, "src")
            AppUtils.loadImage(context, holder.imgThumbnail, img)
        }
        holder.textTitle.text = itemObj.name
//        holder.textPrice.text = "GIÁ NIÊM YẾT: "
//        holder.textPrice.append(AppUtils.getColoredString(moneyFormat(itemObj.priceSale), Color.parseColor("#E8BE27")))
        var priceStr = "GIÁ NIÊM YẾT: " + AppUtils.getStringHtmlColor(moneyFormat(itemObj.priceSale), "#E8BE27")
        AppUtils.setHtmlTextView(priceStr, holder.textPrice)
        val str2 = "<= " + moneyFormat(itemObj.priceEnd)
        val str3 = moneyFormat(itemObj.priceStart)
        val str4 = moneyFormat(itemObj.priceStep)
        holder.textPriceInfo.text = "GIÁ KHỞI ĐIỂM : " + str3 + "      GIÁ CHỐT: " + str2 + "     BƯỚC GIÁ: " + str4 + "     "
        holder.textPriceInfo.isSelected = true
        holder.textPriceBid.text = "ĐẤU GIÁ: " + moneyFormat(itemObj.priceBid)
        if (!itemObj.msg.isNullOrEmpty()) {
            holder.textMessage.text = AppUtils.getHtmlFormat(itemObj.msg)
        }

        val priceBid = try {
            itemObj.priceBid!!.toLong()
        } catch (e: Exception) {
            0L
        }
        val priceEnd = try {
            itemObj.priceEnd!!.toLong()
        } catch (e: Exception) {
            0L
        }
        if (itemObj.isBid || (priceBid > priceEnd)) {
            if (priceBid > priceEnd) {
                holder.textPriceBid.text = "ĐÃ KẾT THÚC"
            }
            holder.buttonBid.isEnabled = false
        }

//        if (itemObj.isWin && !itemObj.playerMessage.isNullOrEmpty()) {
//            val jsonMessage = JsonParserUtil.getJsonObject(itemObj?.playerMessage)
//            val avatar = JsonParserUtil.getString(jsonMessage, "Avatar")
//            val winerName = JsonParserUtil.getString(jsonMessage, "Fullname")
//            val priceWin = moneyFormat(itemObj.priceBid)
//            AppUtils.loadImage(context, holder.imgAvatar, avatar)
//            holder.textName.text = winerName
//            holder.textPriceWin.text = priceWin
//            holder.buttonBid.visibility = View.GONE
//            holder.layoutWiner.visibility = View.VISIBLE
//        }
//        if (!itemObj?.msg.isNullOrEmpty()) {
//            val str = itemObj?.msg!!.substring(0, itemObj?.msg!!.indexOf("sản phẩm") + 8) + "."
//            holder.textMessage.text = AppUtils.getHtmlFormat(str)
//        }
//        holder.layoutRoot.setOnClickListener {
//            onItemClickListener?.onClick(itemObj,position)
//        }
        holder.buttonBid.setOnClickListener {
//            if (itemObj.priceBid!!.toInt() > 0) {
            val msg = "ĐẤU GIÁ SẢN PHẨM VỚI MỨC: " + moneyFormat(itemObj.priceBid) + " ?"
            holder.textNotify.text = msg
//            holder.layoutProduct.visibility = View.INVISIBLE
            holder.layoutNotify.visibility = View.VISIBLE
//            } else {
//            onItemClickListener?.onClickBid(itemObj, position)
//            }
        }
        holder.buttonOk.setOnClickListener {
            holder.layoutProduct.visibility = View.VISIBLE
            holder.layoutNotify.visibility = View.INVISIBLE
            holder.buttonBid.isEnabled = false
//            Handler(Looper.getMainLooper()).postDelayed({
//                holder?.buttonBid?.isEnabled = true
//            }, 3000)
            onItemClickListener?.onClickOk(itemObj, position)

        }
        holder.buttonNo.setOnClickListener {
            holder.layoutProduct.visibility = View.VISIBLE
            holder.layoutNotify.visibility = View.INVISIBLE
            onItemClickListener?.onClickNo(itemObj, position)
        }
        startCountDown(itemObj.endTimeMs, holder)
        Handler(Looper.getMainLooper()).postDelayed({
            startPlayGalery(itemObj.galleryUrl, holder)
        }, 3000)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgThumbnail = itemView.imgAvata
        val slideImgViewPager = itemView.slideImgViewPager
        val textTitle = itemView.textTitle
        val textPriceInfo = itemView.textPriceInfo
        val textPrice = itemView.textPrice
        val buttonBid = itemView.buttonBid
        val textPriceBid = itemView.textButtonBid
        val buttonOk = itemView.buttonOk
        val buttonNo = itemView.buttonNo
        val textNotify = itemView.textNotify
        val layoutNotify = itemView.layoutNotify
        val layoutProduct = itemView.layoutProduct
        val textMessage = itemView.textMessage
        val textTimeCountDown = itemView.textTimeCountDown
        val layoutEffect = itemView.layoutEffect
        val layoutWiner = itemView.layoutWiner
        val textPriceWin = itemView.textPriceWin
        val imgAvatar = itemView.imgAvatar
        val textName = itemView.textName

        init {
        }
    }

    interface OnItemClickListener {
        fun onClickOk(itemObject: ItemBidMulti, position: Int)
        fun onClickNo(itemObject: ItemBidMulti, position: Int)
        fun onClickBid(itemObject: ItemBidMulti, position: Int)
    }

    val timerList: ArrayList<Timer> = ArrayList()
    fun startCountDown(timeEndMs: Long?, holder: MyViewHolder?) {

        if (timeEndMs == null || timeEndMs <= 0 || holder == null) {

            (context as? Activity)?.runOnUiThread {
                holder?.buttonBid?.isEnabled = false
            }
            return
        }
        var timeCount = (timeEndMs - System.currentTimeMillis()) / 1000
        if (timeCount <= 0) {
            return
        }
        if (timeCount >= 44) {
            (context as? Activity)?.runOnUiThread {
                startEffectFirst(holder?.layoutEffect)
            }
        }
        val timer = Timer()
        timerList.add(timer)
        timer!!.schedule(timerTask {
            (context as? Activity)?.runOnUiThread {
                if (timeCount >= 0) {
                    holder?.textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(timeCount)
                    timeCount = timeCount - 1
                } else {
                    holder.buttonBid.isEnabled = false

                }
            }
        }, 0, 1000)
    }

    fun startPlayGalery(galeryArrayString: Array<String>?, holder: MyViewHolder?) {

        if (galeryArrayString == null || galeryArrayString!!.size == 0) {
            return
        }
        Loggers.e("GALERRY", galeryArrayString!!.toString())
        var index = 0
        val list: ArrayList<String> = ArrayList()
        for (i in 0 until galeryArrayString.size) {
            val json = JsonParserUtil.getJsonObject(galeryArrayString.get(i))
            val img = JsonParserUtil.getString(json, "src")
            list.add(img!!)
        }
        val adapter = SlideImageAdapter(context, list)
        holder?.slideImgViewPager?.setAdapter(adapter)
        holder?.slideImgViewPager?.visibility = View.VISIBLE
        val timer = Timer()
        timerList.add(timer)
        timer!!.schedule(timerTask {
            (context as? Activity)?.runOnUiThread {
                Loggers.e("GALERRY", "" + index + "/ " + list.get(index))
                index = index + 1
                if (index > (list.size - 1)) {
                    index = 0
                }
//                AppUtils.loadImage(context, holder!!.imgThumbnail, galeryArrayString.get(index))
                holder?.slideImgViewPager?.setCurrentItem(index, true)
            }
        }, 0, 4000)
    }

    fun stopAllTimer() {
        for (i in 0 until timerList.size) {
            timerList.get(i)?.cancel()
        }
    }

    fun startEffectFirst(view: View?) {
        if (view == null) {
            return
        }
        var count: Int = 5
        var timer = Timer()
        timer.schedule(timerTask {
            (context as? Activity)?.runOnUiThread {
                if (count > 0) {
//                    holderTrue?.layoutRoot?.isActivated = !holderTrue!!.layoutRoot!!.isActivated
                    view?.visibility = if (view!!.visibility == View.VISIBLE) View.GONE else View.VISIBLE
                    count = count - 1
                } else {
                    view?.visibility = View.GONE
                    timer.cancel()
                }
            }
        }, 0, 100L)
    }
}
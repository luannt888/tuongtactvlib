package vn.mediatech.istudiocafe.zinteractive.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_game.view.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.model.ItemMenu
import vn.mediatech.istudiocafe.R
import java.util.*

class ItemMenuAdapter(
        val context: Context, val itemList: ArrayList<ItemMenu>, val type: Int, val style: Int, val
        numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0


    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return position
    }

    fun initViewSize(viewType: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
//        imageWidth = screenWidth / 4
//        imageHeight = imageWidth * 7 / 10
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_game,
                parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemMenu = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemMenu, position: Int) {
        if (!itemObj.icon.isNullOrEmpty() && itemObj.playing!!) {
            holder.layoutRoot.visibility =View.VISIBLE
            holder.imgThumbnail.visibility = View.VISIBLE
            AppUtils.loadImageCircle(context, holder.imgThumbnail, itemObj.icon, -1)
        } else{
            holder.layoutRoot.visibility =View.GONE
            holder.imgThumbnail.visibility = View.GONE
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj,position)
//            holder?.layoutRoot?.isEnabled = false
//            Handler(Looper.getMainLooper()).postDelayed({holder?.layoutRoot?.isEnabled = true},3000)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgThumbnail = itemView.imgAvata
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemMenu, position: Int)
    }



}
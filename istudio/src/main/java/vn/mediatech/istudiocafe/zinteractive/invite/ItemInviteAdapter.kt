package vn.mediatech.interactive.invite

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_presenter.view.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.istudiocafe.R

class ItemInviteAdapter(
    val context: Context,
    val list: ArrayList<ItemInvite>,
    val isResult: Boolean
) : Adapter<ItemInviteAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_presenter,
            parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position >= list.size) {
            return
        }
        if (isResult) {
            if (position == 0) {
                AppUtils.loadImage(context, holder.imgIndex, R.drawable.presenter_1)
                holder.imgIndex.visibility = View.VISIBLE
            } else if (position == 1) {
                AppUtils.loadImage(context, holder.imgIndex, R.drawable.presenter_2)
                holder.imgIndex.visibility = View.VISIBLE

            } else if (position == 2) {
                AppUtils.loadImage(context, holder.imgIndex, R.drawable.presenter_3)
                holder.imgIndex.visibility = View.VISIBLE
            } else {
                holder.imgIndex.visibility = View.INVISIBLE
            }
        } else{
            holder.imgIndex.visibility = View.INVISIBLE
        }
        val itemObjGame: ItemInvite = list.get(position)
        AppUtils.loadImageCircle(
            context,
            holder.imgAvatar,
            itemObjGame.avatar,
            R.drawable.ic_avatar_interactive
        )
        holder.textName.setText(itemObjGame.name)
        holder.textID.setText("ID:" + itemObjGame.userId)
//        holder.textTotalMenber.setText("Có ${itemObjGame.menberCount} thành viên")
        if (!isResult) {
            holder.textPresenterCount.setText("****")
        } else {
            holder.textPresenterCount.setText("${itemObjGame.count}")
        }

        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObjGame, position)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgAvatar = itemView.imgAvatar
        val textID = itemView.textID
        val textPresenterCount = itemView.textPresenterCount
        val textName = itemView.textName
        val imgIndex = itemView.imgIndex
    }

    interface OnItemClickListener {
        fun onClick(itemGame: ItemInvite, position: Int)
        fun onClickJoin(itemGame: ItemInvite, position: Int)
        fun onLongClick(itemGame: ItemInvite, position: Int)
    }

}
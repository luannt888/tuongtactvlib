package vn.mediatech.istudiocafe.zinteractive.emotion

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
//<<<<<<< HEAD
class ItemSeq(
    @SerializedName("seq_start") var seq_start: Int?,
    @SerializedName("seq_stop") var seq_stop: Int?,
    @SerializedName("link") var link: String?
) : Parcelable
//=======
/*
{
"id": "49",
"name": "Dev",
"image": "https://daugiatruyenhinh.com/upload/istudios/emoji/49_ca0260fd2536d2688b27.jpg.png",
"type": "heart",
"seq": {
"seq_start": "1",
"seq_stop": "2",
"link": "https://daugiatruyenhinh.com/upload/istudios/emoji/49_ca0260fd2536d2688b27.jpg.png"
},
"sprite": {
"sprite_long": "12",
"sprite_wide": "25",
"link": "https://daugiatruyenhinh.com/upload/istudios/emoji/49_ca0260fd2536d2688b27.jpg.png"
}
},*/

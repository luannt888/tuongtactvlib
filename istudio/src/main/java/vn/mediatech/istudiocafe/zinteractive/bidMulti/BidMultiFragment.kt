package vn.mediatech.interactive.bidMulti

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_bid_multi.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import java.util.*
import kotlin.collections.ArrayList

class BidMultiFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var list: ArrayList<ItemBidMulti>? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var onItemClickListener: ItemBidMultiAdapter.OnItemClickListener? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bid_multi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                    com.google.android.material.R.id.design_bottom_sheet)
                    ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }
    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
    }
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelableArrayList(ConstantTT.DATA, list)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()
    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        val height = bundle?.getInt(ConstantTT.HEIGHT)
        if (height != null && height > 0) {
            Loggers.e("BID", "HeightBidMulti" + height)
            val param = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height)
//            param?.height = height
            layoutRoot?.layoutParams = param
        }
        list = bundle?.getParcelableArrayList(ConstantTT.DATA)!!
        updateUI(list!!)
    }


    fun initControl() {
        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    var adapter: ItemBidMultiAdapter? = null
    fun updateUI(itemList: ArrayList<ItemBidMulti>?) {
        adapter?.stopAllTimer()
        if (itemList == null || itemList.size == 0) {
            dismiss()
            return
        }
        list = ArrayList()
        for( i in 0 until itemList.size){
            val item = itemList.get(i)
            if(item.status.equals("1")){
                list!!.add(item)
            }
        }
        if(list!!.size == 0){
            dismiss()
            return
        }
        adapter = ItemBidMultiAdapter(requireContext(), list!!, 1, 1, 1)
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        rcvBid?.layoutManager = layoutManager
        rcvBid?.adapter = adapter
        adapter?.onItemClickListener = object : ItemBidMultiAdapter.OnItemClickListener {
            override fun onClickOk(itemObject: ItemBidMulti, position: Int) {
                onItemClickListener?.onClickOk(itemObject, position)
            }

            override fun onClickNo(itemObject: ItemBidMulti, position: Int) {
                onItemClickListener?.onClickNo(itemObject, position)
            }

            override fun onClickBid(itemObject: ItemBidMulti, position: Int) {
                onItemClickListener?.onClickBid(itemObject, position)
            }

        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        adapter?.stopAllTimer()
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

}
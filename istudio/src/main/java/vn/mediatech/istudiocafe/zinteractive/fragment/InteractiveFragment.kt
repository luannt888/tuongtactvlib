package vn.mediatech.istudiocafe.zinteractive.fragment

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_interactive.*
import kotlinx.android.synthetic.main.layout_header_recycler_view_tt.view.*
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import vn.mediatech.interactive.app.*
import vn.mediatech.interactive.app.EncryptUtil
import vn.mediatech.interactive.bidFree.BidFreeFragment
import vn.mediatech.interactive.bidFree.ItemBidProduct
import vn.mediatech.interactive.bidMulti.BidMultiFragment
import vn.mediatech.interactive.bidMulti.ItemBidMulti
import vn.mediatech.interactive.bidMulti.ItemBidMultiAdapter
import vn.mediatech.interactive.bidSamePrice.BidSamePriceFragment
import vn.mediatech.interactive.chat.ChatFragment
import vn.mediatech.interactive.fragment.AccountFragment
import vn.mediatech.interactive.fragment.EffectFragment
import vn.mediatech.interactive.git.GitFragment
import vn.mediatech.interactive.git.ItemGit
import vn.mediatech.interactive.git.ItemGitCategory
import vn.mediatech.interactive.group.GroupGameFragment
import vn.mediatech.interactive.guessPrice.BidGuessPriceFragment
import vn.mediatech.interactive.guide.GuideFragment
import vn.mediatech.interactive.guide.ItemGuide
import vn.mediatech.interactive.iQ.IQFragment
import vn.mediatech.interactive.iQ.ItemIQ
import vn.mediatech.interactive.iQ.ItemIQAnswer
import vn.mediatech.interactive.iQ.ItemIQAnswerAdapter
import vn.mediatech.interactive.invite.InviteGameFragment
import vn.mediatech.interactive.listener.OnMessageListener
import vn.mediatech.interactive.luckySpin.ItemSpin
import vn.mediatech.interactive.luckySpin.SpinFragment
import vn.mediatech.interactive.luckySpin.SpinSpecialFragment
import vn.mediatech.interactive.model.ItemMenu
import vn.mediatech.interactive.model.ItemMessage
import vn.mediatech.interactive.service.OnClearFromRecentService
import vn.mediatech.interactive.service.SocketMangager
import vn.mediatech.interactive.vote.ItemVote
import vn.mediatech.interactive.vote.ItemVoteAnswer
import vn.mediatech.interactive.vote.VoteAdapter
import vn.mediatech.interactive.vote.VoteFragment
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.adapter.ItemWinnerAdapter
import vn.mediatech.istudiocafe.adapter.NewsAdapter
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.fragment.ImageZoomPagerDialog
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemPhoto
import vn.mediatech.istudiocafe.model.ItemWiner
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.istudiocafe.zinteractive.adapter.ItemMenuAdapter
import vn.mediatech.istudiocafe.zinteractive.adapter.ItemMessageAdapter
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT.*
import vn.mediatech.istudiocafe.zinteractive.emotion.EmotionFragment
import vn.mediatech.istudiocafe.zinteractive.emotion.EmotionResultFragment
import vn.mediatech.istudiocafe.zinteractive.emotion.ItemEmotion
import vn.mediatech.istudiocafe.zinteractive.emotion.ItemEmotionAdapter
import vn.mediatech.istudiocafe.zinteractive.iQ.IQResultFragment
import vn.mediatech.istudiocafe.zinteractive.webGame.WebGameFragment
import vn.mediatech.istudiocafe.zinteractive.webGame.WebGameResultFragment
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class InteractiveFragment : Fragment() {
    var userId: String? = null
    var groupId: String? = null
    var userToken: String? = null
    var userName: String? = null
    var avatar: String? = null
    var appID: String? = null
    var userStatus: String? = null
    var isAdmin: Boolean = false
    var onMessageListener: OnMessageListener? = null
    private var port: String? = ""
    var PORT: String? = ""
    private var urlShare: String? = ""
    private val apiHistory: String = "api/user/interactivelog/"
    private val apiGit: String = "api/user/stakelog/"
    private val API_PLAYERS: String = "api/mediatech/Connection"
    private val API_GIFTS: String =
        "interactive/v2/services-sticker-type-gifts"
    private val API_GIFTS_ITEM: String =
        "interactive/v2/services-sticker-item-gifts/"

    private var sumPlayer: String? = ""
    private var mSocketMangager: SocketMangager? = null
    private var popupFragment: BottomSheetDialogFragment? = null
    var savedInstanceState: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_interactive, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
        try {
            activity?.startService(
                Intent(
                    context,
                    OnClearFromRecentService::class.java
                )
            )
        } catch (e: Exception) {
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putString(USER_ID, userId)
            putString(USER_NAME, userName)
            putString(USER_TOKEN, userToken)
            putString(USER_AVATAR, avatar)
        }
        super.onSaveInstanceState(outState)
    }

    private fun initData() {
        if (SharedPreferencesManager.isTest(context) && !SharedPreferencesManager.getChannelID(
                context
            ).isNullOrEmpty()
        ) {
            CHANEL_ID = SharedPreferencesManager.getChannelID(context)
        }
        val bundle = (if (savedInstanceState != null) savedInstanceState else arguments) ?: return
        userId = bundle.getString(USER_ID)
        groupId = bundle.getString(GROUP_ID)
        userName = bundle.getString(USER_NAME)
        userToken = bundle.getString(USER_TOKEN)
        avatar = bundle.getString(USER_AVATAR)
        appID = bundle.getString(APP_ID)
        userStatus = bundle.getString(USER_STATUS)
        PORT = bundle.getString(ConstantTT.PORT)
        urlShare = bundle.getString(SHARE)
        isAdmin = bundle.getBoolean(ROLE, false)
        showNews(bundle.getBoolean(SHOW_NEWS, false))
        if (Constant.IS_DEMO) {
            isAdmin = true
        }
        startTrackPlayerJoin()
        setData()
    }

    fun setData() {
        if (imageStatus == null) {
            return
        }
        buttonGroup?.visibility = View.GONE
        imageStatus?.visibility = View.GONE
        if (userStatus != null) {
            if (userStatus.equals("1")) {
//                imageStatus?.visibility = View.VISIBLE
//                imageStatus?.setImageResource(R.drawable.ic_blue_check)
//                buttonGroup?.visibility = View.VISIBLE
//                buttonConfirm?.visibility = View.GONE
                imageStatus?.visibility = View.VISIBLE
                imageStatus?.setImageResource(R.drawable.ic_blue_check)
                buttonGroup?.visibility = View.GONE
                buttonConfirm?.visibility = View.GONE
            } else if (userStatus.equals("0")) {
                buttonConfirm?.visibility = View.GONE
                imageStatus?.setImageResource(R.drawable.ic_grey_check)
                imageStatus?.visibility = View.VISIBLE
            } else {
                imageStatus.visibility = View.GONE
                buttonConfirm?.visibility = View.GONE
            }
        }
        //        buttonGroup?.visibility = View.VISIBLE

        AppUtils.loadImageCircleClearCache(
            context,
            imgAvatar,
            avatar,
            R.drawable.ic_avatar_interactive
        )
        textName?.setText(userName + " (ID: " + userId + ")")
        textName?.isSelected = true
        getData(TYPE_GET_HISTORY)
        showButtonShareFacebook(!urlShare.isNullOrEmpty())
        if (userId.isNullOrEmpty() || userName.isNullOrEmpty() || userToken.isNullOrEmpty() || PORT.isNullOrEmpty()) {
            activity?.runOnUiThread {
                layoutNotify.visibility = View.VISIBLE
            }
            return
        }
    }

    var isResult = true
    private fun initControl() {
        layoutAccount.setOnClickListener {
            showAccountFragment()
        }
        buttonMyVoucher.setOnClickListener {
            resetAndHideGitCount()
            onClickListenner?.onClickMyVoucher()
        }
        buttonShareFacebook.setOnClickListener {
            onClickListenner?.onClickShareFacebook()
        }

        buttonGroup.setOnClickListener {
//            showInviteGame(null, isResult)
//            isResult = !isResult
            onClickListenner?.onClickGroup()
        }
        buttonConnect.setOnClickListener {
            layoutNotify.visibility = View.GONE
            initSocket()
        }
        buttonMessenger.setOnClickListener {
            onClickListenner?.onClickMessenger()
        }
        buttonConfirm.setOnClickListener {
            onClickListenner?.onClickConfirm()
        }

        buttonToggle.setOnClickListener {
            buttonToggle.rotation = if (buttonToggle.rotation == 0F) 180F else 0F
            onClickListenner?.onClickToggle()
        }
        layoutPlayers.setOnClickListener {
//            onClickListenner?.onClickViews()
//            showWebGame()
//            onClickListenner?.onClickViews()
//            if (isShow) {
//            showEmotionFragment()
//            } else {
//                showEmotionResultFragment()
//            }
//            isShow = !isShow
//            showFirework("", EffectFragment.TYPE_EFFECT_WIN, false, false)

        }
    }

    var isShow = false
    var onClickListenner: OnClickListenner? = null
    private fun showAccountFragment() {
        var fragment = AccountFragment()
        val bundle = Bundle()
        bundle.putString(DATA, userName)
        bundle.putBoolean(ROLE, isAdmin)
        bundle.putString(USER_STATUS, userStatus)
        fragment?.arguments = bundle
        fragment?.onButtonClickListenner = object : AccountFragment.OnButtonClickListenner {
            override fun onClickBuy() {
            }

            override fun onClickHistory() {
                getData(TYPE_GET_HISTORY)
            }

            override fun onClickSignOut() {
                mSocketMangager?.close()
                mSocketMangager = null
                popupFragment?.dismiss()
                effectFragment?.dismiss()
                onClickListenner?.onClickSignOut()
            }

            override fun onClickSupport() {
            }

            override fun onClickGit() {
                getData(TYPE_GET_GIT)
//                onClickAcountListenner?.onClickMyVoucher()
            }

            override fun onClickSetUp() {
                onClickListenner?.onClickSetUp()
            }

            override fun onClickConfirm() {
                onClickListenner?.onClickConfirm()
            }
        }

        fragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        showBottomFragment(fragment, "Account")
    }

    private fun showChat() {
        var fragment = ChatFragment()
        val bundle = Bundle()
        fragment?.arguments = bundle
        fragment?.onClickListener = object : ChatFragment.OnClickButtonSendListenner {
            override fun onClick(message: String?) {
                val json = JsonParserUtil.getJsonObject(dataSend)
                if (json == null) {
                    return
                }
                val jsondata = JSONObject()
                jsondata.put(MESSAGE, message)
                json.put(DATA, jsondata)
                val jsonFrom = JsonParserUtil.getJsonObject(json, FROM)
                jsonFrom?.put(MESSAGE, message)
                json.put(FROM, jsonFrom)
                json.put(SERVICE, SERVICE_CHAT)
                json.put(KEY, CHAT_SEND)
                json.put("fromclient", SOCKET_CLIENT)
                json.remove(SETTING)
                Loggers.e("Send", json.toString())
                mSocketMangager?.send(EncryptUtil.base64Encode(json.toString()))
            }
        }
//        fragment?.onShowDismissListener = object : OnShowDismissListener {
//            override fun onShow() {
//                showDimer()
//            }
//
//            override fun onDismiss() {
//                hideDimer()
//            }
//        }
        showBottomFragment(fragment, "Chat")
    }

    var myHttpRequest: MyHttpRequest? = null
    var isGettingData: Boolean = false
    var myHttpRequestUser: MyHttpRequest? = null
    val TYPE_GET_HISTORY = 1
    val TYPE_GET_PLAYERS = 2
    val TYPE_GET_GIT = 3
    fun getData(type: Int) {
        if (isDetached || context == null || isGettingData) {
            return
        }
        if (!AppUtils.isNetworkConnect(context)) {
            return
        }
        var httpRequest: MyHttpRequest? = null
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(requireContext())
        }
        httpRequest = myHttpRequest
        val requestParams = RequestParams()
        var api: String? = null
        if (type == TYPE_GET_HISTORY) {
            api = validApiGameTT(context, apiHistory) + userId
        } else if (type == TYPE_GET_GIT) {
            api = validApiGameTT(context, apiGit) + userId
        } else {
            if (myHttpRequestUser == null) {
                myHttpRequestUser = MyHttpRequest(requireContext())
            }
            httpRequest = myHttpRequestUser
            api = validApiGameTT(context, API_PLAYERS)

        }
//        requestParams.put("id", "${itemObj!!.id}")
//        requestParams.put("content_type", "${itemObj!!.contentType}")
//        requestParams.put("style", "3")

        httpRequest?.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                isGettingData = false
                if (isDetached) {
                    return
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                isGettingData = false
                if (isDetached) {
                    return
                }
                handleData(type, responseString)
            }
        })
    }

    var list: ArrayList<ItemMessage> = ArrayList()
    private fun handleData(type: Int, responseString: String?) {
        if (type == TYPE_GET_HISTORY || type == TYPE_GET_GIT) {
            val newList: ArrayList<ItemMessage> = ArrayList()
            val gson = Gson()
            if (type == TYPE_GET_GIT) {
                val type = object : TypeToken<ArrayList<ItemMessage>>() {}.getType()
                val itemList: ArrayList<ItemMessage>? = try {
                    gson.fromJson(responseString, type)
                } catch (e: Exception) {
                    e.printStackTrace()
                    null
                }
                newList.addAll(itemList!!)
            } else {
                val array = JsonParserUtil.getJsonArray(responseString)
                if (array != null) {
                    for (i in 0 until array.length()) {
                        val string = array.getString(i)
                        val type = object : TypeToken<ArrayList<ItemMessage>>() {}.getType()
                        val itemList: ArrayList<ItemMessage>? = try {
                            gson.fromJson(string, type)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            null
                        }
                        val arrayList: ArrayList<ItemMessage> = ArrayList()
                        if (itemList != null) {
                            for (i in 0 until itemList.size) {
                                val item = itemList.get(i)
                                if ((!item.message.isNullOrEmpty() || item.service.equals(
                                        SERVICE_GIT
                                    ) || item.service.equals(
                                        SERVICE_IQ
                                    ))
                                ) {
                                    if (item.service!!.equals(SERVICE_IQ) && (!item.key!!.equals("IQ-MESSAGE") && !item.key.equals(
                                            "PLAYER-ANSWER"
                                        ))
                                    ) {
                                        continue
                                    }
                                    arrayList.add(item)
                                }
                            }
                        }
                        if (arrayList.size > 0) {
                            val lastUpdate = arrayList.get(0).lastUpdate
                            newList.add(
                                ItemMessage(
                                    i.toLong(),
                                    "",
                                    "",
                                    "",
                                    TIME,
                                    "",
                                    "",
                                    "",
                                    i.toString(),
                                    "",
                                    lastUpdate,
                                    ""
                                )
                            )
                            newList.addAll(arrayList)
                        }
                    }
                }
            }
            if (activity == null || requireActivity().isFinishing || requireActivity().isDestroyed) {
                return
            }
            activity?.runOnUiThread {
//                Collections.reverse(newList)
                rcvMessage?.stopScroll()
                list.clear()
                list.addAll(newList)
                initAdapter()
//                port = "wss://api.daugiatruyenhinh.com:8088/interactive?uid=243&fullname=Thanh%20Nguyen&access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9tZHR2Lm1lZGlhdGVjaC52biIsImF1ZCI6Imh0dHA6XC9cL21kdHYubWVkaWF0ZWNoLnZuIiwiaWF0IjoxNjA4Nzc5Njk1LCJuYmYiOjE2MDg3Nzk2ODUsImV4cCI6MTYxMTM3MTY5NSwianRpIjoiWGh3OVlzZ0E3YnBSUHZLd2p2b01PYU1iMDhDa0FPbDBHWVltVDQranIzOD0iLCJkYXRhIjp7ImlkIjoiMjQzIiwiZW1haWwiOiJ0bDA2MTBAeWFob28uY29tLnZuIiwiZnVsbG5hbWUiOiJUaGFuaCBOZ3V5ZW4iLCJwaG9uZSI6IjA5MDIxMDI4MDgiLCJhdmF0YXIiOiJodHRwczpcL1wvZ3JhcGguZmFjZWJvb2suY29tXC8zMTQ0MzE3NjA5MDAyOTcwXC9waWN0dXJlP3R5cGU9bGFyZ2UifX0.L4yrmAE6BM-efq6772b8D9CtkfA5riJR94IRbW6LlQM"
//                initSocket()
            }
        } else {
            if (!responseString.isNullOrEmpty()) {
                sumPlayer = responseString!!.trim()
                activity?.runOnUiThread {
//                Loggers.e("Player", responseString!!.trim())
                    textPlayers?.setText(sumPlayer)
                }
            }
        }
    }

    private var hasSignOut = false
    fun initSocket() {
        if (isShowNews != null && isShowNews!!) {
            return
        }
        mSocketMangager = SocketMangager.getInstance()
        if (PORT.isNullOrEmpty() || userId.isNullOrEmpty() || userToken.isNullOrEmpty() || userName.isNullOrEmpty()) {
            progressBar?.visibility = View.GONE
            layoutNotify?.visibility = View.VISIBLE
            return
        }
        if (mSocketMangager != null && !mSocketMangager!!.isConnecting) {
            progressBar?.visibility = View.VISIBLE
            layoutNotify?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            layoutNotify?.visibility = View.GONE
        }
//        mSocketMangager?.cancel()
        val listenner = object : SocketMangager.MySocketListenner {
            override fun onConnected(response: Response) {
                activity?.runOnUiThread {
                    progressBar?.visibility = View.GONE
                    layoutNotify?.visibility = View.GONE
                }
                Loggers.e("Socket", "onConnected:" + port)
                onClickListenner?.onConnectSocket(true)
            }

            override fun onMessage(string: String) {
                autoConnectCount = 0
                var data = EncryptUtil.base64Decode(string)
//                data = "{\"status\":1,\"errCode\":\"\",\"service\":\"DOANGIA\",\"key\":\"DOANGIA-START\",\"cmd\":\"\",\"connection_count\":\"2\",\"from\":{\"ID\":-2,\"Index\":0,\"Sender\":null,\"Phone\":null,\"Fullname\":\"MEDIATECH\",\"Avatar\":null,\"Source\":null,\"Chip\":null,\"Command\":null,\"UpdateTime\":null,\"last_update\":\"0001-01-01T00:00:00\",\"Status\":null,\"FBID\":\"PLAYLIVE\",\"FBLink\":null,\"SyncUrl\":null,\"product\":null,\"BidMoney\":0,\"note\":\"\",\"message\":null,\"hascode\":\"PLAYLIVE\",\"result\":null,\"point\":0,\"channel\":null,\"rank_index\":0,\"rank_total\":0,\"attachment\":null,\"Role\":null,\"access_token\":null},\"data\":{\"id\":1625,\"name\":\"Thẻ điện thoại\",\"desc\":\"Thẻ điện thoại\",\"price\":50000,\"price_sale\":50000,\"price_discount\":50000,\"price_bid_startup\":0,\"price_bid_step\":0,\"price_bid_percent\":0,\"start_discount\":\"2021-01-05\",\"end_discount\":\"2021-01-14\",\"image\":\"http://daugiatruyenhinh.com/upload/product/daf125d9748cfeabd8ff3576f738139b.jpg\",\"image_url\":\"http://daugiatruyenhinh.com/upload/product/daf125d9748cfeabd8ff3576f738139b.jpg\",\"status\":0,\"note\":\"00:30\",\"CountDown\":30,\"template_style\":null,\"price_bid\":0,\"StartTime\":\"0001-01-01T00:00:00\",\"EndTime\":\"0001-01-01T00:00:00\",\"EndGame_Status\":0,\"Template_Local\":0,\"gallery\":[\"\\\\\\\\DEMO-PC\\\\MPI.SHARE\\\\DOWNLOAD\\\\PRODUCT_DATA\\\\1625-1.Png\",\"\\\\\\\\DEMO-PC\\\\MPI.SHARE\\\\DOWNLOAD\\\\PRODUCT_DATA\\\\1625-2.Png\",\"\\\\\\\\DEMO-PC\\\\MPI.SHARE\\\\DOWNLOAD\\\\PRODUCT_DATA\\\\1625-3.Png\",\"\\\\\\\\DEMO-PC\\\\MPI.SHARE\\\\DOWNLOAD\\\\PRODUCT_DATA\\\\1625-4.Png\"],\"gallery_url\":[\"http://daugiatruyenhinh.com/upload/gallery/large/05bfc435f1806066175b29560065fa36.jpg\",\"http://daugiatruyenhinh.com/upload/gallery/large/50b82c77baa446ed02f3e00f99ea005c.jpg\",\"http://daugiatruyenhinh.com/upload/gallery/large/90d69af949b09798a27a936a5d7c6571.jpg\",\"http://daugiatruyenhinh.com/upload/gallery/large/a862522051ecd17fac996ba40a5c8648.jpg\"],\"store_avatar\":\"http://daugiatruyenhinh.com/upload/store/avatar/c8a703fcc8a6c92b526b81c0f4234842.jpg\",\"store_avatar_url\":\"http://daugiatruyenhinh.com/upload/store/avatar/c8a703fcc8a6c92b526b81c0f4234842.jpg\",\"condition\":null},\"message\":null}"
                Loggers.e("Socket", "onMessage :" + data)
                if (data.isNullOrEmpty()) {
                    return
                }
                runHandler {
                    try {
                        handleMessage(data)
                    } catch (e: Exception) {
                    }
                }
//                activity?.runOnUiThread {
//                    val dialog = AppUtils.showDialog(requireActivity(), true, " Connected")
//                    Handler(Looper.getMainLooper()).postDelayed({ dialog?.dismiss() }, 2000)
//                }
            }

            override fun onClosed(code: Int, reason: String) {
                Loggers.e("Socket", "onClosed" + reason)
                if (code == CLOSE_CONNECT) {
                    return
                }
            }

            override fun onClosing(code: Int, reason: String) {
                onClickListenner?.onConnectSocket(false)
                Loggers.e("Socket", "onClosing" + reason)
                activity?.runOnUiThread {
                    progressBar?.visibility = View.GONE
                }
                if (code == CLOSE_CONNECT) {
                    return
                }
                if (!disconnectMessage.isNullOrEmpty()) {
                    val mess = disconnectMessage
                    hasSignOut = true
                    activity?.runOnUiThread {
                        if (!isOnScreen) {
                            disconnectMessage = null
                            return@runOnUiThread
                        }

                        AppUtils.showDialog(
                            activity,
                            false,
                            false,
                            null,
                            mess,
                            "Thoát",
                            "Kết nối",
                            object : AppUtils.OnDialogButtonListener {
                                override fun onLeftButtonClick() {
                                    onClickListenner?.onClickSignOut()
                                    hasSignOut = false
//                                activity?.finish()
                                }

                                override fun onRightButtonClick() {
                                    hasSignOut = false
                                    mSocketMangager?.reconnect()
                                }
                            })
                    }
                    disconnectMessage = null
                } else {
                    tryConnect()
                }
            }

            override fun onFailure(t: Throwable, response: Response?) {
                onClickListenner?.onConnectSocket(false)
                activity?.runOnUiThread {
                    progressBar?.visibility = View.GONE
                }
                Loggers.e(
                    "Socket",
                    "onFailure:" + t.message + " --- " + (if (response != null) (response.body.toString()) else "")
                )
                tryConnect()
            }
        }
//        mSocketMangager?.addSocketlistenner(InteractiveFragment::class.java.canonicalName, listenner)
        mSocketMangager?.listenner = listenner
//        userId = null
        port =
            PORT!!.trim() + "uid=" + userId + "&fullname=" + userName + "&access_token=" + userToken
        mSocketMangager?.run(port)
    }

    var autoConnectCount: Int = 0
    var isShowTryConnect: Boolean = false
    private fun tryConnect() {
        if (autoConnectCount >= 0 && !isShowTryConnect) {
            activity?.runOnUiThread {
                if (!isOnScreen) {
                    return@runOnUiThread
                }
                isShowTryConnect = true
                AppUtils.showDialog(
                    activity,
                    false,
                    false,
                    null,
                    "Không thể kết nối đến máy chủ. Vui lòng kiểm tra và kết nối lại!",
                    "Huỷ",
                    "Kết nối",
                    object : AppUtils.OnDialogButtonListener {
                        override fun onLeftButtonClick() {
                            isShowTryConnect = false
                            layoutNotify?.visibility = View.VISIBLE
                        }

                        override fun onRightButtonClick() {
                            isShowTryConnect = false
                            mSocketMangager?.reconnect()
                        }
                    })
            }
        } else {
            autoConnectCount++
            mSocketMangager?.reconnect()
        }
    }

    fun reconnect() {
        mSocketMangager?.reconnect()
    }

    var adapter: ItemMessageAdapter? = null
    private fun initAdapter() {
        if (adapter != null) {
            adapter?.notifyDataSetChanged()
            rcvMessage?.getLayoutManager()?.scrollToPosition(adapter!!.getItemCount() - 1);
            return
        }
//        val style = Constant.STYLE_HORIZONTAL
        adapter = ItemMessageAdapter(requireActivity(), list, 1, 1, 1)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        rcvMessage?.layoutManager = layoutManager
        rcvMessage?.adapter = adapter
        rcvMessage?.getLayoutManager()?.scrollToPosition(adapter!!.getItemCount() - 1);
    }

    var timer: Timer? = null
    private fun startTrackPlayerJoin(timePeriodMs: Long = 1000) {
        stopTrackPlayerJoin()
        timer = Timer()
        timer?.schedule(timerTask {
            getData(TYPE_GET_PLAYERS)
        }, 0, timePeriodMs)
    }

    private fun stopTrackPlayerJoin() {
        if (timer != null) {
            try {
                timer?.cancel()
                timer?.purge()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            timer = null
        }
    }

    var dataSend: String? = null
    private fun handleMessage(message: String) {
        val json = JsonParserUtil.getJsonObject(message)
        Loggers.e("Socket", "onMessageData :" + JsonParserUtil.getString(json, "data"))
//         json = JsonParserUtil.getJsonObject(startGame)
        if (json == null) {
            return
        }
        val service = JsonParserUtil.getString(json, SERVICE)
        when (service) {
            SERVICE_REGISTER -> {
//                runDelay({
////                showFirework("Chúc mừng bạn đã nhận được phần quà 1.000.000đ", EffectFragment.TYPE_EFFECT_WIN, false)
////               showFirework("Chúc mừng bạn đã nhận được phần quà  1.000.000đ", EffectFragment.TYPE_EFFECT_WIN_ICON,false)
//                showFirework("Chúc mừng bạn đã nhận được phần quà  1.000.000đ", EffectFragment.TYPE_EFFECT_FALSE,false, false)
//            },3000)
                dataSend = message
                val jsonDataSend = JsonParserUtil.getJsonObject(dataSend)
                val jsonFrom = JsonParserUtil.getJsonObject(jsonDataSend, FROM)
                jsonFrom?.put("channel", CHANEL_ID)
                jsonDataSend?.put(FROM, jsonFrom)
                jsonDataSend?.remove(SETTING)
                jsonDataSend?.remove(DATA)
                dataSend = jsonDataSend.toString()
                runDelay({ handleREGISTER(json) }, 200)

            }
            SERVICE_BID -> {
                handleBID(json)
            }
            SERVICE_IQ -> {
                handleIQ(json)
            }
            SERVICE_GUESS_PRICE -> {
                handleGUESS_PRICE(json)
            }

            SERVICE_SPIN -> {
                handleSPIN(json)
            }

            SERVICE_VOTE2 -> {
                handleVOTE2(json)
            }
            SERVICE_CHAT -> {
                getData(TYPE_GET_HISTORY)
            }

            SERVICE_GIT -> {
                handleGIT(json)
            }
            SERVICE_BIDMULTI -> {
                handleBIDMULTI(json)
            }
            SERVICE_DISCONNECT -> {
                handleDISCONNECT(json)
            }
            SERVICE_SETTING -> {
                handleSETTING(json)
            }
            SERVICE_GROUP -> {
                handleGROUP(json)
            }

            SERVICE_INVITE -> {
                handleINVITE(json)
            }
            SERVICE_NOTIFY -> {
                handleNOTIFY(json)
            }
            SERVICE_EMOTION2 -> {
                handleEMOTION2(json)
            }
            SERVICE_WEBGAME -> {
                handleWEBGAME(json)
            }
        }
    }


    private fun handleGROUP(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val message = JsonParserUtil.getString(json, MESSAGE)
        when (key) {
            GROUP_START -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
                try {
                    timeGroupEndMs = JsonParserUtil.getString(json, DATA)!!
                        .toLong() * 1000 + System.currentTimeMillis()
                } catch (e: Exception) {
                    timeGroupEndMs = 0
                }
                showGroupGame(null)
                setOnMenu(MENU_GROUP)
            }

            GROUP_STOP -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
                setOffMenu(MENU_GROUP)
                groupGameFragment?.dismiss()
                timeGroupEndMs = 0L
            }
            GROUP_FINISH -> {
                val data = JsonParserUtil.getString(json, DATA)
                showGroupGame(data)
                if (!message.isNullOrEmpty()) {
                    showFirework(message, EffectFragment.TYPE_EFFECT_WIN, false, false)
                }
            }
        }
    }

    // su kien khi connect thanh cong ( gom thong tin user, data cac game dang choi,
    private fun handleREGISTER(json: JSONObject) {
//        showFirework("handleREGISTER")
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val data = JsonParserUtil.getJsonObject(json, "data")
        val dataGameSetting = JsonParserUtil.getString(data, SETTING)
        initMenu(dataGameSetting)
        val dataRate = JsonParserUtil.getJsonObject(data, "Emotion2")
        infoCurrentGame = JsonParserUtil.getString(data, "Emotion2")
        if (dataRate != null) {
            val isPlaying = JsonParserUtil.getBoolean(dataRate, "Playing")
            if (isPlaying == true) {
                showEmotionFragment()
            }
        }
        val dataBidFree = JsonParserUtil.getString(data, BID_PRODUCT)
        setItemBidProduct(dataBidFree)

        val dataBidMulti = JsonParserUtil.getString(data, "MutilBid")
        setListBidMulti(dataBidMulti)
        showBidMultiFragment()

        val dataIq = JsonParserUtil.getString(data, "iq_quest")
        setItemIQ(dataIq)

        val jsDataGuessPrice = JsonParserUtil.getJsonObject(data, "DoanGia")
        val dataGuessPrice = JsonParserUtil.getString(jsDataGuessPrice, "Product")
        setItemBidGuessPrice(dataGuessPrice)

        val dataSpin = JsonParserUtil.getString(data, "spin")
        if (!dataSpin.isNullOrEmpty()) {
            showSpin(dataSpin)
        }
        val dataVote = JsonParserUtil.getString(data, "vote2")
        setItemVote(dataVote)

        val dataGroup = JsonParserUtil.getJsonObject(data, "group")
        if (dataGroup != null) {
            var timcountdown: Int = -1
            timcountdown = JsonParserUtil.getInt(dataGroup, "countdown")!!
            if (timcountdown > 0) {
                timeGroupEndMs = timcountdown * 1000 + System.currentTimeMillis()
                showGroupGame(null)
                setOnMenu(MENU_GROUP)
            }
        }

        val dataWebGameResume = JsonParserUtil.getJsonObject(data, "Webgame")
        if (dataWebGameResume != null) {
            Loggers.e("WEBGAME", dataWebGameResume.toString())
            val isPlaying = JsonParserUtil.getBoolean(dataWebGameResume, "Playing")
            if (isPlaying == true) {
                dataWebGame = dataWebGameResume.toString()
                startWebgameFragment(dataWebGame)
            }
        }
    }

    private fun handleSETTING(json: JSONObject) {
        val data = JsonParserUtil.getString(json, DATA)
        if (data.isNullOrEmpty()) {
            return
        }
        initMenu(data)
    }

    private fun initMenu(dataGameSetting: String?) {
        var itemGameList = getItemMenuList(dataGameSetting)
        if (itemGameList == null || itemGameList!!.size == 0) {
            itemGameList = ArrayList()
        }
//        itemGameList.add(ItemMenu("", MENU_GUIDE, "https://www.pinclipart.com/picdir/big/35-354980_membership-form-reading-icon-bsbc-book-icon-png.png", true, "", "", "", "", true, ""))
        initMenuAdapter(itemGameList)
    }

    var disconnectMessage: String? = null
    private fun handleDISCONNECT(json: JSONObject) {
        if (disconnectMessage.isNullOrEmpty()) {
            disconnectMessage = JsonParserUtil.getString(json, MESSAGE)
            Loggers.e("Socket_disconnect", disconnectMessage)
        } else {
            disconnectMessage = null
        }
    }

    private fun handleBID(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val message = JsonParserUtil.getString(json, MESSAGE)
        val data = JsonParserUtil.getString(json, DATA)
        Loggers.e("BIDMULTI", data)
        when (key) {
            START_GAME_FREE_BID -> {
                setItemBidProduct(data)
                setOnMenu(MENU_BID_FREE)
            }
            BID_GAME_FREE_COUNTDOWN -> {
                val timeCoudown = JsonParserUtil.getLong(json, CMD)
                if (timeCoudown != null) {
                    itemBidFree?.countDown = timeCoudown
                    itemBidFree?.endTimeMs = System.currentTimeMillis() + timeCoudown * 1000
                    itemBidSamePrice?.countDown = timeCoudown
                    itemBidSamePrice?.endTimeMs = System.currentTimeMillis() + timeCoudown * 1000
                    if ((bidFreeFragment != null && bidFreeFragment!!.isVisible) || (bidSamePriceFragment != null && bidSamePriceFragment!!.isVisible)) {
                        bidFreeFragment?.startCountDown(timeCoudown)
                        bidSamePriceFragment?.startCountDown(timeCoudown)
                    } else {
                        showBidPopUp()
                    }
                }
                runDelay({
                    if (!message.isNullOrEmpty()) {
                        showDialogAutoClose(message)
                    }
                }, 500)
            }
            BID_MESSAGE_CALLBACK -> {
                val messageFrom = JsonParserUtil.getString(from, MESSAGE)
                Loggers.e("MESSAGE", message)
                if (messageFrom.isNullOrEmpty()) {
                    return
                }
                activity?.runOnUiThread {
                    if (messageFrom.contains("thứ tự")) {
                        itemBidFree?.playerMessage = messageFrom
                        itemBidSamePrice?.playerMessage = messageFrom
                        bidFreeFragment?.setTextNotify(messageFrom)
                        bidSamePriceFragment?.setTextNotify(messageFrom)
                        runDelay({
                            bidFreeFragment?.dismiss()
                            bidSamePriceFragment?.dismiss()
                        }, 1500)
                    } else if (messageFrom.contains("mừng bạn")) {
                        showFirework(messageFrom, EffectFragment.TYPE_EFFECT_WIN, true)
                    } else {
                        showDialogAutoClose(messageFrom)
                    }
                    getData(TYPE_GET_HISTORY)
                }
            }
            STOP_GAME_FREE_BID -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
                setOffMenu(MENU_BID_FREE)
                bidFreeFragment?.dismiss()
                bidSamePriceFragment?.dismiss()
                bidFreeFragment = null
                bidSamePriceFragment = null
                itemBidSamePrice = null
                itemBidFree = null
                getData(TYPE_GET_HISTORY)
            }
        }
    }

    private fun setItemBidProduct(string: String?) {
        if (string.isNullOrEmpty()) {
            return
        }
        itemBidFree = null
        itemBidSamePrice = null
        listBidMulti = ArrayList()
        val gson = Gson()
        val arr = JsonParserUtil.getJsonArray(string)
        if (arr == null || arr.length() == 0) {
            return
        }
        for (i in 0 until arr.length()) {
            val item: ItemBidProduct? = try {
                gson.fromJson(arr.get(0).toString(), ItemBidProduct::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
            if (item == null) {
                return
            }
            val timeDifferrent =
                AppUtils.getTimeInMilliSeconds(item.curentTime) - System.currentTimeMillis()
            Loggers.e("TIME", "" + timeDifferrent)
            item.startTimeMs = AppUtils.getTimeInMilliSeconds(item.startTime) - timeDifferrent
            item.endTimeMs = System.currentTimeMillis() + item.countDown!! * 1000
            val json = JSONObject(dataSend)
            json.put(DATA, arr.get(0))
            json.put(SERVICE, SERVICE_BID)
            json.remove(SETTING)
//            if (!item.note.equals(NOTE_BID_MUTIL)) {
            if (item.priceBid != null && item.priceBid!!.toInt() > 0) {
                json.put(KEY, BID_GAME_FREE)
                item.dataSend = json.toString()
                if (itemBidSamePrice == null) {
                    itemBidSamePrice = item
                    break
                }
            } else {
                json.put(KEY, BID_GAME_FREE)
                item.dataSend = json.toString()
                if (itemBidFree == null) {
                    itemBidFree = item
                    break
                }
            }
        }
        showBidPopUp()
    }

    private fun showBidPopUp() {
        if (itemBidFree != null) {
            showBidFreeFragment()
        } else if (itemBidSamePrice != null) {
            showBidSamePriceFragment()
        }
//        else if (listBidMulti != null && listBidMulti!!.size > 0) {
//            showBidMultiFragment()
//        }
    }

    var bidFreeFragment: BidFreeFragment? = null
    var itemBidFree: ItemBidProduct? = null
    private fun showBidFreeFragment() {
        if (itemBidFree == null) {
//            showDialogAutoClose("Mini game chưa bắt đầu!")
            return
        }
        var timStartSecond = -1L;
        var timeLeftSecond = -1L;
        val currentTimeMs = System.currentTimeMillis()
        if (itemBidFree!!.countDown != null && itemBidFree!!.countDown!! > 0) {
            timeLeftSecond = ((itemBidFree!!.endTimeMs!! - currentTimeMs) / 1000)
            Loggers.e(
                "BIDcount",
                "countdown  " + itemBidFree!!.endTime + " /// " + " /// " + currentTimeMs + " /// " + timeLeftSecond
            )
        } else {
            timStartSecond = ((currentTimeMs - itemBidFree!!.startTimeMs!!) / 1000)
            Loggers.e(
                "BIDcount",
                "countup  " + itemBidFree!!.startTime + " /// " + " /// " + currentTimeMs + " /// " + timStartSecond
            )
        }
        bidFreeFragment = BidFreeFragment()
        val bundle = Bundle()
        bundle.putParcelable(DATA, itemBidFree)
        bidFreeFragment?.arguments = bundle
        bidFreeFragment?.onClickListener = object : BidFreeFragment.OnClickButtonBidListenner {
            override fun onClick(item: ItemBidProduct) {
                Loggers.e("Send", item.dataSend!!)
                mSocketMangager?.send(EncryptUtil.base64Encode(item.dataSend!!))
            }
        }
        bidFreeFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        showBottomFragment(bidFreeFragment, "BID_ZERO")
        if (timeLeftSecond > 0) {
            Handler(Looper.getMainLooper()).postDelayed({
                bidFreeFragment?.startCountDown(timeLeftSecond)
            }, 0)
        } else {
            Handler(Looper.getMainLooper()).postDelayed({
                bidFreeFragment?.startCountUp(timStartSecond)
            }, 0)
        }
    }

    var bidSamePriceFragment: BidSamePriceFragment? = null
    var itemBidSamePrice: ItemBidProduct? = null
    private fun showBidSamePriceFragment() {
        if (itemBidSamePrice == null) {
//            showDialogAutoClose("Mini game chưa bắt đầu!")
            return
        }
        var timStartSecond = -1L;
        var timeLeftSecond = -1L;
        val currentTimeMs = System.currentTimeMillis()
        if (itemBidSamePrice!!.countDown != null && itemBidSamePrice!!.countDown!! > 0) {
            timeLeftSecond = ((itemBidSamePrice!!.endTimeMs!! - currentTimeMs) / 1000)
            Loggers.e("BID", "" + timeLeftSecond)
        } else {
            timStartSecond = ((currentTimeMs - itemBidSamePrice!!.startTimeMs!!) / 1000)
        }
        bidSamePriceFragment = BidSamePriceFragment()
        val bundle = Bundle()
        bundle.putParcelable(DATA, itemBidSamePrice)
        bidSamePriceFragment?.arguments = bundle
        bidSamePriceFragment?.onClickListener =
            object : BidSamePriceFragment.OnClickButtonBidListenner {
                override fun onClick(item: ItemBidProduct) {
                    Loggers.e("Send", item.dataSend)
                    mSocketMangager?.send(EncryptUtil.base64Encode(item.dataSend!!))
                }
            }
        bidSamePriceFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        showBottomFragment(bidSamePriceFragment, "BID_SAME_PRICE")
        if (timeLeftSecond > 0) {
            Handler(Looper.getMainLooper()).postDelayed({
                bidSamePriceFragment?.startCountDown(timeLeftSecond)
            }, 0)
        } else {
            Handler(Looper.getMainLooper()).postDelayed({
                bidSamePriceFragment?.startCountUp(timStartSecond)
            }, 0)
        }
    }

    private fun handleBIDMULTI(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val message = JsonParserUtil.getString(json, MESSAGE)
        when (key) {
            START -> {
                val data = JsonParserUtil.getString(json, DATA)
                Loggers.e("BIDMULTI", data)
                setListBidMulti(data)
                showBidMultiFragment()
                setOnMenu(MENU_BID_MULTIL)
            }
            BID -> {
                if (listBidMulti == null || listBidMulti!!.size == 0) {
                    return
                }
                val errorCode = JsonParserUtil.getString(json, ERR_CODE)
                if (errorCode.equals("1")) {
                    if (!message.isNullOrEmpty()) {
                        showDialogAutoClose(message)
                    }
                    updateListBidMulti(JsonParserUtil.getString(json, DATA))
                } else {
                    val id = JsonParserUtil.getString(json, CMD)
                    for (i in 0 until listBidMulti!!.size) {
                        val item = listBidMulti!!.get(i)
                        if (item.id.equals(id)) {
                            val data = JsonParserUtil.getJsonObject(json, DATA)
                            val from = JsonParserUtil.getJsonObject(json, FROM)
                            val idFrom = JsonParserUtil.getString(from, "ID")
                            val bidMoney = JsonParserUtil.getInt(from, "BidMoney")
                            val priceBid = bidMoney!! + item.priceStep!!.toInt()
                            val countDown = JsonParserUtil.getInt(data, "countdown")
                            item.countDown = countDown!!.toLong()
                            item.endTimeMs = System.currentTimeMillis() + item.countDown!! * 1000
                            item.priceBid = priceBid.toString()
                            if (idFrom.equals(userId)) {
                                item.isBid = true
                                if (!message.isNullOrEmpty()) {
                                    item.msg = message.replace(userName!!, "Bạn")
                                }
//                                item.msg = "Bạn đã đấu với mức giá cao nhất là: " + AppUtils.moneyFormat(bidMoney.toString()) + "."
                            } else {
                                item.msg = message
//                                item.msg = JsonParserUtil.getString(from, "Fullname") + " đã đấu với mức giá cao nhất là: " + AppUtils.moneyFormat(bidMoney!!.toString()) + "."

                                item.isBid = false
                            }
                        }
                    }
                }
                if (bidMultiFragment == null || !bidMultiFragment!!.isVisible) {
                    showBidMultiFragment()
                } else {
                    bidMultiFragment!!.updateUI(listBidMulti)
                }
            }

            NEXT -> {
                if (listBidMulti == null || listBidMulti!!.size == 0) {
                    return
                }
                val data = JsonParserUtil.getJsonArray(json, DATA)
                if (data == null || data.length() == 0) {
                    val idRemove = JsonParserUtil.getString(json, CMD)
                    for (i in 0 until listBidMulti!!.size) {
                        if (listBidMulti!!.get(i).id.equals(idRemove)) {
                            try {
                                listBidMulti!!.removeAt(i)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            break
                        }
                    }
                    if (listBidMulti!!.size == 0) {
                        bidMultiFragment?.dismiss()
                        return
                    }
                    if (bidMultiFragment == null || !bidMultiFragment!!.isVisible) {
                        showBidMultiFragment()
                    } else {
                        bidMultiFragment!!.updateUI(listBidMulti)
                    }
                    return
                }
                for (i in 0 until listBidMulti!!.size) {
                    listBidMulti!!.get(i).status = "0"
                }
                for (i in 0 until data!!.length()) {
                    val itemJson = JsonParserUtil.getJsonObject(data.get(i).toString())
                    val id = JsonParserUtil.getString(itemJson, "id")
                    for (j in 0 until listBidMulti!!.size) {
                        var item = listBidMulti!!.get(j)
                        if (item.id.equals(id)) {
                            item.status = "1"
                            item.countDown = JsonParserUtil.getLong(itemJson, "countdown")
                            item.endTimeMs = System.currentTimeMillis() + item.countDown!! * 1000
                            break
                        }
                    }
                }
                if (bidMultiFragment == null || !bidMultiFragment!!.isVisible) {
                    showBidMultiFragment()
                } else {
                    bidMultiFragment!!.updateUI(listBidMulti)
                }
            }
            WIN -> {
                if (!message.isNullOrEmpty()) {
                    showFirework(message, EffectFragment.TYPE_EFFECT_WIN, true)
                }
            }
            STOP -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
                listBidMulti = null
                bidMultiFragment?.dismiss()
                setOffMenu(MENU_BID_MULTIL)
                getData(TYPE_GET_HISTORY)
            }
        }

    }

    private fun setListBidMulti(string: String?) {
        if (string.isNullOrEmpty()) {
            return
        }
        listBidMulti = ArrayList()
        val gson = Gson()
//        val type = object : TypeToken<ArrayList<ItemBidProduct>>() {}.getType()
//        listBidMulti = try {
//            gson.fromJson(string, type)
//        } catch (e: Exception) {
//            null
//        }
        val arr = JsonParserUtil.getJsonArray(string)
        if (arr == null || arr.length() == 0) {
            return
        }
        for (i in 0 until arr.length()) {
            val item: ItemBidMulti? = try {
                gson.fromJson(arr.get(i).toString(), ItemBidMulti::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
            if (item == null) {
                continue
            }
            if (!item.id.equals("0")) {
                if (item.countDown != null) {
                    item.endTimeMs = System.currentTimeMillis() + item.countDown!! * 1000
                }
                if (item.note.equals(userId)) {
                    item.isBid = true
                    if (!item.message.isNullOrEmpty()) {
                        item.msg = item.message!!.replace(userName!!, "Bạn")
                    }
                } else {
                    if (!item.message.isNullOrEmpty()) {
                        item.msg = item.message!!
                    }
                }
                listBidMulti?.add(item)
            }
        }
    }

    private fun updateListBidMulti(string: String?) {
        if (string.isNullOrEmpty()) {
            return
        }
        val gson = Gson()
//        val type = object : TypeToken<ArrayList<ItemBidProduct>>() {}.getType()
//        listBidMulti = try {
//            gson.fromJson(string, type)
//        } catch (e: Exception) {
//            null
//        }
        val arr = JsonParserUtil.getJsonArray(string)
        if (arr == null || arr.length() == 0) {
            return
        }
        for (i in 0 until arr.length()) {
            val item: ItemBidMulti? = try {
                gson.fromJson(arr.get(i).toString(), ItemBidMulti::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
            if (item == null) {
                continue
            }
            if (!item.id.equals("0")) {
                if (item.countDown != null) {
                    item.endTimeMs = System.currentTimeMillis() + item.countDown!! * 1000
                }
                if (item.note.equals(userId)) {
                    item.isBid = true
                    if (!item.message.isNullOrEmpty()) {
                        item.msg = item.message!!.replace(userName!!, "Bạn")
                    }
                } else {
                    if (!item.message.isNullOrEmpty()) {
                        item.msg = item.message!!
                    }
                }
                for (j in 0 until listBidMulti!!.size) {
                    if (item.id != null && item.id.equals(listBidMulti!!.get(j))) {
                        listBidMulti!!.set(j, item)
                        break
                    }
                }
            }
        }
    }

    var bidMultiFragment: BidMultiFragment? = null
    var listBidMulti: ArrayList<ItemBidMulti>? = null
    private fun showBidMultiFragment() {
        if (listBidMulti == null || listBidMulti!!.size == 0) {
//                showDialogAutoClose("Mini game chưa bắt đầu!")
            return
        }
        bidMultiFragment = BidMultiFragment()
        val bundle = Bundle()
        bundle.putParcelableArrayList(DATA, listBidMulti)
        val height =
            layoutRoot.height + resources.getDimensionPixelSize(R.dimen.dimen_56) - layoutHeader.height
        bundle.putInt(HEIGHT, 0)
        bidMultiFragment?.arguments = bundle
        bidMultiFragment?.onItemClickListener = object : ItemBidMultiAdapter.OnItemClickListener {
            override fun onClickOk(itemObject: ItemBidMulti, position: Int) {
                val json = JsonParserUtil.getJsonObject(dataSend)
                if (json == null) {
                    return
                }
                val jsonFrom = JsonParserUtil.getJsonObject(json, FROM)
                jsonFrom?.put("BidMoney", itemObject.priceBid!!.toInt())
                jsonFrom?.put("Command", position + 1)
                jsonFrom?.put("Url_Avatar", avatar)
                json.put(FROM, jsonFrom)
                json.put(SERVICE, SERVICE_BIDMULTI)
                json.put(KEY, BID)
                json.put(CMD, itemObject.id)
                json.remove(SETTING)
                json.remove(DATA)
                Loggers.e("Send", json.toString())
                mSocketMangager?.send(EncryptUtil.base64Encode(json.toString()))
            }

            override fun onClickNo(itemObject: ItemBidMulti, position: Int) {
            }

            override fun onClickBid(itemObject: ItemBidMulti, position: Int) {
            }
        }

        bidMultiFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        showBottomFragment(bidMultiFragment, "BID_MULTI")
    }

    private fun handleIQ(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val message = JsonParserUtil.getString(json, MESSAGE)
        when (key) {
            START_GAME_IQ -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
                val data = JsonParserUtil.getString(json, "data")
                setItemIQ(data)
                setOnMenu(MENU_IQ)

            }
            PLAYER_ANSWER_TIMEOUT -> {
                if (message.isNullOrEmpty()) {
                    return
                }
                showDialogAutoClose(message)
            }
            SHOW_CHART_GAME_IQ -> {
                itemIQ?.dataResult = JsonParserUtil.getString(json, DATA)
                if (iqFragment != null && iqFragment!!.isVisible) {
                    iqFragment?.updateUI(itemIQ)
                }
            }
            IQ_MESSAGE -> {
                if (message.isNullOrEmpty()) {
                    return
                }
                showFirework(message, EffectFragment.TYPE_EFFECT_WIN, true)
            }
            IQ_FINISH -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
                val result = JsonParserUtil.getString(from, NOTE)
                val point = JsonParserUtil.getString(from, "point")
                var msg: String? = ""
                if (!point.isNullOrEmpty()) {
                    msg = "Thành tích của bạn:" + "<br/>" + point + " Điểm"
//                    msg = "Thành tích của bạn" + System.getProperty("line.separator") + point + " Điểm"
                }
                //WIN LOSE N/A
                val dataResult = JsonParserUtil.getString(json, DATA)
                itemIQ?.dataResult = dataResult
                if (iqFragment != null && iqFragment!!.isVisible) {
                    iqFragment?.updateUI(itemIQ)
                }
                getData(TYPE_GET_HISTORY)
                when (result) {
                    "WIN" -> {
                        showFirework(msg, EffectFragment.TYPE_EFFECT_WIN_ICON, false, false)
//                        if (iqFragment != null && iqFragment!!.isVisible) {
////                            if (!timeClick.isNullOrEmpty()) {
////                                msg = "Thời gian trả lời: " + timeClick + "/n" + msg
////                            }
//                            iqFragment?.showWin(msg)
//                        }
                    }

                    "LOSE" -> {
                        showFirework(msg, EffectFragment.TYPE_EFFECT_FALSE, false, false)
//                        if (iqFragment != null && iqFragment!!.isVisible) {
//                            iqFragment?.showFalse(msg!!)
//                        }
                    }
                }
            }
            SHOW_TOP_IQ -> {
                showIQResultFragment()
            }
            STOP_GAME_IQ -> {
                setOffMenu(MENU_IQ)
                iqFragment?.dismiss()
                iqResultFragment?.dismiss()
                iqResultFragment = null
                iqFragment = null
                itemIQ = null
                getData(TYPE_GET_HISTORY)
            }
        }
    }

    var iqResultFragment: IQResultFragment? = null
    private fun showIQResultFragment() {
        if (itemIQ == null || itemIQ!!.dataResult.isNullOrEmpty()) {
            return
        }
        iqResultFragment = IQResultFragment()
        val bundle = Bundle()
        setHeightFragment(bundle)
        bundle.putString(DATA, itemIQ!!.dataResult)
        iqResultFragment?.arguments = bundle
        showBottomFragment(iqResultFragment, "IQ", true)
    }

    var itemIQ: ItemIQ? = null
    private fun setItemIQ(string: String?) {
        itemIQ = null
        if (string.isNullOrEmpty()) {
            return
        }
        val gson = Gson()
        val item: ItemIQ? = try {
            gson.fromJson(string, ItemIQ::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            null
            return
        }
        val json = JSONObject(dataSend)
        json.put(DATA, string)
        json.put(SERVICE, SERVICE_IQ)
        json.put(KEY, PLAYER_ANSWER)
        item?.data = json.toString()
        itemIQ = item
        itemIQ?.indexSelected = -1
        val currentTimeMs = System.currentTimeMillis()
        itemIQ?.timeEndMs = currentTimeMs + itemIQ!!.timeLeft!!.toInt() * 1000
        activity?.runOnUiThread {
            showIQ()
        }
    }

    var iqFragment: IQFragment? = null
    private fun showIQ() {
        if (itemIQ == null) {
            return
        }
        /* val listCaseAnswer: ArrayList<ItemCaseAnswer> = ArrayList()
         listCaseAnswer.add(
             ItemCaseAnswer(
                 arrayOf("Đáp án A", " a ",itemIQ!!.replyA!!),
                 "Đã nhận Đáp án A",
                 "A"
             )
         )
         listCaseAnswer.add(
             ItemCaseAnswer(
                 arrayOf("Đáp án B", " b ", itemIQ!!.replyB!!),
                 "Đã nhận Đáp án B",
                 "B"
             )
         )
         listCaseAnswer.add(
             ItemCaseAnswer(
                 arrayOf("Đáp án C", " c ", itemIQ!!.replyC!!),
                 "Đã nhận Đáp án C",
                 "C"
             )
         )
         listCaseAnswer.add(
             ItemCaseAnswer(
                 arrayOf("Đáp án D", " d ", itemIQ!!.replyD!!),
                 "Đã nhận Đáp án D",
                 "D"
             )
         )
         val question =
             itemIQ!!.nameQuestion + "<br/>" + "A. ${itemIQ!!.replyA}" + "<br/>" + "B. ${itemIQ!!.replyB}" + "<br/>" + "C. ${itemIQ!!.replyC}" + "<br/>" + "D. ${itemIQ!!.replyD}"
         val itemCase = ItemCase("1", question, listCaseAnswer,null,null,"Bạn chọn đáp án nào?")
         val fragment = VoiceControlFragment.showFragment(
             activity,
             itemCase,
             object : VoiceControllerManager.OnResultListener {
                 override fun onPartialResults(str: String?) {
                 }

                 override fun onResult(str: String?) {
                     var json = JsonParserUtil.getJsonObject(itemIQ!!.data)
                     json?.put(CMD, str)
                     val str = json.toString()
                     Loggers.e("Send", str)
                     mSocketMangager?.send(EncryptUtil.base64Encode(str))
                 }

                 override fun onRmsChanged(p0: Float) {
                 }
             },
             false,
             true
         )
         val currentTimeMs = System.currentTimeMillis()
         val timeLeftMS = (itemIQ!!.timeEndMs!! - currentTimeMs)
         Handler(Looper.getMainLooper()).postDelayed({
             fragment?.dismiss()
         }, timeLeftMS)*/ //useVoiceController
        if (iqFragment == null || !iqFragment!!.isVisible) {
            iqFragment = IQFragment()
            val bundle = Bundle()
            bundle.putParcelable(DATA, itemIQ)
            setHeightFragment(bundle)
            iqFragment?.arguments = bundle
            iqFragment?.onClickListener = object : ItemIQAnswerAdapter.OnItemClickListener {
                override fun onClick(itemObject: ItemIQAnswer?, position: Int, isTimeOut: Boolean) {
                    if (itemObject == null) {
                        return
                    }
                    if (isTimeOut) {
                        showDialogAutoClose("Đã hết giờ. Vui lòng chờ xem kết quả!")
                        return
                    }
                    if (itemIQ!!.indexSelected == -1) {
                        itemIQ!!.indexSelected = position
                        var json = JsonParserUtil.getJsonObject(itemIQ!!.data)
                        json?.put(CMD, itemObject.title)
                        val str = json.toString()
                        Loggers.e("Send", str)
                        mSocketMangager?.send(EncryptUtil.base64Encode(str))
                    } else {
                        showDialogAutoClose("Bạn đã chọn đáp án rồi. Vui lòng chờ xem kết quả!")
                    }
                }
            }
            iqFragment?.onShowDismissListener = object : OnShowDismissListener {
                override fun onShow() {
                    showDimer()
                }

                override fun onDismiss() {
                    hideDimer()
                }
            }
            showBottomFragment(iqFragment, "IQ")
        } else {
            activity?.runOnUiThread {
                iqFragment?.updateUI(itemIQ)
            }
        }
        val currentTimeMs = System.currentTimeMillis()
        val timeLeftSecond = (itemIQ!!.timeEndMs!! - currentTimeMs) / 1000
        Handler(Looper.getMainLooper()).postDelayed({
            iqFragment?.startCountDown(timeLeftSecond)
        }, 0)
        Loggers.e("showIQ", itemIQ!!.endTime!! + "----" + timeLeftSecond)
    }

    private fun handleSPIN(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val message = JsonParserUtil.getString(json, MESSAGE)
        when (key) {
            SPIN_REQUEST_QUICK -> {
                activity?.runOnUiThread {
                    val data = JsonParserUtil.getString(json, DATA)
                    showSpin(data)
                }
            }
            SPIN_ACCEPT_QUICK -> {
                activity?.runOnUiThread {
                    showDialogAutoClose(message)
                }
                getData(TYPE_GET_HISTORY)
            }
            SPIN_ACCEPT -> {
                if (message.isNullOrEmpty()) {
                    return
                }
                activity?.runOnUiThread {
                    showDialogAutoClose(message)
                }
            }
            SPIN_RESPONSE -> {
                if (!message.isNullOrEmpty()) {
                    activity?.runOnUiThread {
                        if (!message.contains("lần sau")) {
                            showFirework(message, EffectFragment.TYPE_EFFECT_WIN, true)
                        } else {
                            showDialogAutoClose(message)
                        }
                    }
                }
                getData(TYPE_GET_HISTORY)
            }
        }
    }

    private fun showSpin(data: String?) {
        if (data.isNullOrEmpty()) {
            return
        }
        val itemSpin: ItemSpin? = try {
            Gson().fromJson(data, ItemSpin::class.java)
        } catch (e: Exception) {
            null
        }
        if (itemSpin == null) {
            return
        }
        if (itemSpin.timeCount == null || itemSpin.timeCount <= 0) {
            return
        }
        val dataSpinSend = JsonParserUtil.getJsonObject(dataSend)
        dataSpinSend?.put(SERVICE, SERVICE_SPIN)
        dataSpinSend?.put(KEY, SPIN_ACCEPT_QUICK)
        dataSpinSend?.put(CMD, "accepted")
        dataSpinSend?.remove(SETTING)
        if (itemSpin.isDiemdanh != null && itemSpin.isDiemdanh) {
            val fragment = SpinSpecialFragment()
            val bundle = Bundle()
            bundle.putParcelable(DATA, itemSpin)
            setHeightFragment(bundle)
            fragment.arguments = bundle
            fragment.onClickListener = object : SpinSpecialFragment.OnClickButtonRightListenner {
                override fun onClick(item: ItemSpin) {
                    Loggers.e("Send", dataSpinSend.toString())
                    mSocketMangager?.send(EncryptUtil.base64Encode(dataSpinSend.toString()))
                }
            }
            fragment.onShowDismissListener = object : OnShowDismissListener {
                override fun onShow() {
                    showDimer()
                }

                override fun onDismiss() {
                    hideDimer()
                }
            }
            showBottomFragment(fragment, "SPIN_SPECIAL")
            return
        }
        val fragment = SpinFragment()
        val bundle = Bundle()
        bundle.putParcelable(DATA, itemSpin)
        setHeightFragment(bundle)
        fragment.arguments = bundle
        fragment.onClickListener = object : SpinFragment.OnClickButtonRightListenner {
            override fun onClick(item: ItemSpin) {
                Loggers.e("Send", dataSpinSend.toString())
                mSocketMangager?.send(EncryptUtil.base64Encode(dataSpinSend.toString()))
            }
        }
        fragment.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        showBottomFragment(fragment, "SPIN")
    }

    private fun handleVOTE2(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val message = JsonParserUtil.getString(json, MESSAGE)
        when (key) {
            VOTE2_START -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
                val data = JsonParserUtil.getString(json, DATA)
                setItemVote(data)
                setOnMenu(MENU_VOTE2)
            }

            VOTE2_SEND -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
            }
            VOTE2_UPDATE_CLOCK -> {
                if (message.isNullOrEmpty()) {
                    return
                }
                showDialogAutoClose(message)
            }
            VOTE2_RESPONSE -> {
                val data = JsonParserUtil.getJsonObject(json, DATA)
                val voteDetail = JsonParserUtil.getString(data, "vote_detail")
                if (voteDetail.isNullOrEmpty()) {
                    return
                }
                itemVote?.voteDetail = voteDetail
                if (voteFragment != null && voteFragment!!.isVisible) {
                    voteFragment?.updateUI(itemVote)
                }
            }
            VOTE2_STOP -> {
                setOffMenu(MENU_VOTE2)
                voteFragment?.dismiss()
                voteFragment = null
                itemVote = null
                getData(TYPE_GET_HISTORY)
            }
        }
    }

    private fun setItemVote(data: String?) {
        itemVote = null
        if (data.isNullOrEmpty()) {
            return
        }
        val gson = Gson()
        val item: ItemVote? = try {
            gson.fromJson(data, ItemVote::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            return
        }
        itemVote = item
        if (itemVote != null && !itemVote!!.voteDetail.isNullOrEmpty()) {
            itemVote?.timeEndMs = System.currentTimeMillis() + item?.countDown!! * 1000
            itemVote?.indexSelected = -1
            showVote()
        }
    }

    var itemVote: ItemVote? = null
    var voteFragment: VoteFragment? = null
    fun showVote() {
        if (itemVote == null) {
//            showDialogAutoClose("Mini game chưa bắt đầu!")
            return
        }
        voteFragment = VoteFragment()
        val bundle = Bundle()
        bundle.putParcelable(DATA, itemVote)
        setHeightFragment(bundle)
        voteFragment?.arguments = bundle
        voteFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        voteFragment?.onClickListener = object : VoteAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemVoteAnswer, position: Int) {
                if (itemVote!!.indexSelected == -1) {
                    itemVote!!.indexSelected = position
                    var json = JsonParserUtil.getJsonObject(dataSend)
                    if (json == null) {
                        return
                    }
                    val dataJson = JSONObject()
                    dataJson.put("Avt", avatar)
                    dataJson.put("ID", userId)
                    dataJson.put("Socket_ID", userId)
                    dataJson.put("Name", userName)
                    dataJson.put("Point", 1)
                    dataJson.put("Vote_ID", itemObject.voteID)
                    json.put(SERVICE, SERVICE_VOTE2)
                    json.put(KEY, VOTE2_SEND)
                    json.put(CMD, "ok")
                    json.put(DATA, dataJson)
                    json.remove(SETTING)
                    Loggers.e("Send", json.toString())
                    mSocketMangager?.send(EncryptUtil.base64Encode(json.toString()))
                } else {
                    showDialogAutoClose("Bạn đã bình chọn rồi. Vui lòng chờ xem kết quả!")
                }
            }
        }
        showBottomFragment(voteFragment, "VOTE")
        if (itemVote!!.countDown != null && itemVote!!.countDown!! > 0) {
            val timeLeftSecond = ((itemVote!!.timeEndMs!! - System.currentTimeMillis()) / 1000)
            voteFragment?.startCountDown(timeLeftSecond)
            Loggers.e(
                "BIDcount",
                "countdown  " + itemVote!!.timeEndMs + " /// " + " /// " + System.currentTimeMillis() + " /// " + timeLeftSecond
            )
        }
    }

    private fun handleGUESS_PRICE(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        var message = JsonParserUtil.getString(json, MESSAGE)
        when (key) {
            DOANGIA_START -> {
                val data = JsonParserUtil.getString(json, DATA)
                setItemBidGuessPrice(data)
                setOnMenu(MENU_DOAN_GIA)
            }
            DOANGIA_SEND -> {
                if (!message.isNullOrEmpty()) {
//                    if (guessFragment != null && guessFragment!!.isVisible) {
//                        guessFragment!!.speakWithTyping(message)
//                    } else {
                    showDialogAutoClose(message)
//                    }
                    if (message.contains("đã hết")) {
                        itemBidGuessPrice?.playerMessage = message
                        if (bidGuessPriceFragment != null && bidGuessPriceFragment!!.isVisible && !message.isNullOrEmpty()) {
                            bidGuessPriceFragment!!.setTextNotify(message)
                        }
                        return
                    }
                }

                val cmd = JsonParserUtil.getString(json, CMD)
                if (cmd != null && cmd.contains("true")) {
                    itemBidGuessPrice?.playerMessage = message
                    if (bidGuessPriceFragment != null && bidGuessPriceFragment!!.isVisible && !message.isNullOrEmpty()) {
                        bidGuessPriceFragment!!.setTextNotify(message)
                    }
                    getData(TYPE_GET_HISTORY)
                } else {
                    if (bidGuessPriceFragment != null && bidGuessPriceFragment!!.isVisible) {
                        bidGuessPriceFragment!!.reOpenGuess()
                    }
                }
            }
            DOANGIA_FINISH -> {
                if (message.isNullOrEmpty()) {
                    return
//                    message = "Chúc mừng bạn đã là người chiến thắng! "
                }
                showFirework(message, EffectFragment.TYPE_EFFECT_WIN, true)
            }
            DOANGIA_STOP -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
                bidGuessPriceFragment?.dismiss()
                bidGuessPriceFragment = null
                itemBidGuessPrice = null
                setOffMenu(MENU_DOAN_GIA)
                getData(TYPE_GET_HISTORY)
            }
        }
    }

    private fun setItemBidGuessPrice(string: String?) {
        if (string.isNullOrEmpty()) {
            return
        }
        itemBidGuessPrice = null
        val gson = Gson()
        val item: ItemBidProduct? = try {
            gson.fromJson(string, ItemBidProduct::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
        if (item == null) {
            return
        }
        item.startTimeMs = AppUtils.getTimeInMilliSeconds(item.startTime)
        item.endTimeMs = System.currentTimeMillis() + item.countDown!! * 1000
        val json = JSONObject(dataSend)
        item.dataSend = json.toString()
        itemBidGuessPrice = item
        showBidGuessPrice()
    }


    var bidGuessPriceFragment: BidGuessPriceFragment? = null
    var itemBidGuessPrice: ItemBidProduct? = null

    //    var guessFragment: VoiceControlFragment? = null
    private fun showBidGuessPrice() {
        if (itemBidGuessPrice == null) {
//            showDialogAutoClose("Mini game chưa bắt đầu!")
            return
        }
        /* guessFragment = VoiceControlFragment.showFragment(
             requireActivity(),
             itemBidGuessPrice!!.name,
             object : VoiceControllerManager.OnResultListener {
                 override fun onPartialResults(str: String?) {
                 }

                 override fun onResult(str: String?) {
                     if (!str.isNullOrEmpty()) {
                         var result = str
                         result = result.replace("triệu", "000000")
                         result = result.replace("trăm", "00")
                         result = result.replace("nghìn", "000")
                         result = result.replace("một", "1")
                         result = result.replace("hai", "2")
                         result = result.replace("ba", "3")
                         result = result.replace("bốn", "4")
                         result = result.replace("năm", "5")
                         result = result.replace("sáu", "6")
                         result = result.replace("bảy", "7")
                         result = result.replace("tám", "8")
                         result = result.replace("chín", "9")
                         result = result.replace(".", "")
                         result = result.replace(",", "")
                         val listStr = result.split(" ")
                         for (str in listStr) {
                             try {
                                 val price = str.toInt()
                                 if (price > 1000) {
                                     val json = JSONObject(dataSend)
                                     json.put(SERVICE, SERVICE_GUESS_PRICE)
                                     val jsonFrom = JsonParserUtil.getJsonObject(json, FROM)
                                     jsonFrom?.put("BidMoney", str)
                                     json.put(FROM, jsonFrom)
                                     json.put(KEY, DOANGIA_SEND)
                                     json.put(CMD, "ok")
                                     json.remove(DATA)
                                     json.remove(SETTING)
                                     Loggers.e("SendGuess", json.toString())
                                     mSocketMangager?.send(EncryptUtil.base64Encode(json.toString()))
 //                                    guessFragment?.speakWithTyping("Bạn đã đoán sản phẩm với mức giá $str VNĐ. Vui lòng chờ xem kết quả nhé!")
                                     break
                                 }
                             } catch (e: Exception) {
                             }
                         }
                     }
                 }

                 override fun onRmsChanged(p0: Float) {
                 }
             })
         return*/  //useVoiceCotroller
        bidGuessPriceFragment = BidGuessPriceFragment()
        val bundle = Bundle()
        bundle.putParcelable(DATA, itemBidGuessPrice)
//        setHeightFragment(bundle)
        bidGuessPriceFragment?.arguments = bundle
        bidGuessPriceFragment?.onClickListener =
            object : BidGuessPriceFragment.OnClickButtonBidListenner {
                override fun onClick(item: ItemBidProduct) {
                    val json = JSONObject(dataSend)
                    json.put(SERVICE, SERVICE_GUESS_PRICE)
                    val jsonFrom = JsonParserUtil.getJsonObject(json, FROM)
                    jsonFrom?.put("BidMoney", item.priceBid)
                    json.put(FROM, jsonFrom)
                    json.put(KEY, DOANGIA_SEND)
                    json.put(CMD, "ok")
                    json.remove(DATA)
                    json.remove(SETTING)
                    Loggers.e("SendGuess", json.toString())
                    mSocketMangager?.send(EncryptUtil.base64Encode(json.toString()))
                }
            }
        bidGuessPriceFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        showBottomFragment(bidGuessPriceFragment, "BID_GUESS")
    }

    private fun handleGIT(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val message = JsonParserUtil.getString(json, MESSAGE)
        when (key) {
            EMOTION_SEND -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
            }

            EMOTION_APPROVED -> {
//                if (!message.isNullOrEmpty()) {
//                    showDialogAutoClose(message)
//                }
                getData(TYPE_GET_HISTORY)
            }

        }
    }

    val TYPE_GET_GIFT = 4
    val TYPE_GET_GIFT_ITEM = 5
    var myHttpRequestGift: MyHttpRequest? = null
    var listGiftCategory: ArrayList<ItemGitCategory> = ArrayList()
    fun getDataGift(type: Int, typeID: String?) {
        if (isDetached || context == null) {
            return
        }
        if (!AppUtils.isNetworkConnect(context)) {
            return
        }
        if (myHttpRequestGift == null) {
            myHttpRequestGift = MyHttpRequest(requireContext())
        }
        var api: String? = null
        if (type == TYPE_GET_GIFT) {
            api = validApiGameTT(context, API_GIFTS)
        } else if (type == TYPE_GET_GIFT_ITEM) {
            api = validApiGameTT(context, API_GIFTS_ITEM) + typeID
        }
        val requestParams = RequestParams()
//        requestParams.put("id", "${itemObj!!.id}")
//        requestParams.put("content_type", "${itemObj!!.contentType}")
//        requestParams.put("style", "3")

        myHttpRequestGift?.request(
            false,
            api,
            requestParams,
            object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                    isGettingDataGit = false
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    if (isDetached) {
                        return
                    }

                    try {
                        handleDataGift(type, typeID, responseString)
                    } finally {
                        isGettingDataGit = false
                    }
                }
            })
    }

    private fun handleDataGift(type: Int, typeId: String?, responseString: String?) {
        if (type == TYPE_GET_GIFT) {
            listGiftCategory.clear()
            val json = JsonParserUtil.getJsonObject(responseString)
            if (json == null) {
                return
            }
            val data = JsonParserUtil.getString(json, DATA)
            val gson = Gson()
            val type = object : TypeToken<ArrayList<ItemGitCategory>>() {}.getType()
            val itemList: ArrayList<ItemGitCategory>? = try {
                gson.fromJson(data, type)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
            if (itemList == null || itemList.size == 0) {
                return
            }
            listGiftCategory.addAll(itemList)
            activity?.runOnUiThread {
                showGift()
            }

        } else if (type == TYPE_GET_GIFT_ITEM) {
            for (i in 0 until listGiftCategory.size) {
                if (listGiftCategory.get(i).id.equals(typeId)) {
                    val json = JsonParserUtil.getJsonObject(responseString)
                    if (json != null) {
                        val gson = Gson()
                        val type = object : TypeToken<ArrayList<ItemGit>>() {}.getType()
                        val list: ArrayList<ItemGit>? = try {
                            gson.fromJson(JsonParserUtil.getString(json, DATA), type)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            null
                        }
                        listGiftCategory.get(i).data = list
                        if (popupFragment != null && popupFragment!!.isVisible && popupFragment is GitFragment) {
                            activity?.runOnUiThread {
                                (popupFragment as GitFragment).updateFragment(
                                    listGiftCategory.get(i),
                                    i
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    private var isGettingDataGit: Boolean = false
    private fun showGift() {
        Loggers.e("GIFT", "" + isGettingDataGit)
        if (isGettingDataGit) {
            return
        }
        if (listGiftCategory.size == 0) {
            isGettingDataGit = true
            getDataGift(TYPE_GET_GIFT, null)
            return
        }
        val fragment = GitFragment()
        val bundle = Bundle()
        bundle.putParcelableArrayList(DATA, listGiftCategory)
        setHeightFragment(bundle)
        fragment.arguments = bundle
        fragment.onClickListener = object : GitFragment.OnClickListenner {
            override fun onClick(item: ItemGit, position: Int) {
                val json = JsonParserUtil.getJsonObject(dataSend)
                val arr = JSONArray()
                val jsonData = JSONObject()
                jsonData.put("chip", item.priceChip)
                jsonData.put("filename", item.src)
                jsonData.put("gift", 1)
                jsonData.put("id", item.id)
                jsonData.put("madv", item.madv)
                jsonData.put("name", item.name)
                jsonData.put("nameicon", item.name)
                jsonData.put("singerid", 124)
                jsonData.put("singername", "  ")
                jsonData.put("type", "  ")
                jsonData.put("uid", userId)
                arr.put(jsonData)
                json?.put(DATA, arr)
                val jsonFrom = JsonParserUtil.getJsonObject(json, FROM)
                jsonFrom?.put(MESSAGE, item.name)
                json?.put("fromclient", "#SOCKET-CLIENT")
                json?.put(FROM, jsonFrom)
                json?.put(SERVICE, SERVICE_GIT)
                json?.put(KEY, EMOTION_SEND)
                json?.put(CMD, item.name)
                json?.remove(SETTING)
                Loggers.e("Send", json.toString())
                mSocketMangager?.send(EncryptUtil.base64Encode(json.toString()))
            }

            override fun onSelectPage(itemGitCategory: ItemGitCategory?, position: Int) {
                if (itemGitCategory!!.data == null || itemGitCategory.data!!.size == 0) {
                    getDataGift(TYPE_GET_GIFT_ITEM, itemGitCategory.id)
                }
            }
        }
//        fragment.onShowDismissListener = object : OnShowDismissListener {
//            override fun onShow() {
//                showDimer()
//            }
//
//            override fun onDismiss() {
//                hideDimer()
//            }
//        }
        showBottomFragment(fragment, "GIFT")
    }

    private fun getItemMenuList(string: String?): ArrayList<ItemMenu>? {
        if (string.isNullOrEmpty()) {
            return ArrayList()
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<ItemMenu>>() {}.getType()
        val itemList: ArrayList<ItemMenu>? = try {
            gson.fromJson(string, type)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
        return itemList
    }

    private var itemMenuAdapter: ItemMenuAdapter? = null
    private fun initMenuAdapter(list: ArrayList<ItemMenu>?) {
        if (list == null) {
            return
        }
//        val style = Constant.STYLE_HORIZONTAL
        itemMenuAdapter = ItemMenuAdapter(requireActivity(), list!!, 1, 1, 1)
        val layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL)
        rcvGame?.layoutManager = layoutManager
        rcvGame?.adapter = itemMenuAdapter
        itemMenuAdapter?.onItemClickListener = object : ItemMenuAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemMenu, position: Int) {
                if (itemObject.name.isNullOrEmpty()) {
                    return
                }
                when (itemObject.name) {
                    MENU_START_INTERACTIVE -> {
                    }
                    MENU_CHAT -> {
                        showChat()
                    }
                    MENU_ISTUDIO_VOTE -> {

                    }
                    MENU_ISTUDIO_OFFER -> {

                    }
                    MENU_BID_FREE -> {
                        showBidPopUp()
                    }
                    MENU_BID_MULTIL -> {
                        showBidMultiFragment()
                    }
                    MENU_IQ -> {
                        showIQ()
                    }
                    MENU_DOAN_GIA -> {
                        showBidGuessPrice()
                    }
                    MENU_EMOTION -> {

                    }
                    MENU_VOTE -> {
                    }
                    MENU_VOTE2 -> {
                        showVote()
                    }
                    MENU_GIFT -> {
                        showGift()
                    }
                    MENU_VOICE -> {

                    }
                    MENU_ADS -> {
                    }
                    MENU_VIDEO_CALL -> {

                    }
                    MENU_GUIDE -> {
                        showGuide()
                    }
                    MENU_GROUP -> {
                        showGroupGame(null)
                    }
                    MENU_INVITE -> {
                        showInviteGame(null, false)
                    }
                    MENU_EMOTION2 -> {
                        showEmotionFragment()
                    }
                    MENU_WEBGAME -> {
                        Handler(Looper.getMainLooper()).postDelayed({
                            startWebgameFragment(dataWebGame)
                        }, 100)
                    }
                }
            }
        }
    }

    var timeGroupEndMs: Long = 0L
    var groupGameFragment: GroupGameFragment? = null
    private fun showGroupGame(dataGroup: String?) {
//        timeGroupEndMs = 15000 + System.currentTimeMillis()
        groupGameFragment = GroupGameFragment()
        val bundle = Bundle()
        bundle.putLong(TIME, timeGroupEndMs)
        bundle.putString(DATA, dataGroup)
        setHeightFragment(bundle)
        groupGameFragment?.arguments = bundle
        groupGameFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        showBottomFragment(groupGameFragment, "GROUP")
    }

    private fun handleINVITE(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val message = JsonParserUtil.getString(json, MESSAGE)
        when (key) {
            INVITE_START -> {
                try {
                    timeInviteEndMs = JsonParserUtil.getString(json, DATA)!!
                        .toLong() * 1000 + System.currentTimeMillis()
                } catch (e: Exception) {
                    timeInviteEndMs = 0
                }
                showInviteGame(null, false)
                setOnMenu(MENU_INVITE)
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
            }

            INVITE_STOP -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
                setOffMenu(MENU_INVITE)
                inviteGameFragment?.dismiss()
                inviteGameFragment = null
                timeInviteEndMs = 0L
            }
            INVITE_FINISH -> {
//                val data = JsonParserUtil.getString(json, DATA)
                showInviteGame(null, true)
                if (!message.isNullOrEmpty()) {
                    showFirework(message, EffectFragment.TYPE_EFFECT_WIN, false, false)
                }
            }
        }
    }

    var emotionFragment: EmotionFragment? = null
    var infoCurrentGame: String? = null
    private fun showEmotionFragment() {
        emotionFragment = EmotionFragment()
        val bundle = Bundle()
        setHeightFragment(bundle)
        bundle.putString(USER_ID, userId)
        bundle.putString(USER_TOKEN, userToken)
        bundle.putString(DATA_GAME, infoCurrentGame)
        emotionFragment?.arguments = bundle
        emotionFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        emotionFragment?.onItemClickListener = object : ItemEmotionAdapter.OnItemClickListener {
            override fun onClick(itemGame: ItemEmotion, position: Int) {
                val json = JsonParserUtil.getJsonObject(dataSend)
                if (json == null) {
                    return
                }
                val gson = Gson()
                val jsondata = JSONObject()
                jsondata.put("Icon_Id", itemGame.id)
                jsondata.put("Icon_Png", itemGame.imageFrame)
                jsondata.put(
                    "Icon_Sprite",
                    JsonParserUtil.getJsonObject(gson.toJson(itemGame.sprite))
                )
                jsondata.put("Icon_Seq", JsonParserUtil.getJsonObject(gson.toJson(itemGame.seq)))
                jsondata.put("Rate_Volunm", emotionFragment!!.rate)
                jsondata.put("Icon_Name", itemGame.name)
                json.put(DATA, jsondata)
                json.put(SERVICE, SERVICE_EMOTION2)
                json.put(KEY, EMOTION2_SEND)
                json.put("fromclient", SOCKET_CLIENT)
                json.remove(SETTING)
                Loggers.e("Send", json.toString())
                mSocketMangager?.send(EncryptUtil.base64Encode(json.toString()))
                emotionFragment?.dismiss()
            }
        }
        showBottomFragment(emotionFragment, "INVITE")
    }

    var emotionResultFragment: EmotionResultFragment? = null
    private fun showEmotionResultFragment() {
        emotionResultFragment = EmotionResultFragment()
        val bundle = Bundle()
        setHeightFragment(bundle)
        emotionResultFragment?.arguments = bundle
        emotionResultFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        showBottomFragment(emotionResultFragment, "INVITE")
    }

    private fun handleEMOTION2(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val message = JsonParserUtil.getString(json, MESSAGE)
        when (key) {
            EMOTION2_START -> {
//                if (!message.isNullOrEmpty()) {
//                    showDialogAutoClose(message)
//                }
                infoCurrentGame = JsonParserUtil.getString(json, DATA)
                showEmotionFragment()
                setOnMenu(MENU_EMOTION2)
            }
            EMOTION2_SEND -> {
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
            }
            EMOTION2_STOP -> {
                setOffMenu(MENU_EMOTION2)
                emotionFragment?.dismiss()
                emotionResultFragment?.dismiss()
                emotionResultFragment = null
                emotionFragment = null
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
            }
            EMOTION2_FINISH -> {
                showEmotionResultFragment()
//                if (!message.isNullOrEmpty()) {
//                    showFirework(message, EffectFragment.TYPE_EFFECT_WIN, false, false)
//                }
            }
        }
    }

    var timeInviteEndMs: Long = 0L
    var inviteGameFragment: InviteGameFragment? = null
    private fun showInviteGame(dataGroup: String?, isShowResult: Boolean = false) {
//        timeGroupEndMs = 15000 + System.currentTimeMillis()
        inviteGameFragment = InviteGameFragment()
        val bundle = Bundle()
        bundle.putLong(TIME, timeInviteEndMs)
        bundle.putString(DATA, dataGroup)
        bundle.putBoolean(RESULT, isShowResult)
        setHeightFragment(bundle)
        inviteGameFragment?.arguments = bundle
        inviteGameFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onShow() {
                showDimer()
            }

            override fun onDismiss() {
                hideDimer()
            }
        }
        showBottomFragment(inviteGameFragment, "INVITE")
    }

    private fun setOnMenu(keyMenu: String) {
        if (itemMenuAdapter == null) {
            return
        }
        val list = itemMenuAdapter!!.itemList
        for (i in 0 until list.size) {
            if (list.get(i).name.equals((keyMenu))) {
                list.get(i).playing = true
                itemMenuAdapter?.notifyDataSetChanged()
                return
            }
        }
    }

    private fun setOffMenu(keyMenu: String) {
        if (itemMenuAdapter == null) {
            return
        }
        val list = itemMenuAdapter!!.itemList
        for (i in 0 until list.size) {
            if (list.get(i).name.equals((keyMenu))) {
                list.get(i).playing = false
                itemMenuAdapter?.notifyDataSetChanged()
                return
            }
        }
    }

    private fun handleNOTIFY(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val message = JsonParserUtil.getString(json, MESSAGE)
        if (!message.isNullOrEmpty()) {
            showDialogAutoClose(message)
        }
    }

    var dialog: Dialog? = null
    fun showDialogAutoClose(msg: String?, timeMs: Long) {
        if (msg.isNullOrEmpty() || !isOnScreen) {
            return
        }
        dialog?.dismiss()
        dialog = GeneralUtils.showTextBubleDialog(activity, msg, timeMs, false, 0)
//        dialog = showDialog(AppUtils.getTextHtml(msg))
//        Handler(Looper.getMainLooper()).postDelayed({ dialog?.dismiss() }, timeMs)
//        onMessageListener?.onShow(msg, null, timeMs)
    }

    fun showDialogAutoClose(msg: String?) {
        showDialogAutoClose(msg, 3000)
    }

    fun showDialog(msg: String?): Dialog? {
        if (msg.isNullOrEmpty() || !isOnScreen) {
            return null
        }
        return AppUtils.showDialog(activity, true, AppUtils.getTextHtml(msg))
    }

    private fun stopService() {
        stopTrackPlayerJoin()
        mSocketMangager?.close()
//        mSocketMangager?.clearSocketListenner(InteractiveFragment::class.java.canonicalName)
        myHttpRequest?.cancel()
        myHttpRequestUser?.cancel()
    }


    private fun runDelay(runable: Runnable, timeDelayMs: Long) {
        Handler(Looper.getMainLooper()).postDelayed(runable, timeDelayMs)
    }

    private fun runHandler(runable: Runnable) {
        Handler(Looper.getMainLooper()).post(runable)
    }
//    val resume = "{\"status\":0,\"errCode\":null,\"service\":\"REGISTER\",\"key\":\"CONNECTED\",\"cmd\":\"243\",\"connection_count\":\"4\",\"from\":{\"ID\":243,\"Index\":0,\"Sender\":null,\"Phone\":\"0902102808\",\"Fullname\":\"Thanh Nguyen\",\"Avatar\":\"https://graph.facebook.com/3144317609002970/picture?type=large\",\"Source\":\"http://mdtv.mediatech.vn/api/v1/oauth/auto?access_token=\",\"Chip\":null,\"Command\":null,\"UpdateTime\":null,\"last_update\":\"0001-01-01T00:00:00\",\"Status\":null,\"FBID\":\"243\",\"FBLink\":\"tl0610@yahoo.com.vn\",\"SyncUrl\":null,\"product\":null,\"BidMoney\":0,\"note\":null,\"message\":null,\"hascode\":\"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9tZHR2Lm1lZGlhdGVjaC52biIsImF1ZCI6Imh0dHA6XC9cL21kdHYubWVkaWF0ZWNoLnZuIiwiaWF0IjoxNjA4Nzc5Njk1LCJuYmYiOjE2MDg3Nzk2ODUsImV4cCI6MTYxMTM3MTY5NSwianRpIjoiWGh3OVlzZ0E3YnBSUHZLd2p2b01PYU1iMDhDa0FPbDBHWVltVDQranIzOD0iLCJkYXRhIjp7ImlkIjoiMjQzIiwiZW1haWwiOiJ0bDA2MTBAeWFob28uY29tLnZuIiwiZnVsbG5hbWUiOiJUaGFuaCBOZ3V5ZW4iLCJwaG9uZSI6IjA5MDIxMDI4MDgiLCJhdmF0YXIiOiJodHRwczpcL1wvZ3JhcGguZmFjZWJvb2suY29tXC8zMTQ0MzE3NjA5MDAyOTcwXC9waWN0dXJlP3R5cGU9bGFyZ2UifX0.L4yrmAE6BM-efq6772b8D9CtkfA5riJR94IRbW6LlQM\",\"result\":null,\"point\":0,\"channel\":\"iStudio\",\"rank_index\":0,\"rank_total\":0,\"attachment\":null,\"Role\":\"user\",\"access_token\":null},\"data\":{\"bid_product\":[{\"id\":-1,\"name\":\"Xịt khoáng Sukin\",\"desc\":\"Sukin l&agrave; d&ograve;ng mỹ phẩm từ thi&ecirc;n nhi&ecirc;n được b&aacute;n chạy nhất tại &Uacute;c năm 2019.\",\"price\":168000,\"price_sale\":240000,\"price_discount\":168000,\"price_bid_startup\":0,\"price_bid_step\":5000,\"price_bid_percent\":50,\"start_discount\":\"2020-12-14\",\"end_discount\":\"2020-12-19\",\"image\":\"\\\\\\\\DEMO-PC\\\\MPI.SHARE\\\\DOWNLOAD\\\\PRODUCT_DATA\\\\1611.Png\",\"image_url\":\"http://daugiatruyenhinh.com/upload/product/1611_kem_duong_da.jpg\",\"status\":0,\"note\":\"free\",\"template_style\":\"\",\"price_bid\":0,\"StartTime\":\"0001-01-01T00:00:00\",\"EndTime\":\"0001-01-01T00:00:00\",\"EndGame_Status\":0,\"Template_Local\":0}],\"iq_quest\":null,\"setting\":[{\"ID\":null,\"Name\":\"Start_Interactive\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":true,\"Msg\":\"Tính năng tương tác đã được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"INTERACTIVE\"},{\"ID\":null,\"Name\":\"Chat\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":true,\"Msg\":\"Tính năng Chat vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"CHAT\"},{\"ID\":null,\"Name\":\"IStudio_Vote\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":false,\"Msg\":\"Tính năng Bình chọn bài hát vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"I-VOTE\"},{\"ID\":null,\"Name\":\"IStudio_Offer\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":false,\"Msg\":\"Tính năng Đề xuất bài hát vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"I-OFFER\"},{\"ID\":null,\"Name\":\"Bid_Free\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":true,\"Msg\":\"Tính năng Đấu giá 1 sản phẩm vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"BID FREE\"},{\"ID\":null,\"Name\":\"Bid_Multil\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":false,\"Msg\":\"Tính năng Đấu giá nhiều sản phẩm vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"BID MULTIL\"},{\"ID\":null,\"Name\":\"IQ\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":true,\"Msg\":\"Tính năng Thử thách IQ vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"IQ\"},{\"ID\":null,\"Name\":\"Emotion\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":false,\"Msg\":\"Tính năng Biểu tượng cảm xúc vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"EMOTION\"},{\"ID\":null,\"Name\":\"Vote\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":true,\"Msg\":\"Tính năng Bình chọn vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"VOTE\"},{\"ID\":null,\"Name\":\"Gift\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":false,\"Msg\":\"Tính năng Tặng quà vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"GIFT\"},{\"ID\":null,\"Name\":\"Voice\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":false,\"Msg\":\"Tính năng Tương tác âm thanh vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"VOICE\"},{\"ID\":null,\"Name\":\"Ads\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":false,\"Msg\":\"Tính năng Quảng cáo vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"ADS\"},{\"ID\":null,\"Name\":\"Video_Call\",\"Icon\":\"https://api.daugiatruyenhinh.com/media/icon.png\",\"Playing\":false,\"Msg\":\"Tính năng Đàm thoại hình ảnh vừa được kích hoạt.\",\"StartTime\":-62135596800,\"Duration\":0,\"Status\":1,\"Change\":false,\"Desc\":\"VIDEO CALL\"}],\"vote\":{\"id\":null,\"name\":null,\"count\":0,\"index\":0,\"max_vote\":0,\"start_time\":\"0001-01-01T00:00:00\",\"stop_time\":\"0001-01-01T00:00:00\",\"vote_data\":null},\"bid_5_product\":null,\"istudio\":{\"playing\":false,\"vote\":[],\"offer\":[],\"vote_title\":\"\",\"offer_title\":\"\"},\"vote2\":null,\"DoanGia\":null},\"message\":null}"
//    val startGame = "{\"status\":1,\"errCode\":\"\",\"service\":\"SPIN\",\"key\":\"SPIN-REQUEST-QUICK\",\"cmd\":\"4\",\"connection_count\":\"3\",\"from\":{\"ID\":-2,\"Index\":0,\"Sender\":null,\"Phone\":null,\"Fullname\":\"Đấu giá truyền hình\",\"Avatar\":\"https://api.daugiatruyenhinh.com/media/daugiatruyenhinh.jpg\",\"Source\":null,\"Chip\":null,\"Command\":null,\"UpdateTime\":null,\"last_update\":\"0001-01-01T00:00:00\",\"Status\":null,\"FBID\":\"PLAYLIVE\",\"FBLink\":null,\"SyncUrl\":null,\"product\":null,\"BidMoney\":0,\"note\":\"[{\\\"id\\\":0,\\\"name\\\":\\\"RANDOM\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\0.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\0.seq2\\\",\\\"message\\\":\\\"random\\\",\\\"status\\\":1},{\\\"id\\\":1,\\\"name\\\":\\\"Tiền mặt 50.000đ\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\1.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\1.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được 50.000đ\\\",\\\"status\\\":1},{\\\"id\\\":2,\\\"name\\\":\\\"May mắn lần sau\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\2.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\2.seq2\\\",\\\"message\\\":\\\"Chúc bạn may mắn lần sau\\\",\\\"status\\\":1},{\\\"id\\\":3,\\\"name\\\":\\\"Voucher giảm giá 30%\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\3.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\3.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được\\\",\\\"status\\\":1},{\\\"id\\\":4,\\\"name\\\":\\\"Voucher miễn phí 1 đồ uống\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\4.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\4.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Voucher miễn phí 1 đồ uống\\\",\\\"status\\\":1},{\\\"id\\\":5,\\\"name\\\":\\\"Tiền mặt 100.000đ\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\5.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\5.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Tiền mặt 100.000đ\\\",\\\"status\\\":1},{\\\"id\\\":6,\\\"name\\\":\\\"Voucher giảm giá 50%\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\6.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\6.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Voucher giảm giá 50%\\\",\\\"status\\\":1},{\\\"id\\\":7,\\\"name\\\":\\\"Tiền mặt 200.000đ\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\7.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\7.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Tiền mặt 200.000đ\\\",\\\"status\\\":1},{\\\"id\\\":8,\\\"name\\\":\\\"Voucher miễn phí 1 đồ uống\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\8.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\8.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Voucher miễn phí 1 đồ uống\\\",\\\"status\\\":1},{\\\"id\\\":9,\\\"name\\\":\\\"Tiền mặt 50.000đ\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\9.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\9.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Tiền mặt 50.000đ\\\",\\\"status\\\":1},{\\\"id\\\":10,\\\"name\\\":\\\"May mắn lần sau\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\10.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\10.seq2\\\",\\\"message\\\":\\\"Chúc bạn May mắn lần sau\\\",\\\"status\\\":1},{\\\"id\\\":11,\\\"name\\\":\\\"Voucher giảm giá 30%\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\11.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\11.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Voucher giảm giá 30%\\\",\\\"status\\\":1},{\\\"id\\\":12,\\\"name\\\":\\\"Voucher miễn phí 1 đồ uống\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\12.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\12.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Voucher miễn phí 1 đồ uống\\\",\\\"status\\\":1},{\\\"id\\\":13,\\\"name\\\":\\\"Tiền mặt 100.000đ\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\13.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\13.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Tiền mặt 100.000đ\\\",\\\"status\\\":1},{\\\"id\\\":14,\\\"name\\\":\\\"Voucher giảm giá 50%\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\14.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\14.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Voucher giảm giá 50%\\\",\\\"status\\\":1},{\\\"id\\\":15,\\\"name\\\":\\\"Tiền mặt 200.000đ\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\15.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\15.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Tiền mặt 200.000đ\\\",\\\"status\\\":1},{\\\"id\\\":16,\\\"name\\\":\\\"Voucher miễn phí 1 đồ uống\\\",\\\"img\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\16.png\\\",\\\"seq\\\":\\\"\\\\\\\\\\\\\\\\DEMO-PC\\\\\\\\MPI.SHARE\\\\\\\\TEMPLTE\\\\\\\\241157\\\\\\\\16.seq2\\\",\\\"message\\\":\\\"Chúc mừng bạn nhận được Voucher miễn phí 1 đồ uống\\\",\\\"status\\\":1}]\",\"message\":null,\"hascode\":null,\"result\":null,\"point\":0,\"channel\":null,\"rank_index\":0,\"rank_total\":0,\"attachment\":null,\"Role\":null,\"access_token\":null},\"data\":{\"maxplayer\":4,\"timecount\":20,\"msg\":\"Nhanh tay bấm ĐỒNG Ý để tham gia quay ngay bây giờ!\",\"type\":\"QUICK\"},\"message\":\"Nhanh tay bấm ĐỒNG Ý để tham gia quay ngay bây giờ!\"}"

    var effectFragment: EffectFragment? = null

    private fun showFirework(
        msg: String?,
        type: Int,
        isUpdateGitCount: Boolean,
        isShowReplace: Boolean = true
    ) {
        if (isUpdateGitCount) {
            try {
                showGitCount()
            } catch (e: Exception) {
            }
        }
        if (!isOnScreen) {
            return
        }
        val text = if (msg.isNullOrEmpty()) "" else msg
        try {
            effectFragment?.dismiss()
        } catch (e: Exception) {
        }
        effectFragment = EffectFragment()
        val bundle = Bundle()
        val height =
            layoutRoot.height + resources.getDimensionPixelSize(R.dimen.dimen_56) - layoutHeader.height
        Loggers.e("FireWorkFragment", "Height" + height)
        bundle.putInt(HEIGHT, height)
        bundle.putString(MESSAGE, text)
        bundle.putInt(TYPE, type)
        effectFragment?.arguments = bundle
        showBottomFragment(effectFragment, "Effect", isShowReplace)
//        fireWorkFragment?.onShowDismissListener = object : OnShowDismissListener {
//            override fun onShow() {
//                showDimer()
//            }
//
//            override fun onDismiss() {
//                hideDimer()
//            }
//        }
    }

    var countGit = 0
    private fun showGitCount() {
        countGit++
        textGit?.text = countGit.toString()
        textGit?.visibility = View.VISIBLE
        textGitNotify?.text = countGit.toString()
        textGitNotify?.visibility = View.GONE
    }

    private fun resetAndHideGitCount() {
        countGit = 0
        textGit?.visibility = View.GONE
        textGit?.text = countGit.toString()
        textGitNotify?.visibility = View.GONE
        textGitNotify?.text = countGit.toString()
    }

    private fun showDimer() {
        try {
            viewDimmer?.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    private fun hideDimer() {
        try {
            viewDimmer?.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    var isFirtResume = true
    override fun onResume() {
        super.onResume()
        isOnScreen = true
        if (!isFirtResume) {
            Loggers.e("INTERACTIVE", "onResume")
            if (mSocketMangager == null || (mSocketMangager != null && !mSocketMangager!!.isConnecting) || !port.equals(
                    mSocketMangager!!.url
                )
            ) {
                try {
                    activity?.runOnUiThread {
                        Loggers.e("Socket","initSocket" + "onResume InteractiveFragment")
                        initSocket()
                    }
                } catch (e: Exception) {
                }
            }
            startTrackPlayerJoin()
        }
        isFirtResume = false
    }

    override fun onStop() {
        if (hasSignOut) {
            onClickListenner?.onClickSignOut()
        }
        try {
            popupFragment?.dismiss()
            dialog?.dismiss()
        } catch (e: Exception) {
        }
        super.onStop()
    }

    override fun onDestroyView() {
        stopService()
        super.onDestroyView()
    }

    interface OnClickListenner {
        fun onClickSignOut()
        fun onClickMyVoucher()
        fun onClickShareFacebook()
        fun onClickSetUp()
        fun onClickGroup()
        fun onClickConfirm()
        fun onConnectSocket(isConnect: Boolean)
        fun onClickMessenger()
        fun onClickToggle()
        fun onClickViews()
    }

    private fun showBottomFragment(
        fragment: BottomSheetDialogFragment?,
        tag: String,
        isShowReplace: Boolean = true
    ) {
        if (fragment == null || !isOnScreen || isDetached) {
            return
        }
            dialog?.dismiss()
        if (isShowReplace) {
            try {
                    popupFragment?.dismiss()
            } catch (e: Exception) {
            }
            popupFragment = fragment
        }
        try {
            val ft = childFragmentManager!!.beginTransaction()
            ft.add(fragment, tag)
            runHandler { ft.commitAllowingStateLoss() }
        } catch (e: Exception) {
        }
    }

    var isOnScreen: Boolean = false
    override fun onPause() {
        super.onPause()
        stopTrackPlayerJoin()
        Loggers.e("Lifecycle_Fragment", "onPause")
        isOnScreen = false
    }

    var itemGuideList: ArrayList<ItemGuide> = ArrayList()
    fun getGuideData() {
        if (isDetached || context == null) {
            return
        }
        if (!AppUtils.isNetworkConnect(context)) {
            return
        }
        var httpRequest: MyHttpRequest? = null
        httpRequest = MyHttpRequest(requireContext())
        val requestParams = RequestParams()
        requestParams.put("appid", appID)
//        requestParams.put("content_type", "${itemObj!!.contentType}")
//        requestParams.put("style", "3")

        httpRequest?.request(
            false,
            ConstantTT.API_GET_GUIDE_PLAY_GAME,
            requestParams,
            object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    if (isDetached) {
                        return
                    }
                    var response = responseString!!.trim { it <= ' ' }
                    val jsonObject = JsonParserUtil.getJsonObject(response)
                    if (jsonObject == null) {
                        return
                    }

                    val errorCode = JsonParserUtil.getInt(jsonObject, ConstantTT.CODE)
                    val message = JsonParserUtil.getString(jsonObject, ConstantTT.MESSAGE)
                    if (errorCode != ConstantTT.SUCCESS) {
                        return
                    }
                    val gson = Gson()
                    val result = JsonParserUtil.getString(jsonObject, RESULT)
                    val type = object : TypeToken<ArrayList<ItemGuide>>() {}.getType()
                    val itemList: ArrayList<ItemGuide>? = try {
                        gson.fromJson(result, type)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        null
                    }
                    itemGuideList.clear()
                    itemGuideList.addAll(itemList!!)
//                itemGuideList.addAll(itemList!!)
//                itemGuideList.addAll(itemList!!)
//                itemGuideList.addAll(itemList!!)
//                itemGuideList.addAll(itemList!!)
//                itemGuideList.addAll(itemList!!)
//                for (i in 0 until itemGuideList.size) {
//                    itemGuideList.get(i).title = "Game Dau gia " + i
//                }
                    if (!isDetached) {
                        activity?.runOnUiThread {
                            showGuide()
                        }
                    }
                }
            })
    }

    var currentPosGuide = 0
    fun showGuide() {
        if (itemGuideList.size == 0) {
            getGuideData()
            return
        }
        var guideFragment = GuideFragment()
        val bundle = Bundle()
        bundle.putParcelableArrayList(DATA, itemGuideList)
        bundle.putInt(ConstantTT.POSITION, currentPosGuide)
        setHeightFragment(bundle)
        guideFragment.arguments = bundle
        guideFragment.onClickListener = object : GuideFragment.OnClickListenner {
            override fun onSelectPage(item: ItemGuide?, position: Int) {
                currentPosGuide = position
            }
        }
        showBottomFragment(guideFragment, "GUIDE")
    }

    private fun setHeightFragment(bundle: Bundle, height: Int? = null) {
        if (height != null && height > 0) {
            bundle.putInt(HEIGHT, height)
            return
        }
        val mHeight =
            layoutRoot.height + AppUtils.convertDpToPixel(
                Constant.HEIGHT_TAB_BAR_BOTTOM_DP.toFloat(),
                requireContext()
            ).toInt() - layoutHeader.height
        bundle.putInt(HEIGHT, mHeight)
    }

    fun showButtonShareFacebook(isShow: Boolean) {
        activity?.runOnUiThread {
            buttonShareFacebook?.visibility = if (isShow) View.VISIBLE else View.GONE
        }
    }

    fun closeSocket(closeListener:SocketMangager.CloseSocketListenner? = null) {
        mSocketMangager?.close(closeListener)
    }

    fun initHomeLayout(jsonObj: JSONObject?) {
        var type = JsonParser.getInt(jsonObj, "type")
        var cardType = JsonParser.getString(jsonObj, "card_type")
        val dataArray = JsonParser.getJsonArray(jsonObj, Constant.DATA)
        if (activity == null || cardType == null) {
            return
        }
        if (type == 0) {
            type = Constant.STYLE_HORIZONTAL
        }

        if (cardType.equals(Constant.CARD_TYPE_BANNER)) {
            return
        }
        val title = JsonParser.getString(jsonObj, "name")
        val layoutInflater: LayoutInflater = activity?.getSystemService(
            Context
                .LAYOUT_INFLATER_SERVICE
        ) as LayoutInflater
        val layout: View = layoutInflater.inflate(
            R.layout.layout_header_recycler_view_tt,
            layoutHomeContent, false
        )
        if (!title.isNullOrEmpty()) {
            layout.textTitle.text = title
            layout.textTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            layout.layoutCategoryHeader.visibility = View.VISIBLE
        }

        val recyclerView = layout.recyclerView
        recyclerView.setHasFixedSize(true)
        var numberColumn = 1
        if (type == Constant.STYLE_GRIDVIEW) {
            numberColumn = 2
        }
        var layoutManager: StaggeredGridLayoutManager? = null
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        var itemDecoration: RecyclerView.ItemDecoration? = null
        var isShowButtonInfo = true
        var style = Constant.STYLE_HORIZONTAL
        itemDecoration = SpacesItemDecoration(spacePx, numberColumn, true)
        layoutManager =
            StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.HORIZONTAL)
        when (cardType) {
            Constant.CARD_TYPE_NEWS, Constant.CARD_TYPE_VIDEO -> {
                val itemList =
                    JsonParser.parseItemNewsList(requireActivity(), dataArray)
                if (itemList == null || itemList.size == 0) {
                    return
                }
                for (item in itemList) {
                    if (cardType.equals(Constant.CARD_TYPE_VIDEO)) {
                        item.contentType = Constant.TYPE_VIDEO
                    } else if (cardType.equals(Constant.CARD_TYPE_NEWS)) {
                        item.contentType = Constant.TYPE_NEWS
                    }
                }
                if (cardType.equals(Constant.CARD_TYPE_NEWS)) {
                    type = Constant.STYLE_LISTVIEW
                }
                when (type) {
                    Constant.STYLE_HORIZONTAL -> {
                        style = type
                        itemDecoration = SpacesItemDecoration(spacePx, numberColumn, true)
                        layoutManager = StaggeredGridLayoutManager(
                            numberColumn,
                            StaggeredGridLayoutManager.HORIZONTAL
                        )
                    }

                    Constant.STYLE_LISTVIEW -> {
                        style = type
                        itemDecoration = SpacesItemDecoration(spacePx, numberColumn, false)
                        layoutManager = StaggeredGridLayoutManager(
                            numberColumn,
                            StaggeredGridLayoutManager.VERTICAL
                        )
                    }

                    Constant.STYLE_LISTVIEW_BOX -> {
                        style = type
                        itemDecoration = SpacesItemDecoration(spacePx, numberColumn, false)
                        layoutManager = StaggeredGridLayoutManager(
                            numberColumn,
                            StaggeredGridLayoutManager.VERTICAL
                        )
                    }

                    Constant.STYLE_GRIDVIEW -> {
                        style = type
                        itemDecoration =
                            SpacesItemDecorationGridMultipleViewType(spacePx, numberColumn)
                        layoutManager = StaggeredGridLayoutManager(
                            numberColumn,
                            StaggeredGridLayoutManager.VERTICAL
                        )
                    }
                }
                isShowButtonInfo = false
                val adapter = NewsAdapter(requireActivity(), itemList, style, numberColumn)
                adapter.isBlackTheme = true
                adapter.isShowButtonInfo = isShowButtonInfo
                recyclerView.addItemDecoration(itemDecoration!!)
                activity?.runOnUiThread {
                    recyclerView.layoutManager = layoutManager
                    recyclerView.adapter = adapter
                    recyclerView.scheduleLayoutAnimation()
                    adapter.onItemClickListener = object : NewsAdapter.OnItemClickListener {
                        override fun onClick(
                            itemObject: ItemNews,
                            position: Int,
                            holder: RecyclerView.ViewHolder
                        ) {
                            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                                GeneralUtils.goDetailNewsActivity(activity, itemObject)
                            }, 200)
                        }

                        override fun onLongClick(itemObject: ItemNews, position: Int, view: View) {
                        }

                        override fun onCommentClick(itemObject: ItemNews, position: Int) {
                        }

                        override fun onShareClick(itemObject: ItemNews, position: Int) {
                            (activity as BaseActivity?)!!.shareApp(itemObject)
                        }

                        override fun onOrderClick(
                            itemObject: ItemNews,
                            position: Int,
                            holder: RecyclerView.ViewHolder
                        ) {
                            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                                (activity as? BaseActivity)?.goDetailProductActivity(
                                    itemObject,
                                    holder
                                )
                            }, 200)
                        }
                    }
                    layoutHomeContent.addView(layout)
                }
                return
            }
            Constant.CARD_TYPE_TOP_USER -> {
                val result = JsonParser.getString(jsonObj, Constant.DATA)
                val type = object : TypeToken<ArrayList<ItemWiner>>() {}.getType()
                val gson = Gson()
                val itemWinnerList: ArrayList<ItemWiner>? = try {
                    gson.fromJson(result, type)
                } catch (e: Exception) {
                    null
                }
                if (itemWinnerList == null || itemWinnerList.size == 0) {
                    return
                }
                val itemWinnerAdapter =
                    ItemWinnerAdapter(requireActivity(), itemWinnerList, 1, 1, 5)

                itemWinnerAdapter.isBlackTheme = true
                val layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                recyclerView?.layoutManager = layoutManager
                recyclerView?.adapter = itemWinnerAdapter
                layoutHomeContent.addView(layout, 0)
                itemWinnerAdapter.onItemClickListener =
                    object : ItemWinnerAdapter.OnItemClickListener {
                        override fun onClick(itemObject: ItemWiner, position: Int) {
                            val fragment = ImageZoomPagerDialog()
                            val list = ArrayList<ItemPhoto>()
                            list.add(
                                ItemPhoto(
                                    itemObject.fullName,
                                    itemObject.avatar,
                                    itemObject.showDate
                                )
                            )
                            val bundle = Bundle()
                            bundle.putParcelableArrayList(Constant.DATA, list)
                            fragment.arguments = bundle
                            fragment.show(childFragmentManager, "ImageZoom")
                        }
                    }
                return
            }
        }
    }

    var isShowNews: Boolean? = null
    fun showNews(isShowNews: Boolean) {
        if (this.isShowNews != null && this.isShowNews!!.equals(isShowNews)) {
            return
        }
        this.isShowNews = isShowNews
        if (isShowNews) {
            closeSocket()
            layoutNotify?.visibility = View.VISIBLE
            nestedScrollView?.visibility = View.VISIBLE
        } else {
            initSocket()
            nestedScrollView?.visibility = View.GONE
        }
    }

    fun removeNewsView() {
        layoutHomeContent?.removeAllViews()
    }

    private fun handleWEBGAME(json: JSONObject) {
        val key = JsonParserUtil.getString(json, KEY)
        val from = JsonParserUtil.getJsonObject(json, FROM)
        val message = JsonParserUtil.getString(json, MESSAGE)
        val data = JsonParserUtil.getString(json, DATA)
        when (key) {
            WEBGAME_START -> {
                dataWebGame = data
                Handler(Looper.getMainLooper()).postDelayed({
                    startWebgameFragment(dataWebGame)
                }, 100)
            }
            WEBGAME_FINISH -> {
                setOffMenu(MENU_WEBGAME)
                webGameFragment?.dismiss()
                webGameFragment = null
                dataWebGame = null
                showWebGameResultFragment(data)
                showFirework(message, EffectFragment.TYPE_EFFECT_WIN, false, false)
            }
            WEBGAME_STOP -> {
                webGameResultFragment?.dismiss()
                webGameResultFragment = null
                if (!message.isNullOrEmpty()) {
                    showDialogAutoClose(message)
                }
            }
        }
    }

    var dataWebGame: String? = null
    private fun startWebgameFragment(data: String?) {
        if (data.isNullOrEmpty()) {
            return
        }
        val jsonData = JsonParserUtil.getJsonObject(data)
        val timeEndMs =
            AppUtils.getTimeInMilliSeconds(JsonParserUtil.getString(jsonData, "StopGame"))
        val url = JsonParserUtil.getString(jsonData, "url")
        val name = JsonParserUtil.getString(jsonData, "Name")
        showWebGame(url, name)
        val timeLeft = (timeEndMs - System.currentTimeMillis()) / 1000
        setOnMenu(MENU_WEBGAME)
        webGameFragment?.startCountDown(timeLeft)
    }

    var webGameFragment: WebGameFragment? = null
    fun showWebGame(urlGame: String? = null, game_name: String? = null) {
        if(urlGame == null || !urlGame.startsWith("http",true)){
            return
        }
//        if(webGameFragment != null){
//            (activity as? FragmentActivity)?.supportFragmentManager?.beginTransaction()?.remove(webGameFragment!!)?.commit()
//            webGameFragment = null
//            return
//        }
        webGameFragment = WebGameFragment()
        val bundle = Bundle()
//        val height = ScreenSize(requireActivity()).height - AppUtils.convertDpToPixel(
//            Constant.HEIGHT_TAB_BAR_TOP_DP.toFloat(),
//            requireContext()
//        ).toInt()
//        setHeightFragment(bundle,height)
        setHeightFragment(bundle)
        bundle.putString(DATA, urlGame)
        bundle.putString(NAME, game_name)
        webGameFragment?.arguments = bundle
        startTrackPlayerJoin(20000)
        webGameFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onDismiss() {
                startTrackPlayerJoin()
            }
        }
//        (activity as? FragmentActivity)?.supportFragmentManager?.beginTransaction()?.replace(R.id.layoutDialog,webGameFragment!!)?.commit()
        showBottomFragment(webGameFragment, "WebGame")
    }

    var webGameResultFragment: WebGameResultFragment? = null
    fun showWebGameResultFragment(data: String? = null) {
        webGameResultFragment = WebGameResultFragment()
        val bundle = Bundle()
//        val height = ScreenSize(requireActivity()).height - AppUtils.convertDpToPixel(
//            Constant.HEIGHT_TAB_BAR_TOP_DP.toFloat(),
//            requireContext()
//        ).toInt()
//        setHeightFragment(bundle,height)
        setHeightFragment(bundle)
        bundle.putString(DATA, data)
        webGameResultFragment?.arguments = bundle
        var isShowReplace = true
        if (webGameFragment != null && webGameFragment!!.isVisible) {
            isShowReplace = false
        }
        showBottomFragment(webGameResultFragment, "WebGame", isShowReplace)
    }
}
package vn.mediatech.interactive.iQ

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_iq.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class IQFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var itemObj: ItemIQ? = null
    var isSuccess = false
    var bottomSheetDialog: BottomSheetDialog? = null
    var onClickListener: ItemIQAnswerAdapter.OnItemClickListener? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_iq, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(ConstantTT.DATA, itemObj)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()

    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        val height = bundle?.getInt(ConstantTT.HEIGHT)
        try {
            if (height != null && height > 0) {
                val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
                param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                param?.height = height
            }
        } catch (e: Exception) {
        }
        try {
            itemObj = bundle?.getParcelable(ConstantTT.DATA)!!
        } catch (e: Exception) {
            dismiss()
        }
        if (itemObj == null) {
            dismiss()
            return
        }
        updateUI(itemObj!!)
    }

    var adapter: ItemIQAnswerAdapter? = null
    fun updateUI(item: ItemIQ?) {
        this.itemObj = item
        if (item == null) {
            return
        }
        textTitle.text = "Câu hỏi số " + item!!.index
        textQuestion.text = item.nameQuestion
        //voiceController
//        VoiceControlFragment.showFragment(activity,item.nameQuestion,object : VoiceControllerManager.OnResultListener {
//            override fun onPartialResults(str: String?) {
//            }
//
//            override fun onResult(str: String?) {
//            }
//
//            override fun onRmsChanged(p0: Float) {
//            }
//        })
        val list: ArrayList<ItemIQAnswer> = ArrayList()
        val gson = Gson()
        var itemIQResult = gson.fromJson(item.dataResult, ItemIQResult::class.java)
        if (itemIQResult == null) {
            itemIQResult = ItemIQResult(null, null, null, null, null, null, null, null, null)
        }
        list.add(ItemIQAnswer("A", item.replyA, item.imgreplyA, itemIQResult.A, itemIQResult.perA))
        list.add(ItemIQAnswer("B", item.replyB, item.imgreplyB, itemIQResult.B, itemIQResult.perB))
        list.add(ItemIQAnswer("C", item.replyC, item.imgreplyC, itemIQResult.C, itemIQResult.perC))
        list.add(ItemIQAnswer("D", item.replyD, item.imgreplyD, itemIQResult.D, itemIQResult.perD))

        adapter = ItemIQAnswerAdapter(requireActivity(), list, 1, 1, 1)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView?.layoutManager = layoutManager
        recyclerView?.adapter = adapter
        if (!itemIQResult.chooseReply.isNullOrEmpty()) {
            for (i in 0 until list.size) {
                if (list.get(i).title.equals(itemIQResult.chooseReply)) {
                    adapter?.indexTrue = i
                    break
                }
            }
        }
        adapter?.setSelected(item.indexSelected)
        adapter?.startEffectTrue()
        adapter?.onItemClickListener = object : ItemIQAnswerAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemIQAnswer?, position: Int, isTimeOut: Boolean) {
                var isTimeOutIQ = if (timeLeft < 0) true else false
                if (!isTimeOutIQ) {
                    if (adapter!!.indexSelect == -1) {
                        adapter!!.indexSelect = position
                        adapter!!.notifyDataSetChanged()
                    }
                }
                onClickListener?.onClick(itemObject, position, isTimeOutIQ)
            }
        }
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        timer?.cancel()
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

    var timer: Timer? = null
    fun startCountUp(timeStartSecond: Long) {
        var timeCount = timeStartSecond
        timer?.cancel()
        timer = Timer()
        timer!!.schedule(timerTask {
            activity?.runOnUiThread {
                textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(timeCount)
                timeCount = timeCount + 1
            }
        }, 0, 1000)
    }

    var timeLeft: Long = 0
    fun startCountDown(timeSeconds: Long) {
        timer?.cancel()
        var timeCount = timeSeconds
        timeLeft = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            timeLeft = timeCount
            if (timeCount >= 0) {
                activity?.runOnUiThread {
                    val timeShow = if (timeCount >= 0) timeCount else 0
                    textTimeCountDown?.text = timeShow.toString()
                    if (textTimeCountDown?.visibility != View.VISIBLE) {
                        textTimeCountDown.visibility = View.VISIBLE
                    }
                    if (progressTime?.visibility != View.VISIBLE) {
                        progressTime.visibility = View.VISIBLE
                    }
                    val progress = ((timeShow / timeSeconds.toFloat()) * 100)
                    if (progress >= 0 && progress <= 100) {
                        progressTime.progress = progress.toInt()
                    }
                    if (timeShow <= 10) {
                        textTimeCountDown?.setTextColor(Color.RED)
                    } else {
                        textTimeCountDown?.setTextColor(Color.WHITE)
                    }
                    timeCount = timeCount - 1
                }
            } else {
                timer?.cancel()
                activity?.runOnUiThread {
                textTimeCountDown?.text = "0"
                textTimeCountDown.visibility = View.VISIBLE
                progressTime.progress = 0
                progressTime.visibility = View.VISIBLE}
            }
        }, 0, 1000)
    }

    private fun setupBackPressListener() {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                activity?.finish()
                true
            } else
                false
        }
    }


}
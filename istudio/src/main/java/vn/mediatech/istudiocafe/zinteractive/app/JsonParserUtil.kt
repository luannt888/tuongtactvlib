package vn.mediatech.interactive.app

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class JsonParserUtil {
    companion object {
        fun getString(jObject: JSONObject?, param: String): String? {
            if (!existParam(jObject, param)) {
                return null
            }
            try {
                return jObject?.getString(param)
            } catch (e: JSONException) {
                e.printStackTrace()
                return null
            }
        }

        fun getInt(jObject: JSONObject?, param: String): Int? {
            if (!existParam(jObject, param)) {
                return 0
            }
            try {
                return jObject?.getInt(param)
            } catch (e: JSONException) {
                e.printStackTrace()
                return 0
            }
        }

        fun getLong(jObject: JSONObject?, param: String): Long? {
            if (!existParam(jObject, param)) {
                return 0
            }
            try {
                return jObject?.getLong(param)
            } catch (e: JSONException) {
                e.printStackTrace()
                return 0
            }
        }

        fun getBoolean(jObject: JSONObject?, param: String): Boolean? {
            if (!existParam(jObject, param)) {
                return false
            }
            try {
                return jObject?.getBoolean(param)
            } catch (e: JSONException) {
                e.printStackTrace()
                return false
            }
        }

        fun getJsonObject(jObject: JSONObject?, param: String): JSONObject? {
            if (!existParam(jObject, param)) {
                return null
            }
            try {
                return jObject?.getJSONObject(param)
            } catch (e: JSONException) {
                e.printStackTrace()
                return null
            }
        }

        fun getJsonObject(json: String?): JSONObject? {
            if (json.isNullOrEmpty()) {
                return null
            }
            try {
                return JSONObject(json)
            } catch (e: JSONException) {
                e.printStackTrace()
                return null
            }
        }

        fun getJsonArray(jObject: JSONObject?, param: String?): JSONArray? {
            if (!existParam(jObject, param)) {
                return null
            }
            try {
                return jObject?.getJSONArray(param)
            } catch (e: JSONException) {
                e.printStackTrace()
                return null
            }
        }

        fun getJsonArray(json: String?): JSONArray? {
            try {
                return JSONArray(json)
            } catch (e: JSONException) {
                e.printStackTrace()
                return null
            }
        }

         fun existParam(jObject: JSONObject?, param: String?): Boolean {
            if (jObject == null || param == null) {
                return false
            }
            return if (!jObject.has(param)) {
                false
            } else !jObject.isNull(param)
        }
    }
}
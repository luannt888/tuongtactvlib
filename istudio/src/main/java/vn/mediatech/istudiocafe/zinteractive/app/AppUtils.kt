package vn.mediatech.interactive.app

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.util.DisplayMetrics
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.drawToBitmap
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Loggers
import java.io.File
import java.io.FileOutputStream
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class AppUtils {
    companion object {
        fun isEmpty(data: String?): Boolean {
            return data == null || data.trim { it <= ' ' }.isEmpty()
        }

        fun isNetworkConnect(context: Context?): Boolean {
            if (context == null) {
                return false
            }
            val connMgr = context.applicationContext
                .getSystemService(Activity.CONNECTIVITY_SERVICE) as ConnectivityManager
                ?: return true
            val networkInfo = connMgr.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }

        fun loadImageCircle(
            context: Context?, imageView: ImageView?, url: String?,
            placeholderDrawableId: Int
        ) {
            if (context == null || imageView == null || url.isNullOrEmpty()) {
                return
            }
            val options = if (placeholderDrawableId <= 0) {
                RequestOptions().transform(CircleCrop())
                    .priority(Priority.NORMAL)
            } else {
                RequestOptions().transform(CircleCrop())
                    .placeholder(placeholderDrawableId)
                    .error(placeholderDrawableId)
                    .priority(Priority.NORMAL)
            }
            Glide.with(context).load(url).apply(options).into(imageView!!)
        }

        fun loadImageCircleClearCache(
            context: Context?, imageView: ImageView?, url: String?,
            placeholderDrawableId: Int
        ) {
            val options: RequestOptions
            options = if (placeholderDrawableId <= 0) {
                RequestOptions().transform(CircleCrop())
                    .priority(Priority.NORMAL)
            } else {
                RequestOptions().transform(CircleCrop())
                    .placeholder(placeholderDrawableId)
                    .error(placeholderDrawableId)
                    .priority(Priority.NORMAL).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
            }
            Glide.with(context!!).load(url).apply(options).into(imageView!!)
        }

        fun loadImage(context: Context?, imageView: ImageView?, url: String?) {
            if (imageView == null || isEmpty(url) || context == null) {
                return
            }
            val options: RequestOptions = RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
            Glide.with(context).load(url).apply(options).into(imageView)
        }

        fun loadImage(context: Context?, imageView: ImageView?, res: Int) {
            if (imageView == null || res == 0 || context == null) {
                return
            }
            val options: RequestOptions = RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
            Glide.with(context).load(res).apply(options).into(imageView)
        }

        fun getColoredString(mString: String?, colorId: Int): Spannable {
            val spannable: Spannable = SpannableString(mString)
            spannable.setSpan(
                ForegroundColorSpan(colorId),
                0,
                spannable.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return spannable
        }

        fun convertDpToPixel(dp: Float, context: Context): Float {
            return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }

        fun getStringHtmlColor(mString: String?, hexColor: String): String? {
            if (hexColor.isNullOrEmpty() || mString.isNullOrEmpty() || !hexColor.startsWith("#")) {
                return mString
            }
//            var string = "<span style=\"color: ${hexColor}\">${mString}</span>"
            var string = "<span style=\"color: ${hexColor}\">${mString}</span>"
            Loggers.e("HTML Color", string)
            return string
        }

        fun getColoredSpannable(spannable: Spannable, colorId: Int): Spannable {
            val mSpannable: Spannable = spannable
            mSpannable.setSpan(
                ForegroundColorSpan(colorId),
                0,
                mSpannable.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return mSpannable
        }

        fun getDateTime(strDate: String?): Date {
//        val strDate = "2013-05-15T10:00:00-0700"
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
            val dateFormat2 = SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.ENGLISH)
            val date = try {
                dateFormat.parse(strDate)
            } catch (e: Exception) {
                try {
                    dateFormat2.parse(strDate)
                } catch (e: Exception) {
                    Date()
                }
            }
            return date
        }

        fun getTimeInMilliSeconds(strDate: String?): Long {
            try {
                if (strDate.isNullOrEmpty()) {
                    return -1L
                }
//        val strDate = "2013-05-15T10:00:00-0700"
                val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
                return dateFormat.parse(strDate).time
            } catch (e: Exception) {
                e.printStackTrace()
                return 0L
            }
        }

        fun getDateString(context: Context, strDate: String?): String {
            try {
                val date = getDateTime(strDate)
                val currentDate = Date()
                val dateStr = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(date)
                val dateCurrent = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(currentDate)
                return if (dateStr.equals(dateCurrent)) context.getString(R.string.today) else dateStr
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
        }

        fun getCurrentDateStringFormatServer(): String {
            val currentDate = Date()
            val dateCurrent =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).format(currentDate)
            return dateCurrent
        }

        fun getDateStringFormatServer(milliseconds: Long): String {
            val currentDate = Date(milliseconds)
            val dateCurrent =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).format(currentDate)
            return dateCurrent
        }

        fun getTimeString(strDate: String?): String {
            try {
                val date = getDateTime(strDate)
                return SimpleDateFormat("HH:mm", Locale.ENGLISH).format(date)
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
        }

        fun getTextHtml(str: String?): String {
            try {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    return Html.fromHtml(str, Html.FROM_HTML_MODE_LEGACY).toString()
                } else {
                    return Html.fromHtml(str).toString()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
        }

        fun moneyFormat(amount: String?): String {
            if (amount.isNullOrEmpty()) {
                return "0 Đ"
            }
            val formatter = DecimalFormat("###,###,###")
            return try {
                formatter.format(amount.toDouble()) + " Đ"
            } catch (e: Exception) {
                e.printStackTrace()
                "0 Đ"
            }
        }

        fun getRandomString(arr: Array<String>?): String? {
            if (arr == null || arr.size == 0) {
                return null
            }
            val list = arr.toCollection(ArrayList())
            Collections.shuffle(list)
            return list.get(0)
        }

        fun changeColorHTMLString(string: String?, hexColorString: String?): String {
            if (string.isNullOrEmpty()) {
                return ""
            }

//            val str = "&lt;font color='$hexColorString'&gt;" + string + "&lt;/font&gt;"
            val str = "<font color='$hexColorString'>" + string + "</font>"
            return str
        }

        fun getHtmlFormat(text: String?): Spanned? {
            val data = text ?: ""
            val textSpanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(data, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(data)
            }
            return textSpanned
        }

        fun setHtmlTextView(text: String?, textView: TextView?) {
            if (text.isNullOrEmpty() || textView == null) {
                return
            }
            textView.text = getHtmlFormat(text + "")
        }


        fun convertSecondsToMmSs(seconds: Long): String {
            val s = seconds % 60
            val m = seconds / 60 % 60
            return String.format("%02d:%02d", m, s)
        }

        fun isShowKeyboard(context: Context): Boolean {
            val imm by lazy { context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager }
            val windowHeightMethod =
                InputMethodManager::class.java.getMethod("getInputMethodWindowVisibleHeight")
            val height = windowHeightMethod.invoke(imm) as Int
            return height > 0
        }


        fun showDialog(
            activity: Activity?,
            cancelable: Boolean,
            cancelableOutside: Boolean,
            title: String?,
            message: String?,
            leftButtonTitle: String?,
            rightButtonTitle: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ): Dialog? {
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return null
            }
            val dialog = Dialog(activity)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val lp = WindowManager.LayoutParams()
            //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER
            lp.windowAnimations = R.style.DialogAnimationLeftRight
            dialog.window?.setAttributes(lp)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(cancelable)
            dialog.setCanceledOnTouchOutside(cancelableOutside)
            dialog.setContentView(R.layout.layout_dialog_module)
            val screenSize = ScreenSize(activity)
            dialog.window?.setLayout(
                screenSize.width * 9 / 10,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
            val textTitle: TextView = dialog.findViewById(R.id.textTitle)
            val textMessage: TextView = dialog.findViewById(R.id.textMessage)
            val buttonLeft: TextView = dialog.findViewById(R.id.buttonLeft)
            val buttonRight: TextView = dialog.findViewById(R.id.buttonRight)
            //        View viewDivider = dialog.findViewById(R.id.viewDivider);
            layoutRoot.clipToOutline = true
            layoutRoot.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View, outline: Outline) {
                    val cornerRadiusDP = 15f
                    outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
                }
            }
            if (!title.isNullOrEmpty()) {
                textTitle.text = title
            }
            if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
                setHtmlTextView(message, textMessage)
                textMessage.movementMethod = LinkMovementMethod.getInstance()
            }
            if (leftButtonTitle.isNullOrEmpty()) {
                buttonLeft.visibility = View.GONE
                //            viewDivider.setVisibility(View.GONE);
            } else {
                buttonLeft.text = leftButtonTitle
                buttonLeft.visibility = View.VISIBLE
            }
            if (rightButtonTitle.isNullOrEmpty()) {
                buttonRight.visibility = View.GONE
                //            viewDivider.setVisibility(View.GONE);
            } else {
                buttonRight.text = rightButtonTitle
                buttonRight.visibility = View.VISIBLE
            }
            buttonLeft.setOnClickListener {
                dialog.dismiss()
                if (onDialogButtonListener != null) {
                    onDialogButtonListener.onLeftButtonClick()
                }
            }
            buttonRight.setOnClickListener {
                dialog.dismiss()
                if (onDialogButtonListener != null) {
                    onDialogButtonListener.onRightButtonClick()
                }
            }
            try {
                dialog?.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return dialog
        }

        fun showDialog(activity: Activity?, cancelable: Boolean, msg: String): Dialog? {
            return showDialog(activity, cancelable, cancelable, null, msg, null, null, null)
        }


        fun showDialogBidMulti(
            activity: Activity?, message: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ): Dialog? {
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return null
            }
            val dialog = Dialog(activity)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val lp = WindowManager.LayoutParams()
            //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER
            lp.windowAnimations = R.style.DialogAnimationLeftRight
            dialog.window?.setAttributes(lp)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.layout_dialog_bid_multi)
            val screenSize = ScreenSize(activity)
            dialog.window?.setLayout(
                screenSize.width * 9 / 10,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
            val textMessage: TextView = dialog.findViewById(R.id.textMessage)
            val buttonLeft: TextView = dialog.findViewById(R.id.buttonLeft)
            val buttonRight: TextView = dialog.findViewById(R.id.buttonRight)
            //        View viewDivider = dialog.findViewById(R.id.viewDivider);
            layoutRoot.clipToOutline = true
            layoutRoot.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View, outline: Outline) {
                    val cornerRadiusDP = 15f
                    outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
                }
            }
            if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
                setHtmlTextView(message, textMessage)
                textMessage.movementMethod = LinkMovementMethod.getInstance()
            }
//            if (leftButtonTitle.isNullOrEmpty()) {
//                buttonLeft.visibility = View.GONE
//                //            viewDivider.setVisibility(View.GONE);
//            } else {
//                buttonLeft.text = leftButtonTitle
//                buttonLeft.visibility = View.VISIBLE
//            }
//            if (rightButtonTitle.isNullOrEmpty()) {
//                buttonRight.visibility = View.GONE
//                //            viewDivider.setVisibility(View.GONE);
//            } else {
//                buttonRight.text = rightButtonTitle
//                buttonRight.visibility = View.VISIBLE
//            }
            buttonLeft.setOnClickListener {
                dialog.dismiss()
                if (onDialogButtonListener != null) {
                    onDialogButtonListener.onLeftButtonClick()
                }
            }
            buttonRight.setOnClickListener {
                dialog.dismiss()
                if (onDialogButtonListener != null) {
                    onDialogButtonListener.onRightButtonClick()
                }
            }
            try {
                dialog.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return dialog
        }

        fun createPictureFilePath(context: Context, name: String? = null): String {
//            val appName = activity.getString(R.string.app_name).replace(" ", "")
//            val folder =  File(activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!.absolutePath + "/" + appName)
            val folder =
                File(context.getExternalFilesDir("")!!.absolutePath)
            if (!folder.exists()) {
                folder.mkdirs()
            }
            var fileName = ""
            if (!name.isNullOrEmpty()) {
                fileName = name
            } else {
                val sdf = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
                fileName = "IMG_${sdf.format(Date())}"
            }
            fileName = folder.absolutePath + "/" + fileName + ".jpg"
            return fileName
        }

        fun createAudioFilePath(context: Context): String {
//            val appName = activity.getString(R.string.app_name).replace(" ", "")
//            val folder = File(activity.getExternalFilesDir(Environment.DIRECTORY_MUSIC)!!.absolutePath + "/" + appName)
            val folder =
                File(context.getExternalFilesDir("")!!.absolutePath)
            if (!folder.exists()) {
                folder.mkdirs()
            }
            val sdf = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
            var fileName = "AUDIO_${sdf.format(Date())}"
            fileName = folder.absolutePath + "/" + fileName + ".3gpp"
            return fileName
        }

        fun convertBitmapToFile(context: Context, bitmap: Bitmap?, name: String? = null): String? {
            if (bitmap == null) {
                return null
            }
            val filePath = createPictureFilePath(context, name)
            val file = File(filePath)
            file.createNewFile()
            val fos = FileOutputStream(file)
            bitmap.compress(
                Bitmap.CompressFormat.JPEG,
                100,
                fos
            )
            return filePath
        }

        fun getBitmapFromView(v: View): Bitmap {
            return v.drawToBitmap(Bitmap.Config.ARGB_8888)
        }

        fun getImageFileView(context: Context?, name: String? = null, view: View?): String? {
            if (view == null|| context == null) {
                return null
            }
            val bitmap = getBitmapFromView(view)
            return convertBitmapToFile(context, bitmap, name)
        }
        fun getImageFileView(context: Context?, name: String? = null, bitmap: Bitmap?): String? {
            if (bitmap == null|| context == null) {
                return null
            }
            return convertBitmapToFile(context, bitmap, name)
        }
        fun createVideoFilePath(context: Context, name: String? = null): String {
            val folder =
                File(context.getExternalFilesDir("")!!.absolutePath)
            if (!folder.exists()) {
                folder.mkdirs()
            }
            var fileName = ""
            if (!name.isNullOrEmpty()) {
                fileName = name
            } else {
                val sdf = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
                fileName = "IMG_${sdf.format(Date())}"
            }
            fileName = folder.absolutePath + "/" + fileName + ".mp4"
            return fileName
        }
    }


    interface OnDialogButtonListener {
        fun onLeftButtonClick()
        fun onRightButtonClick()
    }
}
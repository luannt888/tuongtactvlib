package vn.mediatech.istudiocafe.zinteractive.emotion

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_emotion_result.view.*
import vn.mediatech.interactive.app.ScreenSize
import vn.mediatech.istudiocafe.R

class ItemEmotionResultAdapter(
    val context: Context,
    val list: ArrayList<ItemEmotionResult>, val spacePx: Int, val
    numberColumn: Int
) : Adapter<ItemEmotionResultAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var indexSelect = -1
    var currentHolder: MyViewHolder? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val space: Int = context.resources.getDimensionPixelSize(R.dimen.dimen_80)
        val screenSize = ScreenSize(context as Activity)
        imageWidth = (screenSize.width - space - (spacePx * (numberColumn + 1))) / numberColumn
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_emotion_result,
            parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position >= list.size) {
            return
        }
        val index = position + 1
        var indexStr = index.toString()
        if (index < 10) {
            indexStr = "0" + index
        }
        holder.textIndex.setText(indexStr)
        val itemObjGame = list.get(position)
        try {
            holder.spriteView.columns =
                itemObjGame.sprite!!.imageWidth!! / itemObjGame.sprite!!.framewidth!!
            holder.spriteView.fps = itemObjGame.sprite!!.fps!!
            holder.textName.setText(itemObjGame.name)
            holder.textCount.setText(itemObjGame.playerCount.toString())
            holder.spriteView.isFitXY = false
            holder.spriteView.start(itemObjGame.sprite!!.link)
        } catch (e: Exception) {
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObjGame, position)
//            currentHolder?.layoutRoot?.isSelected = false
//            indexSelect = position
//            currentHolder = holder
//            holder.layoutRoot.isSelected = true
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val spriteView = itemView.spriteView
        val textName = itemView.textName
        val textCount = itemView.textCount
        val textIndex = itemView.textIndex

        init {
//            var param = RecyclerView.LayoutParams(imageWidth, ViewGroup.LayoutParams.WRAP_CONTENT)
//            param.marginStart = spacePx
//            param.topMargin = spacePx
//            layoutRoot?.layoutParams = param
        }
    }

    interface OnItemClickListener {
        fun onClick(itemGame: ItemEmotionResult, position: Int)
    }

}
package vn.mediatech.interactive.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import vn.mediatech.istudiocafe.util.ServiceUtilTT


class OnClearFromRecentService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("ClearFromRecentService", "Service Destroyed")
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        Log.e("ClearFromRecentService", "END")
        SocketMangager.getInstance().close()
        ServiceUtilTT.stopRequestConfig()
        stopSelf()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("ClearFromRecentService", "Service Started")
        return START_NOT_STICKY
    }
}
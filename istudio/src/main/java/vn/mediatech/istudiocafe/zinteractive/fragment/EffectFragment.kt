package vn.mediatech.interactive.fragment

import android.animation.Animator
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.LinearLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_bid_multi.*
import kotlinx.android.synthetic.main.fragment_firework.*
import kotlinx.android.synthetic.main.fragment_firework.layoutRoot
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.app.ScreenSize
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.util.*

class EffectFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var msg: String? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    companion object {
        val TYPE_EFFECT_WIN_ICON = 1
        val TYPE_EFFECT_WIN = 2
        val TYPE_EFFECT_FALSE = 3
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_firework, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED

            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putString(ConstantTT.DATA, msg!!)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
        val lp = textNotify.layoutParams as LinearLayout.LayoutParams
        val screenSize = ScreenSize(requireActivity())
        lp.width = screenSize.width * 4 / 5
        textNotify.layoutParams = lp
    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        msg = bundle?.getString(ConstantTT.MESSAGE)!!
        val height = bundle.getInt(ConstantTT.HEIGHT)
        try {
            if (height > 0) {
                Loggers.e("BID", "HeightBidMulti" + height)
                val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
                param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                param?.height = height
//                layoutRoot?.layoutParams = param
            }
        } catch (e: Exception) {
        }

        val type = bundle?.getInt(ConstantTT.TYPE)
        if (type == TYPE_EFFECT_WIN) {
            showFirework(msg)
        } else if (type == TYPE_EFFECT_WIN_ICON) {
            showFireworkIcon(msg)
        } else if (type == TYPE_EFFECT_FALSE) {
            showFalse(msg)
        }
    }

    fun showFirework(msg: String?) {
        if (msg.isNullOrEmpty()) {
            textNotify?.visibility = View.GONE
        } else {
            textNotify?.visibility = View.VISIBLE
            textNotify.text = AppUtils.getHtmlFormat(msg)
        }
        animFirework?.visibility = View.VISIBLE
        animFirework?.playAnimation()
    }

    fun initControl() {
        animFirework.setOnClickListener {
            try {
                dismiss()
            } catch (e: Exception) {
            }
        }
        animFirework?.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                Loggers.e("Firework", "onAnimationEnd")
                dismiss()
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationRepeat(animation: Animator?) {
                Loggers.e("Firework", "onAnimationRepeat")
            }
        })
    }

    var anim: Animation? = null
    private fun initAnim() {
        anim = android.view.animation.AnimationUtils.loadAnimation(
            requireContext(),
            R.anim.anim_result_iq
        )
        anim?.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
                imgIcon?.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation?) {
                /*Handler(Looper.getMainLooper()).postDelayed({
                    imgIcon?.visibility = View.GONE
                }, 1000)*/
            }

            override fun onAnimationRepeat(animation: Animation?) {
            }
        })
    }

    fun showFireworkIcon(msg: String?) {
        if (anim == null) {
            initAnim()
        }
        if (msg.isNullOrEmpty()) {
            textNotify.visibility = View.GONE
        } else {
            textNotify.text = AppUtils.getHtmlFormat(msg)
            textNotify.visibility = View.VISIBLE
        }
        imgIcon?.startAnimation(anim)
        animFirework?.visibility = View.VISIBLE
        animFirework?.playAnimation()
        Handler(Looper.getMainLooper()).postDelayed({
            try {
                dismiss()
            } catch (e: Exception) {
            }
        }, 5000)
    }

    fun showFalse(msg: String?) {
        if (anim == null) {
            initAnim()
        }
        if (msg.isNullOrEmpty()) {
            textNotify.visibility = View.GONE
        } else {
            textNotify.text = AppUtils.getHtmlFormat(msg)
            textNotify.visibility = View.VISIBLE
        }
        imgIcon?.setImageResource(R.drawable.icon_face_sad)
        imgIcon?.startAnimation(anim)
//        animFirework?.playAnimation()
        Handler(Looper.getMainLooper()).postDelayed({
            try {
                dismiss()
            } catch (e: Exception) {
            }
        }, 5000)
    }

    override fun onDismiss(dialog: DialogInterface) {
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

}
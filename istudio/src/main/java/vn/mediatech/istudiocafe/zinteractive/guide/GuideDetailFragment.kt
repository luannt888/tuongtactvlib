package vn.mediatech.interactive.guide

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.webkit.WebViewClient
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_guide_detail.*
import vn.mediatech.interactive.app.ScreenSize
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.interactive.slideImage.SlideImageAdapter
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.util.*
import kotlin.concurrent.timerTask

class GuideDetailFragment : Fragment() {
    private var isViewCreated: Boolean = false
    private var isTabSelected: Boolean = false
    var itemObj: ItemGuide? = null
    var isSuccess = false
    var bottomSheetDialog: BottomSheetDialog? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_guide_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        onShowDismissListener?.onShow()
//        checkRefresh()

        val bundle = savedInstanceState ?: arguments
        if (bundle!!.getInt(ConstantTT.POSITION) == 0) {
            checkRefresh()
        }
        return
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(ConstantTT.DATA, itemObj)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()

    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        try {
            itemObj = bundle?.getParcelable(ConstantTT.DATA)!!
        } catch (e: Exception) {
        }
        if (itemObj == null) {
            return
        }
        updateUI(itemObj!!)
    }

    fun updateUI(item: ItemGuide?) {
        this.itemObj = item
        if (item == null) {
            return
        }
        startPlayGalery(itemObj!!.galery)
        initWebviewGuide(itemObj!!.content)

    }

    fun initControl() {
    }


    @SuppressLint("SetJavaScriptEnabled")
    fun initWebviewGuide(content: String?) {
        if (content.isNullOrEmpty()) {
            webViewGuide.visibility = View.GONE
            return
        }
        val webSettings = webViewGuide.settings
        webSettings.builtInZoomControls = false
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            webSettings.setAppCacheEnabled(true)
        }
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.setSupportMultipleWindows(true)
        webSettings.loadsImagesAutomatically = true
        webSettings.mediaPlaybackRequiresUserGesture = false
        CookieManager.getInstance().setAcceptCookie(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webViewGuide.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            CookieManager.getInstance().setAcceptThirdPartyCookies(webViewGuide, true)
        }
        webViewGuide.webViewClient = WebViewClient()
        webViewGuide.setBackgroundColor(Color.TRANSPARENT)
        val guideContent = "<style>body{padding: 0px; margin: 0px; color: #ffffff;}</style>" + content
//        guideContent += "<br/><br/><img src=\"${itemObj!!.image}\" style=\"width: 100%; height: auto;\" />"
//        guideContent += "<br/><br/><div style=\"color: #989898; font-style: italic;\"> ${itemObj!!.example} </div>"
        webViewGuide.loadDataWithBaseURL(null, guideContent, "text/html", "utf-8", null);
        webViewGuide.visibility = View.VISIBLE
    }

    var timer1: Timer? = null
    fun startPlayGalery(galeryArrayString: Array<String>?) {

        if (galeryArrayString == null || galeryArrayString!!.size == 0) {
            slideImgViewPager.visibility = View.GONE
            return
        }
        val lp = slideImgViewPager.layoutParams as LinearLayout.LayoutParams?
        val screenSize = ScreenSize(requireActivity())
        lp?.height = screenSize.width * 9 / 16
        slideImgViewPager?.layoutParams = lp
        Loggers.e("GALERRY", galeryArrayString!!.toString())
        val adapter = SlideImageAdapter(requireContext(), galeryArrayString.toCollection(ArrayList()))
        adapter.onClickItemListener = object : SlideImageAdapter.OnClickItemListener {
            override fun onClick(urlImage: String, position: Int) {
//                val fragment = ImageZoomPagerDialog()
//                val list = ArrayList<ItemPhoto>()
//                for (string in galeryArrayString) {
//                    list.add(ItemPhoto(requireActivity().getString(R.string.box_guide), string, ""))
//                }
//                val bundle = Bundle()
//                bundle.putParcelableArrayList(Constant.DATA, list)
//                fragment.arguments = bundle
//                fragment.show(childFragmentManager, "ImageZoom")
            }
        }
        slideImgViewPager?.setAdapter(adapter)
        slideImgViewPager?.visibility = View.VISIBLE
        if (galeryArrayString.size == 1) {
            return
        }
        var index = 0
        timer1 = Timer()
        timer1!!.schedule(timerTask {
            if (isDetached) {
                return@timerTask
            }
            activity?.runOnUiThread {
               Loggers.e("GALERRY", "" + index + "/ " + galeryArrayString.get(index))
//                AppUtils.loadImage(context, imageView, galeryArrayString.get(index))
                slideImgViewPager?.setCurrentItem(index, true)
                index = index + 1
                if (index > (galeryArrayString.size - 1)) {
                    index = 0
                }
                if (galeryArrayString.get(index).isNullOrEmpty()) {
                    return@runOnUiThread
                }
            }
        }, 0, 4000)
    }

    override fun onPause() {
        super.onPause()
        timer1?.cancel()
    }

}
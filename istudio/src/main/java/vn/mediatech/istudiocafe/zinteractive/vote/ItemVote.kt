package vn.mediatech.interactive.vote

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemVote(
        @SerializedName("ID") val id: String?,
        @SerializedName("Vote_Title") val voteTitle: String?,
        @SerializedName("Vote_Img") val voteImg: String?,
        @SerializedName("Vote_Count") val voteCount: String?,
        @SerializedName("Current_Vote") val currentVote: String?,
        @SerializedName("Vote_Limit") val voteLimit: String?,
        @SerializedName("Vote_Detail") var voteDetail: String?,
        @SerializedName("Setting_ID") val settingID: String?,
        @SerializedName("CountDown") val countDown: Long?,
        @SerializedName("Status") val status: String?,
        @SerializedName("Playing") val playing: String?,
        @SerializedName("Vote_Mess") val voteMess: String?,
        var data: String?,
        var timeEndMs: Long?,
        var dataResult:String?,
        var indexSelected:Int = -1
//        @SerializedName("gallery_url") val galleryUrl: String?
) : Parcelable {
}
/*
"ID":1,
"Vote_Title":"Bạn muốn được đấu giá sản phẩm nào?",
"Vote_Img":"",
"Current_Vote": "bcc1265c203e4e2497ae6ed5cfec5eba",
"Vote_Count":4,
"Vote_Limit":1,
"Vote_Detail":"[{\"ID\":1,\"Vote_ID\":\"134fc796b4f844d1a53641ba90cccc2a\",\"Title\":\"1 thùng bia Hà Nội\",\"Value\":25,\"Img\":\"\\\\\\\\DEMO-PC\\\\MPI.SHARE\\\\DOWNLOAD\\\\IMG_MESS\\\\vote2_1_cf24157223dd4c938cbd2aa7454b44ec.png\",\"Percent\":43.86,\"Change\":1,\"Ls_Player\":\"\",\"Img_link\":\"https://api.daugiatruyenhinh.com/Media/Vote/vote2_1_534f7d70c70b4aedb5b5ad3e1495e6b0.png\"},{\"ID\":2,\"Vote_ID\":\"b54b0e80718b47e0b1b0527ca5c8c0ab\",\"Title\":\"Combo 3 hộp bánh ngũ hạt\",\"Value\":11,\"Img\":\"\\\\\\\\DEMO-PC\\\\MPI.SHARE\\\\DOWNLOAD\\\\IMG_MESS\\\\vote2_2_cbbc1745d12b418ca5a8e1be85493f64.png\",\"Percent\":19.3,\"Change\":1,\"Ls_Player\":\"\",\"Img_link\":\"https://api.daugiatruyenhinh.com/Media/Vote/vote2_2_07d98383093b4bf69c44ceceea7b3b57.png\"},{\"ID\":3,\"Vote_ID\":\"21269d9f58be4a44b8b5b49e7521fa21\",\"Title\":\"Combo 3 hộp Chocopie\",\"Value\":11,\"Img\":\"\\\\\\\\DEMO-PC\\\\MPI.SHARE\\\\DOWNLOAD\\\\IMG_MESS\\\\vote2_3_ec898c2276a24e2687e4f4f696566a0d.png\",\"Percent\":19.3,\"Change\":1,\"Ls_Player\":\"\",\"Img_link\":\"https://api.daugiatruyenhinh.com/Media/Vote/vote2_3_116d05104fb74b4d82f3a43089ac4a0c.png\"},{\"ID\":4,\"Vote_ID\":\"d90955345ae04765b9afeed7cc1cf133\",\"Title\":\"Combo 3 hướng dương Nga\",\"Value\":10,\"Img\":\"\\\\\\\\DEMO-PC\\\\MPI.SHARE\\\\DOWNLOAD\\\\IMG_MESS\\\\vote2_4_84fdd99acfbb41f99b5ee233f8f20e25.png\",\"Percent\":17.54,\"Change\":1,\"Ls_Player\":\"\",\"Img_link\":\"https://api.daugiatruyenhinh.com/Media/Vote/vote2_4_b8e76fa17412424cb5d21f7ff8c79783.png\"}]",
"Setting_ID":"598e3e40d9224874beba60ada96b7c87",
"CountDown":40,
"Status":0,
"Template":"PT phần trăm vote 4(0)",
"Playing":1,
"Vote_Mess":"{\"Start\":\"Bình chọn đã được bật.!\",\"Stop\":\"Kết thúc bình chọn.!\",\"Sucess\":\"Bình chọn thành công.!\",\"Error\":\"Bình chọn thất bại vui lòng thử lại.!\"}"*/

package vn.mediatech.interactive.guessPrice

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_bid_guess_price.*
import kotlinx.android.synthetic.main.fragment_bid_guess_price.buttonClose
import kotlinx.android.synthetic.main.fragment_bid_guess_price.layoutRoot
import kotlinx.android.synthetic.main.fragment_bid_guess_price.textNotify
import kotlinx.android.synthetic.main.fragment_bid_guess_price.textTimeCountDown
import kotlinx.android.synthetic.main.fragment_list_group_game.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.app.KeyboardHeightProvider
import vn.mediatech.interactive.bidFree.ItemBidProduct
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.util.*
import kotlin.concurrent.timerTask


class BidGuessPriceFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var itemObj: ItemBidProduct? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var onClickListener: OnClickButtonBidListenner? = null
    var onShowDismissListener: OnShowDismissListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bid_guess_price, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
        initHeightProvider()
    }

    private fun initUI() {
//        setupBackPressListener()
        editTextPrice.doOnTextChanged { text, start, before, count ->
//            editTextPrice.setText(formatInteger(text.toString()))
        }
    }

    var data: String? = null
    fun initData() {
        itemObj = arguments?.getParcelable(ConstantTT.DATA)!!
        if (itemObj == null) {
            activity?.onBackPressed()
            return
        }
        val height = requireArguments().getInt(ConstantTT.HEIGHT,0)
        try {
            if (height > 0) {
                val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
                param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                param?.height = height
            }
        } catch (e: Exception) {
        }
        AppUtils.loadImage(context, imgAvatar, itemObj!!.imageUrl!!)
        textTitle.text = itemObj!!.name
//        holder.layoutRoot.setOnClickListener {
//            onItemClickListener?.onClick(itemObj,position)
//        }
        if (!itemObj!!.playerMessage.isNullOrEmpty()) {
            setTextNotify(itemObj!!.playerMessage)
        }
        var timeLeftSecond = -1L;
        val currentTimeMs = System.currentTimeMillis()
        if (itemObj!!.countDown!! > 0) {
            timeLeftSecond = ((itemObj!!.endTimeMs!! - currentTimeMs) / 1000)
        }
        if (timeLeftSecond > 0) {
            startCountDown(timeLeftSecond)
        } else {
            if (itemObj!!.playerMessage.isNullOrEmpty()) {
                setTextNotify(getString(R.string.msg_time_out))
            }
        }
    }

    fun initControl() {
        editTextPrice.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                hideKeyboard()
                submitSend()
                true
            } else false
        }
        textButtonBid.setOnClickListener {
            submitSend()
        }

        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    private fun submitSend() {
        val bidPrice = cleanString(editTextPrice.text.toString().trim())!!.trim()
        try {
            if (bidPrice.isNullOrEmpty() || bidPrice.toLong() <= 1000) {
                Toast.makeText(
                    context,
                    "Giá đoán chưa hợp lệ. Vui lòng nhập lại",
                    Toast.LENGTH_SHORT
                ).show()
                editTextPrice.requestFocus()
                return
            }
        } catch (e: Exception) {
            Toast.makeText(context, "Giá đoán chưa hợp lệ. Vui lòng nhập lại", Toast.LENGTH_SHORT)
                .show()
            editTextPrice.requestFocus()
            return
        }
        itemObj?.priceBid = bidPrice
        onClickListener?.onClick(itemObj!!)
        setTextNotify("")
        hideKeyboard()
        textButtonBid.visibility = View.INVISIBLE
        editTextPrice.clearFocus()
        editTextPrice.isEnabled = false
    }

    override fun onDismiss(dialog: DialogInterface) {
//        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        timer?.cancel()
        hideKeyboard()
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

    var timer: Timer? = null
    fun startCountUp(timeStartSecond: Long) {
        var timeCount = timeStartSecond
        timer?.cancel()
        timer = Timer()
        timer!!.schedule(timerTask {
            activity?.runOnUiThread {
                if (timeCount >= 0) {
                    textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(timeCount)
                }
                timeCount = timeCount + 1
            }
        }, 0, 1000)
    }

    fun startCountDown(timeSeconds: Long) {
        timer?.cancel()
        var timeCount = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            if (timeCount >= 0) {
                val time = timeCount
                activity?.runOnUiThread {
                    textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(time)
                }
                if (timeCount <= 10) {
                    activity?.runOnUiThread {
                        textTimeCountDown?.setTextColor(Color.YELLOW)
                    }
                } else {
                    activity?.runOnUiThread {
                        textTimeCountDown?.setTextColor(Color.WHITE)
                    }
                }
                timeCount = timeCount - 1
            } else {
                activity?.runOnUiThread {
                    try {
                        dismiss()
                    } catch (e: Exception) {
                    }
                }
            }
        }, 0, 1000)
    }

    private fun setupBackPressListener() {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                activity?.finish()
                true
            } else
                false
        }
    }

    fun setTextNotify(msg: String?) {
        if (textNotify != null) {
            textNotify?.visibility = View.VISIBLE
            textNotify?.text = AppUtils.getHtmlFormat(msg!!)
            textButtonBid.visibility = View.INVISIBLE
            editTextPrice.clearFocus()
        }
    }

    interface OnClickButtonBidListenner {
        fun onClick(item: ItemBidProduct)
    }

    private val endFix = "VND"


    private fun cleanString(str: String): String? {
        if (str.isNullOrEmpty()) {
            return ""
        }
        val s = str.replace(endFix, "").replace(",", "")
        return s
    }

    fun reOpenGuess() {
        textNotify.visibility = View.GONE
        textButtonBid.visibility = View.VISIBLE
        editTextPrice.isEnabled = true
        editTextPrice.requestFocus()
    }

    fun hideKeyboard() {
        if (AppUtils.isShowKeyboard(requireContext())) {
            val imm =
                requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        }
    }

    override fun onPause() {
        super.onPause()
        keyboardHeightProvider?.onPause()
    }

    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
        keyboardHeightProvider?.onResume()
        if (textNotify?.visibility != View.VISIBLE) {
           Handler(Looper.getMainLooper()).postDelayed({
               editTextPrice?.post(Runnable {
                   editTextPrice?.requestFocus()
                   val imm =
                       requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                   imm?.showSoftInput(editTextPrice, InputMethodManager.SHOW_IMPLICIT)
               })
           },100)
        }
    }

    private var keyboardHeightProvider: KeyboardHeightProvider? = null
    var bottomMargin: Int = -123
    private fun initHeightProvider() {
        keyboardHeightProvider = KeyboardHeightProvider(requireActivity())
        keyboardHeightProvider?.addKeyboardListener(object :
            KeyboardHeightProvider.KeyboardListener {
            override fun onHeightChanged(height: Int) {
                if (!isDetached) {
                    if (bottomMargin == -123 && layoutRoot != null) {
                        bottomMargin = layoutRoot.paddingBottom
                    }
                    Loggers.e("Height", " " + height + "/" + bottomMargin)
                    val h = if (height > 0) height else bottomMargin
                    layoutRoot?.setPadding(0, 0, 0, h)
                }
            }
        })

    }
}
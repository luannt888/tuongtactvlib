package vn.mediatech.istudiocafe.zinteractive.webGame

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.*
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_webgame_result.*
import vn.mediatech.interactive.app.*
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask


class WebGameResultFragment : BottomSheetDialogFragment() {
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var myHttpRequest: MyHttpRequest? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var itemList: ArrayList<ItemUserWebgame> = ArrayList()


    private val API: String =
        "http://api.daugiatruyenhinh.com/api/minigame/top5"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_webgame_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {

    }

    fun initData() {
        var data: String? = null
        val bundle = arguments
        if(bundle == null){
            dismiss()
            return
        }
        var timeEndMs = bundle.getLong(ConstantTT.TIME, 0)
        val timeStart = (timeEndMs - System.currentTimeMillis()) / 1000
        val height = bundle.getInt(ConstantTT.HEIGHT)
        try {
            if (height != null && height > 0) {
                val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
                param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                param?.height = height
//                layoutRoot?.layoutParams = param
            }
        } catch (e: Exception) {
        }
        val listItemUserWebgame:ArrayList<ItemUserWebgame>? = bundle.getParcelableArrayList(ConstantTT.DATA)
        if(listItemUserWebgame!= null && listItemUserWebgame.size > 0){
            showRcv(listItemUserWebgame)
            return
        }
        data = bundle.getString(ConstantTT.DATA, null)
        if (data.isNullOrEmpty()) {
//            getData()
        } else {
            handleDataGroup(data)
        }
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        progressBar.visibility = View.VISIBLE
        if (!AppUtils.isNetworkConnect(context)) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
//        progressBar?.visibility = View.VISIBLE
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
//            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        val api = API
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                startTrack()
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                startTrack()
                Loggers.e("MyHttpRequest_url", api);
                Loggers.e("MyHttpRequest_result", responseString);
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(activity?.getString(R.string.msg_not_data))
            return
        }
        val responseString = responseStr.trim()
        handleDataGroup(responseString)
    }

    private fun handleDataGroup(data: String) {
        val gson = Gson()
        val gsonType = object : TypeToken<ArrayList<ItemUserWebgame>>() {}.getType()
        val list: ArrayList<ItemUserWebgame>? = try {
            gson.fromJson(data, gsonType)
        } catch (e: Exception) {
            ArrayList<ItemUserWebgame>()
        }

        showRcv(list)
    }

    private fun showRcv(list: ArrayList<ItemUserWebgame>?) {
        activity?.runOnUiThread {
            try {
                if (adapter != null) {
                    recyclerView?.stopScroll()
                    itemList.clear()
                    itemList.addAll(list!!)
                    adapter?.notifyDataSetChanged()
                } else {
                    itemList.clear()
                    itemList.addAll(list!!)
                    initAdapter()
                }
                setData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun setData() {
        if (itemList.size == 0) {
            textNotify.visibility = View.VISIBLE
        }
    }


    var adapter: ItemUserWebGameAdapter? = null
    private fun initAdapter() {
        adapter = ItemUserWebGameAdapter(requireContext(), itemList)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
//        recyclerView.scheduleLayoutAnimation()
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    fun scrollToBottom() {
//        scrollContent.fullScroll(View.FOCUS_DOWN)
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
//            getData()
        }
        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        super.onDestroyView()
    }

    var timerGetData: Timer? = null
    private fun startTrack() {
        val timer = Timer()
        timer.schedule(timerTask {
            activity?.runOnUiThread {
//                getData()
            }
        }, 0, 10000)
    }

    override fun onPause() {
        super.onPause()
        timerGetData?.cancel()
    }

    override fun onResume() {
        super.onResume()
//        if (adapter != null) {
//            startTrack()
//        }
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
    }

    override fun onStop() {
        super.onStop()
        timer?.cancel()
    }

    var timer: Timer? = null
    fun startCountDown(timeSeconds: Long) {
        if (timeSeconds < 0) {
            return
        }
        timer?.cancel()
        var timeCount = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            if (timeCount >= 0) {
                val time = timeCount
                activity?.runOnUiThread {
                    textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(time)
                    if (timeCount <= 10) {
                        textTimeCountDown?.setTextColor(Color.YELLOW)
                    } else {
                        textTimeCountDown?.setTextColor(Color.WHITE)
                    }
                    timeCount = timeCount - 1
                }
            } else {
                timer?.cancel()
                activity?.runOnUiThread {
                    dismiss()
                }
            }
        }, 0, 1000)
    }

    override fun onDismiss(dialog: DialogInterface) {
        timer?.cancel()
        timerGetData?.cancel()
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }
}
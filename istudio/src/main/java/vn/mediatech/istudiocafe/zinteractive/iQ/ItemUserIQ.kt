package vn.mediatech.istudiocafe.zinteractive.iQ

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemUserIQ(
        @SerializedName("rank_index") var index: Int?,
        @SerializedName("ID") val userId: Int?,
        @SerializedName("Fullname") val name: String?,
        @SerializedName("Avatar") val avatar: String?,
        @SerializedName("point") val score: String?,
        @SerializedName("result") val time: String?,
        @SerializedName("count") val count: String?
) : Parcelable

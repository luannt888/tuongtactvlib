package vn.mediatech.istudiocafe.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_menu_tt.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemMenuInteractive
import java.util.*

class ItemMenuInteractiveAdapter(
    val context: Context,
    val itemList: ArrayList<ItemMenuInteractive>,
    val type: Int,
    val style: Int,
    val
    numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var selectIndex: Int = -1
    fun setSelect(index: Int) {
        selectIndex = index
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return position
    }

    fun initViewSize(viewType: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
//        imageWidth = screenWidth / 4
//        imageHeight = imageWidth * 7 / 10
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_menu_tt,
            parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemMenuInteractive = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemMenuInteractive, position: Int) {
        if (!itemObj.icon.isNullOrEmpty()) {
            MyApplication.getInstance().loadImageCircle(context, holder.imgThumbnail, itemObj.icon)
        } else {
            if (itemObj.iconRes != null) {
                MyApplication.getInstance()
                    .loadDrawableImage(context, itemObj.iconRes, holder.imgThumbnail)
            }
        }
        holder.textName.isSelected = position == selectIndex
        holder.textName.text = itemObj.title
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
//            holder?.layoutRoot?.isEnabled = false
//            Handler(Looper.getMainLooper()).postDelayed({holder?.layoutRoot?.isEnabled = true},3000)
        }
        holder.layoutParent.setOnClickListener {
            onItemClickListener?.onClickOut(itemObj, position)
//            holder?.layoutRoot?.isEnabled = false
//            Handler(Looper.getMainLooper()).postDelayed({holder?.layoutRoot?.isEnabled = true},3000)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgThumbnail = itemView.imgAvatar
        val textName = itemView.textName
        val layoutParent = itemView.layoutParent
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemMenuInteractive, position: Int)
        fun onClickOut(itemObject: ItemMenuInteractive, position: Int)
    }
}
package vn.mediatech.istudiocafe.customView

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Handler
import android.text.Html
import android.text.Spanned
import android.util.AttributeSet
import android.widget.TextView
import vn.mediatech.istudiocafe.util.TextUtil

@SuppressLint("AppCompatCustomView")
class TypeWriterTextView : TextView {
    private var mText: CharSequence? = null
    private var mIndex = 0
    private var mDelay: Long = 150 // in ms

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}

    private val mHandler = Handler()
    private val characterAdder: Runnable = object : Runnable {
        override fun run() {
            text = getHtmlFormat(mText!!.subSequence(0, mIndex++) as String?)
//            text = mText!!.subSequence(0, mIndex++)
            if (mIndex <= mText!!.length) {
                mHandler.postDelayed(this, mDelay)
            }
        }
    }

    fun animateText(txt: CharSequence?) {
        mText = txt
        mIndex = 0
        text = ""
        mHandler.removeCallbacks(characterAdder)
        mHandler.postDelayed(characterAdder, mDelay)
    }

    fun setCharacterDelay(m: Long) {
        mDelay = m
    }

    fun getHtmlFormat(text: String?): Spanned? {
        var text = text
        if (text == null) {
            text = ""
        }
        val textSpanned: Spanned
        textSpanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(text)
        }
        return textSpanned
    }
}
package vn.mediatech.istudiocafe.group

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Outline
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_list_group.*
import kotlinx.android.synthetic.main.fragment_list_group.buttonBack
import kotlinx.android.synthetic.main.fragment_list_group.imageBgMain
import kotlinx.android.synthetic.main.fragment_list_group.progressBar
import kotlinx.android.synthetic.main.fragment_list_group.recyclerView
import kotlinx.android.synthetic.main.fragment_list_group.textNotify
import kotlinx.android.synthetic.main.fragment_use_voucher.*
import vn.mediatech.interactive.app.*
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.app.MyFileProvider
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.istudiocafe.util.ScreenSize
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ListGroupFragment : BottomSheetDialogFragment() {
    private val loadMoreAble: Boolean = true
    private var isLoadMore: Boolean = false
    private var page: Int = 1
    private var limit: Int = 200
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var myHttpRequest: MyHttpRequest? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var onGroupListener: OnGroupListener? = null
    var itemUser: ItemUser? = null
    var itemList: ArrayList<ItemGroup> = ArrayList()
    var itemListAll: ArrayList<ItemGroup> = ArrayList()
    var itemListSearch: ArrayList<ItemGroup> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list_group, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        dialog?.window?.getAttributes()?.windowAnimations = R.style.DialogAnimation;
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
//        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)

    }


    fun initData() {
        if (savedInstanceState != null) {
            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
        getData()
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        progressBar?.visibility = View.VISIBLE
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
//            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("page", "${page}")
        requestParams.put("limit", "${limit}")
        requestParams.put("userid", "${itemUser!!.id}")
//        val api = String.format(Locale.ENGLISH, Constant.API_USE_VOUCHER_HISTORY, itemUser!!.id, itemObj!!.id)
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_LIST_GROUP)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                Loggers.e("MyHttpRequest_url", api);
                Loggers.e("MyHttpRequest_result", responseString);
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { message?.let { showErrorNetwork(it) } }
            return
        }
        val resultObj = JsonParser.getString(jsonObject, Constant.RESULT)
        val gson = Gson()
        val gsonType = object : TypeToken<ArrayList<ItemGroup>>() {}.getType()
        var newList: ArrayList<ItemGroup> = ArrayList()
        try {
            val list: ArrayList<ItemGroup>? = gson.fromJson(resultObj, gsonType)
            newList.addAll(list!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        activity?.runOnUiThread {
            try {
                if (adapter != null) {
                    recyclerView?.stopScroll()
                    if (!isLoadMore) {
                        itemList.clear()
                    }
                    itemList.addAll(newList)
                    recyclerView?.getRecycledViewPool()?.clear();
                    adapter?.notifyDataSetChanged()
                } else {
                    itemList.clear()
                    itemList.addAll(newList)
                    initAdapter()
                }
                itemListAll.clear()
                itemListAll.addAll(itemList)
                setData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun setData() {
        if (itemList.size == 0) {
            textNotify.visibility = View.VISIBLE
        }
    }

    fun requestJoinGroup(item: ItemGroup?, pos: Int) {
        if (!MyApplication.getInstance().isNetworkConnect || item == null) {
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
//            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("userid", "${itemUser!!.id}")
        requestParams.put("groupid", "${item.id}")
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_JOIN)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        activity?.getString(R.string.msg_empty_data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                Loggers.e("MyHttpRequest_url", api);
                Loggers.e("MyHttpRequest_result", responseString);
                if (isDetached) {
                    return
                }
                if (responseString.isNullOrEmpty()) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val jsonObject = JsonParser.getJsonObject(responseString.trim());
                if (jsonObject == null) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                if (errorCode != Constant.SUCCESS) {
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    message?.let {
                        activity?.runOnUiThread {
                            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                        }
                    }
                    return
                }
                val result = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
                val mess = JsonParser.getString(result, "mess")
                activity?.runOnUiThread {
                    refreshData()
                    onGroupListener?.onRequireConnectSocket()
                    mess?.let {
                        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                    }
                }
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    fun requestCancelJoin(item: ItemGroup?, pos: Int) {
        if (!MyApplication.getInstance().isNetworkConnect || item == null) {
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
//            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("userid", "${itemUser!!.id}")
        requestParams.put("groupid", "${item.id}")
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_OUT)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        activity?.getString(R.string.msg_empty_data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                Loggers.e("MyHttpRequest_url", api);
                Loggers.e("MyHttpRequest_result", responseString);
                if (isDetached) {
                    return
                }
                if (responseString.isNullOrEmpty()) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val jsonObject = JsonParser.getJsonObject(responseString.trim());
                if (jsonObject == null) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                if (errorCode != Constant.SUCCESS) {
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    message?.let {
                        activity?.runOnUiThread {
                            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                        }
                    }
                    return
                }
                val result = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
                val mess = JsonParser.getString(result, "mess")
                activity?.runOnUiThread {
                    getData()
                    onGroupListener?.onRequireConnectSocket()
                    mess?.let {
                        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()

//                        itemList.get(pos).joined = Constant.GROUP_JOINED
//                        adapter?.notifyItemChanged(pos)
                    }
                }
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    fun requestDeleteGroup(item: ItemGroup?, pos: Int) {
        if (!MyApplication.getInstance().isNetworkConnect || item == null) {
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
//            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
//        requestParams.put("userid", "${itemUser!!.id}")
        requestParams.put("id", "${item.id}")
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_DELETE)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        activity?.getString(R.string.msg_empty_data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                Loggers.e("MyHttpRequest_url", api + "?id =${item.id}");
                Loggers.e("MyHttpRequest_result", responseString);
                if (isDetached) {
                    return
                }
                if (responseString.isNullOrEmpty()) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val jsonObject = JsonParser.getJsonObject(responseString.trim());
                if (jsonObject == null) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                if (errorCode != Constant.SUCCESS) {
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    message?.let {
                        activity?.runOnUiThread {
                            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                        }
                    }
                    return
                }
                val result = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
                val mess = JsonParser.getString(result, "mess")
                activity?.runOnUiThread {
                    refreshData()
                    onGroupListener?.onRequireConnectSocket()
                    mess?.let {
                        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                    }
                }
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    var adapter: ItemGroupAdapter? = null
    private fun initAdapter() {
        adapter = ItemGroupAdapter(requireContext(), itemList)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
//        recyclerView.scheduleLayoutAnimation()
        adapter!!.onItemClickListener = object : ItemGroupAdapter.OnItemClickListener {
            override fun onClick(item: ItemGroup, position: Int) {
                val bundle = Bundle()
                bundle.putParcelable(Constant.DATA, item)
                val fragment = DetailGroupFragment()
                fragment.arguments = bundle
                //                showFragment(fragment);
                Handler(Looper.getMainLooper()).postDelayed({
                    fragment.show(childFragmentManager, "GROUP_DETAIL")
                }, 200)
                fragment.onShowDismissListener = object : OnShowDismissListener {
                    override fun onShow() {

                    }

                    override fun onDismiss() {
                        refreshData()
                    }
                }
            }

            override fun onClickJoin(item: ItemGroup, position: Int) {
                if (Constant.GROUP_JOINED.equals(item.joined)) {
                    if (activity is BaseActivity) {
                        (activity as? BaseActivity)?.showDialog(
                            true,
                            R.string.notification,
                            "Bạn có muốn rời khỏi nhóm ${
                                TextUtil.getStringHtmlStype(
                                    item.name,
                                    "#0e5d30",
                                    true,
                                    false
                                )
                            } không? ",
                            R.string.cancel,
                            R.string.ok,
                            object : OnDialogButtonListener {
                                override fun onLeftButtonClick() {

                                }

                                override fun onRightButtonClick() {
                                    requestCancelJoin(item, position)
                                }
                            })
                    } else {
                        requestCancelJoin(item, position)
                    }
                } else {
                    if (activity is BaseActivity) {
                        (activity as? BaseActivity)?.showDialog(
                            true,
                            R.string.notification,
                            "Bạn có muốn tham gia nhóm ${
                                TextUtil.getStringHtmlStype(
                                    item.name,
                                    "#0e5d30",
                                    true,
                                    false
                                )
                            } không? ",
                            R.string.cancel,
                            R.string.ok,
                            object : OnDialogButtonListener {
                                override fun onLeftButtonClick() {

                                }

                                override fun onRightButtonClick() {
                                    requestJoinGroup(item, position)
                                }
                            })
                    } else {
                        requestJoinGroup(item, position)
                    }
                }
            }

            override fun onLongClick(item: ItemGroup, position: Int) {
                if (itemUser != null && itemUser!!.role.equals(Constant.ADMIN)) {
                    if (activity is BaseActivity) {
                        (activity as? BaseActivity)?.showDialog(
                            true,
                            R.string.notification,
                            "Bạn có muốn xoá nhóm ${
                                TextUtil.getStringHtmlStype(
                                    item.name,
                                    "#0e5d30",
                                    true,
                                    false
                                )
                            } không? ",
                            R.string.cancel,
                            R.string.ok,
                            object : OnDialogButtonListener {
                                override fun onLeftButtonClick() {

                                }

                                override fun onRightButtonClick() {
                                    requestDeleteGroup(item, position)
                                }
                            })
                    } else {
                        requestDeleteGroup(item, position)
                    }
                }
            }
        }
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                Loggers.e("VoucherFrg_onScrollStateChanged", "newState = $newState")
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//
//                }
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView2: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView2, dx, dy)
//                if (isLoading || !loadMoreAble) {
////                    Loggers.e("VoucherFrg", "addOnScrollListener onScrolled_stopGet")
//                    return
//                }
//                val firstPos = layoutManager.findFirstCompletelyVisibleItemPosition()
//                layoutRefresh.isEnabled = firstPos == 0
//
//                val visibleItemCount = layoutManager.childCount
//                val firstVisible = layoutManager.findFirstVisibleItemPosition()
//                val totalItem = layoutManager.itemCount
//                if (visibleItemCount + firstVisible >= totalItem - 4) {
//                    layoutLoadMore.visibility = View.VISIBLE
////                    Loggers.e("VoucherFrg", "addOnScrollListener onScrolled_getData")
//                    getData()
//                }
            }
        })
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    fun scrollToBottom() {
//        scrollContent.fullScroll(View.FOCUS_DOWN)
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        buttonBack.setOnClickListener {
            dismiss()
        }
        editSearch.setOnClickListener {
//            var fragment = SearchSupportFragment()
//            (activity as? ChatSupportActivity)?.showFragment(fragment)
        }
        layoutRefresh.setOnRefreshListener {
            refreshData()
        }
        buttonCreateGroup.setOnClickListener {
            showCreateDialog()
        }
        editSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                itemList.clear()
                if (s.isNullOrEmpty()) {
                    itemList.addAll(itemListAll)
                }
                if (s!!.length > 0) {
                    for (item in itemListAll) {
                        if(item.name == null){
                            continue
                        }
                        if (GeneralUtils.convertNonAccentVietnamese(item.name)!!.contains(
                                GeneralUtils.convertNonAccentVietnamese(s.toString())!!, true))
                            itemList.add(item)
                    }
                }
               adapter?.notifyDataSetChanged()
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })
    }

    private fun refreshData() {
        page = 1
        progressBar.visibility = View.VISIBLE
        layoutRefresh.isRefreshing = false
        getData()
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        super.onDestroyView()
    }


    var currentDialog: MyDialog? = null
    fun showCreateDialog(): MyDialog? {
        if (isDetached || context == null) {
            return null
        }
        val dialog = MyDialog(requireContext())
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = WindowManager.LayoutParams()
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimationLeftRight
        dialog.window?.setAttributes(lp)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_create_group_tt)
        val screenSize = ScreenSize(requireActivity())
        dialog.window?.setLayout(screenSize.width * 8 / 10, WindowManager.LayoutParams.WRAP_CONTENT)
        val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
        val editTextName: EditText = dialog.findViewById(R.id.editTextGroupName)
        val editTextAvatar: EditText = dialog.findViewById(R.id.editTextAvatar)
        val buttonSelectImage: ImageView = dialog.findViewById(R.id.buttonSelectImage)
        val buttonCreateGroup: TextView = dialog.findViewById(R.id.buttonCreateGroup)
        val buttonCancel: TextView = dialog.findViewById(R.id.buttonCancel)
        val imageAvatar: ImageView = dialog.findViewById(R.id.imageAvatar)
        val checkBoxConfirm: CheckBox = dialog.findViewById(R.id.checkBoxConfirm)
        //        View viewDivider = dialog.findViewById(R.id.viewDivider);
        layoutRoot.clipToOutline = true
        layoutRoot.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 15f
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        }
        buttonSelectImage.setOnClickListener {
            showChooseImagePopup()
        }
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }

        buttonCreateGroup.setOnClickListener {
            val name = editTextName.text.toString().trim()
            if (name.length < 3 || name.length > 15) {
                Toast.makeText(
                    requireContext(),
                    "Tên nhóm phải lớn hơn 3 ký tự và nhỏ hơn 16 ký tự",
                    Toast.LENGTH_SHORT
                )
                    .show()
                editTextName.requestFocus()
                return@setOnClickListener
            }
            val isAutoConfirm = checkBoxConfirm.isChecked
            requestCreateGroup(name, isAutoConfirm)
            buttonCreateGroup.isEnabled = true
        }


        currentDialog?.dismiss()
        currentDialog = dialog
        try {
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

    fun requestCreateGroup(nameGroup: String?, isAutoConfirm: Boolean? = false) {
        if (!MyApplication.getInstance().isNetworkConnect || nameGroup.isNullOrEmpty()) {
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
//            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("name", nameGroup)
        requestParams.put("userid", "${itemUser!!.id}")
        requestParams.put("auto_confirm", if (isAutoConfirm!!) "1" else "0")
        if (photoFile != null) {
            if (photoFile!!.exists()) {
                requestParams.put("avatar_file", photoFile.toString())
                requestParams.put("typedata", myHttpRequest!!.getMimeType(photoFile))
                requestParams.put("typeext", myHttpRequest!!.getExtension(photoFile!!.name))
            }
        }
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_CREATE)
        myHttpRequest!!.request(true, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        activity?.getString(R.string.msg_empty_data),
                        Toast.LENGTH_SHORT
                    ).show()
                    currentDialog?.findViewById<TextView>(R.id.buttonCreateGroup)?.isEnabled = true
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    currentDialog?.findViewById<TextView>(R.id.buttonCreateGroup)?.isEnabled = true
                }
                Loggers.e("MyHttpRequest_url", api);
                Loggers.e("MyHttpRequest_result", responseString);
                if (isDetached) {
                    return
                }
                if (responseString.isNullOrEmpty()) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val jsonObject = JsonParser.getJsonObject(responseString.trim());
                if (jsonObject == null) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                if (errorCode != Constant.SUCCESS) {
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    message?.let {
                        activity?.runOnUiThread {
                            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                        }
                    }
                    return
                }

                val result = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
                val mess = JsonParser.getString(result, "mess")
                activity?.runOnUiThread {
                    refreshData()
                    onGroupListener?.onRequireConnectSocket()
                    currentDialog?.dismiss()
                    mess?.let {
                        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                    }
                }
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    var popupMenuChooseImage: PowerMenu? = null
    fun showChooseImagePopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.select_from_gallery)))
        itemList.add(PowerMenuItem(getString(R.string.select_from_camera)))
        val screenSize = ScreenSize(requireActivity())
        val popupWidth = screenSize.width * 2 / 3

        val textView = TextView(activity)
        textView.textSize = 17f
        textView.gravity = Gravity.CENTER
        textView.setTextColor(
            ContextCompat.getColor(
                requireActivity(),
                R.color.color_primary_dark_tt
            )
        )
        textView.text = activity?.getString(R.string.send_image_title) ?: ""
        textView.setPadding(25, 15, 15, 15)
        textView.setBackgroundColor(Color.parseColor("#f8f8f8"))
        textView.setTypeface(Typeface.create("sans-serif-medium", Typeface.BOLD))

        popupMenuChooseImage = PowerMenu.Builder(requireActivity())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOW_UP_CENTER)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    onMenuItemChooseImageClickListener(position, item?.title.toString())
                    popupMenuChooseImage!!.dismiss()
                }
            })
            .setHeaderView(textView)
            .build()
        popupMenuChooseImage!!.showAtCenter(currentDialog!!.findViewById(R.id.buttonSelectImage))
    }

    fun onMenuItemChooseImageClickListener(position: Int, title: String) {
        if (title.equals(getString(R.string.select_from_gallery))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    Constant.REQUEST_CODE_GALLERY
                )
                return
            }
            openGallery()
            return
        }
        if (title.equals(getString(R.string.select_from_camera))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    Constant.REQUEST_CODE_CAMERA
                )
                return
            }
            openCamera()
        }
    }

    fun openGallery() {
//        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(intent, Constant.REQUEST_CODE_GALLERY)
    }

    private fun createImageFile(): File? {
        val fileName = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? =
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(fileName, ".jpg", storageDir)
    }

    var photoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(
                    requireActivity(), requireActivity().packageName + ".provider",
                    photoFile
                )
                this.photoFile = photoFile
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(intent, Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY, Constant.REQUEST_CODE_CAMERA -> {
                for (permission in permissions) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(),
                            permission
                        )
                    ) {
                        (activity as BaseActivity).showDialog(getString(R.string.msg_accept_permission))
                        break
                    } else {
                        if (ActivityCompat.checkSelfPermission(
                                requireActivity(),
                                permission
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                                openGallery()
                            } else if (requestCode == Constant.REQUEST_CODE_CAMERA) {
                                openCamera()
                            }
                        } else {
                            callPermissionSettings(requestCode)
                        }
                    }
                }
            }
        }
    }

    fun callPermissionSettings(requestCode: Int) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", requireActivity().packageName, null)
        intent.data = uri
        startActivityForResult(intent, requestCode)
    }

    var fileCompressor: FileCompressor? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY -> {
                if (data == null) return
                data.let {
                    val selectedImage = data.data
                    if (selectedImage == null) {
                        return
                    }
                    if (fileCompressor == null) {
                        fileCompressor = FileCompressor(requireActivity())
                    }
                    val realPath =
                        fileCompressor!!.getRealPathFromUri(requireActivity(), selectedImage)
                    if (realPath.isNullOrEmpty()) {
                        return
                    }
                    photoFile = fileCompressor!!.compressToFile(File(realPath))
                    if (currentDialog != null && currentDialog!!.isShowing) {
                        MyApplication.getInstance()?.loadImage(
                            requireActivity(),
                            currentDialog!!.findViewById(R.id.imageAvatar),
                            photoFile,
                            false
                        )
                    }
                }
            }
            Constant.REQUEST_CODE_CAMERA -> {
                if (photoFile == null) {
                    return
                }
                if (fileCompressor == null) {
                    fileCompressor = FileCompressor(requireActivity())
                }
                photoFile = fileCompressor!!.compressToFile(photoFile)
                if (currentDialog != null && currentDialog!!.isShowing) {
                    MyApplication.getInstance()?.loadImage(
                        requireActivity(),
                        currentDialog!!.findViewById(R.id.imageAvatar),
                        photoFile,
                        false
                    )
                }
//                photoFile = null
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onShowDismissListener?.onDismiss()
    }

    interface OnGroupListener {
        fun onRequireConnectSocket() {

        }
    }
}
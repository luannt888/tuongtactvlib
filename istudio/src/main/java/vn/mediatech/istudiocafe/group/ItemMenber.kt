package vn.mediatech.istudiocafe.group

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
//<<<<<<< HEAD
class ItemMenber(@SerializedName("id") val id: String?,
                 @SerializedName("groupid") val groupId: String?,
                 @SerializedName("userid") val userId: String?,
                 @SerializedName("fullname") val name: String?,
                 @SerializedName("avatar") val avatar: String?,
                 @SerializedName("status") val status: String?,
                 @SerializedName("level") val level: String?,
                 @SerializedName("level_name") val levelName: String?,
                 @SerializedName("date_join") val timeOnline: String? ,
                 @SerializedName("state") val state: String? ,
//                @SerializedName("type") val type: String?,
//                @SerializedName("mess") var message: String?,
//                @SerializedName("image") val image: String?,
//                @SerializedName("date") val date: String?,
//                @SerializedName("voucher_name") var voucherName: String?,
                 @SerializedName("count") var count: Int?
) : Parcelable
//=======
/*
                "id": "27",
                "userid": "42",
                "groupid": "26",
                "status": "0",
                "level": "1",
                "created_at": "2021-04-28 13:59:08",
                "updated_at": "2021-04-28 13:59:08",
                "fullname": "Nguyen Van Linh",
                "avatar": "http:\/\/mdtv.mediatech.vn\/upload\/user\/istudio.5f88005e06fa7.jpg",
                "level_name": "Thành viên",
                "state": "online",
                "date_join": "51 phút trước"*/

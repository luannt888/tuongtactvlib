package vn.mediatech.istudiocafe.service

import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.WebSocket
import org.json.JSONObject
import java.net.URI

class SocketIOManager {
    private var socket: Socket? = null
    var url: String? = null
    var path: String? = null
    var onSocketListener: OnSocketListener? = null

    companion object {
        fun newInstance() = SocketIOManager().apply {
        }
    }

    private fun configOptions(): IO.Options {
        return IO.Options.builder()
            .setForceNew(false)
            .setMultiplex(true)
//                .setTransports(arrayOf(Polling.NAME, WebSocket.NAME))
            .setTransports(arrayOf(WebSocket.NAME))
            .setUpgrade(true)
            .setRememberUpgrade(false)
            .setPath(path)
            .setReconnection(true)
            .setReconnectionAttempts(Integer.MAX_VALUE)
            .setReconnectionDelay(1000)
            .setReconnectionDelayMax(5000)
            .setRandomizationFactor(0.5)
            .setTimeout(20000)
            .build()
    }

    fun connect(url: String, path: String? = null) {
        close()
        this.url = url
        this.path = path
        val options = configOptions()
        try {
            socket = IO.socket(URI.create(url), options)
        } catch (e: Exception) {
            e.printStackTrace()
            onSocketListener?.onClosed(false, "${e.message}")
            return
        }
        socket?.on(
            Socket.EVENT_CONNECT
        ) { onSocketListener?.onConnected() }
        socket?.on(Socket.EVENT_DISCONNECT) { args ->
            var message = ""
            for (obj in args) {
                if (message.isNotEmpty()) {
                    message += "\n"
                }
                message += obj
            }
            onSocketListener?.onDisconnect(message)
        }
        socket?.on(Socket.EVENT_CONNECT_ERROR) { args ->
            var message = ""
            for (obj in args) {
                if (message.isNotEmpty()) {
                    message += "\n"
                }
                message += obj
            }
            onSocketListener?.onFailure(message)
        }
        socket?.connect()
    }

    fun addEvent(event: String, onMessageListener: OnMessageListener?) {
        socket?.on(event) { args ->
            for (obj in args) {
                onMessageListener?.onMessage(obj.toString())
            }
        }
    }

    fun removeEvent(event: String) {
        socket?.off(event)
    }

    fun removeAllEvent() {
        socket?.off()
    }

    fun sendMessage(event: String, jsonObject: JSONObject) {
        socket?.emit(event, jsonObject)
    }

    fun sendMessage(event: String, message: String) {
        socket?.emit(event, message)
    }

    fun close() {
        if (socket == null || url.isNullOrEmpty()) {
            return
        }
        onSocketListener?.onClosed(true, "")
        socket?.close()
        socket = null
    }

    fun reConnect() {
        close()
        if (url.isNullOrEmpty()) {
            return
        }
        connect(url!!, path)
    }

    interface OnMessageListener {
        fun onMessage(message: String)
    }

    interface OnSocketListener {
        fun onConnected()
        fun onDisconnect(reason: String)
        fun onFailure(message: String)
        fun onClosed(closeByUser: Boolean, reason: String)
//        fun onMessage(message: String)
    }
}
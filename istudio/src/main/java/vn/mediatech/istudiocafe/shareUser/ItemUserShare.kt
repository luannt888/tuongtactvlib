package vn.mediatech.istudiocafe.shareUser

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
//<<<<<<< HEAD
class ItemUserShare(@SerializedName("id") val id: String?,
                    @SerializedName("user_id") val userId: String?,
                    @SerializedName("full_name") val name: String?,
                    @SerializedName("avatar") val avatar: String?,
                    @SerializedName("status_interactive") var statusInteractive: String?,
                    @SerializedName("status_text") val statusText: String?
) : Parcelable
//=======
/*
             full_name: "Lê Văn Hiệp",
avatar: "https://graph.facebook.com/148495670619260/picture?type=large",
id: "896",
user_id: "896",
status_interactive: 1,
status_text: "Giới thiệu thành công"*/

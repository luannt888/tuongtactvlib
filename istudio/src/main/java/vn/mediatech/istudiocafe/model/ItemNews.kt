package vn.mediatech.istudiocafe.model

import android.os.Parcelable
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemNews(
        var id: String? = null,
        var name: String? = null,
        var description: String? = null,
        var info: String? = null,
        var image: String? = null,
        var categoryId: String? = null,
        var categoryName: String? = null,
        var createdAt: String? = null,
        var timeAgo: String? = null,
        var file: String? = null,
        var youtubeUrl: String? = null,
        var url: String? = null,
        var webview: String? = null,
        var view: String? = null,
        var comment: String? = null,
        var basePrice: Int = 0,
        var price: Int? = 0,
        var type: String? = null,
        var cardType: String? = null,
        var contentType: String? = null,
        var rate: Float? = 0f,
        var reviews: String? = null,
        var maxQuantity: Int? = 0,
        var itemUserReviewList: ArrayList<ItemUserReview>? = null,
        var interactive: String? = null,
        var liveStream: String? = null,
        var isLive: Boolean? = false,
        var quantity: Int = 0,
        var publishDate: String? = null,
        var time: String? = null,
        var gallery: String? = null,
        var read: Boolean? = false,
        var expireDate: String? = null)
    : Parcelable {

    fun copyObj(): ItemNews {
        val gson = Gson().toJson(this)
        return Gson().fromJson(gson, ItemNews::class.java)
    }
}
package vn.mediatech.istudiocafe.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemAppConfig(val logo: String?, val bgLaunch: String?, val linkShareApp: String?, val baseApiUrl: String?) : Parcelable
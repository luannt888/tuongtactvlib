package vn.mediatech.istudiocafe.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
//data class ItemUser(val id: Int? = 0, var email: String? = "", var avatar: String? = "", val phone: String? = "", var fullname: String? = "", var gender: Int? = -1, var address: String? = "", var birthday: String? = "", var socialId: String? = "", var accessToken: String? = "", var accessTokenSocial: String? = "", val affiliateId: String? = "",val role: String? = "", val isPassword: Boolean? = false, val chip: Int = 0) : Parcelable

data class ItemUser(
    var id: Int? = 0,
    var accessToken: String? = "",
    var fullname: String? = "",
    var avatar: String? = "",
    var email: String? = "",
    val phone: String? = "",
    var gender: String? = "",
    var address: String? = "",
    var birthday: String? = "",
    var socialId: String? = "",
    var accessTokenSocial: String? = "",
    val affiliateId: String? = "",
    val role: String? = "",
    val isPassword: Boolean? = false,
    val chip: Int = 0,
    @SerializedName("users_status") var userStatus: String? = "", // "1:that, 0:ao, 2:chua xac dinh
    @SerializedName("groupid") var groupId: String? = null,
    @SerializedName("group_name") var groupName: String? = null,
    @SerializedName("group_avt") var groupAvt: String? = null,
    @SerializedName("level") var level: String? = null,
    @SerializedName("url_forum") var urlForum: String? = null,
    @SerializedName("user_forum") var userNameForum: String? = null,
    @SerializedName("pass_forum") var passwordForum: String? = null,
    @SerializedName("job") var job: String? = null,
    @SerializedName("login_status") var loginStatus: String? = null,
    @SerializedName("share_id") var shareId: String? = null

) : Parcelable {
    constructor(id: Int? = 0, fullname: String? = "", accessToken: String? = "") : this(
        id,
        accessToken,
        fullname,
        ""
    ) {
    }

}

/*
@Parcelize
class ItemUser() : Parcelable {
    var id: Int? = 0
    var email: String? = null
    var avatar: String? = null
    var phone: String? = null
    var fullname: String? = null
    var gender: String? = null
    var address: String? = null
    var birthday: String? = null
    var socialId: String? = null
    var accessToken: String? = null
    var accessTokenSocial: String? = null
    var affiliateId: String? = null
    var isPassword: Boolean? = false;


    constructor(id: Int?, email: String?, avatar: String?, phone: String?, fullname: String?, gender: String?, address: String?, birthday: String?, socialId: String?, accessToken: String?, accessTokenSocial: String?, affiliateId: String?, isPassword: Boolean?) : this() {
        this.id = id
        this.email = email
        this.avatar = avatar
        this.phone = phone
        this.fullname = fullname
        this.gender = gender
        this.address = address
        this.birthday = birthday
        this.socialId = socialId
        this.accessToken = accessToken
        this.accessTokenSocial = accessTokenSocial
        this.affiliateId = affiliateId
        this.isPassword = isPassword
    }
}*/

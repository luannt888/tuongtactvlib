package vn.mediatech.istudiocafe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemSimpleParam implements Parcelable {
    private String key;
    private String value;

    public ItemSimpleParam(String key, String value) {
        super();
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private ItemSimpleParam(Parcel source) {
        key = source.readString();
        value = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(value);
    }

    public static final Creator<ItemSimpleParam> CREATOR = new Creator<ItemSimpleParam>() {
        @Override
        public ItemSimpleParam createFromParcel(Parcel source) {
            return new ItemSimpleParam(source);
        }

        @Override
        public ItemSimpleParam[] newArray(int size) {
            return new ItemSimpleParam[size];
        }
    };
}
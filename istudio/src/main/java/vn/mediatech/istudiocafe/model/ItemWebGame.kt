package vn.mediatech.istudiocafe.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemWebGame(
        @SerializedName("id")val id: String?,
        @SerializedName("url")val url: String?,
        @SerializedName("name")val name: String?,
        @SerializedName("type") val type: String?,
        @SerializedName("width") val width: Int = 0,
        @SerializedName("height") val height: Int = 0,
        @SerializedName("image")var image: String? = null) : Parcelable
/*show_date: "2021-05-18",
id: "525",
avatar: "https://graph.facebook.com/2999532883651035/picture?type=large",
full_name: "Lê Việt Hoàng"*/
package vn.mediatech.istudiocafe.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemNotify(
        @SerializedName("id")val id: String?,
        @SerializedName("content_id")val contentId: String?,
        @SerializedName("title")var title: String?,
        @SerializedName("message")val message: String?,
        @SerializedName("image")val image: String?,
        @SerializedName("video")val video: String?,
        @SerializedName("full_image")var fullImage: String?,
        @SerializedName("content_type")val contentType: String?,//video,image,news,web,play
        @SerializedName("url")val url: String?,
        @SerializedName("time_skip")val time: Int?,
        @SerializedName("count")val count: Int?) : Parcelable


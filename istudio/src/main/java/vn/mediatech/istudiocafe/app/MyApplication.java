package vn.mediatech.istudiocafe.app;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.util.ByteConstants;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;

import java.io.File;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.svg.SvgSoftwareLayerSetter;
import vn.mediatech.istudiocafe.util.ScreenSize;

public class MyApplication extends MultiDexApplication {
    private static Context sContext;
    private static MyApplication sInstance = new MyApplication();
    private DataManager dataManager;
    private String appId = "kspbV2Y7diY6kcN";
    private boolean isSendingFcm = false;
    private ScreenSize screenSize;
    private String secretKey;

    public String getSecretKey() {
        return secretKey == null ? appId : secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    //    private String baseApiUrl = "";
    private String baseApiUrl = "http://mdtv.mediatech.vn/api/";


    public String getBaseApiUrl() {
        return baseApiUrl;
    }
    public void setBaseApiUrl(String baseApiUrl) {
        this.baseApiUrl = baseApiUrl;
    }

    public void setScreenSize(ScreenSize screenSize) {
        this.screenSize = screenSize;
    }

    public ScreenSize getScreenSize() {
        return screenSize;
    }

    public void setSendingFcm(boolean sendingFcm) {
        isSendingFcm = sendingFcm;
    }

    public boolean isSendingFcm() {
        return isSendingFcm;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public static MyApplication getInstance() {
        if (sInstance == null) {
            synchronized (MyApplication.class) {
                if (sInstance == null) {
                    sInstance = new MyApplication();
                }
            }
        }
//        int number = new Random().nextInt(100);
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        Glide.get(getApplicationContext()).setMemoryCategory(MemoryCategory.HIGH);
        initFresco();
//        FacebookSdk.sdkInitialize(getApplicationContext());
        /*try {
            //FIX: OKHTTP SSL23_GET_SERVER_HELLO:sslv3
            ProviderInstaller.installIfNeeded(getApplicationContext());
//            SSLContext sslContext;
//            sslContext = SSLContext.getInstance("TLSv1.2");
//            sslContext.init(null, null, null);
//            sslContext.createSSLEngine();
//        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException
//                | NoSuchAlgorithmException | KeyManagementException e) {
//            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException
        e) {
            e.printStackTrace();
        }*/

        try {
            ProviderInstaller.installIfNeeded(getApplicationContext());
            SSLContext sslContext;
            sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, null);
            sslContext.createSSLEngine();
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException
                | NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
    }

    /*private void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws
            CertificateException {
                // Not implemented
            }

            @Override
            public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws
            CertificateException {
                // Not implemented
            }
        }};

        try {
            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }*/

    private void initFresco() {
        int MAX_DISK_CACHE_VERYLOW_SIZE = 10 * ByteConstants.MB;
        int MAX_DISK_CACHE_LOW_SIZE = 20 * ByteConstants.MB;
        int MAX_DISK_CACHE_SIZE = 30 * ByteConstants.MB;
        DiskCacheConfig diskCacheConfig =
                DiskCacheConfig.newBuilder(this).setMaxCacheSize(MAX_DISK_CACHE_SIZE).setMaxCacheSizeOnLowDiskSpace(MAX_DISK_CACHE_LOW_SIZE).setMaxCacheSizeOnVeryLowDiskSpace(MAX_DISK_CACHE_VERYLOW_SIZE)
                        .build();
//        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
//                .setMainDiskCacheConfig(diskCacheConfig)
//                .build();
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setDownsampleEnabled(true)
                .setResizeAndRotateEnabledForNetwork(true)
                .setBitmapsConfig(Bitmap.Config.RGB_565)
                .setMainDiskCacheConfig(diskCacheConfig)
//                .setPoolFactory(poolFactory)
                .build();
        Fresco.initialize(this, config);
    }

    private void clearMemoryCaches() {
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.clearCaches();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static Context getContext() {
        return sContext;
    }

    public static void setsContext(Context sContext) {
        MyApplication.sContext = sContext;
    }

    public boolean isNetworkConnect() {
        if (sContext == null) {
            return false;
        }
        ConnectivityManager connMgr = (ConnectivityManager) sContext
                .getSystemService(Activity.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            return true;
        }
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public boolean isEmpty(String data) {
        return (data == null || data.trim().isEmpty());
    }

    public void loadDrawableLayout(Context context, int drawableId, final RelativeLayout layout) {
        RequestOptions options = new RequestOptions()
                .priority(Priority.NORMAL);
        Glide.with(context).load(drawableId).apply(options).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<?
                    super Drawable> transition) {
                layout.setBackground(resource);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
            }
        });
    }

    public void loadDrawableLayout(Context context, String url, final RelativeLayout layout) {
        RequestOptions options = new RequestOptions()
                .priority(Priority.NORMAL);
        Glide.with(context).load(url).apply(options).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<?
                    super Drawable> transition) {
                layout.setBackground(resource);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
            }
        });
    }

    public void loadDrawableImage(Context context, int drawableId, ImageView imageView) {
        if (imageView == null) {
            return;
        }
        RequestOptions options = new RequestOptions()
                .priority(Priority.HIGH);
        Glide.with(context).load(drawableId).apply(options).into(imageView);
    }

    public void loadDrawableImageNow(Context context, int drawableId, ImageView imageView) {
        if (imageView == null) {
            return;
        }
        RequestOptions options = new RequestOptions()
                .priority(Priority.IMMEDIATE);
        Glide.with(context).load(drawableId).apply(options).into(imageView);
    }

    public void loadImageDrawable(Context context, ImageView imageView, int drawableId) {
        if (imageView == null) {
            return;
        }
        RequestOptions options = new RequestOptions()
                .priority(Priority.HIGH);
        Glide.with(context).load(drawableId).apply(options).into(imageView);
    }

    public void loadAvatar(Context context, ImageView imageView, String url, boolean isCircle) {
        if (imageView == null) {
            return;
        }
        RequestOptions options = new RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.ic_avatar)
                .error(R.drawable.ic_avatar);
        if (isCircle) {
            options = options.transform(new CircleCrop());
        }
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public void loadImage(Context context, ImageView imageView, String url) {
        if (imageView == null) {
            return;
        }
        if (isEmpty(url)) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.img_loading));
            return;
        }
        RequestOptions options = new RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public void loadImage(Context context, ImageView imageView, File file, boolean isCircle) {
        RequestOptions options = new RequestOptions()
                .priority(Priority.IMMEDIATE);
        if (isCircle) {
            options = options.transform(new CircleCrop());
        }
        Glide.with(context).load(file).apply(options).into(imageView);
    }

    public void loadImage(Context context, ImageView imageView, Bitmap bitmap, boolean isCircle) {
        RequestOptions options = new RequestOptions()
                .priority(Priority.IMMEDIATE);
        if (isCircle) {
            options = options.transform(new CircleCrop());
        }
        Glide.with(context).load(bitmap).apply(options).into(imageView);
    }

    public void loadImage(Context context, ImageView imageView, String url,
                          boolean isRoundedCorner) {
        if (imageView == null) {
            return;
        }
        if (isEmpty(url)) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_tab_gallery_selected));
            return;
        }
        if (!isRoundedCorner) {
            loadImage(context, imageView, url);
            return;
        }
        RequestOptions options = new RequestOptions().transform(new CenterCrop(),
                new RoundedCorners(7))
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public void loadImageNoPlaceHolder(Context context, View view, int drawableId) {
        if (view == null || drawableId < 1) {
            return;
        }
        RequestOptions options = new RequestOptions()
                .priority(Priority.NORMAL);
        Glide.with(context).load(drawableId).apply(options).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<?
                    super Drawable> transition) {
                view.setBackground(resource);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
            }
        });
    }

    public void loadImageNoPlaceHolderChat(Context context, String url, CustomTarget<Drawable> target) {
        if (url == null || url.trim().equals("")) {
            return;
        }
        RequestOptions options = new RequestOptions().transform(
                new RoundedCorners(context.getResources().getDimensionPixelSize(R.dimen.dimen_10)))
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading);
        Glide.with(context).load(url).apply(options).into(target);
    }

    public void loadImageNoPlaceHolder(Context context, ImageView imageView, String url) {
        if (imageView == null || isEmpty(url)) {
            return;
        }
        if (url != null && url.endsWith(".svg")) {
            loadImageSVGNoPlaceHolder(context, imageView, url);
            return;
        }
        RequestOptions options = new RequestOptions()
                .priority(Priority.IMMEDIATE);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public void loadImageNoPlaceHolderNoCache(Context context, ImageView imageView, String url) {
        if (imageView == null || isEmpty(url)) {
            return;
        }
        if (url != null && url.endsWith(".svg")) {
            loadImageSVGNoPlaceHolder(context, imageView, url);
            return;
        }
        RequestOptions options = new RequestOptions()
                .priority(Priority.IMMEDIATE)
                .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public void loadImageNoPlaceHolder(Context context, ImageView imageView, int drawableId) {
        if (imageView == null) {
            return;
        }
        RequestOptions options = new RequestOptions()
                .priority(Priority.IMMEDIATE);
        Glide.with(context).load(drawableId).apply(options).into(imageView);
    }

    public void loadImageSVGNoPlaceHolder(Context context, ImageView imageView, String url) {
        RequestBuilder<PictureDrawable> requestBuilder;
        requestBuilder = Glide.with(context)
                .as(PictureDrawable.class)
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(new SvgSoftwareLayerSetter());
        Uri uri = Uri.parse(url);
        requestBuilder.load(uri).into(imageView);
    }

    public void loadImage(Context context, ImageView imageView, String url,
                          int placeholderDrawableId) {
        RequestOptions options = new RequestOptions()
                .placeholder(placeholderDrawableId)
                .error(placeholderDrawableId)
                .priority(Priority.NORMAL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public void loadImageCircle(Context context, ImageView imageView, String url) {
        loadImageCircle(context, imageView, url, 0);
    }

    public void loadImageCircle(Context context, ImageView imageView, String url,
                                int placeholderDrawableId) {
        RequestOptions options;
        if (placeholderDrawableId <= 0) {
            options = new RequestOptions().transform(new CircleCrop())
                    .priority(Priority.NORMAL);
        } else {
            options = new RequestOptions().transform(new CircleCrop())
                    .placeholder(placeholderDrawableId)
                    .error(placeholderDrawableId)
                    .priority(Priority.NORMAL);
        }
        Glide.with(context).load(url).apply(options).into(imageView);
    }
    public void loadImageCircleClearCache(Context context, ImageView imageView, String url,
                                int placeholderDrawableId) {
        RequestOptions options;
        if (placeholderDrawableId <= 0) {
            options = new RequestOptions().transform(new CircleCrop())
                    .priority(Priority.NORMAL);
        } else {
            options = new RequestOptions().transform(new CircleCrop())
                    .placeholder(placeholderDrawableId)
                    .error(placeholderDrawableId)
                    .priority(Priority.NORMAL).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true);
        }
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public void loadDrawableId(Context context, int drawableId,
                               final OnLoadImageListener onLoadImageListener) {
        RequestOptions options = new RequestOptions()
                .priority(Priority.HIGH);
        Glide.with(context).load(drawableId).apply(options).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<?
                    super Drawable> transition) {
                if (onLoadImageListener != null) {
                    onLoadImageListener.onResourceReady(resource);
                }
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                if (onLoadImageListener != null) {
                    onLoadImageListener.onResourceReady(null);
                }
            }

            @Override
            public void onLoadFailed(@Nullable @org.jetbrains.annotations.Nullable Drawable errorDrawable) {
                if (onLoadImageListener != null) {
                    onLoadImageListener.onResourceReady(null);
                }
            }
        });
    }

    public void loadImageListener(Context context, String url,
                                  final OnLoadImageListener onLoadImageListener) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
                .priority(Priority.NORMAL);
        Glide.with(context).load(url).apply(options).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<?
                    super Drawable> transition) {
                if (onLoadImageListener != null) {
                    onLoadImageListener.onResourceReady(resource);
                }
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                if (onLoadImageListener != null) {
                    onLoadImageListener.onResourceReady(null);
                }
            }

            @Override
            public void onLoadFailed(@Nullable @org.jetbrains.annotations.Nullable Drawable errorDrawable) {
                if (onLoadImageListener != null) {
                    onLoadImageListener.onResourceReady(null);
                }
            }
        });
    }
    public void loadImageListenerNoPlaceholderNocache(Context context, String url,
                                  final OnLoadImageListener onLoadImageListener) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
                .priority(Priority.NORMAL);
        Glide.with(context).load(url).apply(options).skipMemoryCache(true).signature(new ObjectKey(String.valueOf(System.currentTimeMillis()))).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<?
                    super Drawable> transition) {
                if (onLoadImageListener != null) {
                    onLoadImageListener.onResourceReady(resource);
                }
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                if (onLoadImageListener != null) {
                    onLoadImageListener.onResourceReady(null);
                }
            }

            @Override
            public void onLoadFailed(@Nullable @org.jetbrains.annotations.Nullable Drawable errorDrawable) {
                if (onLoadImageListener != null) {
                    onLoadImageListener.onResourceReady(null);
                }
            }
        });
    }
    public void loadImageBitmapListener(Context context, String url,
                                        final OnLoadImageBitmapListener onLoadImageBitmapListener) {
        RequestOptions options = new RequestOptions()
                .priority(Priority.HIGH);
        Glide.with(context).asBitmap().load(url).apply(options).into(new CustomTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<?
                    super Bitmap> transition) {
                if (onLoadImageBitmapListener != null) {
                    onLoadImageBitmapListener.onResourceReady(resource);
                }
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                if (onLoadImageBitmapListener != null) {
                    onLoadImageBitmapListener.onResourceReady(null);
                }
            }

            @Override
            public void onLoadFailed(@Nullable @org.jetbrains.annotations.Nullable Drawable errorDrawable) {
                if (onLoadImageBitmapListener != null) {
                    onLoadImageBitmapListener.onResourceReady(null);
                }
            }
        });
    }

    public void loadImageBitmapListener(Context context, int drawableId,
                                        final OnLoadImageBitmapListener onLoadImageBitmapListener) {
        RequestOptions options = new RequestOptions()
                .priority(Priority.HIGH);
        Glide.with(context).asBitmap().load(drawableId).apply(options).into(new CustomTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<?
                    super Bitmap> transition) {
                if (onLoadImageBitmapListener != null) {
                    onLoadImageBitmapListener.onResourceReady(resource);
                }
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                if (onLoadImageBitmapListener != null) {
                    onLoadImageBitmapListener.onResourceReady(null);
                }
            }

            @Override
            public void onLoadFailed(@Nullable @org.jetbrains.annotations.Nullable Drawable errorDrawable) {
                if (onLoadImageBitmapListener != null) {
                    onLoadImageBitmapListener.onResourceReady(null);
                }
            }
        });
    }

    public void loadImage(Context context, SimpleDraweeView imageView, String url) {
        if (isEmpty(url)) {
            return;
        }
        Uri uri = Uri.parse(url);
        imageView.setImageURI(uri);
    }

    public interface OnLoadImageListener {
        void onResourceReady(Drawable resource);
    }

    public interface OnLoadImageBitmapListener {
        void onResourceReady(Bitmap resource);
    }

    private int versionCode = 0;
//    private int versionCode = 27;

    public int getVersionCode(Context context) {
        if (versionCode > 0) {
            return versionCode;
        }
        versionCode = 0;
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            if (packageInfo == null) {
                return versionCode;
            }
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return versionCode;
        }
        return versionCode;
    }

    public DataManager getDataManager() {
        if (dataManager == null) {
            dataManager = new DataManager();
        }
        return dataManager;
    }

    public void clearDataManager() {
        sInstance = null;
        dataManager = null;
    }

    public float convertDpToPixel(Context context, int dp) {
        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }
}
package vn.mediatech.istudiocafe.app;

import android.util.Log;

public class Loggers {
    public static boolean DEBUG_MODE = true;

    public static void e(String TAG, String msg) {
        if (DEBUG_MODE) {
            Log.e(TAG, msg + "");
        }
    }

    public static void e(String TAG, int msg) {
        if (DEBUG_MODE) {
            Log.e(TAG, msg + "");
        }
    }
}
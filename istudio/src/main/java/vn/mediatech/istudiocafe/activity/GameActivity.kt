package vn.mediatech.istudiocafe.activity

import android.app.Activity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_detail_news_tt.progressBar
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.layout_actionbar_tt.*
import org.json.JSONException
import org.json.JSONObject
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemWebGame
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.GeneralUtils
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import vn.mediatech.istudiocafe.zinteractive.webGame.ItemUserWebgame
import vn.mediatech.istudiocafe.zinteractive.webGame.WebGameOfflineFragment
import vn.mediatech.istudiocafe.zinteractive.webGame.WebGameResultFragment

var itemWebGame: ItemWebGame? = null

class GameActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        initUI()
        initData()
        initControl()
    }

    private fun initControl() {
        buttonBack.setOnClickListener {
            finish()
        }
        buttonRank.setOnClickListener {
            getData()
        }
    }

    private fun initUI() {
        setFullStatusTransparent(findViewById(R.id.layout_actionbar))
        setDefaultBackgroundColorImage()
        textActionbarTitle.text = getString(R.string.back)
        buttonGuide.visibility = View.GONE
        buttonRank.visibility = View.VISIBLE
    }

    var webGameFragment: WebGameOfflineFragment? = null
    private fun initData() {
        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            itemWebGame = bundle.getParcelable(Constant.DATA)
        }
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        webGameFragment = WebGameOfflineFragment.newInstance(itemWebGame)
        fragmentTransaction.add(R.id.frameLayout, webGameFragment!!, "WEBGAME").commit()
    }

    companion object {
        fun startGameActivity(
            activity: Activity?,
            itemWebGame: ItemWebGame? = null
        ) {
            if (activity == null || itemWebGame == null || itemWebGame.url.isNullOrEmpty() || !itemWebGame.url.startsWith(
                    "http"
                )
            ) {
                return
            }
            val bundle = Bundle()
            bundle.putParcelable(Constant.DATA, itemWebGame)
            GeneralUtils.gotoActivity(activity, GameActivity::class.java, bundle)
        }
    }

    var isLoading: Boolean = false
    fun getData() {
        if (isLoading) {
            return
        }
        isLoading = true
        progressBar.visibility = View.VISIBLE
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
        val myHttpRequest = MyHttpRequest(this)
        val requestParams = RequestParams()
        requestParams.put("type", "${itemWebGame?.type}")
        var api = ServiceUtilTT.validAPITT(this, Constant.API_WEBGAME_RANK)
        myHttpRequest.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isFinishing || isDestroyed) {
                    return
                }
                isLoading = false
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isFinishing || isDestroyed) {
                    return
                }
                handleData(responseString)
                runOnUiThread {
                    progressBar.visibility = View.GONE
                }
                isLoading = false
            }
        })
    }

    private fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            runOnUiThread {
                showDialog(message)
            }
            return
        }
        val jsonArray = JsonParser.getJsonArray(jsonObject, Constant.RESULT) ?: return
        val itemList: ArrayList<ItemUserWebgame> = ArrayList()
        for (i in 0 until jsonArray.length()) {
            try {
                val jsonObject: JSONObject? = jsonArray.getJSONObject(i)
                val userId: Int? = JsonParser.getInt(jsonObject, "player_id")
                val name: String? = JsonParser.getString(jsonObject, "fullname")
                val avatar: String? = JsonParser.getString(jsonObject, "avatar")
                val score: String? = JsonParser.getString(jsonObject, "score")
                val itemObj = ItemUserWebgame(i, userId, name, avatar, score)
                itemList.add(itemObj)
            } catch (e: JSONException) {
            }
        }
        if (itemList.size > 0) {
            val webGameResultFragment = WebGameResultFragment()
            val bundle = Bundle()
            val height = frameLayout.height
            bundle.putInt(ConstantTT.HEIGHT, height)
            bundle.putParcelableArrayList(ConstantTT.DATA, itemList)
            webGameResultFragment.arguments = bundle
            runOnUiThread {
                webGameResultFragment.show(supportFragmentManager, "WEB_GAME_RESULT")
            }
        }
    }

    fun showErrorNetwork(message: String) {
        runOnUiThread {
            showDialog(
                false,
                R.string.notification,
                message,
                R.string.close,
                R.string.try_again,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        finish()
                    }

                    override fun onRightButtonClick() {
                        getData()
                    }

                })
        }
    }
}
package vn.mediatech.istudiocafe.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnCancelListener;
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener;
import vn.mediatech.istudiocafe.model.ItemUser;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.service.RequestParams;
import vn.mediatech.istudiocafe.util.JsonParser;
import vn.mediatech.istudiocafe.util.ServiceUtilTT;

public class UserEnterPhoneActivity extends BaseActivity {
    private ImageView buttonBack;
    private TextView buttonConfirm;
    private EditText editText;
    private MyHttpRequest myHttpRequest;
    private boolean resultOk = false;
    private String loginType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_enter_phone_tt);
        initUI();
        initData();
        initControl();
    }

    private void initUI() {
        setFullStatusTransparent();
        buttonBack = findViewById(R.id.buttonBack);
        editText = findViewById(R.id.editText);
        buttonConfirm = findViewById(R.id.buttonConfirm);
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            finish();
            return;
        }
        loginType = bundle.getString(Constant.TYPE);
        String phoneEntered = bundle.getString(Constant.PHONE, "");
        editText.setText(phoneEntered);
        editText.setSelection(phoneEntered.length());
        validButtonConfirm();
    }

    private void validForm() {
        String phone = editText.getText().toString().trim();
        if (phone.isEmpty()) {
            showToast(R.string.msg_phone_empty);
            editText.requestFocus();
            return;
        }
        if (phone.length() < 10) {
            showToast(R.string.msg_phone_invalid_format);
            editText.requestFocus();
            return;
        }
        getData();
        /*showLoadingDialog(true, getString(R.string.please_waiting), null);
        ServiceUtil.registerUser(this, type, phone, null, new RequestServiceListener() {
            @Override
            public void onResponse(boolean isSuccess, String response) {
                if (isFinishing() || isDestroyed()) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoadingDialog();
                        if (!isSuccess) {
                            showDialogUser(response);
                            return;
                        }
                        handleRegisterData(response);
                    }
                });
            }
        });*/
    }

    private void getData() {
        if (!MyApplication.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        showLoadingDialog(true, null, new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequest != null) {
                    myHttpRequest.cancel();
                }
            }
        });
        if (myHttpRequest == null) {
            myHttpRequest = new MyHttpRequest(this);
        } else {
            myHttpRequest.cancel();
        }
        String api = "";
        RequestParams requestParams = new RequestParams();
        if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK)) {
            ItemUser itemUserTemp = MyApplication.getInstance().getDataManager().getItemUserTemp();
            if (itemUserTemp == null) {
                hideLoadingDialog();
                showDialogUser(false, null, getString(R.string.msg_data_invalid), null,
                        getString(R.string.yes), new OnDialogButtonListener() {
                    @Override
                    public void onLeftButtonClick() {
                    }

                    @Override
                    public void onRightButtonClick() {
                        finish();
                    }
                });
                return;
            }
            api = Constant.API_USER_REGISTER;
            requestParams.put("type", loginType);
            requestParams.put("password", "");
            requestParams.put("confirm_password", "");
            requestParams.put("social_id", itemUserTemp.getSocialId());
            requestParams.put("email", itemUserTemp.getEmail());
            requestParams.put("fullname", itemUserTemp.getFullname());
        } else if (loginType.equals(Constant.TYPE_FORGOT_PASSWORD)) {
            api = Constant.API_USER_FORGET_PASSWORD;
        }
        if (MyApplication.getInstance().isEmpty(api)) {
            hideLoadingDialog();
            showDialogUser(getString(R.string.msg_data_invalid));
            return;
        }
        String phone = editText.getText().toString().trim();
        requestParams.put("phone", phone);
        myHttpRequest.request(true, ServiceUtilTT.validAPIDGTH(this,api), requestParams,
                new MyHttpRequest.ResponseListener() {
            @Override
            public void onFailure(int statusCode) {
                if (isFinishing() || isDestroyed()) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoadingDialog();
                        showDialogUser(getString(R.string.msg_network_error));
                    }
                });
            }

            @Override
            public void onSuccess(int statusCode, String responseString) {
                if (isFinishing() || isDestroyed()) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoadingDialog();
                        handleRegisterData(responseString);
                    }
                });
            }
        });
    }

    private void handleRegisterData(String responseString) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        String phone = editText.getText().toString().trim();
        int result = JsonParser.Companion.getInt(jsonObject, Constant.RESULT);
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.DATA, result);
        bundle.putString(Constant.TYPE, loginType);
        bundle.putString(Constant.PHONE, phone);
        gotoActivityForResult(UserOTPConfirmActivity.class, bundle, Constant.CODE_LOGIN);
    }

    private void validButtonConfirm() {
        String phone = editText.getText().toString().trim();
        buttonConfirm.setEnabled(phone.length() >= 10);
    }

    private void initControl() {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editText);
                showCancelDialog();
            }
        });
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editText);
                validForm();
            }
        });
    }

    private void showCancelDialog() {
        if (loginType != null && loginType.equals(Constant.TYPE_FORGOT_PASSWORD)) {
            finish();
            return;
        }
        showDialogUser(true, getString(R.string.notification),
                getString(R.string.msg_cancel_register), getString(R.string.cancel),
                getString(R.string.close), new OnDialogButtonListener() {
            @Override
            public void onLeftButtonClick() {
                resultOk = true;
                if (bundleResult == null) {
                    bundleResult = new Bundle();
                }
                bundleResult.putInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SCREEN);
                finish();
            }

            @Override
            public void onRightButtonClick() {

            }
        });
    }

    @Override
    public void onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest.cancel();
        }
        super.onDestroy();
    }

    private Bundle bundleResult;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
            resultOk = true;
            bundleResult = data != null ? data.getExtras() : null;
            finish();
        }
    }

    @Override
    public void finish() {
        if (resultOk) {
            Intent intent = new Intent();
            if (bundleResult != null) {
                intent.putExtras(bundleResult);
            }
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showCancelDialog();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
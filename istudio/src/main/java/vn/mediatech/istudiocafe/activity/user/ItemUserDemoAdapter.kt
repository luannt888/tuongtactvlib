package vn.mediatech.istudiocafe.activity.user

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_user_demo.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.model.ItemUser
import java.util.*

class ItemUserDemoAdapter(
        val context: Context, val itemList: ArrayList<ItemUser>) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null


    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun initViewSize(viewType: Int) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_user_demo,
                parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemUser = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemUser, position: Int) {
        holder.textTitle.setText(itemObj.fullname)
        holder.textTitle.setOnClickListener {
            onItemClickListener?.onClick(itemObj,position)
//            holder?.layoutRoot?.isEnabled = false
//            Handler(Looper.getMainLooper()).postDelayed({holder?.layoutRoot?.isEnabled = true},3000)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val textTitle = itemView.text
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemUser, position: Int)
    }



}
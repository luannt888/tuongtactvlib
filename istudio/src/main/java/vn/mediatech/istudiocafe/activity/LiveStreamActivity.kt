package vn.mediatech.istudiocafe.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import kotlinx.android.synthetic.main.activity_live_stream_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.fragment.InteractiveLiveStreamFragment
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.swipebacklayout.SwipeBackLayout
import vn.mediatech.istudiocafe.swipebacklayout.app.SwipeBackActivity
import vn.mediatech.istudiocafe.util.ScreenSize

class LiveStreamActivity : SwipeBackActivity() {
    var fragmentInteractive: InteractiveLiveStreamFragment? = null
    var itemObj: ItemNews? = null

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(Constant.DATA, itemObj)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_stream_tt)
        initSwipeBack()
        initData(savedInstanceState)
        initControl()
    }

    private fun initSwipeBack() {
        val edgeFlag: Int = SwipeBackLayout.EDGE_LEFT
        val swipeBackLayout: SwipeBackLayout = swipeBackLayout
        swipeBackLayout.setEdgeTrackingEnabled(edgeFlag)
        swipeBackLayout.setEdgeSize(ScreenSize(this).width / 9)
    }

    fun initData(savedInstanceState: Bundle?) {
        var bundle = savedInstanceState ?: intent.extras
        itemObj = bundle?.getParcelable(Constant.DATA)
        if (itemObj == null) {
            finish()
            return
        }
        bundle = Bundle()
        bundle.putInt(Constant.TAB_POSITION, 0)
        bundle.putParcelable(Constant.DATA, itemObj)
        bundle.putString(Constant.LINK_PLAY, itemObj!!.liveStream)
//        bundle.putString(Constant.IMAGE, itemObj!!.image)
        bundle.putString(ConstantTT.PORT, "wss://api.daugiatruyenhinh.com:8088/interactive?")
        if (!itemObj!!.interactive.isNullOrEmpty() && itemObj!!.interactive!!.startsWith("wss://")) {
            bundle.putString(ConstantTT.PORT, itemObj!!.interactive)
        }
//        bundle.putString(InteractiveConstant.PORT, "wss://api.daugiatruyenhinh.com:8089/interactive?")
//        bundle.putString(Constant.TAB_TYPE, itemObj.getType())
//        bundle.putString(Constant.TYPE, itemObj.getType())
        bundle.putBoolean(Constant.IS_RUN_PLAY, true)
        fragmentInteractive = InteractiveLiveStreamFragment()
        fragmentInteractive!!.arguments = bundle
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayoutActivity, fragmentInteractive!!)
        fragmentTransaction.commit()
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            fragmentInteractive!!.checkRefresh()
        }, 0)//200
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        fragmentInteractive?.onActivityResult(requestCode, resultCode, data)
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        fragmentInteractive?.resumePlay()
        fragmentInteractive?.resumeWebview()
    }
}
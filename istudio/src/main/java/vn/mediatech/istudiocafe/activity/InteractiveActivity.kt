package vn.mediatech.istudiocafe.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_interactive.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.fragment.MainInteractiveFragment
import vn.mediatech.istudiocafe.listener.OnCreateViewListener
import vn.mediatech.istudiocafe.listener.OnGetConfigInteractiveListener
import vn.mediatech.istudiocafe.model.ItemInteractiveConfig
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT

class InteractiveActivity : BaseActivity() {
    private var statusBarMargin: Boolean = false

    companion object {
        fun show(
            context: Context?, appID: String?,
            statusBarMargin: Boolean? = false,
            resId: Int? = 0
        ) {
            if (context == null || appID.isNullOrEmpty()) {
                return
            }
            val intent = Intent(context!!, InteractiveActivity::class.java)
            val bundle = Bundle()
            bundle.putString(Constant.APP_ID, appID)
            bundle.putBoolean(Constant.STATUS_BAR_MARGIN, statusBarMargin ?: false)
            resId?.let { bundle.putInt(Constant.IMAGE, it) }
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_interactive)
        init()
        initControl()
    }

    private fun initControl() {
        buttonBack.setOnClickListener {
            finish()
        }
    }

    var mainInteractiveFragment: MainInteractiveFragment? = null
    var appID = ConstantTT.CHANEL_ID
    private fun init() {
        intent?.extras?.let {
            appID = it.getString(Constant.APP_ID, ConstantTT.CHANEL_ID)
            val resBG = it.getInt(Constant.IMAGE, 0)
            statusBarMargin = it.getBoolean(Constant.STATUS_BAR_MARGIN)
            if (0 != resBG) {
                imageBgMain.setBackgroundResource(resBG)
            }
        }
        if (statusBarMargin) {
            setFullStatusTransparent(layoutActionbar)
        }
        startGetConfigInteractive()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        mainInteractiveFragment = MainInteractiveFragment()
//        val bundle = Bundle()
//        bundle.putString(Constant.APP_ID, appID)
//        mainInteractiveFragment!!.arguments = bundle
        fragmentTransaction.add(frameLayout.id, mainInteractiveFragment!!)
        fragmentTransaction.commitNowAllowingStateLoss()
        mainInteractiveFragment?.onCreateViewListener = object : OnCreateViewListener {
            override fun onCreated() {
                mainInteractiveFragment?.checkRefresh()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val list = supportFragmentManager.fragments
        if (list.size > 0) {
            for (item in list) {
                item.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    fun startGetConfigInteractive() {
        ServiceUtilTT.startTimerRequestConfig(
            this,
            appID,
            object : OnGetConfigInteractiveListener {
                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    item: ItemInteractiveConfig?
                ) {
                    isGetConfigFirt = true
                    if (isSuccess) {
                        runOnUiThread {
                            mainInteractiveFragment?.initInteractiveLiveStreamFragment()
                        }
                    }
                }
            },
            10000
        )
    }

    override fun onBackPressed() {
        if (mainInteractiveFragment == null || !mainInteractiveFragment!!.canBack()) {
            super.onBackPressed();
        }
    }

var isGetConfigFirt:Boolean = false
    override fun onResume() {
        super.onResume()
        if (isGetConfigFirt) {
            startGetConfigInteractive()
        }
    }

    override fun onPause() {
        super.onPause()
        ServiceUtilTT.stopRequestConfig()
    }
}
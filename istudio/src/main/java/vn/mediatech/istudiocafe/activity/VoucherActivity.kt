package vn.mediatech.istudiocafe.activity

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.fragment.TabPersonFragment
import vn.mediatech.istudiocafe.listener.OnResultListener
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.ServiceManager
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.voucher.*

class VoucherActivity : BaseActivity() {
    var savedInstanceState: Bundle? = null
    var itemUser: ItemUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voucher_tt)
        this.savedInstanceState = savedInstanceState
        supportFragmentManager.addOnBackStackChangedListener { getListener() }
        initUI()
        initData()
    }

    fun initUI() {
//        setFullStatusTransparent()
//        MyApplication.getInstance().loadDrawableImageNow(this, R.drawable.bg_main, imageBgMain)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun initData() {
        val savedInstanceState = savedInstanceState ?: intent.extras
        if (savedInstanceState != null) {
            itemUser = savedInstanceState.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
        if (itemUser != null) {
            MyApplication.getInstance().dataManager.itemUser = itemUser
        }
        if (savedInstanceState == null) {
            showMyVoucherFragment()
        } else {
            val type = savedInstanceState.getInt(Constant.VOUCHER_TYPE, Constant.MY_VOUCHER)
            if (type == Constant.MY_VOUCHER) {
                showMyVoucherFragment()
            } else if (type == Constant.DETAIL_VOUCHER) {
                showDetailVoucherFragment(savedInstanceState, object : OnResultListener {
                    override fun onResult(isSuccess: Boolean, msg: String?) {
                        showMyVoucherFragment()
                    }
                })
            }
        }
        getVoucherDonated()
    }

    fun getVoucherDonated() {
        val requestParams = RequestParams()
        if (itemUser != null) {
            requestParams.put("userid", "${itemUser!!.id}")
        }
        ServiceManager.getVoucherList(this, ServiceUtilTT.validAPIDGTH(this,Constant.API_VOUCHER_DONATED), requestParams, object : ServiceManager.OnVoucherRequestListener {
            override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
            }

            override fun onResponse(isSuccess: Boolean, message: String?, itemList: ArrayList<ItemVoucher>?) {
                if (isFinishing || isDestroyed || !isSuccess || itemList == null || itemList.size == 0) {
                    return
                }
                runOnUiThread {
                    val itemObj = itemList.get(itemList.size - 1)
                    showDonatedVoucherDialog(itemObj)
                }
            }
        })
    }

    fun showVoucherStoreFragment() {
        val bundle = Bundle()
        bundle.putInt(Constant.VOUCHER_TYPE, Constant.MY_VOUCHER)
        val fragment = VoucherStoreFragment()
        fragment.arguments = bundle
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_right_to_left, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun showMyVoucherFragment() {
        /*val bundle = Bundle()
        bundle.putInt(Constant.VOUCHER_TYPE, Constant.MY_VOUCHER)
        val fragment = VoucherStoreFragment()
        currentFragment = fragment
        fragment.arguments = bundle
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_right_to_left, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()*/
        val fragment = MyVoucherFragment()
        currentFragment = fragment
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun showSearchVoucherFragment() {
        val fragment = SearchVoucherFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_fade_in, R.anim.fragment_fade_out, R.anim.fragment_fade_out, R.anim.fragment_fade_out)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun showDetailVoucherFragment(bundle: Bundle?, onResultListener: OnResultListener? = null) {
        val fragment = DetailVoucherFragment()
        if (bundle != null) {
            fragment.arguments = bundle
        }
        detailVoucFragment = fragment
        fragment.onResultListener = onResultListener
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_right_to_left, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun showLoginFragment() {
        val fragment = TabPersonFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_right_to_left, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun showUseVoucherFragment(bundle: Bundle?) {
        val fragment = UseVoucherFragment()
        if (bundle != null) {
            fragment.arguments = bundle
        }
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_right_to_left, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun showVoucherFilterFragment(bundle: Bundle?, onResultListener: VoucherFilterFragment.OnResultListener? = null) {
        val fragment = VoucherFilterFragment()
        if (bundle != null) {
            fragment.arguments = bundle
        }
        fragment.onResultListener = onResultListener
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_right_to_left, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun showDetailBrandFragment(bundle: Bundle?) {
        val fragment = DetailBrandFragment()
        if (bundle != null) {
            fragment.arguments = bundle
        }
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_right_to_left, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun showUserVoucherFragment(bundle: Bundle?) {
        val fragment = UserVoucherFragment()
        if (bundle != null) {
            fragment.arguments = bundle
        }
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_right_to_left, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right, R.anim.fragment_left_to_right)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment)
        fragmentTransaction.commit()
    }

    fun getListener(): FragmentManager.OnBackStackChangedListener {
        val listener = object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val fragments = supportFragmentManager.fragments
                for (i in 0 until fragments.size) {
                    fragments.get(i).onResume()
                }
            }
        }
        return listener
    }

    fun goBack(isFinish: Boolean) {
        if (isFinish || supportFragmentManager.backStackEntryCount == 0) {
            finish()
            return
        }
        supportFragmentManager.popBackStack()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragments = supportFragmentManager.fragments
        for (i in 0 until fragments.size) {
            fragments.get(i).onActivityResult(requestCode, resultCode, data)
        }
    }

    var currentFragment: Fragment? = null
    var detailVoucFragment: DetailVoucherFragment? = null
    fun refreshData() {
        if (detailVoucFragment != null && detailVoucFragment!!.isVisible) {
            detailVoucFragment?.dismissDonateVoucherFragment()
            goBack(false)
            return
        }
        if (currentFragment != null && currentFragment is MyVoucherFragment) {
            (currentFragment as? MyVoucherFragment)?.refreshData()
        }
    }
    fun dismissDonateVoucherFragment() {
        if (detailVoucFragment != null && detailVoucFragment!!.isVisible) {
            detailVoucFragment?.dismissDonateVoucherFragment()
        }
    }
}
package vn.mediatech.istudiocafe.activity.user;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.Loggers;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnCancelListener;
import vn.mediatech.istudiocafe.listener.OnLoginFormListener;
import vn.mediatech.istudiocafe.listener.OnResultRequestListener;
import vn.mediatech.istudiocafe.model.ItemUser;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.service.RequestParams;
import vn.mediatech.istudiocafe.util.GeneralUtils;
import vn.mediatech.istudiocafe.util.JsonParser;
import vn.mediatech.istudiocafe.util.MyDialog;
import vn.mediatech.istudiocafe.util.ScreenSize;
import vn.mediatech.istudiocafe.util.ServiceUtilTT;
import vn.mediatech.istudiocafe.util.SharedPreferencesManager;
import vn.mediatech.istudiocafe.util.TextUtil;
import vn.mediatech.istudiocafe.zinteractive.app.ConstantTT;

public class UserLoginActivity extends BaseActivity {
    private ImageView buttonBack;
    private TextView buttonRegisterAccount, buttonConfirm,
            buttonForgotPassword;
    private EditText editPhone, editPassword;
    private MyHttpRequest myHttpRequest;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private String loginType = Constant.TYPE_REGISTER_FORM;
    private String accessTokenSocial = "";
    private String userIdSocial = "";
    private String emailSocial = "";
    private ItemUser itemUserTemp = new ItemUser();
    private boolean resultOk = false;
    private String tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login_tt);
        MyApplication.getInstance().getDataManager().setItemUserTemp(null);
        initUI();
        initData();
        initControl();
//        showFormDialog("ThanhNguyen872", "abecd3", null, null, null);
//        showMoreInfoUserDialog();
    }

    private void initUI() {
        setFullStatusTransparent();
        buttonBack = findViewById(R.id.buttonBack);
        editPhone = findViewById(R.id.editPhone);
        editPassword = findViewById(R.id.editPassword);
        buttonConfirm = findViewById(R.id.buttonConfirm);
        buttonRegisterAccount = findViewById(R.id.buttonRegisterAccount);
        buttonForgotPassword = findViewById(R.id.buttonForgotPassword);
//        if (!Constant.IS_TEST) {
        findViewById(R.id.layoutFacebook).setVisibility(View.VISIBLE);
        findViewById(R.id.layoutLoginNomal).setVisibility(View.GONE);
//        findViewById(R.id.layoutFacebook).setVisibility(View.GONE);
//        findViewById(R.id.layoutLoginNomal).setVisibility(View.VISIBLE);
//        } else {
//            findViewById(R.id.layoutFacebook).setVisibility(View.GONE);
//            findViewById(R.id.layoutLoginNomal).setVisibility(View.VISIBLE);
//        }
        TextUtil.setHtmlTextView(getString(R.string.user_form_register), buttonRegisterAccount);
//        setBackgroudActionbarFragment(rootView);
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }
        boolean isRegisterScreen = bundle.getBoolean(Constant.IS_REGISTER, false);
        if (isRegisterScreen) {
            goRegisterScreen();
        }
    }

    private void validForm() {
        String username = editPhone.getText().toString().trim();
        if (username.isEmpty()) {
            editPhone.requestFocus();
            showToast(R.string.msg_username_empty);
            return;
        }
        String password = editPassword.getText().toString().trim();
        if (password.isEmpty()) {
            editPassword.requestFocus();
            showToast(R.string.msg_password_empty);
            return;
        }
        loginType = Constant.TYPE_REGISTER_FORM;
        login(username, password);
    }

    private void login(String username, String password) {
        login(username, password, null, null, null, null, null);
    }

    String id;

    private void login(String username, String password, String gender, String job, String address, String presenterType, String presentervalue) {
        if (!MyApplication.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        showLoadingDialog(true, null, new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequest != null) {
                    myHttpRequest.cancel();
                }
            }
        });
        if (myHttpRequest == null) {
            myHttpRequest = new MyHttpRequest(this);
        } else {
            myHttpRequest.cancel();
        }
        RequestParams requestParams = new RequestParams();
        String api = Constant.API_USER_LOGIN;
        boolean isPostMethod = true;
        if (loginType.equals(Constant.TYPE_REGISTER_FORM)) {
            api = Constant.API_USER_LOGIN;
            requestParams.put("phone", username);
            requestParams.put("password", password);
        } else if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK)) {
           /* if (getApplication().getPackageName().equals("vn.mediatech.istudiocafe")) {
                api = Constant.API_USER_LOGIN_SOCIAL;
            } else {
                api = Constant.API_USER_LOGIN_SOCIAL_NONE_PHONE;
            }*/
            api = Constant.API_USER_LOGIN_SOCIAL_NONE_PHONE;
            requestParams.put("social_id", userIdSocial);
            requestParams.put("email", emailSocial);
            requestParams.put("access_token", accessTokenSocial);
            if (itemUserTemp != null) {
                requestParams.put("fullname", itemUserTemp.getFullname());
            }
        } else if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK_2)) {
            api = Constant.API_USER_LOGIN_FACEBOOK_FORUM;
            requestParams.put("access_token", accessTokenSocial);
            requestParams.put("userID", userIdSocial);
            requestParams.put("email", emailSocial);
            requestParams.put("type", "mb");
            requestParams.put("username", username);
            requestParams.put("password", password);
            requestParams.put("gender", gender);
            requestParams.put("address", address);
            requestParams.put("job", job);
            requestParams.put("id", id);
            requestParams.put("channel", ConstantTT.CHANEL_ID);
            requestParams.put("presenter_type", presenterType);
            requestParams.put("presenter_value", presentervalue);
            if (itemUserTemp != null) {
                requestParams.put("first_name", itemUserTemp.getFullname());
                requestParams.put("last_name", itemUserTemp.getFullname());
            }
        } else if (loginType.equals(Constant.TYPE_REGISTER_GOOGLE)) {
            api = Constant.API_USER_LOGIN_SOCIAL;
            requestParams.put("social_id", userIdSocial);
            requestParams.put("email", emailSocial);
            requestParams.put("access_token", accessTokenSocial);
        }
        requestParams.put("type", loginType);
        myHttpRequest.request(isPostMethod, ServiceUtilTT.validAPITT(this, api), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                showDialogUser(getString(R.string.msg_network_error));
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                handleDataLogin(responseString, username, password);
                            }
                        });
                    }
                });
    }


    private void handleDataLogin(String responseString, String username, String password) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        if (errorCode == Constant.CODE_ENTER_PHONE) {
            Bundle bundle = new Bundle();
            bundle.putString(Constant.TYPE, loginType);
            gotoActivityForResult(UserEnterPhoneActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        if (errorCode == Constant.CODE_VERIFY_OTP) {
            JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
            int expire = JsonParser.Companion.getInt(resultObj, "expire");
            String phone = JsonParser.Companion.getString(resultObj, "phone");
            Bundle bundle = new Bundle();
            bundle.putString(Constant.TYPE, loginType);
            bundle.putInt(Constant.DATA, expire);
            bundle.putString(Constant.PHONE, phone);
            gotoActivityForResult(UserOTPConfirmActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        if (errorCode == Constant.CODE_REGISTER_FORM) {
            Bundle bundle = new Bundle();
            bundle.putString(Constant.TYPE, loginType);
            gotoActivityForResult(UserRegisterActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
        if (errorCode != Constant.SUCCESS) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
        ItemUser itemUser = JsonParser.Companion.parseItemUserLogin(resultObj);
        if (itemUser == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        if (Constant.IS_SHOW_FORM_LOGIN) {
            Constant.IS_SHOW_FORM_LOGIN = false;
            itemUser.setLoginStatus("1");
        }
        if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK_2) && "1".equals(itemUser.getLoginStatus())) {
//            if (itemUser.getUserNameForum() == null || itemUser.getPasswordForum() == null) {
//                return;
//            }
            id = itemUser.getId().toString();
            MyApplication.getInstance().getDataManager().setItemUser(null);
            showFormDialog(itemUser.getUserNameForum(), itemUser.getPasswordForum(), itemUser.getGender(), itemUser.getAddress(), itemUser.getJob());
            return;
        }
        SharedPreferencesManager.saveUser(this, itemUser.getPhone(), itemUser.getAccessToken());
        resultOk = true;
        runOnUiThread(() -> {
            if (isShowDialogForm) {
                isShowDialogForm = false;
                if (isShowMessenger) {
                    isShowMessenger = false;
                    String str =
                            "Tài khoản của tôi là " + itemUser.getFullname() + ", mã số " + itemUser.getId() + ". Chương trình vui lòng xác thực tài khoản giúp tôi!";
                    GeneralUtils.Companion.coppyToClipboard(this, str);
                    //                    progressBar.setVisibility(View.VISIBLE);
//                    GeneralUtils.Companion.openMessenger(UserLoginActivity.this);
                    GeneralUtils.Companion.openMessenger(this);
                    GeneralUtils.Companion.setShowLogin(true);
                } else {
                    GeneralUtils.Companion.setShowLogin(true);
                }
            }
            finish();
        });
    }

    boolean isShowMessenger = false;
    boolean isShowDialogForm = false;

    private void showFormDialog(String mUserName, String mPassword, String mGender, String mAddress, String mJob) {
        UserRegisterFormFragment userRegisterFormFragment = new UserRegisterFormFragment();
        isShowDialogForm = true;
        Bundle bundle = new Bundle();
        bundle.putString(Constant.USERNAME, mUserName);
        bundle.putString(Constant.PASSWORD, mPassword);
//        bundle.putString(Constant.GENDER, mGender);
//        bundle.putString(Constant.ADDRESS, mAddress);
//        bundle.putString(Constant.JOB, mJob);
        userRegisterFormFragment.setArguments(bundle);
//        GeneralUtils.Companion.showFragment(getSupportFragmentManager(),userRegisterFormFragment,R.id.frameLayoutDialog,false,false);
        userRegisterFormFragment.show(getSupportFragmentManager(), "UserRegisterFormFragment");
        userRegisterFormFragment.setOnShowDismissListener(new UserRegisterFormFragment.OnShowDismissListener() {

            @Override
            public void onDismiss(boolean isSuccess, boolean isShowMessengerM, @org.jetbrains.annotations.Nullable String userName, @org.jetbrains.annotations.Nullable String password, @org.jetbrains.annotations.Nullable String gender, @org.jetbrains.annotations.Nullable String job, @org.jetbrains.annotations.Nullable String address, @org.jetbrains.annotations.Nullable String presenterType, @org.jetbrains.annotations.Nullable String presenterValue) {
                if (isSuccess) {
                    isShowMessenger = isShowMessengerM;
                    login(userName, password, gender, job, address, presenterType, presenterValue);
//                    Dialog dialogMoreInfo = showMoreInfoUserDialog();
//                    dialogMoreInfo.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            Loggers.e("INFOLOGIN", userName + "/" + password + "/" + gender + "/" + job + "/" + address + "/" + mPresenterType + "/" + mPresenterValue);
//                            login(userName, password, gender, job, address, mPresenterType, mPresenterValue);
//
//                        }
//                    });
                } else {
                    isShowDialogForm = false;
                }
//                else {
//                    login(mUserName, mPassword, mGender, mJob, mAddress);
//                }
            }

            @Override
            public void onShow() {

            }
        });
    }

    private void validButtonConfirm() {
        String phone = editPhone.getText().toString().trim();
        if (phone.length() < 10) {
            buttonConfirm.setEnabled(false);
            return;
        }
        String password = editPassword.getText().toString().trim();
        if (password.length() < 6) {
            buttonConfirm.setEnabled(false);
            return;
        }
        buttonConfirm.setEnabled(true);
    }

    private void initControl() {
        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                finish();
            }
        });
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                validForm();
            }
        });
        buttonRegisterAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goRegisterScreen();
            }
        });
        findViewById(R.id.layoutLoginFacebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                loginFacebook();
            }
        });
        findViewById(R.id.layoutLoginFacebookBottom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                loginFacebook();
            }
        });
        buttonForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                Bundle bundle = new Bundle();
                bundle.putString(Constant.TYPE, Constant.TYPE_FORGOT_PASSWORD);
                bundle.putString(Constant.PHONE, editPhone.getText().toString().trim());
                gotoActivityForResult(UserEnterPhoneActivity.class, bundle, Constant.CODE_LOGIN);
            }
        });
        initAdapterDemo();
    }

    public void autoLogin() {
        showLoadingDialog(false, null);
        ServiceUtilTT.autoLogin(UserLoginActivity.this, new OnLoginFormListener() {
            @Override
            public void onLogin(boolean hasLogin) {
                hideLoadingDialog();
                if (hasLogin) {
                    resultOk = true;
                    finish();
                }
            }
        });
    }

    private void goRegisterScreen() {
        hideKeyboard(editPhone);
        Bundle bundle = new Bundle();
        bundle.putString(Constant.PHONE, editPhone.getText().toString().trim());
        gotoActivityForResult(UserRegisterActivity.class, bundle, Constant.CODE_LOGIN);
    }

    private void loginFacebook() {
        if (1 == 1) {
            loginFacebook2();
            return;
        }
        showLoadingDialog(false, null);
        if (mAuth == null) {
            mAuth = FirebaseAuth.getInstance();
        }
        if (callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
        }
        loginType = Constant.TYPE_REGISTER_FACEBOOK;
        userIdSocial = "";
        emailSocial = "";
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        exception.printStackTrace();
                        showToast(getString(R.string.msg_login_error) + " (100): " + exception.getMessage());
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile",
                "email"));

    }

    private void loginFacebook2() {
        showLoadingDialog(false, null);
        /*if (mAuth == null) {
            mAuth = FirebaseAuth.getInstance();
        }*/
        if (callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
        }
        loginType = Constant.TYPE_REGISTER_FACEBOOK_2;
        userIdSocial = "";
        emailSocial = "";
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        exception.printStackTrace();
                        showToast(getString(R.string.msg_login_error) + " (100): " + exception.getMessage());
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile",
                "email"));
    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        if (accessToken == null) {
            showToast(getString(R.string.msg_login_error) + " (101)");
            return;
        }
        accessTokenSocial = accessToken.getToken();
        userIdSocial = accessToken.getUserId();
        if (MyApplication.getInstance().isEmpty(accessTokenSocial)) {
            showToast(getString(R.string.msg_login_error) + " (102)");
            return;
        }
        Loggers.e("MY_CHECK_LOGIN Facebook", accessTokenSocial);
//        START: GET USER EMAIL
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Loggers.e("MY_CHECK_LOGIN Facebook_object", object.toString());
                        userIdSocial = JsonParser.Companion.getString(object, "id");
                        emailSocial = JsonParser.Companion.getString(object, "email");
                        String fullNameSocial = JsonParser.Companion.getString(object, "name");
                        String avatar = "https://graph.facebook.com/" + userIdSocial + "/picture" +
                                "?type=large";
                        if (MyApplication.getInstance().isEmpty(emailSocial)) {
                            emailSocial = userIdSocial + "@mediatech.vn";
                        }
                        itemUserTemp.setSocialId(userIdSocial);
                        itemUserTemp.setAvatar(avatar);
                        itemUserTemp.setFullname(fullNameSocial);
                        itemUserTemp.setEmail(emailSocial);
                        MyApplication.getInstance().getDataManager().setItemUserTemp(itemUserTemp);
                        login(null, null);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,name,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
//        END: GET USER EMAIL
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Loggers.e("MY_CHECK_LOGIN", "firebaseAuthWithGoogle id:" + acct.getId());
        Loggers.e("MY_CHECK_LOGIN", "firebaseAuthWithGoogle token:" + acct.getIdToken());
        accessTokenSocial = acct.getIdToken();
        if (MyApplication.getInstance().isEmpty(accessTokenSocial)) {
            showToast(getString(R.string.msg_login_error) + " (102)");
            return;
        }
        Loggers.e("MY_CHECK_LOGIN accessTokenGoogle", accessTokenSocial);
        login(null, null);
    }

    @Override
    public void onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CODE_LOGIN_GOOGLE) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
            }
            return;
        }
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (resultCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
            MyApplication.getInstance().getDataManager().setItemUserTemp(null);
            if (data != null) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    int type = bundle.getInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SCREEN);
                    if (type == Constant.RESULT_CODE_LOGIN_SUCCESS) {
                        resultOk = true;
                        finish();
                    }
                }
            }
//            finish();
        }
    }

    @Override
    public void finish() {
        if (resultOk) {
            Intent intent = new Intent();
//        Bundle bundle = new Bundle();
//        intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideLoadingDialog();
    }

    private String mPresenterType, mPresenterValue;

    private MyDialog showMoreInfoUserDialog() {
        if (isDestroyed() || isFinishing()) {
            return null;
        }
        MyDialog dialog = new MyDialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogAnimationLeftRight;
        dialog.getWindow().setAttributes(lp);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.layout_dialog_user_know_app);
        ScreenSize screenSize = new ScreenSize(this);
        dialog.getWindow().setLayout(screenSize.getWidth() * 10 / 10, WindowManager.LayoutParams.MATCH_PARENT);
        View layoutRoot = dialog.findViewById(R.id.layoutRoot);
        EditText editPresenterID = dialog.findViewById(R.id.editPresenterID);
        EditText editOther = dialog.findViewById(R.id.editOther);
        ImageView buttonCheck = dialog.findViewById(R.id.buttonCheck);
        TextView textName = dialog.findViewById(R.id.textName);
        ImageView imageAvatar = dialog.findViewById(R.id.imageAvatar);
        TextView buttonSkip = dialog.findViewById(R.id.buttonCancel);
        TextView buttonNext = dialog.findViewById(R.id.buttonConfirm);
        Spinner spinnerTv = dialog.findViewById(R.id.spinnerTV);
        Spinner spinnerSocial = dialog.findViewById(R.id.spinnerSocial);
        Spinner spinnerType = dialog.findViewById(R.id.spinnerType);
        View layoutPresenter = dialog.findViewById(R.id.layoutPresenter);
        View layoutTV = dialog.findViewById(R.id.layoutTV);
        View layoutSocial = dialog.findViewById(R.id.layoutSocial);
        View layoutOther = dialog.findViewById(R.id.layoutOther);
        View progressBar = dialog.findViewById(R.id.progressBar);
        layoutRoot.setClipToOutline(true);
        layoutRoot.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                float cornerRadiusDP = 15f;
                outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), cornerRadiusDP);
            }
        });
        initSpinerTV(spinnerTv, buttonNext);
        initSpinerSocial(spinnerSocial, buttonNext);
        buttonCheck.setOnClickListener(v -> {
            String userId = editPresenterID.getText().toString().trim();
            if (userId.equals("")) {
                Toast.makeText(this, "Mã giới thiệu không đúng", Toast.LENGTH_SHORT).show();
                editPresenterID.requestFocus();
                return;
            }
            getName(textName, imageAvatar, userId, progressBar);
        });
        editPresenterID.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                getName(textName, imageAvatar, editPresenterID.getText().toString().trim(), progressBar);
            }
            GeneralUtils.Companion.hideKeyboard(UserLoginActivity.this);
            return true;
        });

        editPresenterID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textName.setVisibility(View.GONE);
                imageAvatar.setVisibility(View.GONE);
                if (editPresenterID.getText().toString().trim().length() >= 1) {
//                    getName(textName, imageAvatar, editPresenterID.getText().toString().trim(), progressBar);
//                    buttonNext.setEnabled(true);
                } else {
                    buttonNext.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editOther.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editOther.getText().toString().trim().length() >= 1) {
                    buttonNext.setEnabled(true);
                } else {
                    buttonNext.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ArrayList<View> listLayout = new ArrayList(Arrays.asList(layoutSocial, layoutTV, layoutOther, layoutPresenter));
        initSpinerType(spinnerType);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent != null) {
                    GeneralUtils.Companion.hideKeyboard(UserLoginActivity.this);
                    type = parent.getItemAtPosition(position).toString();
                    if (type.equals(DEFAULT_STRING)) {
                        type = null;
                        showLayout(layoutRoot, listLayout);
                    } else if (type.equals(TYPE_PRESENTER)) {
                        showLayout(layoutPresenter, listLayout);
                    } else if (type.equals(TYPE_SOCIAL)) {
                        showLayout(layoutSocial, listLayout);
                        if (social != null) {
                            buttonNext.setEnabled(true);
                        } else {
                            spinnerSocial.performClick();
                            buttonNext.setEnabled(false);
                        }
                    } else if (type.equals(TYPE_TV)) {
                        showLayout(layoutTV, listLayout);
                        if (social != null) {
                            buttonNext.setEnabled(true);
                        } else {
                            spinnerTv.performClick();
                            buttonNext.setEnabled(false);
                        }
                    } else if (type.equals(TYPE_OTHER)) {
                        showLayout(layoutOther, listLayout);
                        if (other != null) {
                            buttonNext.setEnabled(true);
                        } else {
                            editOther.requestFocus();
                            buttonNext.setEnabled(false);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        buttonSkip.setOnClickListener(v ->
                dialog.dismiss());
        buttonNext.setOnClickListener(v -> {
            if (type.equals(TYPE_PRESENTER)) {
                mPresenterType = TYPE_PRESENTER_VALUE;
                String id = editPresenterID.getText().toString().trim();
                if (GeneralUtils.Companion.isEmpty(id)) {
                    showKeyboardFocus(editPresenterID);
                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Mã giới thiệu không đúng. Vui lòng nhập và kiểm tra lại.");
                    return;
                } else {
                    ServiceUtilTT.getNameUser(UserLoginActivity.this, id, new OnResultRequestListener() {
                        @Override
                        public void onResult(boolean isSuccess, @org.jetbrains.annotations.Nullable String msg, @org.jetbrains.annotations.Nullable String avatar) {
                            runOnUiThread(() -> {
                                if (isSuccess) {
                                    dialog.dismiss();
                                    mPresenterValue = id;
                                } else {
                                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Mã giới thiệu không đúng. Vui lòng nhập lại!");
                                    editPresenterID.requestFocus();
                                }
                            });
                        }
                    });
                    return;
                }
            } else if (type.equals(TYPE_SOCIAL)) {
                mPresenterType = TYPE_SOCIAL_VALUE;
                if (GeneralUtils.Companion.isEmpty(social)) {
                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Vui lòng chọn mạng xã hội.");
                    spinnerSocial.requestFocus();
                    return;
                }
                mPresenterValue = social;
            } else if (type.equals(TYPE_TV)) {
                mPresenterType = TYPE_TV_VALUE;
                if (GeneralUtils.Companion.isEmpty(tv)) {
                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Vui lòng chọn đài truyền hình.");
                    spinnerTv.requestFocus();
                    return;
                }
                mPresenterValue = tv;
            } else if (type.equals(TYPE_OTHER)) {
                mPresenterType = TYPE_OTHER_VALUE;
                if (GeneralUtils.Companion.isEmpty(editOther.getText().toString().trim())) {
                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Bạn vui lòng nhập nội dung khác.");
                    return;
                }
                mPresenterValue = editOther.getText().toString().trim();
            }

            dialog.dismiss();
        });
        try {
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dialog;
    }

    MyHttpRequest myHttpRequestGetName;

    private void getName(TextView textName, ImageView imageAvatar, String userId, View progress) {
        if (myHttpRequestGetName != null) {
            myHttpRequestGetName.cancel();
        }
        progress.setVisibility(View.VISIBLE);
        myHttpRequestGetName = ServiceUtilTT.getNameUser(this, userId, (isSuccess, msg, avatar) -> {
            runOnUiThread(() -> {
                progress.setVisibility(View.GONE);
                if (!isSuccess) {
//                    GeneralUtils.Companion.showToast(this, msg);
                    textName.setText(msg);
                    textName.setTextColor(Color.RED);
                    imageAvatar.setVisibility(View.GONE);
                    textName.setVisibility(View.VISIBLE);
                } else {
                    textName.setTextColor(Color.parseColor("#4f8cff"));
                    textName.setText(msg);
                    textName.setVisibility(View.VISIBLE);
                    if (!GeneralUtils.Companion.isEmpty(avatar)) {
                        imageAvatar.setVisibility(View.VISIBLE);
                        GeneralUtils.Companion.loadImageCircle(this, imageAvatar, avatar, R.drawable.avatar_placeholder);
                    } else {
                        imageAvatar.setVisibility(View.GONE);
                    }

                }
            });
        });
    }

    private String DEFAULT_STRING = "Chọn";
    private String other, social, type;

    private void showLayout(View layout, ArrayList<View> layoutArrayList) {
        if (layoutArrayList == null || layoutArrayList.size() == 0 || layout == null) {
            return;
        }
        for (View item : layoutArrayList) {
            if (!item.equals(layout)) {
                item.setVisibility(View.INVISIBLE);
            } else {
                item.setVisibility(View.VISIBLE);
            }
        }
    }

    private void initSpinerTV(Spinner spinner, View buttonNext) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GeneralUtils.Companion.hideKeyboard(UserLoginActivity.this);
                if (parent != null) {
                    tv = parent.getItemAtPosition(position).toString().trim();
                    if (tv.equals(DEFAULT_STRING.trim())) {
                        tv = null;
                        buttonNext.setEnabled(false);
                    } else {
                        buttonNext.setEnabled(true);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayList<String> places = new ArrayList(
                Arrays.asList(
                        DEFAULT_STRING,
                        "Hà Nội",
                        "Bắc Giang",
                        "Bắc Kạn",
                        " Bắc Ninh",
                        " Bình Phước",
                        "Hà Nam",
                        "Hưng Yên",
                        "Khánh Hòa",
                        "Lạng Sơn",
                        "Lào Cai",
                        "Nghệ An",
                        "Ninh Bình",
                        "Quảng Ninh",
                        "Quảng Trị",
                        "Thái Bình",
                        "Thái Nguyên",
                        "Thanh Hóa",
                        "Thừa Thiên Huế",
                        "Tiền Giang",
                        "Trà Vinh",
                        "Tuyên Quang",
                        "Vĩnh Long",
                        "Vĩnh Phúc",
                        "Khác"
                )
        );
        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivity.this, R.layout.item_spinner_user, places);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop);
        spinner.setAdapter(dataAdapter);
    }

    private final String TYPE_PRESENTER = "Người giới thiệu";
    private final String TYPE_TV = "Đài truyền hình";
    private final String TYPE_SOCIAL = "Mạng xã hội";
    private final String TYPE_OTHER = "Khác";
    private final String TYPE_PRESENTER_VALUE = "user";
    private final String TYPE_TV_VALUE = "tv_station";
    private final String TYPE_SOCIAL_VALUE = "social";
    private final String TYPE_OTHER_VALUE = "other";

    private void initSpinerSocial(Spinner spinner, View buttonNext) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent != null) {
                    GeneralUtils.Companion.hideKeyboard(UserLoginActivity.this);
                    social = parent.getItemAtPosition(position).toString().trim();
                    if (social.equals(DEFAULT_STRING.trim())) {
                        social = null;
                        buttonNext.setEnabled(false);
                    } else {
                        buttonNext.setEnabled(true);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayList<String> categories = new ArrayList();
        categories.add(DEFAULT_STRING);
        categories.add("Facebook");
        categories.add("Instagram");
        categories.add("YouTube");
        categories.add("TikTok");
        categories.add("Twitter");
        categories.add("Khác");
        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivity.this, R.layout.item_spinner_user, categories);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop);
        spinner.setAdapter(dataAdapter);
    }

    private void initSpinerType(Spinner spinner) {
        ArrayList<String> categories = new ArrayList();
        categories.add(DEFAULT_STRING);
        categories.add(TYPE_TV);
        categories.add(TYPE_SOCIAL);
        categories.add(TYPE_PRESENTER);
        categories.add(TYPE_OTHER);
//        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivity.this, android.R.layout.simple_spinner_item, categories);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivity.this, R.layout.item_spinner_user, categories);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop);
        spinner.setAdapter(dataAdapter);
    }

    private void initAdapterDemo() {
        RecyclerView recyclerView = findViewById(R.id.rcvDemo);
        if (!Constant.IS_DEMO) {
            recyclerView.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.VISIBLE);
        ArrayList<ItemUser> list = new ArrayList<>();
        list.add(new ItemUser(1411, "Tiến Minh", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYmFkOWNkMjIwMTdlZTdmZDZmNWVmNjZiYjZhODBlMDM3NWJiZmUzZWExM2M3YmMwMzJhNmIxOTEwODgxZTFjOWFhMDU3NTc0OGQ0YjY4MDkiLCJpYXQiOjE2NDAxNjY3MjQuMTk3NDc5LCJuYmYiOjE2NDAxNjY3MjQuMTk3NDg2LCJleHAiOjE2NzE3MDI3MjQuMTc3NDA2LCJzdWIiOiIxNDExIiwic2NvcGVzIjpbXX0.MpqXzHb0ZlURXf7J74LVDDwM8LO5hthk3xKVFcA20uorPXX0dYOL2bnbbiBhFKptFP44vF8RkLyXyRKDjHmJaxYxiaDoPcXhCyy2OGdYa8T4s_K1AnOLzPxTqVvDkTaoOYwuOur_OezZuIIt8oVCBzTCJVvFouoivX8pZWYeyhTaySA8yg_8DV5aVI0zm2QKai8fVgwyBpTJ0O03k6XxsIJO-kobHEO_G7gL2Y6nDYiL8i91Km4qKJ__VghelpF2fbpNT6qStn6EiaQP3nhCInw8YR4hlhrjL4mQ9KvDnxw5GDcZpY7X0C-gpePpLMfifGhfAOsAdUNTfdbeeiDR2yw45SR8x2KMMz7XKYAAM6U4iukrfCIR6DngPyMZB8u-IOUf8hyQClZi16xE9Xtu7Rom2p9fYaGDsMZwuGg9pmGgd2IIpoBeeahu6BmXTbC0lZVwH0PNGDdtaq_qKMPTPF6GvlUoUZ5Uexx9FQxSZp_0Jowx6kJ1cn4_fMgdnruK7rveiyAdeVInEoI41OWZD0RvwTpHjmeR-nbewVIE3Ky8e1oUQja5A8Igg8YI5SpB4WlMKoqc-5-X1UNaFc5F81QMW8PVwxX_Mh0f98dIAS8q48V8n_lmBCKCaOcE9ceQM3OA38HARp0svXGWw7k5NB90kbQFnn9zxzAwQnW5BAw"));
        list.add(new ItemUser(1413, "Huy Hoàng", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODEwYTdkNTUyMGQwYmM5ZTIwOGY4YjIxMjQ1ZjM3OTQ2NjE1OGVjZWFmMWFjNWZkM2IxNjY0MjE5Nzk2OGMyNDFiZDE4ODdkZDVkMTUyYjAiLCJpYXQiOjE2NDAxNjY4NTYuNzg3NTgzLCJuYmYiOjE2NDAxNjY4NTYuNzg3NTg5LCJleHAiOjE2NzE3MDI4NTYuNzgwNjgsInN1YiI6IjE0MTMiLCJzY29wZXMiOltdfQ.GJtkAY5hUNB4aQh9sTkEP_F_xp12z1tdw6eWJ40N4bkHJ46oIqb4hKwwKbmrJSl6yvEEBsT0zm_JXTagu8wXlM16um2kzy3cUav7lsaRtmZfcjSyb45WRph89A4plzkyDTXaQr0BlORPjuNPbyhwfgn3vK_38U3XRJB5NXNS_cDoP5Qjb9xBknLbsZLZSgcV3-omzSuza9VDX5b4stg3h7dDvMrzXPCBOkXpCI-WmzB1n2soFY_6X434alZNbuMeb-ULJRVaZUa2I3wybeU7lsC41ROLosSJDnOfUtNOFK9vj7Rof_HBeT3McACOfz08ntB3Eoyc6eITy6R2oWKDA6YlsKM4FYJx1hPliK1zLJ68vj3UFxW7-ubRCOnzCOsiMVzkPffyt9KvPkBu4dOzwgWRhf4jemUd7bpOQfDeePI1W3jlJxigY0Rxx86p-QYuImtR6aI0oG4JEvCbKZu3GExjP_oObZHtxAOtwfHhfdf3ojqvXGQIMMDfH1STafTnJyd6vVpGRipInSK3lEnM4GqmVT1IjFld1AcaHvc3xCvcyEpole9UAD0U33hgTwnRKkGhYv74uQOavKUWR_tZp__epvSUd5SF7py9jgjxk2TBDGoBAnGOnf00AJwHJfM5MYhDkB_ELbkmfb4VqxVcX0wbm6zaW4i__FWSkMzkXaw"));
        list.add(new ItemUser(1414, "Thành Công", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOThhMmFiNzBiNDJmMjBhZjQ1MzMzNGI5MGRjYzFjMjUzYjZkNTVmNjc0NzkxNTcxNTllNGVlMzk0Y2VkOWQ2MGMxMjVjNDBhYjlkNjg0ZTciLCJpYXQiOjE2NDAxNjY5OTIuNjc4NzczLCJuYmYiOjE2NDAxNjY5OTIuNjc4Nzc3LCJleHAiOjE2NzE3MDI5OTIuNjcxNjkyLCJzdWIiOiIxNDE0Iiwic2NvcGVzIjpbXX0.yuQPhBywqJI_FErjiSqU0vBxzt4XNjdFINJrRlJzjYnF67z4dxe7VeR0e7piG7eIvUiwRCYLPxVg6zYTOLudEedUDsWKZv1HoDVu2WkgJcmTGtxgZIy-O9wk7K58KtBFzlkDYQq1h2HxUzlnW7tplC3qvQP0xCIBjxSt7SYXpWd_uqJpHVH7FA8FR3oF1jY9u4RYJuPCd-EeQfpmapGE5UvINYLc-owvOBCP5-Jzjfmx2FfXUZ_bGGv3JNafUWU5e-eIuuPHiQu2x2p_7cGj5tkpxk7w6urDrob_WfddnqP5Xrzxu6HPJUv1i_RF_elfe7hiibYOlzag4EJaRvH3bkeOO9qaFcyYJhjS-akaGfczPIVDKGAb3DMDpubwRPWC7JdGjagNRE6LfkBCsq3I2eo_KLmmFPiWt5vQiUFI4VmgDv460veVR95Wo9o9hxvnUhbTNiTgF0VQCy526dWO9k7FFb1FPkjzA-NjBBUKkxNsGAWn91UClPWxc2ThxblMgY7aKZ3DHW6jHAD8ceumc9HK3SuBlNAjMFx3Jmwa0isVA6pX8_DqDUYur0GDM_WcNRsGttyC0jEpziUcT9XcUPErectIjHlJSQ8J8LGS847wUi_2dvp1SJs5XFajjeJ0PPbMvNvP3gA0nbyLbxPYJjd_iWEKmBMfX45jyXDogP0"));
        list.add(new ItemUser(1415, "Tấn Phát", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjgwNTRlYWUyMWU4MjkzMzgyZDUyMjFkZjNkNjZhMGIyNmQ4NzMwZTA5YTUwM2MwOGZmMTg4NTBkZDExYmQwNDUyNTQzNDE2MmFmMmI4NWIiLCJpYXQiOjE2NDAxNjcwOTUuODY0NzE2LCJuYmYiOjE2NDAxNjcwOTUuODY0NzIzLCJleHAiOjE2NzE3MDMwOTUuODU3ODIyLCJzdWIiOiIxNDE1Iiwic2NvcGVzIjpbXX0.y2D1XY2Y5aDPK_hKOHKrCkWrWaG-lWiaXN0vxOfXchVHtE65DlUxXceQngp6MY9rhE_xcnS5gcxlrKZ8W5bIU8Ng6N1I0wjoQwJrpRY_-PAr_uS-bp_WI4uKObzyn4Iqj5FMtFwNLgrI7yKRbGq_J8pHQg2n9NCyx-xSgFtoh-TldcwL9dIHICjJ53u_7PEXzA_Ibw4KrSnL_p3bPrNR3Ieorjgv0-yYtNgkJhYzr7vY90wKJu9LZWO7Uht8qiMc9dnEH28i7jkr3xPhAHi0tGYLebL5JH8EZbTkHz3sn4uvfS8uqJCvOXVvkCsunO--ohkX3Y6O-CGv_oJ9WOu10kBgI_bTYeQpZi57Xpv1R79vyUUwHVQxHRQoTVpGBriRMbd4TrKQosVntWHRMmIIotqYeUnjnUt7e70WUgRePM4chQRDpbbDIpjXAdR4iel8_0imiqIh8UBirmp3yN5DXUUWAhuYnXzClKRRbwN7Zoh47AV4LuqGHkrKvOEMnbUhvEJmcxJeUtmivyleu2eU8kJOD59aY4tiuLKGZpZQDwF2EyFkWNiXyq-nFHxCr-wWq_VbD8j8YbrkbawPQICWUWXqNBN1GYoeE1-CzXnevS3JqSvQ304rlyiazthmAqw-fPneg1G4N6yTfee62xthNSHJrF_pGZI9KC71zY9vTq0"));
        list.add(new ItemUser(1412, "Hùng Dũng", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNGQ2OWNjYjkxNDU0MTkwZDU1MDdhNGY0MTJhZmQ3ZGMwNjY5OWFjMWNjZGEyMmM1Y2Q1NjFjMDQxNzg3ZWNhYThiMmQ2NWVhYWFlYzM2MjIiLCJpYXQiOjE2NDAxNjcyMDcuMDg5NTU1LCJuYmYiOjE2NDAxNjcyMDcuMDg5NTYyLCJleHAiOjE2NzE3MDMyMDcuMDgyODA2LCJzdWIiOiIxNDEyIiwic2NvcGVzIjpbXX0.dZrpE6Nq70z1Al9wvmxzpC04J5wNX77mu3GI5Dq8Dqhu3yX7UXerfKpJ5eIrmRsnTk894Mj1YFcexhkhF5t_6KgTzxwXoaqPBK8vZvN3kORExj2tJC-QDyotOUxFw7NLB7vFoteguvN0wF_6nR5WTUHhWdFFwwgp1Ls2xjwtnGrF2ZqMskCSfOv5FhfX6vK2U7TU0RjkvnoU47xe0rdPcJOMpKbg1lfdzCYmrB8xzCBoupSq72DgMIIfNtmXr0Ec7RoIwHMU9A5Unp1xsVMeKMPB59gL-L_mvrE0tmfOdvnpeSldv6XYRSJG3P86J1pYjNS5dUpf1V3CpMkeUd-Y6Bf6WX2fbaTe3lcT9DxyJVD4Wg1eBNxvWV8sF605RERJ27ke8S0OaP4SSy1mlCYx41tZ85y_FU8NzxwdMx3gLn5QzA9tt1uAPL1sEadS8AuLhnC4MsVKkIJS_COkc79QS7c-6pxUxyYCxiwhwt8p1MAXuATEYSMIcWqcidI3wHCNvl4R36q32qu9uUbHrLNMvKEFGlLb5t1tO8Y9ewYounZLJddo8jQXilQ8Chp1UOO5J2DHd8E8t79C5xHXqzem-uQEulWFDfotmYDqCEIRYwTVxaEGhWYHCbl8aEx-uo0_GpBPwbQStRlw8ylZQxW_L2ouF5ZkRLP5bekQLr4YkCg"));
        list.add(new ItemUser(1465, "Thuỷ tiên", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZGY3YjJkYmMzNTY5Njc1MDMyNmE5MjQwNzM4M2MxYjI0NTVkODdkYjQ5N2VmYjJmNzhmODE0NTZkN2MwYmMyN2JkNTVlMTcxOWEwNWEzMTMiLCJpYXQiOjE2NDAxNjUxNzEuMjY4OTc3LCJuYmYiOjE2NDAxNjUxNzEuMjY4OTg0LCJleHAiOjE2NzE3MDExNzEuMjYxNjE2LCJzdWIiOiIxNDY1Iiwic2NvcGVzIjpbXX0.ZzGe9oZw_MerHcUUE8gtGYXrk_jTqJLxhJXjVITEyEMIavNhIWjEmVAByWkArtSTsWmZaRRJtSR-EYqQoYiLsvGFiJRzggjZecH0UYAKZbw8hM1RgKI6K3Vd9lN_O-4JbIYQ8fzHmln_wP_2d3Yc6GV0HtQ-zI8iy1XZJinsrES63Hbd8J9pCbbPw3PeWCwsGWvhypZXUCW0OofFhalyT5LUN3SzZrtDIxRdRHC3JDlNXrrbWLxDeWuQXQSn_Ne9LocJsOvNLeaC5leuGgImnLzv3HBm07HVRiMJOGcINtnvKKO7CbIq1WjAeUa8rW9t69XHP4mc6wYCssykY4ooNb-dGWtWe8RgTFNX-koLaU0rOWtgJAOk61A_VskwOMFOpU1-4O_BL9ROm6Ysgx4mK1WonQ7mfs7d5uM8IRXn_rPzauatIfvRHculxPbZUFM0Kt4h9kRcS49Gct7OfFrLRBeE_B1AgxJjNuIAKV1m2dmMohfbQUXkdrqSkJYrB5A8RofE6ihZFeYwnWqA0ecrlUSKnXlUPkb4TtE3lLmi0wr-N9K05oYdFvxx66-w6UI1fV-v6WAVg0FiClcTOXBgdCC2-pCKB9wGwN6yRbELHvhv_bbnQH9883lmZtYw7Bluq_Zq4HgJBKuUlF9wlKpq_3madW-ehk_CuEirOoGEkwg"));
        list.add(new ItemUser(1463, "Khánh Huyền", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZDY0MTQxOTFlMzVmM2ZiZGUxNjdmOWZlNjRkZjlkODdlMjIwN2EzYjRiMzk4ZjA2NWZhZmE3ODAyODk4OTNmYmE5NWRlZjVlN2E0ZTQ0MDciLCJpYXQiOjE2NDAxNjYyMTkuMDU0MDU2LCJuYmYiOjE2NDAxNjYyMTkuMDU0MDYzLCJleHAiOjE2NzE3MDIyMTkuMDQ3OTkyLCJzdWIiOiIxNDYzIiwic2NvcGVzIjpbXX0.ynYTWrAKd9Xnxy6oJntyCseb4i83QytSghiu0ay8hQzObgxLQ8byxRL3pJfCGoXF2p5rQxGP66_Xl7FjOP8ewPslL7RZCFaV8yxF4-0_nNAHYhfvEhy8oeKfbToproEGsXfdGIKGqnaEoEozC2KvgZqwFGjrRA8Uez0aRROINc0p7l80S3-xvGPQLO8KUDEYWFsiVsld9z_6xfAN_WwfvVF4Arnh8rxgFcLaR3kGDtBGGuiYIag2c1hXbdReaFFuROxdbLUIWwlWeKetOLyWQrOCmdJNKejZbGRmto9EjujJOFvyvI-McRZigC6PC2sxzKeqhyFhiFhsm_QNbBwRnBSuYAGNrvK7nds5PrXqmaujVtrWsdNuqyL95XXur_RQ4ec-DS2lAoLSHvd-eUpAb6swnJGXAHCiX7IgZe-wKqLjSmdgIUBgUw_erzvKqzfu8xR_S0s-HuDPxmifoTN4lFcLO0mUDxPLnadO2Kq9pCl0Gd_FEFy1fANifvhHEkghD5tmC1Li_SAlOZKfAv2ij-00OwZafc_ws-Dlb17F77vwnOlxtu3MfVwpGs_aDdRow3KMJ9NVIuOyEfmWAq1fnONtZ9ZefZWVioQR-Tb45E4l2UY7pDFwFhnrSdEKOAUCdPeYgsnX2w67RCnxg8wjzxus-HBPrnuzeSbC-cGKqAg"));
        list.add(new ItemUser(1464, "Mỹ Duyên", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiM2UyZGZmNzdkMDZjMWM1YWRhZGYxYzE5YjFlNDljNmVkYTU2OTMzYmRmZWI4Y2Y0MDBhODNiZGQ1ZTU0ZDI4NjYzZDFlM2I5YWE5N2I5MGMiLCJpYXQiOjE2NDAxNjYzMzEuOTE2OTEyLCJuYmYiOjE2NDAxNjYzMzEuOTE2OTE5LCJleHAiOjE2NzE3MDIzMzEuOTA5NzIxLCJzdWIiOiIxNDY0Iiwic2NvcGVzIjpbXX0.eL0cVYXd8MDqW5TS0u8bJe8FDFP_9AisMAp-fwwpT-hGsRoIUM1ecGJZBJZlx2V8nVYYm3R4sooQ4IkGvMhCH_QfzBmHvqGVGroXkbmDYuEXX2crA9ERnA-J726vi-loh3hSyvUAtDokpgbZdqVZ7aIntqmRJZGST6kXKEtGb5UD2qXLnwgQNBzhQMFG29NRB9ljn-19iFR871qg0wRq1kXSTG_I0Rq8biDPqcVxaSJBemULYajE3RMtIZGMsLdZL4QcIqGSZubqKqHMq-VDJR71RfzgFI9b5BEHIJKbMxBv14vLFVK4nXnT-nwOFssDdTkflvvDj7udpItdcf_ZvIqYUDTHLnS7e9NH6hEbn-3XnwmgsrglDtjcPxtExaKnsQXW830o3Lx5uzqmZE6UflY1CCXFR-m6XtA7RYl0sqhG0_FXR4lNdmZcwBBKmlJ7Toi67D5ar3cfRCyD6a2p6S_ZEZxucGzWHOYdzKdYroiGjhm5hHP-LqRczqqj8sz6BmjDBXGGK8rhw2lGGbj-IgD3RUVBmjMvhm_Dd-G-vCU3S6WCRCc7ge3dQ-21wx3Ix4rejKFJZH170BZXX7-rpPTOvO6topUmzJU9SMLiv5wF4c6coVfeg539GJsHq870wBC1ADm32hO7M-GlVZ4hfMK7j7mAzahC33NO15aWwlo"));
        list.add(new ItemUser(1466, "Thu Thảo", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZmVlMWI0ZTEyNmI0ZWJkMDdhNWQxNDhlM2Y5ODc4MzQyM2IzMDk5ZWQ1Yjg1MDdkZDc4ZDRjOGIyNmFmM2RjM2RlNmJiMGE1YzQyMDZiODAiLCJpYXQiOjE2NDAxNjY0NjAuNDc4MTA0LCJuYmYiOjE2NDAxNjY0NjAuNDc4MTExLCJleHAiOjE2NzE3MDI0NjAuNDYxMTQyLCJzdWIiOiIxNDY2Iiwic2NvcGVzIjpbXX0.uaHboWVZ0kXiBkcN0u2346loeHzJF_LJEJKaJJZDpV2wmSekiKCtxq7lrHpariJczG74wvCu3ve69KPyq6X8cMDtn4CwuN1lz6XNWqtSVFxPbRygiEkSyuUTJsqjumR-sRqGESYLjomwPEprNeWHXzC5o9ae1ztu8H4QROh1BuyGvb6wX8BZkhzUAfRsATzSCjLRVW_uYd8NwnHs1MOQPUYSrAaWRIjlhK2BX34uXLFq0cJ8tth80ReLFJDp3AlADA1LNgzIlipDOYjK9REdF1jpKgxLeRPKVatNXhgI9zuub6uWLRWiXIJmduxbjYl2v0qNDnEM6hwGiiUcaSAO6j6R73pRDGLdXYjnOvu9IVLT5bdzWZzx_3JVrdp2peiuGs__vj2R_uD_T_NDbqXBnghTsd-neYmordFQqk8S2Qr6dGxP3MAVkiQCOO3rhqmxSEn8irKwR48dF5xOhGC2iH0-DBg9hyu1kq15Ed-BZE3P3drbyKa4bYYp2okyzK7iHe-mqNgEap-jjAvir92pVRjgZDRoT0fmZU_rTIjlz5YoG5KUMRf-i-KMl1yQ2QojhkGH5oqwIogY7rRElOhw4QwiWURw_vftUHcGYAkdmd1pl02K7T1nE1Rcm51ZeTd3hRJELXQkxDyVrowRJ2kga2SukVSOJsTkoEo4GuRT8ZQ"));
        list.add(new ItemUser(1467, "Hà Linh", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYTI1OGI1ZDZkOGU2MzYzZGQ5MjRkNDZjOWE2M2M4OGU0YmFmYzQ3YmUzZWNjNzM1ZjFkNTQ0ODQ0NTY1OGY5Y2M4ZDMxNmVlMGRmYTBmN2UiLCJpYXQiOjE2NDAxNjY2MTMuNDY3MDg3LCJuYmYiOjE2NDAxNjY2MTMuNDY3MDk0LCJleHAiOjE2NzE3MDI2MTMuNDYwMDg2LCJzdWIiOiIxNDY3Iiwic2NvcGVzIjpbXX0.BekT2sEEGfziOXJdFYd4Sizmpl4iGKrdCyH_RZYMhCXBOFUHtGxW3DDi_6DfwBELAraboIHPNFFzuvrfRcCz5S_MP6gQuyFLj7I7qdtmKm8zRL9miu4hylB6rm8cOCo2rB9KMnKh8CxkZDwhvxDb426VEiRqyYz4pjQL5jWrPhctpa1f0f2Zg9b6d25Bw-fPeEZGoe2YV1drGyvcIANANUw577URe-iEAsjGB5LrEeg3t9AoQlgo_gWuGZx6JXtBiBcj1reQ7rySqcYXoy-LG49jDAyLK0KDwCPq1tdFoTbxb4LlGnlucvqMDqNgqaRNQs3MG9kIS43kL-Ua_-IQBLfymMb1AxN-ElTbZN9CoO1cuEvQHb4ZqUhOHzW8ahcNcwW3APbazLDDIQeP-91YpMxTdKiXbkt429ArMCzbOeOEp9vo1MIWMic9BVhlVr244pcbHIv_0cncTxYMOUTlJULePIFnCa3DUXO95kiK-_Hcaif1vLxZf8v3rFxeXZxTmmc6fHbe8Y6Xb3zSKxe8ZRJ8V4_JMKrKIdqmOMuqLiJ7b1WIUzhOtYuIDRfww35l5WhWwFPOSVup8ZiGNg8p2sGHwFOMfOzHQL9dF-XQM5zcUGyoI1RtsDgT2Q4s7g4-Vttv_GG0HBexup17N0ntb7qOV2ZjnkypcIBBnX0uDJY"));

//        list.add(new ItemUser(1614, "Tiến Minh", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjE2MTQiLCJwaG9uZSI6IiIsImZ1bGxuYW1lIjoiVGlcdTFlYmZuIE1pbmgiLCJhdmF0YXIiOiJodHRwczpcL1wvZGF1Z2lhdHJ1eWVuaGluaC5jb21cL3VwbG9hZFwvaXN0dWRpb3NcL2F2YXRhclwvMTYxNF9UaWVuX01pbmguanBnIiwidXNlcnNfc3RhdHVzIjoiMiIsImVtYWlsIjoidmJ2amdybmNkel8xNjI3NTQ2MzI0QHRmYm53Lm5ldCIsImFkZHJlc3MiOiJIXHUwMGUwIE5cdTFlZDlpIiwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiJOYW0iLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJjaGVja19wYXNzd29yZCI6IjEiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5SXlZemRsTTJVaVhRLjlsN1Jvd093RWgxeUtzQkpnS21oYm9zX1NEdzU5S2xQRTJ3UjZwV2x3RnciLCJpc191c2VyIjoiMCIsInVzZXJGb3J1bSI6IlRpZW5NaW5oMTYxNCIsImpvYiI6Ilx1MDExMGkgbFx1MDBlMG0iLCJjaGFubmVsIjoidHVvbmd0YWMudHYiLCJtZW1iZXJfY2xhc3NpZmljYXRpb24iOm51bGwsInVybF9mb3J1bSI6Imh0dHA6XC9cL3R1b25ndGFjLnR2XC9pbmRleC5waHA_bG9naW5cL2FwaS10b2tlbiZ0b2tlbj1JY2NTQVZiOEZ3TEtrdHBXTEhpS0pwV0thMDl4LW5nMCZmb3JjZT0wJnJlbWVtYmVyPTEiLCJwYXNzRm9ydW0iOiIyYzdlM2UiLCJhY2Nlc3NfdG9rZW4iOnt9fQ.bElFa65awoTOQ9MmB_Z3hSq4AArytxL8r0uYRYO1Ycs"));
//        list.add(new ItemUser(1615, "Hùng Dũng", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxNSwicGhvbmUiOiIiLCJmdWxsbmFtZSI6IkhcdTAwZjluZyBEXHUwMTY5bmciLCJhdmF0YXIiOiJodHRwczpcL1wvZGF1Z2lhdHJ1eWVuaGluaC5jb21cL3VwbG9hZFwvaXN0dWRpb3NcL2F2YXRhclwvMTYxNV9IdW5nX0R1bmcuanBnIiwidXNlcnNfc3RhdHVzIjoiMiIsImVtYWlsIjoieWFoZWRkb21lY18xNjI3NTQ2NzA3QHRmYm53Lm5ldCIsImFkZHJlc3MiOiJCXHUxZWFmYyBHaWFuZyIsImJpcnRoZGF5IjoiIiwiZ2VuZGVyIjoiTmFtIiwicm9sZSI6InVzZXIiLCJjaGlwIjoiIiwicGFzc19mb3J1bSI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5XeUl3TURCbFpXSWlYUS5LNXk3NHdZU0x3SXZUR3BhUkhxNWJzQWdsY3VMa0VGNXl2Tlo3WnUzOTdjIiwidXNlckZvcnVtIjoiSHVuZ0R1bmcxNjE1Iiwiam9iIjoiU2luaCBWaVx1MDBlYW4iLCJjaGVja19wYXNzd29yZCI6IjEiLCJjaGFubmVsIjoidHVvbmd0YWMudHYiLCJtZW1iZXJfY2xhc3NpZmljYXRpb24iOm51bGwsInVybF9mb3J1bSI6Imh0dHA6XC9cL3R1b25ndGFjLnR2XC9pbmRleC5waHA_bG9naW5cL2FwaS10b2tlbiZ0b2tlbj1PZEFQNXlJcThaeVdwbnJlckRLcjIzbUZyc21SNGhVZCZmb3JjZT0wJnJlbWVtYmVyPTEiLCJwYXNzRm9ydW0iOiIwMDBlZWIifQ.gtNaEVVtC1xWbf842rbB6GS85v9EanSFH719OHEY_0Q"));
//        list.add(new ItemUser(1618, "Tấn Phát", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxOCwicGhvbmUiOiIiLCJmdWxsbmFtZSI6IlRcdTFlYTVuIFBoXHUwMGUxdCIsImF2YXRhciI6Imh0dHBzOlwvXC9kYXVnaWF0cnV5ZW5oaW5oLmNvbVwvdXBsb2FkXC9pc3R1ZGlvc1wvYXZhdGFyXC8xNjE4X1Rhbl9QaGF0LmpwZyIsInVzZXJzX3N0YXR1cyI6IjIiLCJlbWFpbCI6InV2anRyZWN5emlfMTYyNzU0NjY5M0B0ZmJudy5uZXQiLCJhZGRyZXNzIjoiSFx1MDBlMCBOXHUxZWQ5aSIsImJpcnRoZGF5IjoiIiwiZ2VuZGVyIjoiTmFtIiwicm9sZSI6InVzZXIiLCJjaGlwIjoiIiwicGFzc19mb3J1bSI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5XeUkxWmpjeE9XRWlYUS5wWmJCVnJoSjJxOVBVNFpWYXotYlV5azE4Vl9HU05hNkdfRnJSc085aVBrIiwidXNlckZvcnVtIjoiVGFuUGhhdDE2MTgiLCJqb2IiOiJTaW5oIFZpXHUwMGVhbiIsImNoZWNrX3Bhc3N3b3JkIjoiMSIsImNoYW5uZWwiOiJ0dW9uZ3RhYy50diIsIm1lbWJlcl9jbGFzc2lmaWNhdGlvbiI6bnVsbCwidXJsX2ZvcnVtIjoiaHR0cDpcL1wvdHVvbmd0YWMudHZcL2luZGV4LnBocD9sb2dpblwvYXBpLXRva2VuJnRva2VuPWQ5RExyR0R1QlRIbUV2aUI2eWhTRHdzdlhGVktNNmROJmZvcmNlPTAmcmVtZW1iZXI9MSIsInBhc3NGb3J1bSI6IjVmNzE5YSJ9.IKQfleBakh5EIWt04yT2IfI4YUXyGx0vroHpDPp3xWs"));
//        list.add(new ItemUser(1666, "Khánh Huyền", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTY2NiwicGhvbmUiOiIiLCJmdWxsbmFtZSI6IktoXHUwMGUxbmggSHV5XHUxZWMxbiIsImF2YXRhciI6Imh0dHBzOlwvXC9ncmFwaC5mYWNlYm9vay5jb21cLzExMDgxMTg0NDc0MzcxMVwvcGljdHVyZT90eXBlPWxhcmdlIiwidXNlcnNfc3RhdHVzIjoiMSIsImVtYWlsIjoicWd5cHZxZHpneF8xNjM2NjE3MzcxQHRmYm53Lm5ldCIsImFkZHJlc3MiOiJIXHUwMGUwIE5cdTFlZDlpIiwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiJOXHUxZWVmIiwicm9sZSI6InVzZXIiLCJjaGlwIjoiIiwicGFzc19mb3J1bSI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5XeUl4TVRkbU1HTWlYUS44ejZPbW9ScHdPc2hnc2VYRm10ZGNWXzJyZXVYMnRFZnhxcF8tLXg3SVIwIiwidXNlckZvcnVtIjoiS2hhbmhIdXllbjE2NjYiLCJqb2IiOiJTaW5oIFZpXHUwMGVhbiIsImNoZWNrX3Bhc3N3b3JkIjoiMSIsImNoYW5uZWwiOiJ0dW9uZ3RhYy50diIsIm1lbWJlcl9jbGFzc2lmaWNhdGlvbiI6bnVsbCwidXJsX2ZvcnVtIjpudWxsLCJwYXNzRm9ydW0iOiIxMTdmMGMifQ.y9t0xO3t3ZlalTYofB_tqywnjQRYIi-1iVVwKxrj4wI"));
//        list.add(new ItemUser(1667, "Mỹ Duyên", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTY2NywicGhvbmUiOiIiLCJmdWxsbmFtZSI6Ik1cdTFlZjkgRHV5XHUwMGVhbiIsImF2YXRhciI6Imh0dHBzOlwvXC9ncmFwaC5mYWNlYm9vay5jb21cLzEwMzc1NDk5ODc4ODI4MlwvcGljdHVyZT90eXBlPWxhcmdlIiwidXNlcnNfc3RhdHVzIjoiMSIsImVtYWlsIjoic2h4ZW9zaW1xYl8xNjM2NjE3MzcxQHRmYm53Lm5ldCIsImFkZHJlc3MiOiJDYW8gQlx1MWViMW5nIiwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiJOXHUxZWVmIiwicm9sZSI6InVzZXIiLCJjaGlwIjoiIiwicGFzc19mb3J1bSI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5XeUl5T1dabU5UTWlYUS5rNjRQakR4a2lJZTgwUXJpY3Zid1V4cmFzR045aWJEUTV1d0pFMWM3MUpFIiwidXNlckZvcnVtIjoiTXlEdXllbjE2NjciLCJqb2IiOiJcdTAxMTBpIGxcdTAwZTBtIiwiY2hlY2tfcGFzc3dvcmQiOiIxIiwiY2hhbm5lbCI6InR1b25ndGFjLnR2IiwibWVtYmVyX2NsYXNzaWZpY2F0aW9uIjpudWxsLCJ1cmxfZm9ydW0iOm51bGwsInBhc3NGb3J1bSI6IjI5ZmY1MyJ9.o5y8MYxLnvveSGyUde_z-7qAM6QVKRM0VjnTJ5Ju5c4"));
//        list.add(new ItemUser(1669, "Thu Thảo", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTY2OSwicGhvbmUiOiIiLCJmdWxsbmFtZSI6IlRodSBUaFx1MWVhM28iLCJhdmF0YXIiOiJodHRwczpcL1wvZ3JhcGguZmFjZWJvb2suY29tXC8xMDc4NDQ4NDgzNzk4MDlcL3BpY3R1cmU_dHlwZT1sYXJnZSIsInVzZXJzX3N0YXR1cyI6IjIiLCJlbWFpbCI6ImRhbWxoZ3JuY3hfMTYzNjYxNzM3MUB0ZmJudy5uZXQiLCJhZGRyZXNzIjoiQlx1MWVhZmMgS1x1MWVhMW4iLCJiaXJ0aGRheSI6IiIsImdlbmRlciI6Ik5cdTFlZWYiLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5SXdNamd6TW1NaVhRLmZBenFwbDkxSjd6V3c5Vm92ekNJMWVQUm9lSFhrOVFGeXFReV9vV3ozWTQiLCJ1c2VyRm9ydW0iOiJUaHVUaGFvMTY2OSIsImpvYiI6Ilx1MDExMGkgbFx1MDBlMG0iLCJjaGVja19wYXNzd29yZCI6IjEiLCJjaGFubmVsIjoidHVvbmd0YWMudHYiLCJtZW1iZXJfY2xhc3NpZmljYXRpb24iOm51bGwsInVybF9mb3J1bSI6bnVsbCwicGFzc0ZvcnVtIjoiMDI4MzJjIn0.QviAir8awlYubPw6sgiGd0KeZC8K1P_H0d5b1lxlB34"));
//        list.add(new ItemUser(1668, "Thuỷ tiên", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTY2OCwicGhvbmUiOiIiLCJmdWxsbmFtZSI6IlRodVx1MWVmNyBUaVx1MDBlYW4iLCJhdmF0YXIiOiJodHRwczpcL1wvZ3JhcGguZmFjZWJvb2suY29tXC8xMTMxOTE5OTc4MjM5NjJcL3BpY3R1cmU_dHlwZT1sYXJnZSIsInVzZXJzX3N0YXR1cyI6IjIiLCJlbWFpbCI6Impkc2hxZnNsY2ZfMTYzNjYxNzM3MUB0ZmJudy5uZXQiLCJhZGRyZXNzIjoiSFx1MDBlMCBOXHUxZWQ5aSIsImJpcnRoZGF5IjoiIiwiZ2VuZGVyIjoiTlx1MWVlZiIsInJvbGUiOiJ1c2VyIiwiY2hpcCI6IiIsInBhc3NfZm9ydW0iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuV3lJNFlqQmxORElpWFEuWXhPS3lrWlBXSUNrRVpOZnpHdnp6UmdpS18tcHBSVFJPeEpVdmJDVHEwTSIsInVzZXJGb3J1bSI6IlRodXlUaWVuMTY2OCIsImpvYiI6Ilx1MDExMGkgbFx1MDBlMG0iLCJjaGVja19wYXNzd29yZCI6IjEiLCJjaGFubmVsIjoidHVvbmd0YWMudHYiLCJtZW1iZXJfY2xhc3NpZmljYXRpb24iOm51bGwsInVybF9mb3J1bSI6bnVsbCwicGFzc0ZvcnVtIjoiOGIwZTQyIn0._ZJa3m63TT0jZLXiKd32OPbXYDzy4dy6WAs-oLaEPDY"));
//        list.add(new ItemUser(1670, "Hà Linh", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTY3MCwicGhvbmUiOiIiLCJmdWxsbmFtZSI6IkhcdTAwZTAgTGluaCIsImF2YXRhciI6Imh0dHBzOlwvXC9ncmFwaC5mYWNlYm9vay5jb21cLzEwNDA4NTQ0ODc1NjEzMlwvcGljdHVyZT90eXBlPWxhcmdlIiwidXNlcnNfc3RhdHVzIjoiMiIsImVtYWlsIjoibmZ0YXVhc2pxeV8xNjM2NjIxNTY5QHRmYm53Lm5ldCIsImFkZHJlc3MiOiJIXHUwMGUwIE5cdTFlZDlpIiwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiJOXHUxZWVmIiwicm9sZSI6InVzZXIiLCJjaGlwIjoiIiwicGFzc19mb3J1bSI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5XeUptWkdabE5qUWlYUS5URnFmSlR0cVBxZGdjSHR6bGViWEVGUG02dGZ5Z2pvTTdkd0xIVDB5TGswIiwidXNlckZvcnVtIjoiSGFMaW5oMTY3MCIsImpvYiI6Ilx1MDExMGkgbFx1MDBlMG0iLCJjaGVja19wYXNzd29yZCI6IjEiLCJjaGFubmVsIjoidHVvbmd0YWMudHYiLCJtZW1iZXJfY2xhc3NpZmljYXRpb24iOm51bGwsInVybF9mb3J1bSI6bnVsbCwicGFzc0ZvcnVtIjoiZmRmZTY0In0._W2-VrRGOH2vBSVypEzpJTA1S0Az4TFKryWkQfJImcc"));
//        list.add(new ItemUser(1616, "Huy Hoàng", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxNiwicGhvbmUiOiIiLCJmdWxsbmFtZSI6Ikh1eSBIb1x1MDBlMG5nIiwiYXZhdGFyIjoiaHR0cHM6XC9cL2RhdWdpYXRydXllbmhpbmguY29tXC91cGxvYWRcL2lzdHVkaW9zXC9hdmF0YXJcLzE2MTZfSHV5X0hvYW5nLmpwZyIsInVzZXJzX3N0YXR1cyI6IjIiLCJlbWFpbCI6ImV1d3V1aGRiYmZfMTYyNzU0NjQzMUB0ZmJudy5uZXQiLCJhZGRyZXNzIjoiVHAgSFx1MWVkMyBDaFx1MDBlZCBNaW5oIiwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiJOYW0iLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5STFZV05pTlRNaVhRLmJwYzlPNkJ1VkVMOWhWSEUzWEQ1RzI5YkNiUUdKVlVDalhjdl9WdEduNDQiLCJ1c2VyRm9ydW0iOiJIdXlIb2FuZzE2MTYiLCJqb2IiOiJcdTAxMTBpIGxcdTAwZTBtIiwiY2hlY2tfcGFzc3dvcmQiOiIxIiwiY2hhbm5lbCI6InR1b25ndGFjLnR2IiwibWVtYmVyX2NsYXNzaWZpY2F0aW9uIjpudWxsLCJ1cmxfZm9ydW0iOiJodHRwOlwvXC90dW9uZ3RhYy50dlwvaW5kZXgucGhwP2xvZ2luXC9hcGktdG9rZW4mdG9rZW49YnoxSENCQ3AzbVRacXlKUnRpV0MwSDdsbmpvZ28wekwmZm9yY2U9MCZyZW1lbWJlcj0xIiwicGFzc0ZvcnVtIjoiNWFjYjUzIn0.-SrZdQmDWLmiUZM2lebhp1JfT_btX4Ao5uUzSBmf4-M"));
//        list.add(new ItemUser(1617, "Thành Công", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxNywicGhvbmUiOiIiLCJmdWxsbmFtZSI6IlRoXHUwMGUwbmggQ1x1MDBmNG5nIiwiYXZhdGFyIjoiaHR0cHM6XC9cL2RhdWdpYXRydXllbmhpbmguY29tXC91cGxvYWRcL2lzdHVkaW9zXC9hdmF0YXJcLzE2MTdfVGhhbmhfQ29uZy5qcGciLCJ1c2Vyc19zdGF0dXMiOiIyIiwiZW1haWwiOiJmZ2Zha2VmZXh4XzE2Mjc1NDY0NDdAdGZibncubmV0IiwiYWRkcmVzcyI6IkhcdTAwZTAgTlx1MWVkOWkiLCJiaXJ0aGRheSI6IiIsImdlbmRlciI6Ik5hbSIsInJvbGUiOiJ1c2VyIiwiY2hpcCI6IiIsInBhc3NfZm9ydW0iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuV3lJd1pEUXlObUlpWFEuZWZVTmRjVjhjUjJ5MUNkQ2F1LXJjcmpKUm1wNzdYcWh3SHZyYUlaZ2luVSIsInVzZXJGb3J1bSI6IlRoYW5oQ29uZzE2MTciLCJqb2IiOiJTaW5oIFZpXHUwMGVhbiIsImNoZWNrX3Bhc3N3b3JkIjoiMSIsImNoYW5uZWwiOiJ0dW9uZ3RhYy50diIsIm1lbWJlcl9jbGFzc2lmaWNhdGlvbiI6bnVsbCwidXJsX2ZvcnVtIjoiaHR0cDpcL1wvdHVvbmd0YWMudHZcL2luZGV4LnBocD9sb2dpblwvYXBpLXRva2VuJnRva2VuPWdrVDVSTXFtVGFjYXlsSEpXcXVOZHFKVkVNTEV2LTVnJmZvcmNlPTAmcmVtZW1iZXI9MSIsInBhc3NGb3J1bSI6IjBkNDI2YiJ9.SUqR7b10JaAqUi3LhUiz299O6zQfmgzV_NWT4wsWex4"));
//        list.add(new ItemUser(1083, "Pham Trang", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTA4MywicGhvbmUiOiIiLCJmdWxsbmFtZSI6IlBoYW0gVHJhbmciLCJhdmF0YXIiOiJodHRwczpcL1wvZ3JhcGguZmFjZWJvb2suY29tXC8xMzMwMjEwODIyNjY2NDJcL3BpY3R1cmU_dHlwZT1sYXJnZSIsInVzZXJzX3N0YXR1cyI6IjAiLCJlbWFpbCI6InB0dHJhbmcxMzEwOTVAZ21haWwuY29tIiwiYWRkcmVzcyI6IkhcdTAwZTAgTlx1MWVkOWkiLCJiaXJ0aGRheSI6IjIwMDItMDEtMDIiLCJnZW5kZXIiOiIyIiwicm9sZSI6InVzZXIiLCJjaGlwIjoiIiwicGFzc19mb3J1bSI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5XeUpoWVRRMlltSWlYUS43eHVjSU1sWW1ZczlPQ3EybDMxNjBsLW94WDUzZnMzY2FtUnRpcVZyaXhRIiwidXNlckZvcnVtIjoiMTA4MyIsImpvYiI6IiIsImNoZWNrX3Bhc3N3b3JkIjoiMSIsImNoYW5uZWwiOiJUdW9uZ1RhYy5UViIsInVybF9mb3J1bSI6bnVsbCwicGFzc0ZvcnVtIjoiYWE0NmJiIn0.DW1rR6EnmHOd07cMEfGdArG2yv_HJ9rHng4thZklbvM"));
//        list.add(new ItemUser(1087, "Khánh Vân", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwODciLCJwaG9uZSI6IiIsImZ1bGxuYW1lIjoiS2hcdTAwZTFuaCBWXHUwMGUybiIsImF2YXRhciI6Imh0dHBzOlwvXC9ncmFwaC5mYWNlYm9vay5jb21cLzg1Njc4ODI4NDkyODg2M1wvcGljdHVyZT90eXBlPWxhcmdlIiwidXNlcnNfc3RhdHVzIjoiMSIsImVtYWlsIjoiODU2Nzg4Mjg0OTI4ODYzQG1lZGlhdGVjaC52biIsImFkZHJlc3MiOm51bGwsImJpcnRoZGF5IjoiIiwiZ2VuZGVyIjoiIiwicm9sZSI6InVzZXIiLCJjaGlwIjoiIiwiY2hlY2tfcGFzc3dvcmQiOiIxIiwicGFzc19mb3J1bSI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5XeUpMYUdGdWFIWmhiak13TWpVeE5TSmQuTGhhdHZ2VndmeWJRdDBaRVJVUUNqM1BpcGZqWDV4M3JldmlWMHVkT1gzWSIsImlzX3VzZXIiOiIxIiwidXNlckZvcnVtIjoiS2hhbmhWYW4xMDg3Iiwiam9iIjoiIiwiY2hhbm5lbCI6IlR1b25nVGFjLlRWIiwidXJsX2ZvcnVtIjoiaHR0cDpcL1wvdHVvbmd0YWMudHZcL2luZGV4LnBocD9sb2dpblwvYXBpLXRva2VuJnRva2VuPWtaVmtLUGIxdXRVVVM2SXFtUmJSREF0YXVleEljRFhNJmZvcmNlPTAmcmVtZW1iZXI9MSIsInBhc3NGb3J1bSI6IktoYW5odmFuMzAyNTE1IiwiYWNjZXNzX3Rva2VuIjp7fX0.rasHAbKH5StKUJTNrfhfmN1vij4jNTt_MeMm-6v9Enk"));
//        list.add(new ItemUser(1040, "Thu Huyền", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwNDAiLCJwaG9uZSI6IiIsImZ1bGxuYW1lIjoiVGh1IEh1eVx1MWVjMW4iLCJhdmF0YXIiOiJodHRwczpcL1wvZ3JhcGguZmFjZWJvb2suY29tXC8xMDQ2Mzg4NTE5MTAxNTMxXC9waWN0dXJlP3R5cGU9bGFyZ2UiLCJ1c2Vyc19zdGF0dXMiOiIxIiwiZW1haWwiOiIxMDQ2Mzg4NTE5MTAxNTMxQG1lZGlhdGVjaC52biIsImFkZHJlc3MiOiJDaFx1MDFiMGEgY1x1MDBmMyB0aFx1MDBmNG5nIHRpbiIsImJpcnRoZGF5IjoiMjAwMS0xMi0yNCIsImdlbmRlciI6Ik5cdTFlZWYiLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJjaGVja19wYXNzd29yZCI6IjEiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5SjBkVzl1WjNSaFkzUjJJbDAuWXpIa1BYcmpFNEJqUFE2U1doa1JiTDRDSHpmcF9NeHoyeHNWXzhKZlM3dyIsImlzX3VzZXIiOiIxIiwidXNlckZvcnVtIjoiVGh1SHV5ZW4xMDQwIiwiam9iIjoiQ2hcdTAxYjBhIGNcdTAwZjMgdGhcdTAwZjRuZyB0aW4iLCJjaGFubmVsIjoiVHVvbmdUYWMuVFYiLCJ1cmxfZm9ydW0iOiJodHRwOlwvXC90dW9uZ3RhYy50dlwvaW5kZXgucGhwP2xvZ2luXC9hcGktdG9rZW4mdG9rZW49Y082cDNxN2tCb2tVb1RjemxmWlZ1UDAyelh4UW1JaEsmZm9yY2U9MCZyZW1lbWJlcj0xIiwicGFzc0ZvcnVtIjoidHVvbmd0YWN0diIsImFjY2Vzc190b2tlbiI6e319.PSN7kGtwv3hiZQN78bnkwMTf8jnxZttIcJOB1jQM1no"));
//        list.add(new ItemUser(1037, "Hiền Hồ", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMzciLCJwaG9uZSI6IiIsImZ1bGxuYW1lIjoiSFx1MWVkMyBIaVx1MWVjMW4iLCJhdmF0YXIiOiJodHRwczpcL1wvZ3JhcGguZmFjZWJvb2suY29tXC81NDU1NTY3MDY1Njg3MTRcL3BpY3R1cmU_dHlwZT1sYXJnZSIsInVzZXJzX3N0YXR1cyI6IjEiLCJlbWFpbCI6IjU0NTU1NjcwNjU2ODcxNEBtZWRpYXRlY2gudm4iLCJhZGRyZXNzIjpudWxsLCJiaXJ0aGRheSI6IiIsImdlbmRlciI6IiIsInJvbGUiOiJ1c2VyIiwiY2hpcCI6IiIsImNoZWNrX3Bhc3N3b3JkIjoiMSIsInBhc3NfZm9ydW0iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuV3lJNFpEWTVPV1VpWFEuR3JLQTA4b2xHdEdQZDNicFlZSDRMVjhYcTNZM29yNGQ4dllXM3NpYTBFZyIsImlzX3VzZXIiOiIxIiwidXNlckZvcnVtIjoiSG9IaWVuMTAzNyIsImpvYiI6IiIsImNoYW5uZWwiOiJUdW9uZ1RhYy5UViIsInVybF9mb3J1bSI6Imh0dHA6XC9cL3R1b25ndGFjLnR2XC9pbmRleC5waHA_bG9naW5cL2FwaS10b2tlbiZ0b2tlbj12alZxZDVJdzQ2TTFaSzJqQVo4bmZBbDFtc0pTck1wbyZmb3JjZT0wJnJlbWVtYmVyPTEiLCJwYXNzRm9ydW0iOiI4ZDY5OWUiLCJhY2Nlc3NfdG9rZW4iOnt9fQ.2SaaEbpm6Qy2SKDL4xkN4wBVkrrKOjr2GGPRb4Jroyo"));
//        list.add(new ItemUser(1033, "Nhật Tiến", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMzMiLCJwaG9uZSI6IiIsImZ1bGxuYW1lIjoiTmhcdTFlYWR0IFRpXHUxZWJmbiIsImF2YXRhciI6Imh0dHBzOlwvXC9ncmFwaC5mYWNlYm9vay5jb21cLzI5MjIxNzM2MTQ3MzI0ODNcL3BpY3R1cmU_dHlwZT1sYXJnZSIsInVzZXJzX3N0YXR1cyI6IjEiLCJlbWFpbCI6InRpZW45a3JAZ21haWwuY29tIiwiYWRkcmVzcyI6bnVsbCwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiIiLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJjaGVja19wYXNzd29yZCI6IjEiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5SXdPVE0xTVRraVhRLkNVRDVYOU02bU1pZ2k2ZUxvb2JNcmdYcWViUnQtSjdSZkliRWM4Wjg2UFEiLCJpc191c2VyIjoiMSIsInVzZXJGb3J1bSI6Ik5oYXRUaWVuMTAzMyIsImpvYiI6IiIsImNoYW5uZWwiOiJUdW9uZ1RhYy5UViIsInVybF9mb3J1bSI6Imh0dHA6XC9cL3R1b25ndGFjLnR2XC9pbmRleC5waHA_bG9naW5cL2FwaS10b2tlbiZ0b2tlbj1vRlVaMVpkTi16ZE9hWXgyMHFCYXVGVERBOVgtWXVxRyZmb3JjZT0wJnJlbWVtYmVyPTEiLCJwYXNzRm9ydW0iOiIwOTM1MTkiLCJhY2Nlc3NfdG9rZW4iOnt9fQ.4Ty4G_kqDtHGWQNQINANBwdGClbcMGNY0B3p1WLkh3k"));
//        list.add(new ItemUser(1020, "Nguyễn Khải Hoàn", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMjAiLCJwaG9uZSI6IiIsImZ1bGxuYW1lIjoiTmd1eVx1MWVjNW4gS2hcdTFlYTNpIEhvXHUwMGUwbiIsImF2YXRhciI6Imh0dHBzOlwvXC9ncmFwaC5mYWNlYm9vay5jb21cLzEyOTQ0NDg2MTA5NzQ5OTBcL3BpY3R1cmU_dHlwZT1sYXJnZSIsInVzZXJzX3N0YXR1cyI6IjEiLCJlbWFpbCI6ImtoYWlob2FuZHRkQGdtYWlsLmNvbSIsImFkZHJlc3MiOiJIXHUwMWIwbmcgWVx1MDBlYW4iLCJiaXJ0aGRheSI6IiIsImdlbmRlciI6Ik5hbSIsInJvbGUiOiJ1c2VyIiwiY2hpcCI6IiIsImNoZWNrX3Bhc3N3b3JkIjoiMSIsInBhc3NfZm9ydW0iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuV3lJMVpHVmtOREFpWFEudFk4NU9jcWRCaFlwd2I0NW9zX2lRR193N2phdlFDZEJmNkY2TGVPakp6QSIsImlzX3VzZXIiOiIxIiwidXNlckZvcnVtIjoiTmd1eWVuSG9hbjEwMjAiLCJqb2IiOiJTaW5oIFZpXHUwMGVhbiIsImNoYW5uZWwiOiJ0dW9uZ3RhYy50diIsInBhc3NGb3J1bSI6IjVkZWQ0MCIsImFjY2Vzc190b2tlbiI6e319.nYCTVVCY91DKGdCYvVPGElUniDXesXCVKRFDbiA8FQU"));
//        list.add(new ItemUser(1018, "Dương Đức Huy", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMTgiLCJwaG9uZSI6IiIsImZ1bGxuYW1lIjoiRFx1MDFiMFx1MDFhMW5nIFx1MDExMFx1MWVlOWMgSHV5IiwiYXZhdGFyIjoiaHR0cHM6XC9cL2dyYXBoLmZhY2Vib29rLmNvbVwvMTQ1NzM5MjgwNDYwNDY2N1wvcGljdHVyZT90eXBlPWxhcmdlIiwidXNlcnNfc3RhdHVzIjoiMSIsImVtYWlsIjoiZHVvbmdodXk2MTIyMDAyQGdtYWlsLmNvbSIsImFkZHJlc3MiOiJIXHUwMWIwbmcgWVx1MDBlYW4iLCJiaXJ0aGRheSI6IjIwMDItMTItMDYiLCJnZW5kZXIiOiJOYW0iLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJjaGVja19wYXNzd29yZCI6IjEiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5Sm9kWGsyTVRJeU1EQXlJbDAuejlwdGw4ZTZKRElpNjl3TGh1WFhtdjZ1NXhpWXFHRHhCM0lEYVpVeG5lYyIsImlzX3VzZXIiOiIxIiwidXNlckZvcnVtIjoiRHVvbmdIdXkxMDE4Iiwiam9iIjoic2luaCB2aVx1MDBlYW4iLCJjaGFubmVsIjoidHVvbmd0YWMudHYiLCJ1cmxfZm9ydW0iOiJodHRwOlwvXC90dW9uZ3RhYy50dlwvaW5kZXgucGhwP2xvZ2luXC9hcGktdG9rZW4mdG9rZW49UmM2eXB4TTI4TWVybTNYS3gzNl9LdFBZRHFlLXBVb0omZm9yY2U9MCZyZW1lbWJlcj0xIiwicGFzc0ZvcnVtIjoiaHV5NjEyMjAwMiIsImFjY2Vzc190b2tlbiI6e319.A4RxCQXaWDJo-XVFwW0T8RUaRGBEgACfN_M2FGZlEmo"));
//        list.add(new ItemUser(971, "Bảo Xuyến", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Ijk3MSIsInBob25lIjoiIiwiZnVsbG5hbWUiOiJCXHUxZWEzbyBYdXlcdTFlYmZuIiwiYXZhdGFyIjoiaHR0cHM6XC9cL2dyYXBoLmZhY2Vib29rLmNvbVwvODEyODMxMzE2MTAzOTgwXC9waWN0dXJlP3R5cGU9bGFyZ2UiLCJ1c2Vyc19zdGF0dXMiOiIyIiwiZW1haWwiOiI4MTI4MzEzMTYxMDM5ODBAbWVkaWF0ZWNoLnZuIiwiYWRkcmVzcyI6bnVsbCwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiIiLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJjaGVja19wYXNzd29yZCI6IjEiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5SmxPVGRoT0dVaVhRLmtJN2x0N3VkNHhuT1RVWHJHSGpBbndVQnAxVnZSb0xTUGQwRjVvckgxbnMiLCJpc191c2VyIjoiMSIsInVzZXJGb3J1bSI6IkJhb1h1eWVuOTcxIiwiam9iIjoiIiwiY2hhbm5lbCI6InR1b25ndGFjLnR2IiwibWVtYmVyX2NsYXNzaWZpY2F0aW9uIjoiMSIsInVybF9mb3J1bSI6Imh0dHA6XC9cL3R1b25ndGFjLnR2XC9pbmRleC5waHA_bG9naW5cL2FwaS10b2tlbiZ0b2tlbj1nYURXRkRFNEtyNm9UU09JelQ0SHpTNGxXaDdwVjhHaiZmb3JjZT0wJnJlbWVtYmVyPTEiLCJwYXNzRm9ydW0iOiJlOTdhOGUiLCJhY2Nlc3NfdG9rZW4iOnt9fQ.mVk7ks6bNjCoILeyhXa-kpPT90xKlcvP_7eNv5UCmqc"));
//        list.add(new ItemUser(1002, "Nguyễn Đức Hoàng", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTAwMiwicGhvbmUiOiIiLCJmdWxsbmFtZSI6Ik5ndXlcdTFlYzVuIFx1MDExMFx1MWVlOWMgSG9cdTAwZTBuZyIsImF2YXRhciI6Imh0dHBzOlwvXC9ncmFwaC5mYWNlYm9vay5jb21cLzUxODc3NTUzNjIwMzkzMFwvcGljdHVyZT90eXBlPWxhcmdlIiwidXNlcnNfc3RhdHVzIjoiMiIsImVtYWlsIjoiaG9hbmduZ3V5ZW5objEwMDQwMkBnbWFpbC5jb20iLCJhZGRyZXNzIjoiIiwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiIiLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5Sm9iMkZ1WnpFeU15SmQueXdCVW5ScUZDVHNsbE9wN3MwNzFfSEZmUm13c0lZc0RuUDc1SjdoTkpISSIsInVzZXJGb3J1bSI6Ik5ndXllbkhvYW5nMTAwMiIsImpvYiI6IiIsImNoZWNrX3Bhc3N3b3JkIjoiMSIsImNoYW5uZWwiOiJ0dW9uZ3RhYy50diIsInVybF9mb3J1bSI6Imh0dHA6XC9cL3R1b25ndGFjLnR2XC9pbmRleC5waHA_bG9naW5cL2FwaS10b2tlbiZ0b2tlbj0tWjFhNkh1Zm1uRzk4V05OeDJJbnJqeFpmOFhxb3EzVSZmb3JjZT0wJnJlbWVtYmVyPTEiLCJwYXNzRm9ydW0iOiJob2FuZzEyMyJ9._GxbMLwUlu-FBq6aghDU-vjv_BTMWacdoj5iD-9NDYg"));
//        list.add(new ItemUser(926, "Lê Hà Hưng", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjkyNiIsInBob25lIjoiIiwiZnVsbG5hbWUiOiJMXHUwMGVhIEhcdTAwZTAgSFx1MDFiMG5nIiwiYXZhdGFyIjoiaHR0cHM6XC9cL2dyYXBoLmZhY2Vib29rLmNvbVwvMjk1ODA4MTM5MTE4MTk2N1wvcGljdHVyZT90eXBlPWxhcmdlIiwidXNlcnNfc3RhdHVzIjoiMSIsImVtYWlsIjoiaHVuZzIwMDB5dEBnbWFpbC5jb20iLCJhZGRyZXNzIjoiQ2hcdTAxYjBhIGNcdTAwZjMgdGhcdTAwZjRuZyB0aW4iLCJiaXJ0aGRheSI6IjIwMDAtMDMtMTEiLCJnZW5kZXIiOiJOYW0iLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJjaGVja19wYXNzd29yZCI6IjEiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5SXhNVEF6TWpBd01IbDBhQ0pkLmxHZ28zd2tVN0hXUzl5M0tqc29fNzBrSWtTS1MxTU52aG1oVTNvRkNfa3ciLCJpc191c2VyIjoiMSIsInVzZXJGb3J1bSI6IkxlSHVuZzkyNiIsImpvYiI6IkNoXHUwMWIwYSBjXHUwMGYzIHRoXHUwMGY0bmcgdGluIiwiY2hhbm5lbCI6InR1b25ndGFjLnR2IiwibWVtYmVyX2NsYXNzaWZpY2F0aW9uIjoiMiIsInVybF9mb3J1bSI6Imh0dHA6XC9cL3R1b25ndGFjLnR2XC9pbmRleC5waHA_bG9naW5cL2FwaS10b2tlbiZ0b2tlbj1tSjAtMXVaRmROMXVXeVA5czk1ZXo1MmNaTHh1WUc5SyZmb3JjZT0wJnJlbWVtYmVyPTEiLCJwYXNzRm9ydW0iOiIxMTAzMjAwMHl0aCIsImFjY2Vzc190b2tlbiI6e319.dyqXF3m-BZiH2qqIWuoPCdafAz39gCBnc9XNN77cEMk"));
//        list.add(new ItemUser());
//        list.add(new ItemUser());
//        list.add(new ItemUser());
        ItemUserDemoAdapter adapter = new ItemUserDemoAdapter(this, list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        adapter.setOnItemClickListener((itemObject, position) -> {
            countDemo = 0;
            checkUserOnline(position, list);
        });
        recyclerView.setAdapter(adapter);
    }

    int countDemo = 0;

    private void checkUserOnline(int position, ArrayList<ItemUser> items) {
        showLoadingDialog(false, null);
        if (items == null || position >= items.size()) {
            return;
        }
        final int index = position;
        countDemo++;
        if (countDemo > items.size()) {
            SharedPreferencesManager.saveAccessToken(UserLoginActivity.this, items.get(position).getAccessToken());
            autoLogin();
            return;
        }
        MyHttpRequest myHttpRequest = new MyHttpRequest(this);
        String api = ConstantTT.validApiGameTT(this,"api/user/") + items.get(index).getId();
        myHttpRequest.request(false, api, null, new MyHttpRequest.ResponseListener() {
            @Override
            public void onFailure(int statusCode) {

            }

            @Override
            public void onSuccess(int statusCode, String responseString) {
                runOnUiThread(() -> {
                    if (("0").equals(responseString)) {
                        SharedPreferencesManager.saveAccessToken(UserLoginActivity.this, items.get(position).getAccessToken());
                        autoLogin();
                    } else if ("1".equals(responseString)) {
                        int pos = index + 1;
                        if (pos >= items.size()) {
                            pos = 0;
                        }
                        checkUserOnline(pos, items);
                    }
                });

            }
        });
    }
}
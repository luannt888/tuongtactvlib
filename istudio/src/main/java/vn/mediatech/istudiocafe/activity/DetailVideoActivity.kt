package vn.mediatech.istudiocafe.activity

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_detail_video_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.adapter.ViewPagerAdapter
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.fragment.PlayerFragment
import vn.mediatech.istudiocafe.fragment.RelateNewsFragment
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.swipebacklayout.SwipeBackLayout
import vn.mediatech.istudiocafe.swipebacklayout.app.SwipeBackActivity
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.TextUtil

class DetailVideoActivity : SwipeBackActivity() {
    var isLoading: Boolean = false
    var isLayoutVideoInfoClicked = false
    var loadMoreAble: Boolean = true
    var isFirstLoad: Boolean = true
    var isHandleLoadMore: Boolean = false
    var itemObj: ItemNews? = null
    var myHttpRequest: MyHttpRequest? = null
    var preOrientation = 0
    var fragmentList: ArrayList<Fragment> = ArrayList()
    var page: Int = 1
    val limit = 10
    var relateNewsFragment: RelateNewsFragment? = null
    var playerFragment: PlayerFragment? = null
    var publishDate: String? = null

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(Constant.DATA, itemObj)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_video_tt)
        initSwipeBack()
        initUI()
        preOrientation = resources.configuration.orientation
        initData(savedInstanceState)
        initControl()
    }

    override fun onResume() {
        super.onResume()
        trackScreen(getString(R.string.fa_detail_video))
        playerFragment?.resumePlayer()
    }

    fun initSwipeBack() {
        val edgeFlag: Int = SwipeBackLayout.EDGE_LEFT
        swipeBackLayout.setEdgeTrackingEnabled(edgeFlag)
        swipeBackLayout.setEdgeSize(ScreenSize(this).width / 9)
//        mSwipeBackLayout.setTouchFullScreen(true);
    }

    fun initUI() {
//        setStatusbarColor("#000000", false)
    }

    fun initData(savedInstanceState: Bundle?) {
        val bundle = savedInstanceState ?: intent.extras
        itemObj = bundle?.getParcelable(Constant.DATA)
        if (itemObj == null) {
            finish()
            return
        }
        initPager()
        getData()
    }

    fun initPager() {
        relateNewsFragment = RelateNewsFragment()
        fragmentList.add(relateNewsFragment!!)
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, fragmentList, null)
        viewPager.adapter = viewPagerAdapter
        viewPager.offscreenPageLimit = fragmentList.size
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }

    fun showErrorNetwork(message: String) {
        runOnUiThread {
            showDialog(
                false,
                R.string.notification,
                message,
                R.string.close,
                R.string.try_again,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        finish()
                    }

                    override fun onRightButtonClick() {
                        getData()
                    }

                })
        }
    }

    fun getData() {
        if (isLoading) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(this)
        } else {
            myHttpRequest!!.cancel()
        }
//        val api = String.format(Locale.ENGLISH, Constant.API_VIDEO_DETAIL, itemObj!!.id, page, limit)
        val requestParams = RequestParams()
        requestParams.put("id", "${itemObj!!.id}")
        requestParams.put("content_type", "${itemObj!!.contentType}")
        requestParams.put("style", "3")
        if (!publishDate.isNullOrEmpty()) {
            requestParams.put("update_at", publishDate)
        }
        var api = ServiceUtilTT.validAPITT(this, Constant.API_VIDEO_DETAIL_NEW)
//  api = ServiceUtil.validAPI(Constant.API_NEWS_DETAIL)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isFinishing || isDestroyed) {
                    return
                }
                isLoading = false
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isFinishing || isDestroyed) {
                    return
                }
                handleData(responseString)
                runOnUiThread {
                    progressBar.visibility = View.GONE
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            runOnUiThread { showDialog(message) }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
        var dataObj = JsonParser.getJsonObject(resultObj, Constant.DATA)
        if (dataObj == null) {
            dataObj = JsonParser.getJsonObject(resultObj, "video")
        }
        itemObj = JsonParser.parseItemNews(this, dataObj, null)
        if (itemObj == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        var dataOtherArr = JsonParser.getJsonArray(resultObj, "relate")
        if (dataOtherArr == null) {
            dataOtherArr = JsonParser.getJsonArray(resultObj, "list_same")
        }
        val list = JsonParser.parseItemNewsList(this, dataOtherArr)
        if (list == null || list.size == 0) {
            /*if (!isFirstLoad) {
                runOnUiThread {
                    showDialogFinish(getString(R.string.msg_empty_data))
                }
            } else {
                loadMoreAble = false
                relateNewsFragment!!.setLoadMoreAble(loadMoreAble)
            }
            return*/
            loadMoreAble = false
            relateNewsFragment!!.setLoadMoreAble(loadMoreAble)
        } else {
            for (item in list) {
                item.contentType = Constant.TYPE_VIDEO
            }
            page++
            loadMoreAble = list.size >= limit
            val lastItemObj = list.get(list.size - 1)
            publishDate = lastItemObj.publishDate
        }
        runOnUiThread {
            if (isFirstLoad) {
                layoutVideoInfo.visibility = View.VISIBLE
                setData()
                isFirstLoad = false
            }
            if (relateNewsFragment != null) {
                relateNewsFragment!!.setLoadMoreAble(loadMoreAble)
                relateNewsFragment!!.setData(list)
            }
        }
    }

    fun setData() {
        textTitle.text = itemObj!!.name
        textCategoryName.text = itemObj!!.categoryName
        textTime.text = itemObj!!.timeAgo
        if (itemObj!!.description.isNullOrEmpty() || itemObj!!.name.equals(itemObj!!.description)) {
            textDescription.visibility = View.GONE
        } else {
            TextUtil.setHtmlTextView(itemObj!!.description, textDescription)
            textDescription.visibility = View.VISIBLE
        }
        textView.text = itemObj!!.view
        textComment.text = itemObj!!.comment
        if (!isHandleLoadMore) {
            initPlayer()
        }
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
    }

    fun initPlayer() {
        if (playerFragment != null) {
            frameLayout.visibility = View.VISIBLE
            playerFragment?.linkPlay = itemObj!!.file
            playerFragment?.youtubeUrl = itemObj!!.youtubeUrl
            playerFragment?.setImageThumbnailAudioUrl(itemObj!!.image)
            playerFragment?.isAudio = false
            playerFragment?.startPosition = 0
            playerFragment?.checkPlayLink()
            return
        }
        val screenSize = ScreenSize(this)
        val height = screenSize.width * 9 / 16
        val lp: LinearLayout.LayoutParams = frameLayout.layoutParams as LinearLayout.LayoutParams
        lp.height = height
        frameLayout.layoutParams = lp
        frameLayout.visibility = View.VISIBLE
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        playerFragment = PlayerFragment()
        val bundle = Bundle()
        bundle.putString(Constant.DATA, itemObj?.file)
        bundle.putString(Constant.YOUTUBE_URL, itemObj!!.youtubeUrl)
        bundle.putString(Constant.TYPE, Constant.TYPE_VIDEO)
        bundle.putBoolean(Constant.IS_TAB_SELECTED, true)
        bundle.putBoolean(Constant.IS_RUN_PLAY, true)
        bundle.putBoolean(Constant.IS_LIVE_STREAM, false)
        bundle.putInt(Constant.FRAME_PLAYER_HEIGHT, height)
        bundle.putInt(Constant.TIME_START_POSITION, 0)
        if (itemObj!!.contentType.equals(Constant.TYPE_AUDIO)) {
            bundle.putString(Constant.IMAGE, itemObj!!.image)
            bundle.putBoolean(Constant.IS_AUDIO, true)
        }
        playerFragment!!.arguments = bundle
        fragmentTransaction.add(frameLayout.id, playerFragment!!)
        fragmentTransaction.commit()

    }

    fun onItemRelateNewsClicked(itemObj: ItemNews) {
        when (itemObj?.contentType) {
            Constant.TYPE_VIDEO -> {
                this@DetailVideoActivity.itemObj = itemObj
                reloadDataClicked()
            }
            else -> {
                goDetailNewsActivity(itemObj)
            }
        }
        isHandleLoadMore = false
    }

    fun reloadDataClicked() {
        playerFragment?.releasePlayer()
        setData()
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            finish()
        }
        textShare.setOnClickListener {
            shareApp(itemObj!!)
        }
        textComment.setOnClickListener {
            showDialog(getString(R.string.msg_comming_soon))
        }
        layoutVideoInfo.setOnClickListener {
            isLayoutVideoInfoClicked = !isLayoutVideoInfoClicked
            val titleMaxLine = if (isLayoutVideoInfoClicked) 10 else 3
            textTitle.maxLines = titleMaxLine
            textDescription.maxLines = titleMaxLine
        }
    }

    override fun finish() {
        myHttpRequest?.cancel()
        requestedOrientation = preOrientation
        super.finish()
    }
}
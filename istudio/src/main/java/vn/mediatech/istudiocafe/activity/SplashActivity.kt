package vn.mediatech.istudiocafe.activity

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_splash_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.htextview.AnimationListener
import vn.mediatech.istudiocafe.htextview.HTextView
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemAppConfig
import vn.mediatech.istudiocafe.model.ItemVersion
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.VoiceControl
import vn.mediatech.voicecontrol.voiceControl.VcUtils
import vn.mediatech.voicecontrol.voiceControl.VoiceControllerManager

class SplashActivity : BaseActivity() {
    var myHttpRequest: MyHttpRequest? = null
    var currentBgLaunch: String? = null
    var currentLogo: String? = null
    var isEndAnimation = false
    var isStopSloganAnimation = false
    var currentSloganPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_tt)
        MyApplication.getInstance().clearDataManager()
        if (VcUtils.ENABLE_VOICE_CONTROLLER && VoiceControl.ENABLE_VOICE_CONTROL) {
            VoiceControllerManager.instance?.initTTS(this)
        }
//            InteractiveActivity.show(this,"mediatech",true,R.drawable.bg_main_cds)
        initUI()
        initData()
    }

    fun initUI() {
        MyApplication.getInstance().loadDrawableImage(this, R.drawable.bg_splash, imageBackground)
        val sharedPreferences = getSharedPreferences(Constant.CONFIG, MODE_PRIVATE)
        currentBgLaunch = sharedPreferences.getString(Constant.IMG_LAUNCH, null)
        currentLogo = sharedPreferences.getString(Constant.LOGO, null)
        if (!currentBgLaunch.isNullOrEmpty()) {
            MyApplication.getInstance()
                .loadImageNoPlaceHolder(this, imageBackgroundServer, currentBgLaunch)
            imageBackgroundServer.visibility = View.VISIBLE
        }

        /*MyApplication.getInstance().loadImageNoPlaceHolder(this, imageLogoActionbarCenter, R.drawable.ic_logo_actionbar)
        if (!currentLogo.isNullOrEmpty()) {
            MyApplication.getInstance().loadImageNoPlaceHolder(this, imageLogoActionbarCenter, currentLogo)
        }
        initAnimation()*/
//        if (!currentLogo.isNullOrEmpty()) {
//            MyApplication.getInstance().loadImageBitmapListener(this, currentLogo, object : MyApplication.OnLoadImageBitmapListener {
//                override fun onResourceReady(resource: Bitmap?) {
//                    initAnimation(resource)
//                }
//            })
//        } else {
        MyApplication.getInstance().loadImageBitmapListener(
            this,
            R.drawable.logo_white,
            object : MyApplication.OnLoadImageBitmapListener {
                override fun onResourceReady(resource: Bitmap?) {
                    initAnimation(resource)
                }
            })
//        }
    }

    fun initAnimation(resource: Bitmap?) {
        if (resource == null) {
            isEndAnimation = true
            return
        }
        val screenSize = ScreenSize(this)
//        val layoutParamsParent: RelativeLayout.LayoutParams = imageLogoActionbarCenter.layoutParams as RelativeLayout.LayoutParams
//        layoutParamsParent.width = screenSize.width / 2

        val imageWidth = resource.width
        val imageHeight = resource.height
        val widthRatio: Float = imageWidth * 1f / imageHeight

        val imageCenterWidth = screenSize.width / 2
        val imageCenterHeight = (imageCenterWidth / widthRatio).toInt()
        val lpLogoActionbarCenter: LinearLayout.LayoutParams =
            imageLogoActionbarCenter.layoutParams as LinearLayout.LayoutParams
        lpLogoActionbarCenter.width = imageCenterWidth
        lpLogoActionbarCenter.height = imageCenterHeight
//        imageLogoActionbarCenter.setImageBitmap(resource)
        imageLogoActionbarCenter.setImageBitmap(
            Bitmap.createScaledBitmap(
                resource,
                imageCenterWidth,
                imageCenterHeight,
                false
            )
        )

        val anim = AnimationUtils.loadAnimation(this, R.anim.zoom_in)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                val sloganList = resources.getStringArray(R.array.slogan_list)
                textSlogan.setAnimationListener(object : AnimationListener {
                    override fun onAnimationEnd(hTextView: HTextView?) {
                        Handler(Looper.getMainLooper()).postDelayed(Runnable {
                            if (isDestroyed || isFinishing || isStopSloganAnimation) {
                                return@Runnable
                            }
                            if (currentSloganPosition >= sloganList.size) {
                                currentSloganPosition = 0
                                isEndAnimation = true
                            }
                            try {
                                textSlogan.animateText(sloganList[currentSloganPosition])
                                currentSloganPosition++
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }, 300)
//                        }, 1300)
                    }
                })
                try {
                    textSlogan.animateText(sloganList[currentSloganPosition])
                } catch (e: Exception) {
                }
                currentSloganPosition++
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })
        imageLogoActionbarCenter.startAnimation(anim)
    }

    fun initData() {
        getData()
    }

    fun getData() {
        if (!MyApplication.getInstance().isNetworkConnect) {
            runOnUiThread {
                showErrorNetwork()
            }
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(this)
        } else {
            myHttpRequest!!.cancel()
        }

        val requestParams = RequestParams()
        requestParams.put("type", "android")
        myHttpRequest!!.request(
            false,
            Constant.API_CONFIG,
            requestParams,
            object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isFinishing || isDestroyed) {
                        return
                    }
                    runOnUiThread { showErrorNetwork() }
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    if (isFinishing || isDestroyed) {
                        return
                    }
                    handleData(responseString)
                }
            })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            runOnUiThread { showErrorNetwork() }
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread { showErrorNetwork() }
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            runOnUiThread { showDialog(message) }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
        val itemAppConfig = JsonParser.parseItemAppConfig(resultObj)
        if (itemAppConfig == null) {
            runOnUiThread { showErrorNetwork() }
            return
        }
        val itemVersion: ItemVersion? = JsonParser.parseItemVersion(resultObj)
        if (itemVersion == null) {
            runOnUiThread { showErrorNetwork() }
            return
        }
        val currentVersion: Int = MyApplication.getInstance().getVersionCode(this)
        if (currentVersion < itemVersion.versionCode!! && !itemVersion.url.isNullOrEmpty()) {
            runOnUiThread {
                val leftButton = if (itemVersion.isUpdate!!) null else getString(R.string.close)
                showDialog(
                    false,
                    itemVersion.title,
                    itemVersion.message,
                    leftButton,
                    getString(R.string.update),
                    object : OnDialogButtonListener {
                        override fun onLeftButtonClick() {
                            showWaitingScreen(itemAppConfig)
                        }

                        override fun onRightButtonClick() {
                            openLink(itemVersion.url)
                        }
                    })
            }
            return
        }
        showWaitingScreen(itemAppConfig)
    }

    fun showWaitingScreen(itemAppConfig: ItemAppConfig) {
        if (!itemAppConfig.bgLaunch.isNullOrEmpty()) {
            runOnUiThread { imageBackgroundServer.visibility = View.VISIBLE }
            val sharedPreferences = getSharedPreferences(Constant.CONFIG, MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            if (currentBgLaunch.isNullOrEmpty() || (!currentBgLaunch.isNullOrEmpty() && !currentBgLaunch.equals(
                    itemAppConfig.bgLaunch
                ))
            ) {
                runOnUiThread {
                    MyApplication.getInstance()
                        .loadImageNoPlaceHolder(this, imageBackgroundServer, itemAppConfig.bgLaunch)
                }
                editor.putString(Constant.IMG_LAUNCH, itemAppConfig.bgLaunch)
            }

            if (currentLogo.isNullOrEmpty() || (!currentLogo.isNullOrEmpty() && !currentLogo.equals(
                    itemAppConfig.logo
                ))
            ) {
                currentLogo = itemAppConfig.logo
                if (!isEndAnimation) {
                    runOnUiThread {
                        imageLogoActionbarCenter.clearAnimation()
                    }
                    if (!currentLogo.isNullOrEmpty()) {
                        MyApplication.getInstance().loadImageBitmapListener(
                            this,
                            currentLogo,
                            object : MyApplication.OnLoadImageBitmapListener {
                                override fun onResourceReady(resource: Bitmap?) {
                                    runOnUiThread {
                                        initAnimation(resource)
                                    }
                                }
                            })
                    } else {
                        isEndAnimation = true
                    }
                }
                editor.putString(Constant.LOGO, itemAppConfig.logo)
            }
            editor.apply()
        }
        goMainActivity(itemAppConfig)
    }

    fun goMainActivity(itemAppConfig: ItemAppConfig) {
        if (!isEndAnimation) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                if (isDestroyed || isFinishing) {
                    return@Runnable
                }
                goMainActivity(itemAppConfig)
            }, 200)
            return
        }
        isStopSloganAnimation = true
//        textSlogan.clearAnimation()
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            if (isDestroyed || isFinishing) {
                return@Runnable
            }
            var bundle = intent.extras
//            gotoActivity(MainActivity::class.java, bundle)
            val intent = Intent(this, MainActivity::class.java)
            if (bundle == null) {
                bundle = Bundle()
            }
            val imageLogoActionbarCenterLocation = IntArray(2)
            imageLogoActionbarCenter.getLocationOnScreen(imageLogoActionbarCenterLocation)
            bundle.putIntArray(Constant.LOCATION, imageLogoActionbarCenterLocation)
            bundle.putParcelable(Constant.APP_CONFIG, itemAppConfig)
//            Loggers.e("TYPE_SHOW_NOTIFY_SPLASH",bundle.getString(Constant.TYPE_SHOW_NOTIFY))
            intent.putExtras(bundle)

            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }, 500)
    }

    fun showErrorNetwork() {
        showDialog(
            false,
            R.string.notification,
            R.string.msg_network_error,
            R.string.close,
            R.string.try_again,
            object : OnDialogButtonListener {
                override fun onLeftButtonClick() {
                    finish()
                }

                override fun onRightButtonClick() {
                    getData()
                }
            })
    }

    override fun finish() {
        myHttpRequest?.cancel()
        super.finish()
    }
}
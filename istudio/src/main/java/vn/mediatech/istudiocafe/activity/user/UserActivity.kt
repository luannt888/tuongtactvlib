package vn.mediatech.istudiocafe.activity.user

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_user_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.fragment.TabPersonFragment
import vn.mediatech.istudiocafe.listener.OnCreateViewListener

class UserActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_tt)
        if (frameLayout != null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction();
            val fragment = TabPersonFragment()
            fragmentTransaction.add(frameLayout.id, fragment, "TAB_PERSON")
            fragmentTransaction.commitNowAllowingStateLoss()
            fragment.onCreateViewListener = object : OnCreateViewListener {
                override fun onCreated() {
                    fragment?.checkRefresh()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val list = supportFragmentManager.fragments
        if (list.size > 0) {
            for (item in list) {
                item.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

//    val startForResult =
//        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
//            if (result.resultCode == Activity.RESULT_OK) {
//                val intent = result.data
//                // Handle the Intent
//                //do stuff here
//
//            }
//        }
}
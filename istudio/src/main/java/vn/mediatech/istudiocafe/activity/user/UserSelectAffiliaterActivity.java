package vn.mediatech.istudiocafe.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;

import java.util.Arrays;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.Loggers;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnLoginFormListener;
import vn.mediatech.istudiocafe.listener.OnCancelListener;
import vn.mediatech.istudiocafe.model.ItemUser;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.service.RequestParams;
import vn.mediatech.istudiocafe.util.JsonParser;
import vn.mediatech.istudiocafe.util.ServiceUtilTT;

public class UserSelectAffiliaterActivity extends BaseActivity {
    private ImageView buttonBack;
    private TextView buttonConfirm, textIgnore;
    private EditText editText;
    private MyHttpRequest myHttpRequest;
    private OnLoginFormListener loginFormListener;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private int loginType = Constant.LOGIN_TYPE_NORMAL;
    private String accessTokenSocial = "";
    private String userIdSocial = "";
    private String emailSocial = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_select_affiliater_tt);
        initUI();
        initData();
        initControl();
    }

    private void initUI() {
        setFullStatusTransparent();
        buttonBack = findViewById(R.id.buttonBack);
        editText = findViewById(R.id.editText);
        textIgnore = findViewById(R.id.textIgnore);
        buttonConfirm = findViewById(R.id.buttonConfirm);
//        setBackgroudActionbarFragment(rootView);
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }
    }

    private void validForm() {
        String username = editText.getText().toString().trim();
        if (username.isEmpty()) {
            editText.requestFocus();
            showToast(R.string.msg_username_empty);
            return;
        }
        loginType = Constant.LOGIN_TYPE_NORMAL;
//        login(username, password);
    }

    private void login(String username, String password) {
        if (!MyApplication.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        showLoadingDialog(true, null, new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequest != null) {
                    myHttpRequest.cancel();
                }
            }
        });
        if (myHttpRequest == null) {
            myHttpRequest = new MyHttpRequest(this);
        } else {
            myHttpRequest.cancel();
        }
        RequestParams requestParams = new RequestParams();
        String api = Constant.API_USER_LOGIN;
        boolean isPostMethod = false;
        if (loginType == Constant.LOGIN_TYPE_NORMAL) {
            isPostMethod = true;
            api = Constant.API_USER_LOGIN;
            requestParams.put("phone", username);
            requestParams.put("password", password);
        } else if (loginType == Constant.LOGIN_TYPE_FACEBOOK) {
//            api = Constant.API_LOGIN_FACEBOOk;
            api = Constant.API_USER_LOGIN_SOCIAL;
            requestParams.put("access_token", accessTokenSocial);
            requestParams.put("id", userIdSocial);
            requestParams.put("email", emailSocial);
        } else if (loginType == Constant.LOGIN_TYPE_GOOGLE) {
//            api = Constant.API_LOGIN_GOOGLE;
            api = Constant.API_USER_LOGIN_SOCIAL;
            requestParams.put("access_token", accessTokenSocial);
        }
        myHttpRequest.request(isPostMethod, ServiceUtilTT.validAPIDGTH(this,api), requestParams,
                new MyHttpRequest.ResponseListener() {
            @Override
            public void onFailure(int statusCode) {
                Loggers.e("login onFailure", "statusCode =" + statusCode);
                if (isFinishing() || isDestroyed()) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoadingDialog();
                        showDialogUser(getString(R.string.msg_network_error));
                    }
                });
            }

            @Override
            public void onSuccess(int statusCode, String responseString) {
                Loggers.e("login onSuccess", responseString);
                if (isFinishing() || isDestroyed()) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoadingDialog();
                        handleDataLogin(responseString, username, password);
                    }
                });
            }
        });
    }

    private void handleDataLogin(String responseString, String username, String password) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
        ItemUser itemUser = JsonParser.Companion.parseItemUserLogin(resultObj);
        if (itemUser == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        itemUser.setAccessTokenSocial(accessTokenSocial);
        MyApplication.getInstance().getDataManager().setItemUser(itemUser);
        if (loginType == Constant.LOGIN_TYPE_FACEBOOK) {
            username = emailSocial;
        }
        saveUser(username, password, userIdSocial, loginType, accessTokenSocial);
        if (loginFormListener != null) {
            MyApplication.getInstance().getDataManager().setNeedRefreshLeftMenu(true);
            loginFormListener.onLogin(true);
            /*if (getActivity() instanceof MainActivity) {
                ((MainActivity) getActivity()).initLeftMenuData();
                MyApplication.getInstance().getDataManager().setNeedRefreshLeftMenu(false);
            }*/
        }
//        removeRegisterForm();
    }

    private void initControl() {
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editText);
//                removeUserOTPConFirmForm();
            }
        });
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editText);
                gotoActivityForResult(UserKnowAppActivity.class, Constant.CODE_LOGIN);
//                validForm();
            }
        });
    }

    private void loginFacebook() {
        if (mAuth == null) {
            mAuth = FirebaseAuth.getInstance();
        }
        if (callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
        }
        loginType = Constant.LOGIN_TYPE_FACEBOOK;
        userIdSocial = "";
        emailSocial = "";
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        exception.printStackTrace();
                        showToast(getString(R.string.msg_login_error) + " (100)");
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile",
                "email"));
    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        if (accessToken == null) {
            showToast(getString(R.string.msg_login_error) + " (101)");
            return;
        }
        accessTokenSocial = accessToken.getToken();
        userIdSocial = accessToken.getUserId();
        if (MyApplication.getInstance().isEmpty(accessTokenSocial)) {
            showToast(getString(R.string.msg_login_error) + " (102)");
            return;
        }
        Loggers.e("MY_CHECK_LOGIN Facebook", accessTokenSocial);
//        START: GET USER EMAIL
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Loggers.e("MY_CHECK_LOGIN Facebook_object", object.toString());
                        emailSocial = JsonParser.Companion.getString(object, "email");
                        login(null, null);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email");
        request.setParameters(parameters);
        request.executeAsync();
//        END: GET USER EMAIL
//        login(null, null);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Loggers.e("MY_CHECK_LOGIN", "firebaseAuthWithGoogle id:" + acct.getId());
        Loggers.e("MY_CHECK_LOGIN", "firebaseAuthWithGoogle token:" + acct.getIdToken());
        accessTokenSocial = acct.getIdToken();
        if (MyApplication.getInstance().isEmpty(accessTokenSocial)) {
            showToast(getString(R.string.msg_login_error) + " (102)");
            return;
        }
        Loggers.e("MY_CHECK_LOGIN accessTokenGoogle", accessTokenSocial);
        login(null, null);
    }

    @Override
    public void onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//                Toast.LENGTH_SHORT).show();
        Loggers.e("MY_CHECK_ onActivityResult",
                "login fragment _ requestCode = " + requestCode + " _ resultCode = " + resultCode);
        if (requestCode == Constant.REQUEST_CODE_LOGIN_GOOGLE) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
            }
            return;
        }
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
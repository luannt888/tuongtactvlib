//package vn.mediatech.istudiocafe.activity.user
//
//import android.graphics.Color
//import android.os.*
//import android.text.Editable
//import android.text.TextWatcher
//import android.view.*
//import android.widget.*
//import com.google.android.material.bottomsheet.BottomSheetBehavior
//import com.google.android.material.bottomsheet.BottomSheetDialog
//import com.google.android.material.bottomsheet.BottomSheetDialogFragment
//import kotlinx.android.synthetic.main.fragment_user_register_form.*
//import kotlinx.android.synthetic.main.layout_actionbar_login_tt.*
//import vn.mediatech.interactive.app.*
//import vn.mediatech.istudiocafe.R
//import vn.mediatech.istudiocafe.util.*
//import java.util.*
//
//
//class UserRegisterFormFragmentOld : BottomSheetDialogFragment() {
//    var isLoading: Boolean = false
//    var isViewCreated: Boolean = false
//    var savedInstanceState: Bundle? = null
//    var bottomSheetDialog: BottomSheetDialog? = null
//    var onShowDismissListener: OnShowDismissListener? = null
//
//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        return inflater.inflate(R.layout.fragment_user_register_form, container, false)
//    }
//
//    override fun onActivityCreated(savedInstanceState: Bundle?) {
////        dialog?.window?.getAttributes()?.windowAnimations = R.style.DialogAnimation;
//        super.onActivityCreated(savedInstanceState)
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        this.savedInstanceState = savedInstanceState
//        dialog?.setCanceledOnTouchOutside(true)
//        dialog?.setOnShowListener {
//            bottomSheetDialog = it as BottomSheetDialog
//            bottomSheetDialog!!.behavior.skipCollapsed = true
//            bottomSheetDialog!!.behavior.isDraggable = false
//            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
//            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
//                    com.google.android.material.R.id.design_bottom_sheet)
//                    ?: return@setOnShowListener
//            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
//        }
//        isViewCreated = true
//        onShowDismissListener?.onShow()
//        checkRefresh()
//        return
//    }
//
//    fun checkRefresh() {
//        if (!isViewCreated) {
//            Handler(Looper.getMainLooper()).postDelayed(Runnable {
//                checkRefresh()
//            }, 2000)
//            return
//        }
//        init()
//    }
//
//    private fun init() {
//        initUI()
//        initData()
//        initControl()
//    }
//
//    fun initUI() {
////        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
//    }
//
//    fun initData() {
//    }
//
//    fun initControl() {
//        editUserName.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
//            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
//                validButtonConfirm()
//            }
//
//            override fun afterTextChanged(editable: Editable) {}
//        })
//        editPassword.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
//            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
//                validButtonConfirm()
//            }
//
//            override fun afterTextChanged(editable: Editable) {}
//        })
//        editConfirmPassword.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
//            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
//                validButtonConfirm()
//            }
//
//            override fun afterTextChanged(editable: Editable) {}
//        })
//        buttonBack.setOnClickListener(View.OnClickListener {
//            dismiss()
//        })
//        buttonConfirm.setOnClickListener {
//            val userName = editUserName.text.toString().trim()
//            val password = editPassword.text.toString().trim()
//            val passwordConfirm = editConfirmPassword.text.toString().trim()
//            if (userName.isEmpty() || userName.length < 6 || userName.contains(" ")) {
//                Toast.makeText(activity, "Vui lòng nhập tên đăng nhập viết liền và lớn hơn 6 ký tự!", Toast.LENGTH_SHORT).show()
//                editUserName.requestFocus()
//                return@setOnClickListener
//            }
//            if (password.isEmpty() || password.length < 6) {
//                Toast.makeText(activity, "Vui lòng nhập mật khẩu lớn hơn 6 ký tự!", Toast.LENGTH_SHORT).show()
//                editPassword.requestFocus()
//                return@setOnClickListener
//            }
//            if (passwordConfirm.isEmpty() || passwordConfirm.length < 6) {
//                Toast.makeText(activity, "Vui lòng nhập mật khẩu lớn hơn 6 ký tự!", Toast.LENGTH_SHORT).show()
//                editConfirmPassword.requestFocus()
//                return@setOnClickListener
//            }
//
//            if (!password.equals(passwordConfirm)) {
//                Toast.makeText(activity, "Mật khẩu không trùng khớp. Vui lòng nhập lại mật khẩu", Toast.LENGTH_SHORT).show()
//                editConfirmPassword.requestFocus()
//                return@setOnClickListener
//            }
//            dismiss()
//            onShowDismissListener?.onDismiss(true, userName, password)
//        }
//    }
//
//    private fun validButtonConfirm() {
//        val userName: String = editUserName.getText().toString().trim { it <= ' ' }
//        if (userName.length < 6) {
//            buttonConfirm.isEnabled = false
//            return
//        }
//        val password = editPassword.text.toString().trim { it <= ' ' }
//        if (password.length < 6) {
//            buttonConfirm.isEnabled = false
//            return
//        }
//        val passwordConfirm = editConfirmPassword.text.toString().trim { it <= ' ' }
//        if (passwordConfirm.length < 6) {
//            buttonConfirm.isEnabled = false
//            return
//        }
//        buttonConfirm.isEnabled = password == passwordConfirm
//    }
//
//    interface OnShowDismissListener {
//        fun onShow()
//        fun onDismiss(isSuccess: Boolean, userName: String, password: String)
//    }
//}
package vn.mediatech.istudiocafe.activity

import android.app.AlarmManager
import android.app.Dialog
import android.app.PendingIntent
import android.content.*
import android.graphics.Color
import android.graphics.Outline
import android.graphics.Point
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.text.method.LinkMovementMethod
import android.util.TypedValue
import android.view.*
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ShareCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_main_tt.*
import kotlinx.android.synthetic.main.activity_voucher_tt.*
import kotlinx.android.synthetic.main.layout_home_actionbar_tt.*
import kotlinx.android.synthetic.main.layout_loading_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.fragment.ImageZoomPagerBottomSheetDialog
import vn.mediatech.istudiocafe.listener.OnCancelListener
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.listener.OnKeyboardVisibilityListener
import vn.mediatech.istudiocafe.listener.OnResultListener
import vn.mediatech.istudiocafe.model.ItemAppConfig
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.service.fcm.MyFirebaseData
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.istudiocafe.voucher.ItemVoucher
import vn.mediatech.istudiocafe.voucher.OnDialogButtonVoucherListener
import vn.mediatech.istudiocafe.voucher.chatsupport.ChatSupportActivity
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemChat
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemConversation
import java.util.*


open class BaseActivity : AppCompatActivity() {
    var firebaseAnalytics: FirebaseAnalytics? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }

    fun trackScreen(screenName: String?) {
//        firebaseAnalytics?.setCurrentScreen(this, screenName, this.javaClass.canonicalName)
        if (screenName.isNullOrEmpty()) {
            return
        }
        firebaseAnalytics?.logEvent(screenName, null)
    }

    var loadingDialog: Dialog? = null

    fun showLoadingDialog(cancelable: Boolean, onCancelListener: OnCancelListener?) {
        showLoadingDialog(cancelable, R.string.please_waiting, onCancelListener)
    }

    fun showLoadingDialog(cancelable: Boolean, message: Int, onCancelListener: OnCancelListener?) {
        showLoadingDialog(cancelable, getString(message), onCancelListener)
    }

    fun showLoadingDialog(
        cancelable: Boolean,
        message: String?,
        onCancelListener: OnCancelListener?
    ) {
        if (isFinishing || isDestroyed) {
            return
        }
        if (loadingDialog != null) {
            loadingDialog?.dismiss()
            loadingDialog?.setCancelable(cancelable)
            loadingDialog?.setCanceledOnTouchOutside(false)
            if (message.isNullOrEmpty()) {
                textMessage?.visibility = View.GONE
            } else {
                textMessage?.text = message
                textMessage?.visibility = View.VISIBLE
            }
            loadingDialog?.show()
            return
        }
        loadingDialog = Dialog(this)
        if (loadingDialog?.window != null) {
            loadingDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        loadingDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loadingDialog?.setCancelable(cancelable)
        loadingDialog?.setCanceledOnTouchOutside(false)
        loadingDialog?.setContentView(R.layout.layout_loading_tt)
        if (message.isNullOrEmpty()) {
            textMessage?.visibility = View.GONE
        } else {
            textMessage?.text = message
            textMessage?.visibility = View.VISIBLE
        }
        loadingDialog?.setOnCancelListener {
            if (onCancelListener != null) {
                onCancelListener.onCancel(true)
            }
        }
        loadingDialog?.show()
    }

    fun hideLoadingDialog() {
        loadingDialog?.dismiss()
    }

    open fun setFullStatusTransparent(layoutActionbar: View?) {
        val window = window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        window.statusBarColor = Color.TRANSPARENT
        //        String bgColor = "#00000000";
//        setBackgroudColor(layout, bgColor);
        if (layoutActionbar == null) {
            return
        }
        val statusBarHeight = getStatusBarHeight()
        if (layoutActionbar?.getLayoutParams() is RelativeLayout.LayoutParams) {
            val params = layoutActionbar?.getLayoutParams() as RelativeLayout.LayoutParams
            params.topMargin = statusBarHeight
            layoutActionbar?.setLayoutParams(params)
        } else if (layoutActionbar?.getLayoutParams() is LinearLayout.LayoutParams) {
            val params = layoutActionbar?.getLayoutParams() as LinearLayout.LayoutParams
            params.topMargin = statusBarHeight
            layoutActionbar?.setLayoutParams(params)
        } else if (frameLayoutFullscreen?.layoutParams is RelativeLayout.LayoutParams) {
            val params = frameLayoutFullscreen?.getLayoutParams() as RelativeLayout.LayoutParams
            params.topMargin = statusBarHeight
            frameLayoutFullscreen?.setLayoutParams(params)
        }
    }

    fun setFullStatusTransparent() {
//        return
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.decorView.systemUiVisibility = (
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        } else {
//            window.setDecorFitsSystemWindows(false)
            window.insetsController?.let {
                it.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_TOUCH
                it.show(WindowInsets.Type.navigationBars())
            }
        }
        window.statusBarColor = Color.TRANSPARENT
        setBackgroudColor(layoutActionbar, "#00000000")
        val statusBarHeight = getStatusBarHeight()
        if (layoutActionbar?.getLayoutParams() is RelativeLayout.LayoutParams) {
            val params = layoutActionbar?.getLayoutParams() as RelativeLayout.LayoutParams
            params.topMargin = statusBarHeight
            layoutActionbar?.setLayoutParams(params)
        } else if (layoutActionbar?.getLayoutParams() is LinearLayout.LayoutParams) {
            val params = layoutActionbar?.getLayoutParams() as LinearLayout.LayoutParams
            params.topMargin = statusBarHeight
            layoutActionbar?.setLayoutParams(params)
        } else if (frameLayoutFullscreen?.layoutParams is RelativeLayout.LayoutParams) {
            val params = frameLayoutFullscreen?.getLayoutParams() as RelativeLayout.LayoutParams
            params.topMargin = statusBarHeight
            frameLayoutFullscreen?.setLayoutParams(params)
        }
    }

    fun setBackgroudActionbar() {
        val layout = findViewById<View>(R.id.layoutActionbar) ?: return
        setBackgroudColor(layout, actionBarColor)

    }

    val actionBarColor = "#230F33"
    fun setBackgroudActionbar(actionBarView: View?) {
        actionBarView ?: return
        setBackgroudColor(actionBarView, actionBarColor)
    }

    open fun setBackgroudActionbarFragment(rootView: View?) {
        if (rootView == null) {
            return
        }
        val layout = rootView.findViewById<View>(R.id.layoutActionbar) ?: return
//        val itemAppConfig: ItemAppConfig =
//            MyApplication.getInstance().dataManager.getItemAppConfig()
//        if (itemAppConfig == null || MyApplication.getInstance()
//                .isEmpty(itemAppConfig.getBgHeader())
//        ) {
//            return
//        }
//        val bgColor: String = itemAppConfig.getBgHeader().trim()
        val bgColor: String = "#230F33"
        setBackgroudColor(layout, bgColor)
    }

    open fun setDefaultBackgroundColorImage() {
        val layout = findViewById<View>(R.id.layoutBackground) ?: return
//        val itemAppConfig: ItemAppConfig =
//            MyApplication.getInstance().dataManager.getItemAppConfig()
//        if (itemAppConfig == null || MyApplication.getInstance()
//                .isEmpty(itemAppConfig.getBgHeader())
//        ) {
//            return
//        }
//        val bgColor: String = itemAppConfig.getBgHeader().trim()
        setBackgroudColor(layout, actionBarColor)
    }

    open fun setDefaultBackgroudColor() {
        val layout = findViewById<View>(R.id.layoutBackground) ?: return
//        val itemAppConfig: ItemAppConfig =
//            MyApplication.getInstance().dataManager.getItemAppConfig()
//        if (itemAppConfig == null || MyApplication.getInstance()
//                .isEmpty(itemAppConfig.getBackground())
//        ) {
//            return
//        }
//        val bgColor: String = itemAppConfig.getBackground().trim()
        val bgColor: String = "#230F33"
        setBackgroudColor(layout, bgColor)
    }

    fun setFullStatusTransparentFragment(layoutView: View? = null) {
        /* if (6 % 2 == 0) {
             return
         }*/
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.decorView.systemUiVisibility = (
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        } else {
//            window.setDecorFitsSystemWindows(false)
            window.insetsController?.let {
                it.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_TOUCH
                it.show(WindowInsets.Type.navigationBars())
            }
//            window.insetsController?.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_TOUCH
//            window.insetsController?.show(WindowInsets.Type.navigationBars())
        }
        window.statusBarColor = Color.TRANSPARENT
        if (layoutView != null) {
            val statusBarHeight = getStatusBarHeight()
            setBackgroudColor(layoutView, "#00000000")
            if (layoutView.layoutParams is RelativeLayout.LayoutParams) {
                val params = layoutView.layoutParams as RelativeLayout.LayoutParams
                params.topMargin = statusBarHeight
                layoutView.layoutParams = params
            } else if (layoutView.layoutParams is LinearLayout.LayoutParams) {
                val params = layoutView.layoutParams as LinearLayout.LayoutParams
                params.topMargin = statusBarHeight
                layoutView.layoutParams = params
            } else if (layoutView.layoutParams is ConstraintLayout.LayoutParams) {
                val params = layoutView.layoutParams as ConstraintLayout.LayoutParams
                params.topMargin = statusBarHeight
                layoutView.layoutParams = params
            }
        }
    }

    fun setStatusbarColor(bgColor: String?, isDarkTextStatusbar: Boolean) {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.parseColor(bgColor)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            if (!isDarkTextStatusbar) {
                window.decorView.systemUiVisibility =
                    window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() //set status text  light
            } else {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
    }

    fun setTextStatusbarColor(isDarkTextStatusbar: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            if (!isDarkTextStatusbar) {
                window.decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR //set status text  light
            } else {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
    }

    fun setBackgroudColor(view: View?, bgColor: String) {
        view?.setBackgroundColor(Color.parseColor(bgColor))
    }

    fun getStatusBarHeight(): Int {
        val res = resources.getIdentifier("status_bar_height", "dimen", "android")
        var statusBarHeight = 0
        if (res != 0) {
            statusBarHeight = resources.getDimensionPixelSize(res)
        }
        return statusBarHeight
    }

    fun setDefaultBackgroundImage() {
        MyApplication.getInstance().loadDrawableImageNow(this, R.drawable.bg_main_tt, imageBgMain)
    }

    fun setTabProductBackgroundImage() {
        MyApplication.getInstance()
            .loadDrawableImageNow(this, R.drawable.bg_tab_product, imageBgMain)
    }

    fun loadImageLogoActionbar() {
        val itemAppConfig = MyApplication.getInstance().dataManager.itemAppConfig
        if (itemAppConfig?.logo.isNullOrEmpty()) {
            return
        }
        imageLogoActionbar.maxWidth = ScreenSize(this).width / 3
//        MyApplication.getInstance().loadImageNoPlaceHolder(this, imageLogoActionbar, itemAppConfig?.logo)
        MyApplication.getInstance()
            .loadImageNoPlaceHolder(this, imageLogoActionbarCenter, itemAppConfig?.logo)
//        imageLogoActionbar.setVisibility(View.VISIBLE)
    }

    open fun showDialogFinish(message: String?) {
        showDialog(
            false,
            getString(R.string.notification),
            message,
            getString(R.string.close),
            null,
            object : OnDialogButtonListener {
                override fun onLeftButtonClick() {
                    finish()
                }

                override fun onRightButtonClick() {}
            })
    }

    fun showDialog(message: String?) {
        showDialog(
            true,
            getString(R.string.notification),
            message,
            getString(R.string.close),
            null,
            null
        )
    }

    fun showDialog(message: String?, onDialogButtonListener: OnDialogButtonListener?) {
        showDialog(
            true,
            getString(R.string.notification),
            message,
            getString(R.string.close),
            null,
            onDialogButtonListener
        )
    }

    fun showDialog(
        cancelable: Boolean,
        message: String?,
        onDialogButtonListener: OnDialogButtonListener?
    ) {
        showDialog(
            cancelable,
            getString(R.string.notification),
            message,
            getString(R.string.close),
            null,
            onDialogButtonListener
        )
    }

    fun showDialog(
        cancelable: Boolean, titleId: Int, messageId: Int,
        leftButtonTitleId: Int, rightButtonTitleId: Int,
        dialogButtonListener: OnDialogButtonListener?
    ) {
        var message: String? = null
        if (messageId != 0) {
            message = getString(messageId)
        }
        showDialog(
            cancelable,
            titleId,
            message,
            leftButtonTitleId,
            rightButtonTitleId,
            dialogButtonListener
        )
    }

    fun showDialog(
        cancelable: Boolean, titleId: Int, message: String?,
        leftButtonTitleId: Int, rightButtonTitleId: Int,
        dialogButtonListener: OnDialogButtonListener?
    ) {
        var title: String? = null
        var leftButtonTitle: String? = null
        var rightButtonTitle: String? = null
        if (titleId != 0) {
            title = getString(titleId)
        }
        if (leftButtonTitleId != 0) {
            leftButtonTitle = getString(leftButtonTitleId)
        }
        if (rightButtonTitleId != 0) {
            rightButtonTitle = getString(rightButtonTitleId)
        }
        showDialog(
            cancelable,
            title,
            message,
            leftButtonTitle,
            rightButtonTitle,
            dialogButtonListener
        )
    }

    fun showDialog(
        cancelable: Boolean, title: String?, message: String?,
        leftButtonTitle: String?, rightButtonTitle: String?,
        onDialogButtonListener: OnDialogButtonListener?
    ): MyDialog? {
        return showDialog(
            cancelable,
            false,
            title,
            message,
            leftButtonTitle,
            rightButtonTitle,
            onDialogButtonListener
        )
    }

    fun setFullScreen(isFullScreen: Boolean) {
        if (isFullScreen) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                window.setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
                )
            } else {

            }

        } else {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            } else {

            }
        }
    }

    fun showDialog(
        cancelable: Boolean, cancelableOutside: Boolean, title: String?, message: String?,
        leftButtonTitle: String?, rightButtonTitle: String?,
        onDialogButtonListener: OnDialogButtonListener?
    ): MyDialog? {
        if (isFinishing || isDestroyed) {
            return null
        }
        val dialog = MyDialog(this)

        setCurrentDialog(dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = WindowManager.LayoutParams()
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimationLeftRight
        dialog.window?.setAttributes(lp)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(cancelable)
        dialog.setCanceledOnTouchOutside(cancelableOutside)
        dialog.setContentView(R.layout.layout_dialog_tt)
        val screenSize = ScreenSize(this)
        dialog.window?.setLayout(screenSize.width * 9 / 10, WindowManager.LayoutParams.WRAP_CONTENT)
        val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
        val textTitle: TextView = dialog.findViewById(R.id.textTitle)
        val textMessage: TextView = dialog.findViewById(R.id.textMessage)
        val buttonLeft: TextView = dialog.findViewById(R.id.buttonLeft)
        val buttonRight: TextView = dialog.findViewById(R.id.buttonRight)
        //        View viewDivider = dialog.findViewById(R.id.viewDivider);
        layoutRoot.clipToOutline = true
        layoutRoot.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 15f
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        }
        if (!title.isNullOrEmpty()) {
            textTitle.text = title
        }
        if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
            TextUtil.setHtmlTextView(message, textMessage)
            textMessage.movementMethod = LinkMovementMethod.getInstance()
        }
        if (leftButtonTitle.isNullOrEmpty()) {
            buttonLeft.visibility = View.GONE
            //            viewDivider.setVisibility(View.GONE);
        } else {
            buttonLeft.text = leftButtonTitle
            buttonLeft.visibility = View.VISIBLE
        }
        if (rightButtonTitle.isNullOrEmpty()) {
            buttonRight.visibility = View.GONE
            //            viewDivider.setVisibility(View.GONE);
        } else {
            buttonRight.text = rightButtonTitle
            buttonRight.visibility = View.VISIBLE
        }
        buttonLeft.setOnClickListener {
            dialog.dismiss()
            onDialogButtonListener?.onLeftButtonClick()
        }
        buttonRight.setOnClickListener {
            dialog.dismiss()
            onDialogButtonListener?.onRightButtonClick()
        }
        try {
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

    fun showDialogUser(message: String?) {
        showDialogUser(
            true,
            getString(R.string.notification),
            message,
            getString(R.string.yes),
            null,
            null
        )
    }

    fun showDialogUser(
        cancelable: Boolean,
        title: String?,
        message: String?,
        leftButtonTitle: String?,
        rightButtonTitle: String?,
        onDialogButtonListener: OnDialogButtonListener?
    ): Dialog? {
        if (isFinishing || isDestroyed) {
            return null
        }
        val dialog = MyDialog(this)
        if (dialog.window != null) {
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        val lp = WindowManager.LayoutParams()
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimationLeftRight
        dialog.window!!.attributes = lp
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(cancelable)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_dialog_user_tt)
        val screenSize = ScreenSize(this)
        dialog.window!!.setLayout(
            screenSize.width * 9 / 10,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
        val textTitle: TextView = dialog.findViewById(R.id.textTitle)
        val textMessage: TextView = dialog.findViewById(R.id.textMessage)
        val buttonLeft: Button = dialog.findViewById(R.id.buttonLeft)
        val buttonRight: Button = dialog.findViewById(R.id.buttonRight)
        val viewDivider: View = dialog.findViewById(R.id.viewDivider)
        layoutRoot.clipToOutline = true
        layoutRoot.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 15f
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        }
        if (!title.isNullOrEmpty()) {
            textTitle.text = title
        }
        if (!message.isNullOrEmpty()) {
            textMessage.text = message
        }
        if (leftButtonTitle.isNullOrEmpty()) {
            buttonLeft.visibility = View.GONE
            viewDivider.visibility = View.GONE
        } else {
            buttonLeft.text = leftButtonTitle
            buttonLeft.visibility = View.VISIBLE
        }
        if (rightButtonTitle.isNullOrEmpty()) {
            buttonRight.visibility = View.GONE
            viewDivider.visibility = View.GONE
        } else {
            buttonRight.text = rightButtonTitle
            buttonRight.visibility = View.VISIBLE
        }
        buttonLeft.setOnClickListener {
            dialog.dismiss()
            onDialogButtonListener?.onLeftButtonClick()
        }
        buttonRight.setOnClickListener {
            dialog.dismiss()
            onDialogButtonListener?.onRightButtonClick()
        }
        try {
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

    fun showDialogVoucher(
        cancelable: Boolean,
        cancelableOutside: Boolean,
        imageUrl: String?,
        title: Int?,
        message: String?,
        leftButtonTitle: Int?,
        centerButtonTitle: Int?,
        rightButtonTitle: Int?,
        listener: OnDialogButtonVoucherListener?
    ): MyDialog? {
        val titleStr = if (title != null) getString(title) else null
        val leftButtonTitleStr = if (leftButtonTitle != null) getString(leftButtonTitle) else null
        val centerButtonTitleStr =
            if (centerButtonTitle != null) getString(centerButtonTitle) else null
        val rightButtonTitleStr =
            if (rightButtonTitle != null) getString(rightButtonTitle) else null
        return showDialogVoucher(
            cancelable,
            cancelableOutside,
            imageUrl,
            titleStr,
            message,
            leftButtonTitleStr,
            centerButtonTitleStr,
            rightButtonTitleStr,
            listener
        )
    }

    fun showDialogVoucher(
        cancelable: Boolean,
        cancelableOutside: Boolean,
        imageUrl: String?,
        title: String?,
        message: String?,
        leftButtonTitle: String?,
        centerButtonTitle: String?,
        rightButtonTitle: String?,
        listener: OnDialogButtonVoucherListener?
    ): MyDialog? {
        if (isFinishing || isDestroyed) {
            return null
        }
        val dialog = MyDialog(this)
        setCurrentDialogVoucher(dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = WindowManager.LayoutParams()
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimationLeftRight
        dialog.window?.setAttributes(lp)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(cancelable)
        dialog.setCanceledOnTouchOutside(cancelableOutside)
        dialog.setContentView(R.layout.layout_voucher_notification)
        val screenSize = ScreenSize(this)
        dialog.window?.setLayout(screenSize.width * 9 / 10, WindowManager.LayoutParams.WRAP_CONTENT)
        val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
        val textTitle: TextView = dialog.findViewById(R.id.textTitle)
        val textMessage: TextView = dialog.findViewById(R.id.textMessage)
        val image: ImageView = dialog.findViewById(R.id.image)
        val buttonLeft: TextView = dialog.findViewById(R.id.buttonLeft)
        val buttonCenter: TextView = dialog.findViewById(R.id.buttonCenter)
        val buttonRight: TextView = dialog.findViewById(R.id.buttonRight)
        //        View viewDivider = dialog.findViewById(R.id.viewDivider);
        layoutRoot.clipToOutline = true
        layoutRoot.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 15f
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        }
        if (!title.isNullOrEmpty()) {
            textTitle.text = title
        }
        if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
            TextUtil.setHtmlTextView(message, textMessage)
            textMessage.movementMethod = LinkMovementMethod.getInstance()
        }
        if (!leftButtonTitle.isNullOrEmpty()) {
            buttonLeft.text = leftButtonTitle
            buttonLeft.visibility = View.VISIBLE
        }
        if (!centerButtonTitle.isNullOrEmpty()) {
            buttonCenter.text = centerButtonTitle
            buttonCenter.visibility = View.VISIBLE
        }
        if (!rightButtonTitle.isNullOrEmpty()) {
            buttonRight.text = rightButtonTitle
            buttonRight.visibility = View.VISIBLE
        }
        if (!imageUrl.isNullOrEmpty()) {
            image.visibility = View.VISIBLE
            val imageHeight = ScreenSize(this).width * 5 / 8
            val lparams = image.layoutParams as LinearLayout.LayoutParams
            lparams.height = imageHeight
            MyApplication.getInstance().loadImage(this, image, imageUrl)
        }
        buttonLeft.setOnClickListener {
            dialog.dismiss()
            listener?.onLeftButtonClick()
        }
        buttonCenter.setOnClickListener {
            dialog.dismiss()
            listener?.onCenterButtonClick()
        }
        buttonRight.setOnClickListener {
            dialog.dismiss()
            listener?.onRightButtonClick()
        }
        try {
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showToast(messageId: Int) {
        Toast.makeText(this, messageId, Toast.LENGTH_SHORT).show()
    }

    fun gotoActivity(intent: Intent?) {
        if (intent != null) {
            startActivity(intent)
        }
    }

    fun gotoActivity(cla: Class<*>?) {
        if (cla == null) {
            return
        }
        val intent = Intent(this, cla)
        startActivity(intent)
    }

    fun gotoActivity(cla: Class<*>?, flag: Int?) {
        if (cla == null) {
            return
        }
        val intent = Intent(this, cla)
        if (flag != null) {
            intent.setFlags(flag)
        }
        startActivity(intent)
    }

    fun gotoActivity(cla: Class<*>?, bundle: Bundle?) {
        if (cla == null) {
            return
        }
        val intent = Intent(this, cla)
        if (bundle != null) {
            intent.putExtras(bundle)
        }
        startActivity(intent)
    }

    fun gotoActivityForResult(cla: Class<*>?, bundle: Bundle?, requestCode: Int) {
        if (cla == null) {
            return
        }
        val intent = Intent(this, cla)
        if (bundle != null) {
            intent.putExtras(bundle)
        }
        startActivityForResult(intent, requestCode)
    }

    fun gotoActivityForResult(cla: Class<*>?, requestCode: Int) {
        if (cla == null) {
            return
        }
        val intent = Intent(this, cla)
        startActivityForResult(intent, requestCode)
    }

    fun showExitCofirmDialog() {
        showDialog(
            true,
            true,
            getString(R.string.notification),
            getString(R.string.exit_msg),
            getString(R.string.yes2),
            getString(R.string.no),
            object : OnDialogButtonListener {
                override fun onLeftButtonClick() {
                    finish()
                }

                override fun onRightButtonClick() {
                }
            })
    }

    fun showKeyboardFocus(editText: EditText?) {
        editText?.requestFocus()
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    fun hideKeyboard(editText: EditText?) {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            editText?.windowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    fun saveUser(
        username: String?,
        password: String?,
        userId: String?,
        loginType: Int,
        accessToken: String?
    ) {
        val sharedPreferences = getSharedPreferences(Constant.CONFIG, MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(Constant.USERNAME, username)
        editor.putString(Constant.PASSWORD, password)
        editor.putString(Constant.ACCESS_TOKEN, accessToken)
        editor.putString(Constant.USER_ID, userId)
        editor.putInt(Constant.LOGIN_TYPE, loginType)
        editor.apply()
    }

    fun shareLinkApp() {
        val itemAppConfig: ItemAppConfig? = MyApplication.getInstance().dataManager.itemAppConfig
        if (itemAppConfig?.linkShareApp.isNullOrEmpty()) {
            return
        }
        ServiceUtilTT.addShareLinkApp(this)
        shareApp(itemAppConfig?.linkShareApp)
    }

    fun shareApp(itemObj: ItemNews) {
        shareApp(itemObj.url)
        ServiceUtilTT.addShare(this, itemObj.id)
    }

    fun shareApp(url: String?) {
        if (url == null || url.isEmpty()) {
            showToast(R.string.msg_empty_data)
            return
        }
        ShareCompat.IntentBuilder.from(this)
            .setType("text/plain")
            .setText(url)
            .setChooserTitle(getString(R.string.select_share_app))
            .startChooser()
    }

    fun keepScreenOn() {
        Loggers.e("CHECK_KEEP_SCREEN", "keepScreenOn")
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    fun removeKeepScreenOn() {
        try {
            Loggers.e("CHECK_KEEP_SCREEN", "removeKeepScreenOn")
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } catch (e: java.lang.Exception) {
            Loggers.e("CHECK_KEEP_SCREEN", "removeKeepScreenOn Exception")
            e.printStackTrace()
        }
    }

    fun openLink(link: String?) {
        if (link.isNullOrEmpty()) {
            return
        }
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace();
        } catch (e: Exception) {
            e.printStackTrace();
        }
    }

    open fun setOnKeyboardVisibilityListener(onKeyboardVisibilityListener: OnKeyboardVisibilityListener) {
        val parentView = (findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0)
        parentView.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            private var alreadyOpen = false
            private val defaultKeyboardHeightDP = 100
            private val EstimatedKeyboardDP =
                defaultKeyboardHeightDP + if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) 48 else 0
            private val rect = Rect()
            override fun onGlobalLayout() {
                val estimatedKeyboardHeight = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    EstimatedKeyboardDP.toFloat(),
                    parentView.resources.displayMetrics
                ).toInt()
                parentView.getWindowVisibleDisplayFrame(rect)
                val heightDiff = parentView.rootView.height - (rect.bottom - rect.top)
                val isShown = heightDiff >= estimatedKeyboardHeight
                if (isShown == alreadyOpen) {
                    Loggers.e("Keyboard state", "Ignoring global layout change...")
                    return
                }
                alreadyOpen = isShown
                onKeyboardVisibilityListener.onVisibilityChanged(isShown)
            }
        })
    }

    fun goDetailNewsActivity(itemObj: ItemNews) {
        val bundle = Bundle()
        bundle.putParcelable(Constant.DATA, itemObj)
        var cla: Class<*>? = DetailNewsActivity::class.java
        when (itemObj.contentType) {
            Constant.TYPE_NEWS -> cla = DetailNewsActivity::class.java
            Constant.TYPE_VIDEO -> cla = DetailVideoActivity::class.java
        }
        gotoActivity(cla, bundle)
    }

    fun goDetailProductActivity(itemObj: ItemNews, holder: RecyclerView.ViewHolder?) {
        val bundle = Bundle()
        bundle.putParcelable(Constant.DATA, itemObj)
        /*if (holder != null) {
            val intent = Intent(this, DetailProductActivity::class.java)
            intent.putExtras(bundle)
            val options = ActivityOptions.makeSceneTransitionAnimation(this,
                    Pair.create(holder.itemView.imageThumbnail, "imageThumbnail"),
//                    Pair.create(holder.itemView.textPrice, "textPrice"),
                    Pair.create(holder.itemView.textTitle, "textTitle"))
            startActivity(intent, options.toBundle())
        } else {
            gotoActivity(DetailProductActivity::class.java, bundle)
        }*/
        gotoActivity(DetailProductActivity::class.java, bundle)
    }

    fun goDetailEventActivity(itemObj: ItemNews, holder: RecyclerView.ViewHolder?) {
        val bundle = Bundle()
        bundle.putParcelable(Constant.DATA, itemObj)
        /*if (holder != null) {
            val intent = Intent(this, DetailEventActivity::class.java)
            intent.putExtras(bundle)
            val options = ActivityOptions.makeSceneTransitionAnimation(this,
                    Pair.create(holder.itemView.imageThumbnail, "imageThumbnail"),
//                Pair.create(holder.itemView.textTime, "textTime"),
                    Pair.create(holder.itemView.textTitle, "textTitle"))
            startActivity(intent, options.toBundle())
        } else {
            gotoActivity(DetailEventActivity::class.java, bundle)
        }*/
        gotoActivity(DetailEventActivity::class.java, bundle)
    }

    fun showImageZoomPagerBottomSheetDialog(itemObj: ItemNews) {
        val fragment = ImageZoomPagerBottomSheetDialog()
        val bundle = Bundle()
        bundle.putParcelable(Constant.DATA, itemObj)
        fragment.arguments = bundle
        fragment.show(supportFragmentManager, fragment.tag)
    }

    var isActivityPaused = false

    fun View.getLocationOnScreen(): Point {
        val location = IntArray(2)
        this.getLocationOnScreen(location)
        return Point(location[0], location[1])
    }

    fun showRestartDialog() {
        val appId = MyApplication.getInstance().appId
        if (appId.isNullOrEmpty()) {
            showDialog(
                false,
                getString(R.string.notification),
                getString(R.string.msg_app_error_restart),
                getString(R.string.close),
                null,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        restartApp()
                    }

                    override fun onRightButtonClick() {}
                })
        }
    }

    open fun restartApp() {
        val mStartActivity = Intent(this, SplashActivity::class.java)
        val mPendingIntentId = Random().nextInt(10000)
        val mPendingIntent = PendingIntent.getActivity(
            this,
            mPendingIntentId,
            mStartActivity,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
        val mgr = getSystemService(ALARM_SERVICE) as AlarmManager
        mgr[AlarmManager.RTC, System.currentTimeMillis() + 300] = mPendingIntent
        System.exit(0)
    }

    open fun showBottomSheetDialog(fragment: Fragment, bundle: Bundle?) {
        if (fragment !is BottomSheetDialogFragment) {
            return
        }
        if (bundle != null) {
            fragment.setArguments(bundle)
        }
        Handler(Looper.getMainLooper()).postDelayed({
            fragment.show(supportFragmentManager, fragment.getTag())
        }, Constant.DELAY_HANDLE)
    }

    private var broadCastRecv: BroadcastReceiver? = null

    fun initBroadcastReceiver() {
        if (broadCastRecv != null) {
            return
        }
        broadCastRecv = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action.isNullOrEmpty()) {
                    return
                }
                val bundle = intent.extras ?: return
                var voucherType = -1
                if ((intent.action == Constant.VOUCHER_TYPE)) {
                    voucherType = bundle.getInt(
                        Constant.TYPE_NOTIFY_VOUCHER_KEY,
                        Constant.TYPE_NOTIFY_VOUCHER_DONATE
                    )
                }
                if (Constant.TYPE_INTERACTIVE.equals(MainActivity.typeShowNotify) || Constant.TYPE_FORUM.equals(
                        MainActivity.typeShowNotify
                    )
                ) {
                    if (this@BaseActivity is MainActivity) {
                        this@BaseActivity.showNotify()
                    } else {
                        val intentInteractive = Intent(this@BaseActivity, MainActivity::class.java)
                        intentInteractive.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intentInteractive)
                    }
                    return
                }
                if (Constant.TYPE_VOUCHER.equals(MainActivity.typeShowNotify)) {
                    GeneralUtils.openMyVoucher(this@BaseActivity)
                    MainActivity.typeShowNotify = null
                    return
                }
                val itemParcelable: Parcelable = bundle.getParcelable(Constant.DATA) ?: return
                handleBroadcast(voucherType, itemParcelable)
//>>>>>>> master_interactive

            }
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(Constant.VOUCHER_TYPE)
        intentFilter.addAction(Constant.TYPE_CHAT)
        intentFilter.addAction(Constant.TYPE_NEWS)
        intentFilter.addAction(Constant.TYPE_VIDEO)
        intentFilter.addAction(Constant.TYPE_INTERACTIVE)
        intentFilter.addAction(Constant.TYPE_FORUM)
        registerReceiver(broadCastRecv, intentFilter)
    }

    fun handleBroadcast(voucherType: Int, itemParcelable: Parcelable?) {
        if (itemParcelable == null) {
            return
        }
        MyApplication.getInstance().dataManager.itemParcelable = null
        if (itemParcelable is ItemNews) {
            goDetailNewsActivity(itemParcelable)
            return
        }
        if (itemParcelable is ItemChat) {
            val itemChat = itemParcelable
            val itemConversation = ItemConversation(
                "",
                itemChat.voucherId,
                itemChat.userId,
                itemChat.uniqueId,
                itemChat.userName,
                itemChat.userAvatar,
                itemChat.content,
                itemChat.image,
                itemChat.content,
                "",
                0,
                null,
                itemChat.zType, itemChat.prizeType
            )
            val bundleChat = Bundle()
            bundleChat.putParcelable(Constant.DATA, itemConversation)
            if (this@BaseActivity is ChatSupportActivity) {
                runOnUiThread {
                    if (MyApplication.getInstance().dataManager.isChat) {
                        this.showChatConversationFragment(itemConversation)
                    } else {
                        this.updateConversion(itemConversation)
                    }
                }
                return
            }
            val intentChat = Intent(this, ChatSupportActivity::class.java)
            intentChat.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intentChat.putExtras(bundleChat)
            startActivity(intentChat)
            return
        }
        if (voucherType == Constant.TYPE_NOTIFY_VOUCHER_DONATE) {
            if (itemParcelable is ItemVoucher) {
                val itemObj: ItemVoucher = itemParcelable
                showDonatedVoucherDialog(itemObj)
            }
            return
        }
        if (voucherType == Constant.TYPE_NOTIFY_VOUCHER_DONATE_RESULT) {
            if (itemParcelable is MyFirebaseData) {
                val myFirebaseData: MyFirebaseData = itemParcelable
                dismissAllDialog()
                if (this@BaseActivity is VoucherActivity) {
                    this@BaseActivity.refreshData()
                }
                showDialogVoucher(
                    true,
                    true,
                    myFirebaseData.img,
                    R.string.notification,
                    myFirebaseData.body,
                    null,
                    null,
                    R.string.close,
                    null
                )
//                Handler(Looper.getMainLooper()).postDelayed({
//                    dismissAllDialog()
//                }, 2000)
            }
        }
    }

    //    START: SHOW DONATED VOUCHER DIALOG
    fun showDonatedVoucherDialog(itemObj: ItemVoucher, onResultListener: OnResultListener? = null) {
        showDialogVoucher(
            true,
            false,
            itemObj.image,
            R.string.notification,
            itemObj.message,
            R.string.receive,
            null,
            R.string.decline,
            object : OnDialogButtonVoucherListener {
                override fun onLeftButtonClick() {
                    updateDonatedStatus(itemObj, "yes", onResultListener)
                }

                override fun onCenterButtonClick() {
                }

                override fun onRightButtonClick() {
                    updateDonatedStatus(itemObj, "no", onResultListener)
                }
            })
    }

    fun updateDonatedStatus(
        itemObj: ItemVoucher,
        type: String,
        onResultListener: OnResultListener? = null
    ) {
        val requestParams = RequestParams()
        requestParams.put("voucherid", "${itemObj.id}")
        requestParams.put("unique_id", "${itemObj.uniqueId}")
        requestParams.put("type", type)
        ServiceManager.requestBase(
            this,
            Constant.API_VOUCHER_DONATED_CONFIRM,
            requestParams,
            object : ServiceManager.OnBaseRequestListener {
                override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                    showLoadingDialog(true, null)
                }

                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    responseString: String?
                ) {
                    if (isDestroyed || isFinishing) {
                        return
                    }
                    runOnUiThread {
                        hideLoadingDialog()
                        if (!isSuccess) {
                            if (message != null && !message.equals(getString(R.string.msg_network_error))) {
                                onResultListener?.onResult(true, message)
                            }
                            showDialogVoucher(
                                true,
                                false,
                                null,
                                R.string.notification,
                                message,
                                null,
                                null,
                                R.string.close,
                                object : OnDialogButtonVoucherListener {
                                    override fun onLeftButtonClick() {
                                    }

                                    override fun onCenterButtonClick() {
                                    }

                                    override fun onRightButtonClick() {
                                    }

                                })
                            return@runOnUiThread
                        }
                        onResultListener?.onResult(true, message)
                        showDialogVoucher(
                            true,
                            false,
                            null,
                            R.string.notification,
                            message,
                            null,
                            R.string.view_voucher,
                            R.string.close,
                            object : OnDialogButtonVoucherListener {
                                override fun onLeftButtonClick() {
                                }

                                override fun onCenterButtonClick() {
                                    val itemUser = MyApplication.getInstance().dataManager.itemUser
                                    if (itemUser != null) {
                                        itemObj.userId = itemUser.id
                                    }
                                    val bundle = Bundle()
                                    bundle.putParcelable(Constant.DATA, itemObj)
                                    if (this@BaseActivity is VoucherActivity) {
                                        this@BaseActivity.dismissDonateVoucherFragment()
                                        this@BaseActivity.showDetailVoucherFragment(
                                            bundle,
                                            object : OnResultListener {
                                                override fun onResult(
                                                    isSuccess: Boolean,
                                                    msg: String?
                                                ) {
                                                    this@BaseActivity.refreshData()
                                                }
                                            })
                                    } else {
                                        bundle.putInt(
                                            Constant.VOUCHER_TYPE,
                                            Constant.DETAIL_VOUCHER
                                        )
                                        gotoActivity(VoucherActivity::class.java, bundle)
                                    }
                                }

                                override fun onRightButtonClick() {
                                    if (this@BaseActivity is VoucherActivity) {
                                        this@BaseActivity.refreshData()
                                    }
                                }

                            })
                    }
                }
            },
            false
        )
    }
//    END: SHOW DONATED VOUCHER DIALOG

    override fun onResume() {
        super.onResume()
        isActivityPaused = false
        MyApplication.getInstance().dataManager.isRunBackground = false
        if (this.javaClass.canonicalName != SplashActivity::class.java.canonicalName) {
            showRestartDialog()
            if (broadCastRecv == null) {
                initBroadcastReceiver()
            }
        }
        handleBroadcast(
            MyApplication.getInstance().dataManager.typeNotifyVoucherKey,
            MyApplication.getInstance().dataManager.itemParcelable
        )
        if (MainActivity.typeShowNotify != null && (MainActivity.typeShowNotify.equals(Constant.TYPE_INTERACTIVE) || MainActivity.typeShowNotify.equals(
                Constant.TYPE_FORUM
            ))
        ) {
            if (this@BaseActivity !is MainActivity) {
                val intentInteractive = Intent(this@BaseActivity, MainActivity::class.java)
                intentInteractive.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intentInteractive)
            }
        }
        if (Constant.TYPE_VOUCHER.equals(MainActivity.typeShowNotify)) {
            GeneralUtils.openMyVoucher(this@BaseActivity)
            MainActivity.typeShowNotify = null
        }
        //Luan add
        if (this !is MainActivity) {
            initITV()
        } else {
            if (voiceControl != null) {
                voiceControl!!.startListenContinuos()
            }
        }
    }


    override fun onPause() {
        super.onPause()
        isActivityPaused = true
        MyApplication.getInstance().dataManager.isRunBackground = true
        if (broadCastRecv != null) {
            try {
                unregisterReceiver(broadCastRecv)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            broadCastRecv = null;
        }
        //Luan add
        voiceControl?.pauseVoiceControl()
    }


    override fun onDestroy() {
        super.onDestroy()
        if (this.javaClass.canonicalName == MainActivity::class.java.canonicalName) {
            MyApplication.getInstance().clearDataManager()
        }
        System.gc()
    }

    fun playVibrator(minilliseconds: Long = 0) {
        val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
// Vibrate for 500 milliseconds
// Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v?.vibrate(
                VibrationEffect.createOneShot(
                    minilliseconds,
                    VibrationEffect.DEFAULT_AMPLITUDE
                )
            )
        } else {
            //deprecated in API 26
            v?.vibrate(minilliseconds)
        }
    }

    var dialogVoucher: Dialog? = null
    var curDialog: Dialog? = null
    fun setCurrentDialog(dialog: Dialog?) {
        curDialog?.dismiss()
        curDialog = dialog
    }

    fun setCurrentDialogVoucher(dialog: Dialog?) {
        dialogVoucher?.dismiss()
        dialogVoucher = dialog
    }

    fun dismissAllDialog() {
        curDialog?.dismiss()
        dialogVoucher?.dismiss()
    }

    open fun initITV(isFirstInit: Boolean = false) {
        val frameLayoutITV = findViewById<FrameLayout>(R.id.frameLayoutITV) ?: return
        frameLayoutITV.post {
            var iTVHeightInPx = frameLayoutITV.height
            if (iTVHeightInPx < 0) {
                iTVHeightInPx = 0
            }
            initVoiceControl(isFirstInit, iTVHeightInPx)
        }
    }

    var voiceControl: VoiceControl? = null
    private var isFirstInit = false
    private var oniTVListener: VoiceControl.OnITVListener? = null

    open fun initVoiceControl(isFirstInit: Boolean, iTVHeightInPx: Int = 0) {
        if (this is SplashActivity) {
            return
        }
        val isStatusBarMargin = if (this@BaseActivity is DetailNewsActivity) true else false
        this.isFirstInit = isFirstInit
        this.oniTVListener = oniTVListener
        if (voiceControl == null) {
            voiceControl = VoiceControl(this, isFirstInit, object : VoiceControl.OnITVListener {
                override fun onGoDetailNews(itemObj: ItemNews) {
                }

                override fun onPlayPlayer(isPlay: Boolean) {
                }

                override fun onSwitchAppHandle(type: Int, keyword: String?) {
                    navigationToMainActivity()
                }

                override fun onUtilShowing(isShowing: Boolean) {
//                    if (!isShowing) {
//                        setStatusbarColor("#ffffff", true)
//                    if(isShowing){
//                        return
//                    }
                    val activity: MainActivity? = getMainActivity()
                    if (activity != null) {
                        activity.setCanBack(!isShowing)
                    }
//                    if (this@BaseActivity is MainActivity || this@BaseActivity is UserLoginActivity) {
//                        setFullStatusTransparent()
//                    } else {
//                        setStatusbarColor("#ffffff", true)
//                    }
                }
            }, iTVHeightInPx, isStatusBarMargin)
            voiceControl!!.initVoiceManger()
        }
        voiceControl!!.startListenContinuos()
    }

    private fun navigationToMainActivity() {
        try {
        } catch (e: Exception) {
        }
        if (this@BaseActivity !is MainActivity) {
            gotoActivity(MainActivity::class.java, Intent.FLAG_ACTIVITY_CLEAR_TOP)
        } else {
            this@BaseActivity.showNotify()
        }
    }

    private fun getMainActivity(): MainActivity? {
        return if (this !is MainActivity) {
            null
        } else this as MainActivity
    }
}
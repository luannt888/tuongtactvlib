package vn.mediatech.istudiocafe.activity

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_detail_event_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.user.UserLoginActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.fragment.BookEventFragment
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.swipebacklayout.SwipeBackLayout
import vn.mediatech.istudiocafe.swipebacklayout.app.SwipeBackActivity
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.TextUtil

class DetailEventActivity : SwipeBackActivity() {
    var itemObj: ItemNews? = null
    var myHttpRequest: MyHttpRequest? = null
    var bookEventFragment: BookEventFragment? = null
    var isBookEventFragmentShowing = false

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(Constant.DATA, itemObj)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_event_tt)
        initSwipeBack()
        initUI()
        initData(savedInstanceState)
        initControl()
    }

    override fun onResume() {
        super.onResume()
        trackScreen(getString(R.string.fa_detail_event))
    }

    fun initSwipeBack() {
        val edgeFlag: Int = SwipeBackLayout.EDGE_LEFT
        swipeBackLayout.setEdgeTrackingEnabled(edgeFlag)
        swipeBackLayout.setEdgeSize(ScreenSize(this).width / 9)
//        mSwipeBackLayout.setTouchFullScreen(true);
    }

    fun initUI() {
        val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout.LayoutParams
//        lp.height = ScreenSize(this).height / 2
        lp.height = ScreenSize(this).width
    }

    fun initData(savedInstanceState: Bundle?) {
        val bundle = savedInstanceState ?: intent.extras
        itemObj = bundle?.getParcelable(Constant.DATA)
        if (itemObj == null) {
            finish()
            return
        }
        setData()
        getData()
    }

    fun showErrorNetwork(message: String) {
        runOnUiThread {
            textNotify.text = message
            textNotify.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            layoutContent.visibility = View.GONE
        }
    }

    fun getData() {
        if (!MyApplication.getInstance().isNetworkConnect) {
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
//        progressBar.visibility = View.VISIBLE
//        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(this)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("id", "${itemObj!!.id}")
        myHttpRequest!!.request(false, ServiceUtilTT.validAPI(Constant.API_EVENT_DETAIL), requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isFinishing || isDestroyed) {
                    return
                }
//                isLoading = false
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isFinishing || isDestroyed) {
                    return
                }
                handleData(responseString)
                runOnUiThread {
                    progressBar.visibility = View.GONE
                }
//                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            runOnUiThread { message?.let { showErrorNetwork(it) } }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
        val dataObj = JsonParser.getJsonObject(resultObj, Constant.DATA)
        itemObj = JsonParser.parseItemNews(this, dataObj, null)
        if (itemObj == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        runOnUiThread {
            layoutContent.visibility = View.VISIBLE
            setData()
        }
    }

    var isFirst = true
    fun setData() {
        textTitle.text = itemObj!!.name
        textTime.text = itemObj!!.time
//        textDescription.text = itemObj!!.description
        TextUtil.setHtmlTextView(itemObj!!.description, textDescription)
//        MyApplication.getInstance().loadImage(this, imageThumbnail, itemObj!!.image)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            MyApplication.getInstance().loadImage(this, imageThumbnail, itemObj!!.image)
        }, 300)
        /*if (isFirst) {
            MyApplication.getInstance().loadImage(this, imageThumbnail, itemObj!!.image)
            isFirst = false
        } else {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                MyApplication.getInstance().loadImage(this, imageThumbnail, itemObj!!.image)
            }, 0)
        }*/

        val price = if (itemObj!!.basePrice <= 0) getString(R.string.free) else TextUtil.formatMoney(itemObj!!.basePrice)
        textPriceInfo.text = price
    }

    fun showBookEventFragment() {
        bookEventFragment = BookEventFragment()
        val bundle = Bundle()
        bundle.putParcelable(Constant.DATA, itemObj)
        bookEventFragment!!.arguments = bundle
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
        fragmentTransaction.add(android.R.id.content, bookEventFragment!!)
        fragmentTransaction.commit()
        isBookEventFragmentShowing = true
    }

    fun removeBookEventFragment(): Boolean {
        if (isBookEventFragmentShowing && bookEventFragment != null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.slide_out_down, R.anim.slide_in_down)
            fragmentTransaction.remove(bookEventFragment!!)
            isBookEventFragmentShowing = false
            fragmentTransaction.commit()
            return true
        }
        return false
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            supportFinishAfterTransition()
        }
        buttonOrder.setOnClickListener {
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser == null) {
                gotoActivityForResult(UserLoginActivity::class.java, Constant.CODE_LOGIN)
                return@setOnClickListener
            }
            showBookEventFragment()
        }
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                if (removeBookEventFragment()) {
                    return true
                }
            }
        }
        return super.onKeyUp(keyCode, event)
    }

    override fun onDestroy() {
        myHttpRequest?.cancel()
        super.onDestroy()
    }
}
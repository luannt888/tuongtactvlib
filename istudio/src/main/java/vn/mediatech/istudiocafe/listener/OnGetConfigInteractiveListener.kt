package vn.mediatech.istudiocafe.listener

import vn.mediatech.istudiocafe.model.ItemInteractiveConfig


interface OnGetConfigInteractiveListener {
    fun onResponse(isSuccess: Boolean, message: String?, item:ItemInteractiveConfig?)
}
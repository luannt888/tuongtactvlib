package vn.mediatech.istudiocafe.listener;

import vn.mediatech.istudiocafe.model.ItemSimpleParam;

public interface OnItemDialogClickListener {
    void onClick(ItemSimpleParam itemSimpleParam, int selectedPosition);
}

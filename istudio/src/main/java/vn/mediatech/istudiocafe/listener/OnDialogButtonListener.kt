package vn.mediatech.istudiocafe.listener

interface OnDialogButtonListener {
    fun onLeftButtonClick()
    fun onRightButtonClick()
}
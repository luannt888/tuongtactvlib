package vn.mediatech.istudiocafe.listener

interface OnBaseRequestListener {
    fun onResponse(isSuccess: Boolean, message: String?, response: String?)
}
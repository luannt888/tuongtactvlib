package vn.mediatech.istudiocafe.listener

import androidx.recyclerview.widget.RecyclerView

interface OnScrolledRecyckerViewListener {
    fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int)
}

package vn.mediatech.voicecontrol

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_suggest_vc.view.*

class ItemSuggestAdapter(
    val context: Context, val itemList: Array<String>
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0


    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_suggest_vc,
            parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: String = itemList.get(position)
        if (holder is MyViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: MyViewHolder, itemObj: String, position: Int) {
        holder.textTitle.text = itemObj
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val textTitle = itemView.textTitle
    }

    interface OnItemClickListener {
        fun onClick(itemObject: String, position: Int)
    }


}
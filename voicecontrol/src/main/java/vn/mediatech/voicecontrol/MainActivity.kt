package vn.mediatech.voicecontrol

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main_vc.*
import java.util.*

class MainActivity : AppCompatActivity() {
    var speechRecognizer: SpeechRecognizer? = null
    private var speechintent: Intent? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_vc)
        initUI()
        initData()
        initControl()
    }

    private fun initControl() {
        buttonVoice.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                when (event.getAction()) {
                    MotionEvent.ACTION_DOWN -> {
                        textVoice.setText("")
                        speechRecognizer!!.startListening(speechintent)
                    }
                    MotionEvent.ACTION_UP -> speechRecognizer!!.stopListening()
                }
                return false
            }
        })
    }

    private fun initData() {
        checkVoiceCommandPermission()
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this)
        speechintent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechintent?.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        );
        speechintent?.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        speechRecognizer?.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(p0: Bundle?) {
                textVoice.setText("start")
            }

            override fun onBeginningOfSpeech() {
            }

            override fun onRmsChanged(p0: Float) {
            }

            override fun onBufferReceived(p0: ByteArray?) {
            }

            override fun onEndOfSpeech() {
            }

            override fun onError(p0: Int) {
            }

            override fun onResults(results: Bundle?) {
                var str = String()
                Loggers.e("VOICE", "onResults $results")
                val data: ArrayList<String>? =
                    results!!.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                for (i in 0 until data!!.size) {
                    Loggers.e("VOICE", "result " + data.get(i))
                    str += data!!.get(i)
                }
                textVoice.setText("results: " + str)
            }

            override fun onPartialResults(p0: Bundle?) {
            }

            override fun onEvent(p0: Int, p1: Bundle?) {
            }
        })
    }

    private fun initUI() {
    }

    private fun checkVoiceCommandPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this@MainActivity,
                    Manifest.permission.RECORD_AUDIO
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                val intent = Intent(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:$packageName")
                )
                startActivity(intent)
                finish()
            }
        }
    }
}
package vn.mediatech.voicecontrol.ui

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.text.Spanned
import android.util.AttributeSet
import android.widget.TextView
import java.util.*
import kotlin.concurrent.timerTask

@SuppressLint("AppCompatCustomView")
class TypeWriterTextView : TextView {
    private var mText: CharSequence? = null
    private var mIndex = 0
    private var mDelay: Long = 150 // in ms

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}

    private var timer: Timer? = null

    fun animateText(txt: CharSequence?) {
        mText = getHtmlFormat(txt.toString())
        mIndex = 0
        text = ""
        timer?.cancel()
        timer?.purge()
        timer = Timer()
        timer?.schedule(timerTask {
            val indexCont = mIndex++
            if (indexCont <= mText!!.length) {
                Handler(Looper.getMainLooper()).post { text = mText!!.subSequence(0, indexCont) }
            } else {
                timer?.cancel()
                timer?.purge()
            }
        }, 0, mDelay)
    }

    fun setCharacterDelay(m: Long) {
        mDelay = m
    }

    fun getHtmlFormat(str: String?): Spanned? {
        var s = str
        if (s == null) {
            s = ""
        }
        val textSpanned: Spanned
        textSpanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(s, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(s)
        }
        return textSpanned
    }

    fun stopAnimate() {
        try {
            timer?.cancel()
            timer?.purge()
        } catch (e: Exception) {
        }
    }
}
/*import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.text.Spanned
import android.util.AttributeSet
import android.widget.TextView

@SuppressLint("AppCompatCustomView")
class TypeWriterTextView : TextView {
    private var mText: CharSequence? = null
    private var mIndex = 0
    private var mDelay: Long = 150 // in ms

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}

    private val mHandler = Handler(Looper.getMainLooper())
    private val characterAdder: Runnable = object : Runnable {
        override fun run() {
            text = getHtmlFormat(mText!!.subSequence(0, mIndex++) as String?)
//            text = mText!!.subSequence(0, mIndex++)
            if (mIndex <= mText!!.length) {
                mHandler.postDelayed(this, mDelay)
            }
        }
    }

    fun animateText(txt: CharSequence?) {
        mText = txt
        mIndex = 0
        text = ""
        mHandler.removeCallbacks(characterAdder)
        mHandler.postDelayed(characterAdder, mDelay)
    }

    fun setCharacterDelay(m: Long) {
        mDelay = m
    }

    fun getHtmlFormat(text: String?): Spanned? {
        var text = text
        if (text == null) {
            text = ""
        }
        val textSpanned: Spanned
        textSpanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(text)
        }
        return textSpanned
    }
    fun stopAnimate(){
        mHandler?.removeCallbacks(characterAdder)
    }
}*/


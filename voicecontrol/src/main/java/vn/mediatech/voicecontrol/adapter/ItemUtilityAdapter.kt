package vn.mediatech.voicecontrol.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import vn.mediatech.voicecontrol.R
import vn.mediatech.voicecontrol.model.ItemUtility
import vn.mediatech.voicecontrol.voiceControl.VcUtils
import java.util.*

class ItemUtilityAdapter(
    val context: Context,
    val itemList: ArrayList<ItemUtility>,
    var style: Int,
    var spacePx: Int, var width: Int = 0, var height: Int = 0
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    private var selectIndex: Int = -1
    fun setSelect(index: Int) {
        selectIndex = index
        notifyDataSetChanged()
    }

    companion object {
        const val HORIZONTAL = 1
        const val VERTICAL = 2
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun initViewSize(style: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
//        imageWidth = screenWidth / 4
//        imageHeight = imageWidth * 7 / 10
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val resLayout =
            if (style == HORIZONTAL) R.layout.item_utility_vc_horizontal else R.layout.item_utility_vc
        val view = LayoutInflater.from(parent.context).inflate(
            resLayout,
            parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemUtility = itemList[position]
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemUtility, position: Int) {
        if (!itemObj.icon.isNullOrEmpty()) {
            VcUtils.loadImageCircle(context, holder.imgThumbnail, itemObj.icon, 0)
        } else {
            if (itemObj.iconRes != null) {
                VcUtils.loadDrawableImage(context, itemObj.iconRes, holder.imgThumbnail)
            }
        }
        holder.textName.isSelected = position == selectIndex
        holder.textName.text = itemObj.title
        if (style == HORIZONTAL && !itemObj.shortTitle.isNullOrEmpty()) {
            holder.textName.text = itemObj.shortTitle
        }
        if (style == VERTICAL) {
            VcUtils.setRound(context, holder.layoutRoot, 21)
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
//            holder?.layoutRoot?.isEnabled = false
//            Handler(Looper.getMainLooper()).postDelayed({holder?.layoutRoot?.isEnabled = true},3000)
        }
        holder.layoutParent.setOnClickListener {
            onItemClickListener?.onClickOut(itemObj, position)
//            holder?.layoutRoot?.isEnabled = false
//            Handler(Looper.getMainLooper()).postDelayed({holder?.layoutRoot?.isEnabled = true},3000)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot: ConstraintLayout = itemView.findViewById(R.id.layoutRoot)
        val imgThumbnail: ImageView = itemView.findViewById(R.id.imgAvatar)
        val textName: TextView = itemView.findViewById(R.id.textName)
        val layoutParent: RelativeLayout = itemView.findViewById(R.id.layoutParent)

        init {
            if (style == HORIZONTAL) {
                val params = layoutParent.layoutParams as GridLayoutManager.LayoutParams
                if (width > 0) {
                    params.width = width
                }
                if (height > 0) {
                    params.height = height
                }
                if (spacePx > 0) {
                    params.marginStart = spacePx
                    params.topMargin = spacePx
                }

            }
        }
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemUtility, position: Int)
        fun onClickOut(itemObject: ItemUtility, position: Int)
    }
}
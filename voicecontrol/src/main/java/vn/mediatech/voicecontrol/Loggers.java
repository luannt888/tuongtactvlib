package vn.mediatech.voicecontrol;

import android.util.Log;

public class Loggers {
    public static boolean DEBUG_MODE = false;

    public static void e(String TAG, String msg) {
        if (DEBUG_MODE) {
            Log.e(TAG, msg + "");
        }
    }

    public static void e(String TAG, int msg) {
        if (DEBUG_MODE) {
            Log.e(TAG, msg + "");
        }
    }
}
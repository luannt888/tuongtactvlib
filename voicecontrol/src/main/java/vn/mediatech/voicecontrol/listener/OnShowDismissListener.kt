package vn.mediatech.voicecontrol.listener

interface OnShowDismissListener {
    fun onShow() {}
    fun onDismiss()
}
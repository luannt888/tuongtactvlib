package vn.mediatech.voicecontrol.voiceControl

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.util.Log
import androidx.core.content.getSystemService
import vn.mediatech.voicecontrol.Loggers
import vn.mediatech.voicecontrol.listener.RecognitionCallback

/**
 * Created by stephenvinouze on 16/05/2017.
 */
class KontinuousRecognitionManager(
    private val context: Context,
    private val activationKeywords: Array<String>?,
    private val shouldMute: Boolean = false,
    private val callback: RecognitionCallback? = null
) : RecognitionListener {
    companion object {
        private var instance: KontinuousRecognitionManager? = null
        fun destroy() {
            instance?.destroyRecognizer()
            instance = null
        }
    }

    init {
        instance = this
    }

    private var isActivated: Boolean = false
    private val speech: SpeechRecognizer by lazy { SpeechRecognizer.createSpeechRecognizer(context,
        ComponentName.unflattenFromString("com.google.android.googlequicksearchbox/com.google.android.voicesearch.serviceapi.GoogleRecognitionService")
    ) }
    private val audioManager: AudioManager? = context.getSystemService()
    private val recognizerIntent by lazy {
        Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
            putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
            )
            putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, context.packageName)
            putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3)
            putExtra(
                RecognizerIntent.EXTRA_PARTIAL_RESULTS,
                true
            )
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true)
//            }
        }
    }

    fun createRecognizer() {
        if (SpeechRecognizer.isRecognitionAvailable(context)) {
            speech.setRecognitionListener(this)
            callback?.onPrepared(RecognitionStatus.SUCCESS)
        } else {
            callback?.onPrepared(RecognitionStatus.UNAVAILABLE)
        }
    }

    fun destroyRecognizer() {
        speech.destroy()
//        muteRecognition(false)
    }

    fun startRecognition() {
        muteRecognition(true)
        speech.startListening(recognizerIntent)
    }

    fun stopRecognition() {
        speech.stopListening()
    }

    fun cancelRecognition() {
        speech.cancel()
    }

    @Suppress("DEPRECATION")
    fun muteRecognition(mute: Boolean) {
        audioManager?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val flag = if (mute) AudioManager.ADJUST_MUTE else AudioManager.ADJUST_UNMUTE
                it.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, flag, 0)
//                it.adjustStreamVolume(AudioManager.STREAM_ALARM, flag, 0)
//                it.adjustStreamVolume(AudioManager.STREAM_MUSIC, flag, 0)
//                it.adjustStreamVolume(AudioManager.STREAM_RING, flag, 0)
//                it.adjustStreamVolume(AudioManager.STREAM_SYSTEM, flag, 0)
            } else {
                it.setStreamMute(AudioManager.STREAM_NOTIFICATION, mute)
//                it.setStreamMute(AudioManager.STREAM_ALARM, mute)
//                it.setStreamMute(AudioManager.STREAM_MUSIC, mute)
//                it.setStreamMute(AudioManager.STREAM_RING, mute)
//                it.setStreamMute(AudioManager.STREAM_SYSTEM, mute)
            }
        }
    }

    override fun onBeginningOfSpeech() {
        callback?.onBeginningOfSpeech()
    }

    override fun onReadyForSpeech(params: Bundle) {
        muteRecognition(shouldMute || !isActivated)
        callback?.onReadyForSpeech(params)
    }

    override fun onBufferReceived(buffer: ByteArray) {
        callback?.onBufferReceived(buffer)
    }

    override fun onRmsChanged(rmsdB: Float) {
//        Loggers.e("RecognitionononRms", rmsdB.toString())
        callback?.onRmsChanged(rmsdB)
    }

    override fun onEndOfSpeech() {
        callback?.onEndOfSpeech()
    }

    override fun onError(errorCode: Int) {
//        Loggers.e("RecognitiononError", errorCode.toString())
        if (isActivated) {
            callback?.onError(errorCode)
        }
        isActivated = false

        when (errorCode) {
            SpeechRecognizer.ERROR_RECOGNIZER_BUSY -> cancelRecognition()
            SpeechRecognizer.ERROR_SPEECH_TIMEOUT -> {
                destroyRecognizer()
                createRecognizer()
            }
        }
        startRecognition()
    }

    override fun onEvent(eventType: Int, params: Bundle) {
        callback?.onEvent(eventType, params)
    }

    override fun onPartialResults(partialResults: Bundle) {
        val matches = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
        if (isActivated && matches != null) {
            Loggers.e("RecognitiononPart", matches[0])
            callback?.onPartialResults(matches)
        }
        if (matches != null) {
            Loggers.e("RecognitiononResultsPar", matches[0])
        }
        if (activationKeywords == null || activationKeywords.isEmpty()) {
            return
        }
        for (key in activationKeywords) {
            matches?.firstOrNull { it.contains(other = key, ignoreCase = true) }
                ?.let {
                    isActivated = true
                    //Luan add
                    destroyRecognizer()
                    callback?.onKeywordDetected()
                    return
                }
        }
    }

    override fun onResults(results: Bundle) {
        val matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
        val scores = results.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES)
        if (matches != null) {
            Loggers.e("RecognitiononResults", matches[0])
            if (isActivated) {
                isActivated = false
                callback?.onResults(matches, scores)
                destroyRecognizer()
            } else {
                if (activationKeywords == null || activationKeywords.isEmpty()) {
                    return
                }
                for (key in activationKeywords) {
                    matches.firstOrNull { it.contains(other = key, ignoreCase = true) }
                        ?.let {
                            isActivated = true
                            //Luan add
                            destroyRecognizer()
                            callback?.onKeywordDetected()
                            return
                        }
                }
                startRecognition()
            }
        }
    }

}
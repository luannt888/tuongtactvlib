package vn.mediatech.voicecontrol.voiceControl

import android.Manifest
import android.animation.Animator
import android.animation.ValueAnimator
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.speech.RecognitionListener
import android.speech.tts.UtteranceProgressListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_voice_controller.*
import vn.mediatech.voicecontrol.ItemSuggestAdapter
import vn.mediatech.voicecontrol.Loggers
import vn.mediatech.voicecontrol.R
import vn.mediatech.voicecontrol.ScreenSize
import vn.mediatech.voicecontrol.adapter.ItemUtilityAdapter
import vn.mediatech.voicecontrol.listener.OnDoTaskListener
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.voicecontrol.log.ItemLog
import vn.mediatech.voicecontrol.log.ItemLogAdapter
import vn.mediatech.voicecontrol.model.ItemCase
import vn.mediatech.voicecontrol.model.ItemUtility
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class VoiceControlFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var username: String? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var savedInstanceState: Bundle? = null
    var recognitionListener: RecognitionListener? = null
    var onResultListener: VoiceControllerManager.OnResultListener? = null
    var onDoTaskListener: OnDoTaskListener? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var isInitSTT = false
    var isSpeakMsg: Boolean = false
    var itemUtilityList: ArrayList<ItemUtility>? = null
    var onClickItemUtilityListener: ItemUtilityAdapter.OnItemClickListener? = null
    var isListerFirst: Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_voice_controller, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
        VcUtils.muteRecognition(activity, true)
        VcUtils.setListeningBackgroundCurrent(activity)
        VcUtils.setListeningAutoCurrent(activity)
        buttonGuide.isSelected = layoutSuggest.visibility == View.VISIBLE

    }

    var item: ItemCase? = null
    var data: String? = null
    fun initData() {
        if (!VoiceControllerManager.isRecognitionGoogleAvailable(requireContext())) {
            val ac = activity
            val mess =
                "Thiết bị của bạn hiện chưa hỗ trợ tính năng này. Vui lòng bật dịch vụ Google và cho phép ứng dụng Google truy cập micro"
            VcUtils.showDialog(
                ac,
                "Thông báo",
                mess,
                "Không",
                "Đồng ý",
                object : VcUtils.OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        dismiss()
                    }

                    override fun onRightButtonClick() {
                        val packageService = "com.google.android.googlequicksearchbox"
                        val link = "market://details?id=$packageService"
                        if (!VcUtils.isPackageInstalled(context, packageService)) {
                            VcUtils.openLink(ac, link)
                        } else {
                            val intent = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts(
                                    "package",
                                    "com.google.android.googlequicksearchbox",
                                    null
                                )
                            )
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            ac?.startActivity(intent)
                        }
                    }
                })
            dismiss()
            return
//            Toast.makeText(
//                activity,
//                "Thiết bị của bạn hiện chưa hỗ trợ tính năng này. Vui lòng bật dịch vụ Google và cho phép ứng dụng Google truy cập micro",
//                Toast.LENGTH_LONG
//            )
//                .show()

        }

        initAdapterLog()
        val bundle = savedInstanceState ?: arguments
        var msg: String? = null
        if (bundle != null) {
            msg = bundle.getString(VcUtils.MESSAGE, null)
            item = bundle.getParcelable(VcUtils.DATA)
            isSpeakMsg = bundle.getBoolean(VcUtils.SPEAK, false)
            if (item != null) {
                msg = item!!.title
            }
            itemUtilityList = bundle.getParcelableArrayList(VcUtils.UTILITY)
            suggestList = bundle.getStringArray(VcUtils.SUGGEST)
            val heightPx = bundle.getInt(VcUtils.HEIGHT, 0)
            if (heightPx > 0) {
                layoutRoot?.layoutParams?.height = heightPx
            }

        }
        val isListen = VoiceControllerManager.isSetListening
        VoiceControllerManager.isSetListening = false
        initSTT()
        if (itemUtilityList != null && itemUtilityList!!.size > 0) {
            initAdapterUtility()
        }
        if (suggestList != null && suggestList!!.size > 0) {
            initAdapterSuggest(suggestList)
        }
        if (!msg.isNullOrEmpty()) {
            if (isSpeakMsg) {
                if (item != null && !item!!.titleSpeak.isNullOrEmpty()) {
                    speakOnly(item!!.titleSpeak, isListen, null)
                    return
                }
                speakWithTyping(msg, isListen)
            } else {
                startListening(isListen)
            }
        } else {
//            textTitle.visibility = View.VISIBLE
            startListening(isListen)
        }
        VoiceControllerManager.instance?.getSupportedVoices()
    }

    private fun initAdapterUtility() {
        try {
            val spanColumns = 4
            val spanRows = 2
//            val spaceDecoration = SpaceDecoration(space.toInt(),4,GridLayoutManager.VERTICAL)
//            rcvUtility?.addItemDecoration(spaceDecoration)
            if (itemUtilityList == null) {
                return
            }

            var layoutManager =
                GridLayoutManager(context, spanColumns, GridLayoutManager.VERTICAL, false)
            if (itemUtilityList!!.size >= (spanColumns * spanRows - 1)) {
                GridLayoutManager(context, spanRows, GridLayoutManager.HORIZONTAL, false)
            }
            rcvUtility?.layoutManager = layoutManager
            val space = VcUtils.convertDpToPixel(5F, requireContext()).toInt()
            val rcvWidth =
                ScreenSize(requireContext()).width - VcUtils.convertDpToPixel(30F, requireContext())
            val width: Int =
                ((rcvWidth - space * (spanColumns + 1)) / spanColumns).toInt()
            val adapter =
                ItemUtilityAdapter(requireContext(), itemUtilityList!!, 1, space, width, 0)
            rcvUtility.adapter = adapter
            adapter.onItemClickListener = onClickItemUtilityListener
        } catch (e: Exception) {
        }

    }

    var msgTyping: String? = null
    fun speakWithTyping(
        msg: String?,
        isListenerAfter: Boolean = false,
        utteranceProgressListener: UtteranceProgressListener? = null, isTyping: Boolean = true
    ) {
        if (msg.isNullOrEmpty()) {
            return
        }
        msgTyping = msg
        val msgSpeak = msg.replace("itv", "ITV", true)
        val mUtteranceId = System.currentTimeMillis().toString()
//        VoiceControllerManager.instance?.isInitTTS = false
        VoiceControllerManager.instance?.speak(activity, msgSpeak, mUtteranceId,
            object : UtteranceProgressListener() {
                override fun onStart(p0: String?) {
                    Loggers.e("TTS", msg + "onStart" + "p0")
//                    if (mUtteranceId.equals(p0)) {
                    activity?.runOnUiThread {
                        addItv(msgTyping, isTyping)
                    }
                    utteranceProgressListener?.onStart(p0)
//                    }
                }

                override fun onDone(p0: String?) {
                    Loggers.e("TTS", msg + "onDone" + "p0")
//                    if (mUtteranceId.equals(p0)) {
                    handleSpeakDoneOrError()
                    utteranceProgressListener?.onDone(p0)
//                    }
                }

                override fun onError(p0: String?) {
                    Loggers.e("TTS", msg + "onError" + "p0")
                    handleSpeakDoneOrError(true)
                    utteranceProgressListener?.onError(p0)
                }

                private fun handleSpeakDoneOrError(isError: Boolean = false) {
                    activity?.runOnUiThread {
                        if (isError) {
                            if (isTyping) {
                                activity?.runOnUiThread {
                                    addItv(msgTyping)
                                }
                            }
                            utteranceProgressListener?.onDone("")
                        }
                        if (VcUtils.isListeningAuto(activity) || isListerFirst) {
                            isListerFirst = false
                            startListening(isListenerAfter)
                        }
                    }
                }
            })
    }

    fun speakOnly(
        msg: String?,
        isListenerAfter: Boolean = false,
        utteranceProgressListener: UtteranceProgressListener? = null
    ) {
        if (msg.isNullOrEmpty()) {
            return
        }
        val msgSpeak = msg.replace("itv", "ITV", true)
        val mUtteranceId = System.currentTimeMillis().toString()
        VoiceControllerManager.instance?.speak(activity, msgSpeak, mUtteranceId,
            object : UtteranceProgressListener() {
                override fun onStart(p0: String?) {
                    Loggers.e("TTS", msg + "onStart" + "$p0")
                    if (mUtteranceId.equals(p0)) {
                        utteranceProgressListener?.onStart(p0)
                    }
                }

                override fun onDone(p0: String?) {
                    Loggers.e("TTS", msg + "onDone" + "$p0")
                    if (mUtteranceId.equals(p0)) {
                        activity?.runOnUiThread {
                            startListening(isListenerAfter)
                        }
                        utteranceProgressListener?.onDone(p0)
                    }
                }

                override fun onError(p0: String?) {
                    utteranceProgressListener?.onError(p0)
                }
            })
    }

    private fun initSTT() {
        if (isInitSTT) {
            return
        }
        VoiceControllerManager.instance?.initSTT(
            requireActivity(),
            buttonVoice,
            voiceProgressView,
            object : RecognitionListener {
                override fun onReadyForSpeech(p0: Bundle?) {
                    recognitionListener?.onReadyForSpeech(p0)
                }

                override fun onBeginningOfSpeech() {
                    recognitionListener?.onBeginningOfSpeech()
                }

                override fun onRmsChanged(p0: Float) {
                    recognitionListener?.onRmsChanged(p0)
                }

                override fun onBufferReceived(p0: ByteArray?) {
                    recognitionListener?.onBufferReceived(p0)
                }

                override fun onEndOfSpeech() {
                    stopCountUp()
                    recognitionListener?.onEndOfSpeech()
                }

                override fun onError(p0: Int) {
                    recognitionListener?.onError(p0)
//                    if(p0 == SpeechRecognizer.ERROR_NO_MATCH){
//                        VoiceControllerManager.instance?.startListening()
//                    }
                }

                override fun onResults(p0: Bundle?) {
                    recognitionListener?.onResults(p0)
                }

                override fun onPartialResults(p0: Bundle?) {
                    recognitionListener?.onPartialResults(p0)
                }

                override fun onEvent(p0: Int, p1: Bundle?) {
                    recognitionListener?.onEvent(p0, p1)
                }
            },
            object : VoiceControllerManager.OnResultListener {
                override fun onPartialResults(str: String?) {
                    stopCountUpListen()
                    if (resultPartial.isNullOrEmpty()) {
                        startCountUp()
                    }
                    onResultListener?.onPartialResults(str)
                    resultPartial = str
                }

                override fun onResult(str: String?) {
                    stopCountUpListen()
                    handleResultListening(str)
                }

                override fun onRmsChanged(p0: Float) {
                    onResultListener?.onRmsChanged(p0)
                    val progress: Float = if (p0 <= 0) 0F else if (p0 >= 20) 20F else p0
                    voiceProgressView?.update((progress * 22760 / 20).toInt())
                }
            })
        isInitSTT = true
    }

    private fun handleResultListening(resultStr: String?) {
        var str = resultStr
        VcUtils.setSoundWhenListening(activity, false)
        stopCountUp()
        if (str.isNullOrEmpty()) {
            return
        }
        str = str.replace("covit", "covid")
        addUserMsg(str)
        val strLower = str.lowercase()
        if (item != null && strLower.isNotEmpty()) {
            if (item!!.itemCaseAnswerList == null || item!!.itemCaseAnswerList!!.size == 0) {
                return
            }
            for (itemObj in item!!.itemCaseAnswerList!!) {
                if (itemObj.anserKey == null || itemObj.anserKey!!.size == 0) {
                    continue
                }
                val msgFinish: String = itemObj.questionNext ?: "Ok!"
                for (stringKey in itemObj.anserKey!!) {
                    if (strLower.contains(stringKey.lowercase()) || strLower.trim()
                            .equals(stringKey.lowercase().trim())
                    ) {
                        speakWithTyping(
                            msgFinish,
                            false,
                            object : UtteranceProgressListener() {
                                override fun onStart(p0: String?) {
                                }

                                override fun onDone(p0: String?) {
                                    if (!itemObj.key.isNullOrEmpty()) {
                                        onResultListener?.onResult(itemObj.key)
                                    } else {
                                        onDoTaskListener?.onDoTask(itemObj.id, itemObj.type)
                                    }
                                    activity?.runOnUiThread {
                                        dismiss()
                                    }
                                }

                                override fun onError(p0: String?) {
                                }
                            })
                        return
                    }
                }
            }
            return
        }
        onResultListener?.onResult(str)
    }

    fun startListening(isStartListening: Boolean = false) {
        if (!isStartListening) {
            return
        }
        if (isInitSTT) {
            VoiceControllerManager.instance?.startListening()
            if (!VcUtils.isListeningAuto(activity)) {
                startCountUpListenSilence()
            }
        } else {
            VoiceControllerManager.isSetListening = true
            initSTT()
        }
    }

    var suggestList: Array<String>? = null
    var adapter: ItemSuggestAdapter? = null
    private fun initAdapterSuggest(list: Array<String>?) {
        if (list == null || list.size == 0) {
            return
        }
        suggestList = list
        VcUtils.setHtmlStypeTextView(buttonHideSuggest, "Ẩn", null, false, true)
        adapter = ItemSuggestAdapter(requireContext(), list)
        val layoutManager =
//            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.HORIZONTAL)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        adapter?.onItemClickListener = object : ItemSuggestAdapter.OnItemClickListener {
            override fun onClick(itemObject: String, position: Int) {
//                VoiceControllerManager.instance?.speak(activity, itemObject)
                onResultListener?.onResult(itemObject)
            }
        }
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            dismiss()
        }

        buttonVoiceSetting.setOnClickListener {
            VoiceControllerManager.instance?.setTextToSpeechVoice(activity)
        }
        buttonVoice.setOnClickListener {
            if (buttonVoice.isSelected) {
                stopVC(false)
            } else {
                startListening(true)
            }
        }
        buttonGuide.setOnClickListener {
            tooggleSuggest()
        }
        buttonHideSuggest.setOnClickListener {
            tooggleSuggest()
        }
        layoutListenAuto.setOnClickListener {
            layoutListenAuto.isEnabled = false
            Handler(Looper.getMainLooper()).postDelayed(
                { layoutListenAuto?.isEnabled = true }, 500
            )
            val isListenAuto = VcUtils.isListeningAuto(activity)
            VcUtils.setListeningAuto(activity, !isListenAuto)
            stopVC(false)
            if (!isListenAuto) {
                addItv(
                    "Bạn đã bật tính năng \"Tự động nghe\". Chế độ này giúp iTV trò chuyện liên tục với bạn cho đến khi hiển thị kết quả. iTV có thể giúp gì cho bạn?",
                    false
                )
                startListening(true)
            } else {
                addItv(
                    "Bạn đã tắt tính năng \"Tự động nghe\". Hãy nhấn biểu tượng mic ở phía trên bên trái màn hình để ra lệnh cho iTV nhé!",
                    false
                )

            }

        }

        layoutListenBackground.setOnClickListener {
            /*var count = VcUtils.getInt(context, "VOICE_BACKGROUND")
            if (count < 1) {
                VcUtils.showDialogAskListeningBackground(activity)
                count += 1
                VcUtils.saveInt(context, count, "VOICE_BACKGROUND")
            } else {
                VcUtils.setListeningContinuousBackground(
                    activity,
                    !VcUtils.isListeningContinuousBackground(activity)
                )
            }*/
            layoutListenBackground?.isEnabled = false
            Handler(Looper.getMainLooper()).postDelayed(
                { layoutListenBackground?.isEnabled = true }, 500
            )
            val isListeningBackground = VcUtils.isListeningBackground(activity)
            VcUtils.setListeningContinuousBackground(
                activity,
                !VcUtils.isListeningBackground(activity)
            )

            stopVC(false)
            if (!isListeningBackground) {
                addItv(
                    "Bạn đã bật tính năng \" iTV Chạy ngầm\". Khi ẩn giao diện điều khiển bạn chỉ cần nói \"iTV\" thì giao diện điều khiển sẽ hiện lên.",
                    false
                )
            } else {
                addItv(
                    "Bạn đã tắt tính năng \" iTV Chạy ngầm\".",
                    false
                )
            }
            startListening(VcUtils.isListeningAuto(activity))
        }
        /*       imgListenAutoVc.setOnLongClickListener(object : View.OnLongClickListener {
                   override fun onLongClick(p0: View?): Boolean {
                       VcUtils.showDialogAskListeningFragment(activity)
                       return true
                   }
               })
               buttonListenBackgroundVc.setOnLongClickListener(object : View.OnLongClickListener {
                   override fun onLongClick(p0: View?): Boolean {
                       VcUtils.showDialogAskListeningBackground(activity)
                       return true
                   }
               })*/
    }

    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
        if (isListening != null) {
            startListening(isListening!!)
        }
    }

    var isListening: Boolean? = null
    override fun onPause() {
        super.onPause()
        isListening = VoiceControllerManager.instance?.isListening
        stopVC(false)
    }

    override fun onDismiss(dialog: DialogInterface) {
        stopVC()
//        VcUtils.muteRecognition(activity, false)
        super.onDismiss(dialog)
        onShowDismissListener?.onDismiss()
    }

    private fun stopVC(isRemoveSTT: Boolean = true) {
        buttonVoice?.isSelected = false
        voiceProgressView?.visibility = View.INVISIBLE
        stopCountUp()
        VoiceControllerManager.destroy(isRemoveSTT)
        VcUtils.setSoundWhenListening(requireActivity(), false)
    }

    fun handResultCase(
        result: String? = null,
        resultKey: Array<String>? = null,
        answer: String? = null, isListenerAfter: Boolean = true, isDismiss: Boolean = false
    ): Boolean {
        if (result.isNullOrEmpty() || resultKey == null || resultKey.size == 0 || answer.isNullOrEmpty()) {
            return false
        }
        for (str in resultKey) {
            if (result.contains(str, false) || (result.lowercase().trim()).equals(
                    str.lowercase().trim()
                )
            ) {
                if (!isDismiss) {
                    speakWithTyping(answer, isListenerAfter)
                } else {
                    speakWithDismiss(answer)
                }
                return true
            }
        }
        return false
    }

    fun speakWithDismiss(msg: String?, task: Runnable? = null) {
        if (msg.isNullOrEmpty()) {
            return
        }
        speakWithTyping(msg, false, object : UtteranceProgressListener() {
            override fun onStart(p0: String?) {
            }

            override fun onDone(p0: String?) {
                activity?.runOnUiThread {
                    task?.run()
                    dismiss()
                }
            }

            override fun onError(p0: String?) {
            }
        })
    }

    companion object {
        fun showFragment(
            activity: Activity?,
            itemCase: ItemCase?,
            suggestList: Array<String>? = null,
            onResultListener: VoiceControllerManager.OnResultListener?,
            itemUtilityList: ArrayList<ItemUtility>? = null,
            onClickItemUtilityListener: ItemUtilityAdapter.OnItemClickListener? = null,
            isListenerAfter: Boolean = true, isSpeak: Boolean = true, heightPx: Int = 0
        ): VoiceControlFragment? {
            if (activity == null || activity !is FragmentActivity || activity.isDestroyed || activity.isFinishing) {
                return null
            }
            var voiceControlFragment: VoiceControlFragment? = null
            if (!VoiceControllerManager.hasPermissionRecordAudio(activity)) {
                MyRequestPermissions.requestPermission(
                    activity,
                    Manifest.permission.RECORD_AUDIO,
                    object : MyRequestPermissions.OnMyRequestPermissionListener {
                        override fun onPermissionGranted() {
                            voiceControlFragment = showFragment(
                                activity,
                                itemCase, suggestList,
                                onResultListener, itemUtilityList, onClickItemUtilityListener,
                                isListenerAfter,
                                isSpeak, heightPx
                            )
                        }

                        override fun onPermissionDenied() {
                        }
                    })
                return voiceControlFragment
            }
            VoiceControllerManager.isSetListening = isListenerAfter
            val fragment = VoiceControlFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(VcUtils.DATA, itemCase)
                    putBoolean(VcUtils.SPEAK, isSpeak)
                    putStringArray(VcUtils.SUGGEST, suggestList)
                    putInt(VcUtils.HEIGHT, heightPx)
                    putParcelableArrayList(VcUtils.UTILITY, itemUtilityList)
                }
            }
            fragment.onResultListener = onResultListener
            fragment.onClickItemUtilityListener = onClickItemUtilityListener
            fragment.show(
                activity.supportFragmentManager,
                VoiceControllerManager::class.java.simpleName
            )
            return fragment
        }

        fun showFragment(
            activity: Activity?,
            msg: String?,
            onResultListener: VoiceControllerManager.OnResultListener?,
            isListenerAfter: Boolean = true,
            isSpeak: Boolean = true,
            suggestList: Array<String>? = null,
            itemUtilityList: ArrayList<ItemUtility>? = null,
            onClickItemUtilityListener: ItemUtilityAdapter.OnItemClickListener? = null,
            heightPx: Int = 0, isShow: Boolean = true
        ): VoiceControlFragment? {
            if (activity == null || activity !is FragmentActivity || activity.isDestroyed || activity.isFinishing) {
                return null
            }
            var voiceControlFragment: VoiceControlFragment? = null
            if (!VoiceControllerManager.hasPermissionRecordAudio(activity)) {
                MyRequestPermissions.requestPermission(
                    activity,
                    Manifest.permission.RECORD_AUDIO,
                    object : MyRequestPermissions.OnMyRequestPermissionListener {
                        override fun onPermissionGranted() {
                            voiceControlFragment = showFragment(
                                activity,
                                msg,
                                onResultListener,
                                isListenerAfter,
                                isSpeak,
                                suggestList,
                                itemUtilityList,
                                onClickItemUtilityListener,
                                heightPx
                            )
                        }

                        override fun onPermissionDenied() {
                        }
                    })
                return voiceControlFragment
            }
            VoiceControllerManager.isSetListening = isListenerAfter
            val fragment = VoiceControlFragment().apply {
                arguments = Bundle().apply {
                    putString(VcUtils.MESSAGE, msg)
                    putBoolean(VcUtils.SPEAK, isSpeak)
                    putStringArray(VcUtils.SUGGEST, suggestList)
                    putParcelableArrayList(VcUtils.UTILITY, itemUtilityList)
                    putInt(VcUtils.HEIGHT, heightPx)
                }
            }
            fragment.onResultListener = onResultListener
            fragment.onClickItemUtilityListener = onClickItemUtilityListener
            if (isShow) {
                fragment.show(
                    activity.supportFragmentManager,
                    VoiceControlFragment::class.java.simpleName
                )
            }
            return fragment
        }
    }

    private var itemLogList: ArrayList<ItemLog> = ArrayList()
    private var adapterLog: ItemLogAdapter? = null
    private fun initAdapterLog(isTyping: Boolean = true) {
        if (activity == null || requireActivity().isFinishing || requireActivity().isDestroyed) {
            return
        }
        if (adapterLog != null) {
            adapterLog?.isTyping = isTyping
            adapterLog?.stopAnimText()
            adapterLog?.notifyDataSetChanged()
            rcvLog?.getLayoutManager()?.scrollToPosition(adapterLog!!.getItemCount() - 1);
            return
        }
        adapterLog = ItemLogAdapter(requireContext(), itemLogList)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        rcvLog?.layoutManager = layoutManager
        rcvLog?.adapter = adapterLog
        rcvLog?.getLayoutManager()?.scrollToPosition(adapterLog!!.getItemCount() - 1);
    }

    private fun addItv(msg: String?, isTyping: Boolean = true) {
        if (msg.isNullOrEmpty()) {
            return
        }
        itemLogList.add(ItemLog(1, msg, 1))
        initAdapterLog(isTyping)
    }

    private fun addUserMsg(msg: String?, isTyping: Boolean = true) {
        if (msg.isNullOrEmpty()) {
            return
        }
        itemLogList.add(ItemLog(1, msg, 2))
        initAdapterLog(isTyping)
    }

    var heightShow: Int = -1
    private fun tooggleSuggest() {
        if (suggestList.isNullOrEmpty()) {
            return
        }
        if (heightShow == -1 && layoutSuggest.height > 0) {
            heightShow = layoutSuggest.height
        }
        if (recyclerView.visibility != View.VISIBLE) {
            recyclerView.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.GONE
        }
        val action = if (recyclerView.visibility == View.VISIBLE) "Ẩn" else "Hiện"
        VcUtils.setHtmlStypeTextView(buttonHideSuggest, action, null, false, true)
        buttonGuide?.isSelected = layoutSuggest.visibility == View.VISIBLE
        return
        val heightChange = if (layoutSuggest.height > 0) 0 else heightShow
        val anim = ValueAnimator.ofInt(layoutSuggest.height, heightChange)
        anim.addUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue as Int
            val layoutParams = layoutSuggest!!.layoutParams
            layoutParams.height = value
            layoutSuggest?.layoutParams = layoutParams
        }
        anim.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(p0: Animator?) {
            }

            override fun onAnimationEnd(p0: Animator?) {
                if (heightChange > 0) adapter?.notifyDataSetChanged()
            }

            override fun onAnimationCancel(p0: Animator?) {
            }

            override fun onAnimationRepeat(p0: Animator?) {
            }
        })
        anim.duration = 250
        anim.start()
    }

    var timer: Timer? = null
    var timeListen: Long = 0
    var resultPartial: String? = ""
    private fun startCountUp() {
        timer?.cancel()
        timeListen = 0
        resultPartial = ""
        timer = Timer()
        timer?.schedule(timerTask {
            if (timeListen >= VcUtils.TIME_LISTEN_MAX && !resultPartial.isNullOrEmpty()) {
                timer?.cancel()
                activity?.runOnUiThread {
                    VoiceControllerManager.destroy(false)
                    buttonVoice?.isSelected = false
                    voiceProgressView?.visibility = View.INVISIBLE
                    handleResultListening(resultPartial)
                }
            }
            timeListen = timeListen + 500
        }, 0, 500)
    }

    private fun stopCountUp() {
//        stopCountUpListen()
        timer?.cancel()
        timeListen = 0
        resultPartial = ""
    }

    var timerListenSilence: Timer? = null
    var timeListenSilence: Long = 0
    private fun startCountUpListenSilence() {
        timerListenSilence?.cancel()
        timeListenSilence = 0
        timerListenSilence = Timer()
        timerListenSilence?.schedule(timerTask {
            if (timeListenSilence >= 10000) {
                activity?.runOnUiThread {
                    VoiceControllerManager.destroy(false)
                    buttonVoice?.isSelected = false
                    voiceProgressView?.visibility = View.INVISIBLE
                    stopCountUpListen()
                }
            }
            timeListenSilence += 500
            Loggers.e("timerListenSilence", " " + timeListenSilence)
        }, 0, 500)
    }

    private fun stopCountUpListen() {
        timerListenSilence?.cancel()
        timeListenSilence = 0
    }
}
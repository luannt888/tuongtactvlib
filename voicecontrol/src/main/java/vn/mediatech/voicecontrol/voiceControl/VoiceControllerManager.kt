package vn.mediatech.voicecontrol.voiceControl

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ResolveInfo
import android.content.res.Configuration
import android.content.res.Resources
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.speech.RecognitionListener
import android.speech.RecognitionService
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.speech.tts.Voice
import android.text.Html
import android.text.Spanned
import android.text.method.ScrollingMovementMethod
import android.util.DisplayMetrics
import android.view.*
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.marginBottom
import androidx.recyclerview.widget.LinearLayoutManager
import com.mlsdev.animatedrv.AnimatedRecyclerView
import vn.mediatech.voicecontrol.Loggers
import vn.mediatech.voicecontrol.R
import vn.mediatech.voicecontrol.ScreenSize
import vn.mediatech.voicecontrol.adapter.ItemUtilityAdapter
import vn.mediatech.voicecontrol.listener.OnClickListener
import vn.mediatech.voicecontrol.model.ItemUtility
import vn.mediatech.voicecontrol.ui.TypeWriterTextView
import java.util.*

class VoiceControllerManager {
    private var mRecognitionListener: RecognitionListener? = null
    var speechRecognizer: SpeechRecognizer? = null
    private var mSpeechRecognizerIntent: Intent? = null

    companion object {
        @SuppressLint("StaticFieldLeak")
        var instance: VoiceControllerManager? = null
            get() {
                if (field == null) {
                    instance = VoiceControllerManager()
                }
                return field
            }

        fun destroy(isRemoveSTT: Boolean = true) {
            instance?.destroySTT(isRemoveSTT)
            instance?.stopSpeak()
        }

        fun isRecognitionAvailable(context: Context): Boolean {
            return SpeechRecognizer.isRecognitionAvailable(context)
        }

        fun isRecognitionGoogleAvailable(context: Context): Boolean {
            var isAvailable: Boolean = false
            val services: List<ResolveInfo> = context.packageManager.queryIntentServices(
                Intent(RecognitionService.SERVICE_INTERFACE), 0
            )
            if (services != null && services.size > 0) {
                for (resolveInfo in services) {
                    if (
                        ("com.google.android.googlequicksearchbox").equals(resolveInfo.serviceInfo.packageName)
                    ) {
                        isAvailable = true
                        break
                    }
                }
            }
            return isAvailable
        }

        var isShowing: Boolean = false
        var isSetListening: Boolean = false
        fun showIconMenuVoice(
            activity: Activity?,
            msg: String?,
            heightBottomTab: Float = 0F,
            timeDelayHideMsg: Long = 5000L,
            onClickListener: OnClickListener? = null,
            isSpeak: Boolean = false,
            itemUtilityList: ArrayList<ItemUtility>? = null,
            onClickItemUtility: ItemUtilityAdapter.OnItemClickListener? = null
        )
                : View? {
            if (activity == null || activity.isDestroyed || activity.isFinishing) {
                return null
            }

            var layoutIconVoice: View? = null
            if (!hasPermissionRecordAudio(activity)) {
                MyRequestPermissions.requestPermission(
                    activity,
                    Manifest.permission.RECORD_AUDIO,
                    object : MyRequestPermissions.OnMyRequestPermissionListener {
                        override fun onPermissionGranted() {
                            layoutIconVoice = showIconMenuVoice(
                                activity,
                                msg,
                                heightBottomTab,
                                timeDelayHideMsg,
                                onClickListener,
                                isSpeak, itemUtilityList, onClickItemUtility
                            )
                        }

                        override fun onPermissionDenied() {
                        }

                        override fun onError() {
                        }
                    })
            } else {
                val inflater =
                    activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                layoutIconVoice = inflater.inflate(R.layout.layout_dialog_bubble_vc, null, false)
                val textBubleMessageDialog =
                    layoutIconVoice!!.findViewById<TypeWriterTextView>(R.id.textBubleMessageDialog)
                val buttonMenuDialog =
                    layoutIconVoice!!.findViewById<ImageView>(R.id.buttonMenuDialog)
                val layoutTextBubble =
                    layoutIconVoice!!.findViewById<RelativeLayout>(R.id.layoutTextBubble)
                val recyclerViewMenu =
                    layoutIconVoice!!.findViewById<AnimatedRecyclerView>(R.id.recyclerViewMenu)
                val viewOverlay = layoutIconVoice!!.findViewById<View>(R.id.viewOverlay)
                val textStatus = layoutIconVoice!!.findViewById<TextView>(R.id.textStatusVc)
                textStatus.visibility =
                    if (VcUtils.isListeningBackground(activity)) View.VISIBLE else View.GONE
                val params = textBubleMessageDialog.layoutParams as ViewGroup.LayoutParams
                val width = ScreenSize(activity).width * 2 / 3
                params.width = width
                val px10 = activity.resources.getDimensionPixelSize(R.dimen.dimen_10)
                val px20 = activity.resources.getDimensionPixelSize(R.dimen.dimen_40)
                if (msg.isNullOrEmpty()) {
                    layoutTextBubble?.visibility = View.GONE
                } else {
                    textBubleMessageDialog.setPadding(px10, px10, px20, px10)
                    textBubleMessageDialog?.setText("");
                    if (isSpeak) {
                        val msgSpeak = msg.replace("iTV", "ITV")
                        val mUtteranceId = System.currentTimeMillis().toString()
                        instance?.speak(
                            activity,
                            msgSpeak,
                            mUtteranceId,
                            object : UtteranceProgressListener() {
                                override fun onStart(p0: String?) {
                                    activity.runOnUiThread {
                                        layoutTextBubble?.visibility = View.VISIBLE
                                        textBubleMessageDialog?.setCharacterDelay(20);
                                        textBubleMessageDialog?.setMovementMethod(
                                            ScrollingMovementMethod()
                                        )
                                        textBubleMessageDialog?.animateText(msg);
                                    }
                                }

                                override fun onDone(p0: String?) {
                                    instance?.removeUtteranceProgressListener()

                                }

                                override fun onError(p0: String?) {
                                }
                            })
                    } else {
                        layoutTextBubble?.visibility = View.VISIBLE
                        textBubleMessageDialog?.setCharacterDelay(20);
                        textBubleMessageDialog?.setMovementMethod(ScrollingMovementMethod())
                        textBubleMessageDialog?.animateText(msg);
                    }
                }

                val layoutParams = buttonMenuDialog.layoutParams as ConstraintLayout.LayoutParams
                layoutParams.bottomMargin =
                    buttonMenuDialog.marginBottom + convertDpToPixel(
                        heightBottomTab,
                        activity
                    ).toInt() + VcUtils.getNavigationBarHeight(
                        activity,
                        Configuration.ORIENTATION_PORTRAIT
                    )
                textBubleMessageDialog.setOnClickListener {
                    textBubleMessageDialog?.visibility = View.GONE
                    layoutTextBubble?.visibility = View.GONE
                    if (instance?.tts?.isSpeaking == true) {
                        instance?.stopSpeak()
                    }
                }
                viewOverlay.setOnClickListener {
                    if (recyclerViewMenu.visibility == View.VISIBLE) {
                        recyclerViewMenu.visibility = View.INVISIBLE
                        showViewOverlay(activity, viewOverlay, false)
                    }
                }
                buttonMenuDialog.setOnClickListener {
                    onClickListener?.onClick()
                    if (recyclerViewMenu.visibility != View.VISIBLE) {
                        initUtilityAdapter(
                            activity,
                            viewOverlay,
                            recyclerViewMenu,
                            itemUtilityList,
                            onClickItemUtility
                        )
                    } else {
                        recyclerViewMenu.visibility = View.INVISIBLE
                        showViewOverlay(activity, viewOverlay, false)
                    }
                }
                textStatus.setOnClickListener {
                    if (recyclerViewMenu.visibility == View.VISIBLE) {
                        recyclerViewMenu.visibility = View.INVISIBLE
                        showViewOverlay(activity, viewOverlay, false)
                    }
                    VcUtils.showDialogAskListeningBackground(activity)
                }
                buttonMenuDialog.setOnLongClickListener(object : View.OnLongClickListener {
                    override fun onLongClick(p0: View?): Boolean {
                        if (recyclerViewMenu.visibility == View.VISIBLE) {
                            recyclerViewMenu.visibility = View.INVISIBLE
                            showViewOverlay(activity, viewOverlay, false)
                        }
                        onClickListener?.onLongClick()
                        return true
                    }
                })
                val mParams: WindowManager.LayoutParams = WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT
                )
                (activity.getWindow().getDecorView().getRootView() as ViewGroup).addView(
                    layoutIconVoice, mParams
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    textBubleMessageDialog?.visibility = View.GONE
                    layoutTextBubble?.visibility = View.GONE
                }, timeDelayHideMsg)

            }
            return layoutIconVoice
        }

        private fun initUtilityAdapter(
            activity: Activity?,
            viewOverlay: View?,
            recyclerViewMenu: AnimatedRecyclerView?,
            itemList: ArrayList<ItemUtility>? = null,
            onClickItemUtility: ItemUtilityAdapter.OnItemClickListener? = null
        ) {
            if (itemList == null || itemList.size == 0 || activity == null || activity.isDestroyed || activity.isFinishing || viewOverlay == null || recyclerViewMenu == null) {
                return
            }
            val adapter = ItemUtilityAdapter(activity!!, itemList, 2, 1)
            val layoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            showViewOverlay(activity, viewOverlay, true)
            recyclerViewMenu.visibility = View.VISIBLE
            recyclerViewMenu.layoutManager = layoutManager
            recyclerViewMenu.adapter = adapter
            recyclerViewMenu.scheduleLayoutAnimation()
            adapter?.onItemClickListener = object : ItemUtilityAdapter.OnItemClickListener {
                override fun onClick(itemObject: ItemUtility, position: Int) {
                    onClickItemUtility?.onClick(itemObject, position)
                    showViewOverlay(activity, viewOverlay, false)
                    recyclerViewMenu.visibility = View.INVISIBLE
                }

                override fun onClickOut(itemObject: ItemUtility, position: Int) {
                    onClickItemUtility?.onClickOut(itemObject, position)
                    showViewOverlay(activity, viewOverlay, false)
                    recyclerViewMenu.visibility = View.INVISIBLE
                }
            }
        }

        private fun showViewOverlay(activity: Activity?, viewOverlay: View?, isShow: Boolean) {
            if (activity == null || activity.isDestroyed || activity.isFinishing || viewOverlay == null) {
                return
            }
            viewOverlay.clearAnimation()
            if (isShow) {
                viewOverlay.visibility = View.VISIBLE
                val anim: Animation = AlphaAnimation(0f, 1f)
                anim.interpolator = DecelerateInterpolator()
                anim.duration = 400
                viewOverlay.setBackgroundColor(
                    ContextCompat.getColor(
                        activity,
                        R.color.transparent_alpha
                    )
                )
                anim.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation?) {
                    }

                    override fun onAnimationEnd(animation: Animation?) {
                        viewOverlay.setBackgroundColor(
                            ContextCompat.getColor(
                                activity,
                                R.color.transparent_alpha
                            )
                        )
                        viewOverlay.visibility = View.VISIBLE
                    }

                    override fun onAnimationRepeat(animation: Animation?) {
                    }
                })
                viewOverlay.startAnimation(anim)
                return
            }
            val anim: Animation = AlphaAnimation(1f, 0f)
            anim.interpolator = DecelerateInterpolator()
            anim.duration = 400
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation?) {
                }

                override fun onAnimationEnd(animation: Animation?) {
                    viewOverlay.setBackgroundColor(
                        ContextCompat.getColor(
                            activity,
                            R.color.transparent
                        )
                    )
                    viewOverlay.visibility = View.GONE
                }

                override fun onAnimationRepeat(animation: Animation?) {
                }

            })
            viewOverlay.startAnimation(anim)
        }

        fun hasPermissionRecordAudio(activity: Activity?) =
            MyRequestPermissions.hasPermissions(
                activity,
                Manifest.permission.RECORD_AUDIO
            )

        //        fun listen() {
//
//            // startListening should be called on Main thread
//            val mainHandler = Handler(Looper.getMainLooper())
//            val myRunnable = Runnable {
//                speechRecognizer.startListening(
//                    speechIntent
//                )
//            }
//            mainHandler.post(myRunnable)
//        }
        fun convertDpToPixel(dp: Float, context: Context): Float {
            return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }

        fun getNavBarHeight(c: Context): Int {
            val result = 0
            val hasMenuKey: Boolean = ViewConfiguration.get(c).hasPermanentMenuKey()
            val hasBackKey: Boolean = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK)
            if (!hasMenuKey && !hasBackKey) {
                //The device has a navigation bar
                val resources: Resources = c.resources
                val orientation: Int = resources.getConfiguration().orientation
                val resourceId: Int
                resourceId =
                    resources.getIdentifier(
                        if (orientation == Configuration.ORIENTATION_PORTRAIT) "navigation_bar_height" else "navigation_bar_width",
                        "dimen",
                        "android"
                    )
                if (resourceId > 0) {
                    return resources.getDimensionPixelSize(resourceId)
                }
            }
            return result
        }
    }

    var isListening: Boolean = false;
    fun initSTT(
        activity: Activity?,
        buttonSpeech: View?,
        progressView: View?,
        recognitionListener: RecognitionListener?,
        onResultListener: OnResultListener?
    ) {
        if (activity == null || activity.isFinishing || activity.isDestroyed) {
            return
        }
        destroySTT()
        if (!checkPermission(activity)) {
            Toast.makeText(activity, "Vui lòng cấp quyền micro cho ứng dụng", Toast.LENGTH_SHORT)
                .show()
            return
        }
        var language = Locale.getDefault()
        var TIME_DELAY_MS = 10000L
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(
            activity.applicationContext,
            ComponentName.unflattenFromString("com.google.android.googlequicksearchbox/com.google.android.voicesearch.serviceapi.GoogleRecognitionService")
        )
        mSpeechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent?.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        );
        mSpeechRecognizerIntent?.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        mSpeechRecognizerIntent?.putExtra(
            RecognizerIntent.EXTRA_CALLING_PACKAGE,
            activity.getPackageName()
        );
        mSpeechRecognizerIntent?.putExtra(
            RecognizerIntent.EXTRA_PARTIAL_RESULTS,
            true
        ); // For streaming result

        mSpeechRecognizerIntent?.putExtra(
            RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS,
            10
        )
//        mSpeechRecognizerIntent?.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
//        mSpeechRecognizerIntent?.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1)
        mRecognitionListener = object : RecognitionListener {
            override fun onReadyForSpeech(p0: Bundle?) {
                buttonSpeech?.isSelected = true
                progressView?.visibility = View.VISIBLE
                Loggers.e("VOICE-onReadyForSpeech", " $p0")
//                tts?.stop()
                VcUtils.setSoundWhenListening(activity, true, 20)
                VcUtils.muteRecognition(activity, true)
                recognitionListener?.onReadyForSpeech(p0)
            }

            override fun onBeginningOfSpeech() {
                recognitionListener?.onBeginningOfSpeech()
                Loggers.e("VOICE-onBeginningOfSpee", "onBeginningOfSpeech")
                isListening = true
            }

            override fun onRmsChanged(p0: Float) {
                onResultListener?.onRmsChanged(p0)
//                Loggers.e("VOICE-onRmsChanged", " $p0")
                recognitionListener?.onRmsChanged(p0)
                isListening = true
            }

            override fun onBufferReceived(p0: ByteArray?) {
                Loggers.e("VOICE-onBufferReceived", " $p0")
                recognitionListener?.onBufferReceived(p0)
            }

            override fun onEndOfSpeech() {
                recognitionListener?.onEndOfSpeech()
                isListening = false
                Loggers.e("VOICE-onEndOfSpeech", "onEndOfSpeech")
            }

            override fun onError(p0: Int) {
                isListening = false
                Loggers.e("VOICE-onError", " $p0")
                when (p0) {
                    SpeechRecognizer.ERROR_RECOGNIZER_BUSY -> cancelListening()
                    SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS -> {
                        cancelListening()
                        val mess =
                            "Thiết bị của bạn hiện chưa hỗ trợ tính năng này. Vui lòng cho phép ứng dụng Google truy cập micro"
                        VcUtils.showDialog(
                            activity,
                            "Thông báo",
                            mess,
                            "Không",
                            "Đồng ý",
                            object : VcUtils.OnDialogButtonListener {
                                override fun onLeftButtonClick() {
                                }

                                override fun onRightButtonClick() {
                                    val intent = Intent(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts(
                                            "package",
                                            "com.google.android.googlequicksearchbox",
                                            null
                                        )
                                    )
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    activity.startActivity(intent)
                                }
                            })
                        return
                    }
                    SpeechRecognizer.ERROR_SPEECH_TIMEOUT -> {
                        destroySTT(false)
                        speechRecognizer?.setRecognitionListener(mRecognitionListener)
                    }
                }
                startListening()
                recognitionListener?.onError(p0)
            }

            override fun onResults(p0: Bundle?) {
                isListening = false
                VcUtils.setSoundWhenListening(activity, false)
                buttonSpeech?.isSelected = false
                progressView?.visibility = View.INVISIBLE
                isListening = false
                var str = String()
                val data: ArrayList<String>? =
                    p0!!.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
//                for (i in 0 until data!!.size) {
////                    Loggers.e("VOICEonResults", "result " + data.get(i))
//                    str += data!!.get(i)
//                }
                str = data!!.get(0)
                onResultListener?.onResult(str)
                Loggers.e("VOICE-onResults", " $str")
                destroySTT(false)
                recognitionListener?.onResults(p0)
            }

            override fun onPartialResults(p0: Bundle?) {
                var str = String()
                val data: ArrayList<String>? =
                    p0!!.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
//                for (i in 0 until data!!.size) {
////                    Loggers.e("VOICEonPartialResults", "result " + data.get(i))
//                    str += data!!.get(i)
//                }
                str = data!!.get(0)
                Loggers.e("VOICE-onResultsPartial", "$str")
                onResultListener?.onPartialResults(str)
                recognitionListener?.onPartialResults(p0)
            }

            override fun onEvent(p0: Int, p1: Bundle?) {
                recognitionListener?.onEvent(p0, p1)
                Loggers.e("VOICE-onEvent", " $p0 / $p1")
            }
        }

        speechRecognizer?.setRecognitionListener(mRecognitionListener)
        if (isSetListening) {
            isSetListening = false
            speechRecognizer?.startListening(mSpeechRecognizerIntent)
        }
    }

    fun setTextToSpeechVoice(activity: Activity?) {
        if (activity == null) {
            return
        }
        val supportedVoices = getSupportedVoices()
        if (supportedVoices == null || supportedVoices.isEmpty()) {
//            AlertDialog.Builder(activity)
//                .setTitle(R.string.set_tts_voices)
//                .setMessage(R.string.no_tts_voices)
//                .setPositiveButton("OK", null)
//                .show()
            return
        }

        // Sort TTS voices
        val items = arrayOfNulls<CharSequence>(supportedVoices.size)
        val iterator = supportedVoices.iterator()
        var j = 0
        val voiceStr = VcUtils.getVoiceString(activity)
        var currentIndex = 0
        var selectIndex = 0
        while (iterator.hasNext()) {
            val voice = iterator.next()
            val index = j + 1
            if (index < 10) {
                items[j] = "Giọng nói 0" + index
            } else {

                items[j] = "Giọng nói " + index
            }
            if (voice.name.equals(voiceStr)) {
                currentIndex = j
            }
            j++
        }
        val dialog = AlertDialog.Builder(activity)
            .setTitle("Danh sách giọng nói iTV")
            .setSingleChoiceItems(items, currentIndex, object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    selectIndex = which
                    val mVoice = supportedVoices.get(which)
                    tts?.voice = mVoice
                    speak(activity, "ITV xin chào")
                }
            })
//            { dialogInterface, i ->
//                val mVoice = supportedVoices.get(i)
//                tts?.setVoice(mVoice)
//                VcUtils.saveVoiceString(activity, mVoice.name)
//            }
            .setPositiveButton("Cancel", null)
            .setNegativeButton(
                "Đồng ý"
            ) { dialog, which -> currentIndex = selectIndex }
            .create()
        dialog.show()
        dialog.setOnDismissListener {
            val mVoice = supportedVoices.get(currentIndex)
            tts?.voice = mVoice
            VcUtils.saveVoiceString(activity, mVoice.name)
        }
    }

    var tts: TextToSpeech? = null
    var isInitTTS: Boolean = false
    private var textToSpeechOnInitListener: TextToSpeech.OnInitListener? = null
    fun initTTS(
        activity: Activity?, msg: String? = null,
        mUtteranceId: String? = null,
        utteranceProgressListener: UtteranceProgressListener? = null
    ) {
        if (activity == null) {
            return
        }
        tts = TextToSpeech(activity.applicationContext, object : TextToSpeech.OnInitListener {
            override fun onInit(p0: Int) {
                Loggers.e("TTS", "" + p0)
                if (p0 != TextToSpeech.ERROR) {
                    isInitTTS = true
                    tts?.language = Locale.getDefault();
//                        tts?.add
                    val locations = tts?.availableLanguages
                    if (locations != null) {
                        for (item in locations) {
                            if (item.country.equals("VN") || item.country.equals("vn") || item.language.equals(
                                    "vi"
                                ) || item.language.equals("VI")
                            ) {
                                tts?.setLanguage(item)
                                break
                            }
                        }
                    }
                    setCurrentVoices(activity)
                    speak(activity, msg, mUtteranceId, utteranceProgressListener)
                    textToSpeechOnInitListener?.onInit(p0)
                }
            }
        }, "com.google.android.tts")
        tts?.setOnUtteranceProgressListener(utteranceProgressListener)

    }

    fun speak(
        activity: Activity?,
        msg: String?,
        mUtteranceId: String? = null,
        utteranceProgressListener: UtteranceProgressListener? = null,
        speechRate: Float = 1F,
        speechPitch: Float = 1F
    ) {
        if (activity == null || activity.isDestroyed || activity.isFinishing || msg.isNullOrEmpty()) {
            return
        }
        val msgVoice = msg.replace("iTV", "ITV").replace("itv", "ITV")
        if (isInitTTS && tts != null) {
            val params = Bundle()
            params.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "")
            val utteranceId = mUtteranceId ?: UUID.randomUUID().toString()
//            setUtteranceProgressListener(utteranceProgressListener)
            tts?.setOnUtteranceProgressListener(utteranceProgressListener)
            tts?.setSpeechRate(speechRate)
            tts?.setPitch(speechPitch)
            tts?.speak(getHtmlFormat(msgVoice), TextToSpeech.QUEUE_FLUSH, null, utteranceId)
        } else {
            isInitTTS = false
            initTTS(activity, msg, mUtteranceId, utteranceProgressListener)
        }
    }

    fun speakRandom(
        activity: Activity?,
        arrayString: Array<String>?,
        mUtteranceId: String? = null,
        utteranceProgressListener: UtteranceProgressListener? = null,
        speechRate: Float = 1F,
        speechPitch: Float = 1F
    ) {
        if (activity == null || activity.isDestroyed || activity.isFinishing || arrayString.isNullOrEmpty()) {
            return
        }
        speak(
            activity,
            VcUtils.getRandomString(arrayString),
            mUtteranceId,
            utteranceProgressListener,
            speechRate,
            speechPitch
        )
    }

    fun addSpeak(activity: Activity?, msg: String?) {
        if (activity == null || activity.isDestroyed || activity.isFinishing || msg.isNullOrEmpty()) {
            return
        }
        val params = Bundle()
        params.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "")
        val utteranceId = UUID.randomUUID().toString()
        tts?.speak(getHtmlFormat(msg), TextToSpeech.QUEUE_ADD, params, utteranceId)
    }

    fun setUtteranceProgressListener(
        utteranceProgressListener: UtteranceProgressListener?
    ) {
        if (utteranceProgressListener != null) {
            tts?.setOnUtteranceProgressListener(utteranceProgressListener)
        }
    }

    fun removeUtteranceProgressListener() {
        tts?.setOnUtteranceProgressListener(null)
    }

    fun checkPermission(activity: Activity?): Boolean {
        if (activity == null || activity.isFinishing || activity.isDestroyed) {
            return false
        }
        if (!hasPermissionRecordAudio(activity)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity,
                    Manifest.permission.RECORD_AUDIO
                )
            ) {
                val intent = Intent(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:$activity.packageName")
                )
                activity.startActivity(intent)
            } else {
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.RECORD_AUDIO),
                    VcUtils.REQUEST_CODE_PERMISSION_AUDIO
                )
            }
            return false
        }
        return true
    }

    fun getSupportedVoices(): ArrayList<Voice>? {
        if (tts != null) {
            val voices: Set<Voice> = tts!!.voices
            val voicesList = ArrayList<Voice>()
            for (voice in voices) {
                if ("vi".equals(voice.locale.language)) {
                    voicesList.add(voice)
                }
            }
            return voicesList
        }
        return null
    }

    fun setCurrentVoices(activity: Activity?) {
        if ((tts == null) || VcUtils.getVoiceString(activity).isNullOrEmpty()) {
            return
        }
        if (tts != null) {
            val voices: Set<Voice> = tts!!.voices
            for (voice in voices) {
                if (VcUtils.getVoiceString(activity).equals(voice.name)) {
                    tts?.setVoice(voice)
                    return
                }
            }
        }
    }

    fun destroySTT(isRemove: Boolean = true) {
        try {
            isListening = false
            speechRecognizer?.stopListening()
            speechRecognizer?.cancel()
            speechRecognizer?.destroy()
            if (isRemove) {
                speechRecognizer = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface OnResultListener {
        fun onPartialResults(str: String?)
        fun onResult(str: String?)
        fun onRmsChanged(p0: Float)
    }

    fun startListening() {
        if (!isListening) {
            speechRecognizer?.setRecognitionListener(mRecognitionListener)
            speechRecognizer?.startListening(mSpeechRecognizerIntent)
        }
    }

    fun cancelListening() {
        speechRecognizer?.cancel()
    }

    fun stopListening() {
        speechRecognizer?.stopListening()
    }

    fun stopSpeak() {
        tts?.stop()
    }

    fun destroyTTS() {
        try {
            tts?.stop()
            tts?.shutdown()
            isInitTTS = false
            tts = null
        } catch (e: Exception) {
        }
    }

    var isCurrentListen = false
    fun getHtmlFormat(text: String?): Spanned? {
        var text = text
        if (text == null) {
            text = ""
        }
        val textSpanned: Spanned
        textSpanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(text)
        }
        return textSpanned
    }
}